<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
	$user=User::model()->find('LOWER(username)=?',array(strtolower($this->username)));
	$stat=User::model()->findAll();
		if($user===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		elseif($user->password!==md5($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else{
			$this->username=$user->username;
			Yii::app()->session->add('id', $user->user_id);
			// Yii::app()->session->add('name', 'Operator');
			Yii::app()->session->add('username', $user->username);
			Yii::app()->session->add('level', $user->level);
			Yii::app()->session->add('status', $user->user_status);
			$this->errorCode=self::ERROR_NONE;
		}

		$sName=Yii::app()->session->get('username');
		// $karyawan=Karyawan::model()->findByAttributes(array('id_pd'=>$sName));
		// if(!empty($karyawan)){
		// 	Yii::app()->session->add('namaKaryawan', $karyawan->nm_pd);
		// }else{
		// 	Yii::app()->session->add('namaKaryawan', $karyawan->nm_pd);
		// }


		$getstat=Yii::app()->session->get('status');
		if($getstat == 1){
			return true;
		}elseif($getstat == 0){
			throw new CHttpException('403', 'Access denied. Your account is inactive / doesnt exists ');
		}elseif($getstat == 2){
			throw new CHttpException('403', 'Access denied. Your account has been deleted by Administrator');
		}else


		return !$this->errorCode;

	}
}