<?php
/* @var $this AbsensiController */
/* @var $model Absensi */

$this->breadcrumbs=array(
	'Absensis'=>array('index'),
	$model->abs_id,
);

$this->menu=array(
	array('label'=>'List Absensi', 'url'=>array('index')),
	array('label'=>'Create Absensi', 'url'=>array('create')),
	array('label'=>'Update Absensi', 'url'=>array('update', 'id'=>$model->abs_id)),
	array('label'=>'Delete Absensi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->abs_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Absensi', 'url'=>array('admin')),
);
?>

<h1>View Absensi #<?php echo $model->abs_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'abs_id',
		'abs_jkk_tetap',
		'abs_jkk_kontrak',
		'abs_timesheet',
		'abs_lembur',
		'latitude',
		'longitude',
		'abs_lat',
		'abs_lon',
	),
)); ?>
