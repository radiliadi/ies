<?php
/* @var $this AbsensiController */
/* @var $model Absensi */

$this->breadcrumbs=array(
	'Absensis'=>array('index'),
	$model->abs_id=>array('view','id'=>$model->abs_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Absensi', 'url'=>array('index')),
	array('label'=>'Create Absensi', 'url'=>array('create')),
	array('label'=>'View Absensi', 'url'=>array('view', 'id'=>$model->abs_id)),
	array('label'=>'Manage Absensi', 'url'=>array('admin')),
);
?>

<h1>Update Absensi <?php echo $model->abs_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>