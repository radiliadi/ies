<?php
/* @var $this JabatanController */
/* @var $model Jabatan */

$this->breadcrumbs=array(
	'Jabatan'=>array('admin'),
	$model->id_jabatan=>array('view','id'=>$model->id_jabatan),
	'Update',
);
?>

<h1>Update Jabatan <?php echo $model->id_jabatan; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>