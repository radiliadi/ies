<?php
$this->breadcrumbs=array(
	'Bank CV'=>array('admin'),
	'Manage',
);
?>
<div class="row">

	<div class="page-title">                    
                    <h2><span class="fa fa-file-text-o"></span> Bank CV</h2>
                </div>



<div class="col-md-12">
<div class="panel panel-default">
                                <div class="panel-heading">
									<div class="btn-group pull-right">
                                        <?php echo CHtml::link('<i class="fa fa-plus"></i> Tambah', 'create', array('class'=>'btn btn btn-primary pull-right')); ?>
	
                                    </div> 
                                </div>

   
    <div class="panel-body">

<?php echo CHtml::beginForm(array('mahasiswa/exportExcel')); ?>
<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'mahasiswa-grid',
'dataProvider'=>$model->searchx(),
'filter'=>$model,
'columns'=>array(
		array(
		'header' => 'No.',
		'htmlOptions' => array(
			'width' => '20px',
			'style' => 'text-align:center'
		),
		'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		// 'class'=>'booster.widgets.TbTotalSumColumn',
		),
		// 'id_cv',
		'nm_cv',
		// 'link_cv',
		array(
            'name'=>'link_cv',
            'filter'=>false,
            //'header'=>'Download',
            'type'=>'raw',
            'value'=>'CHtml::link($data->nm_cv, array("../cv/". $data->nm_cv.".pdf"))',
            //'htmlOptions'=>array('width'=>'10%'),
        ),
array(
'class'=>'booster.widgets.TbButtonColumn',
'template'=>'{delete}',
),
	),
)); ?>


			</div>
		</div>
	</div>
  </div>