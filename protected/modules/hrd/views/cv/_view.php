<?php
/* @var $this CvController */
/* @var $data Cv */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_cv')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_cv), array('view', 'id'=>$data->id_cv)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nm_cv')); ?>:</b>
	<?php echo CHtml::encode($data->nm_cv); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('link_cv')); ?>:</b>
	<?php echo CHtml::encode($data->link_cv); ?>
	<br />


</div>