<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cv-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

<div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                            
                            <form class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Upload</strong> CV</h3>
                                </div>
                                <div class="panel-body form-group-separated">
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-12 control-label">Nama Karyawan</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <?php echo $form->dropDownList($model,'nm_cv',CHtml::);?>
                                            </div>                                            
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-12 control-label">Curriculum Vitae</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <?php echo $form->fileField($model,'link_cv',array('class'=>"fileinput btn-primary")); ?> 
                                            </div>                                            
                                        </div>
                                    </div>
                                    

                                </div>
                                <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
                                </div>
                                </div>
                            </div>
                            </form>
                            
                        </div><div class="col-md-1"></div>
                    </div> 
                    <?php $this->endWidget(); ?>
</div><!-- form -->