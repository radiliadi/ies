<?php
/* @var $this KaryawanController */
/* @var $model Karyawan */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_pd'); ?>
		<?php echo $form->textField($model,'id_pd',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nm_pd'); ?>
		<?php echo $form->textField($model,'nm_pd',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jk'); ?>
		<?php echo $form->textField($model,'jk',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nik'); ?>
		<?php echo $form->textField($model,'nik',array('size'=>16,'maxlength'=>16)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tmpt_lahir'); ?>
		<?php echo $form->textField($model,'tmpt_lahir',array('size'=>32,'maxlength'=>32)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_lahir'); ?>
		<?php echo $form->textField($model,'tgl_lahir'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_agama'); ?>
		<?php echo $form->textField($model,'id_agama'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_kk'); ?>
		<?php echo $form->textField($model,'id_kk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jln'); ?>
		<?php echo $form->textField($model,'jln',array('size'=>60,'maxlength'=>80)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rt'); ?>
		<?php echo $form->textField($model,'rt',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rw'); ?>
		<?php echo $form->textField($model,'rw',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nm_dsn'); ?>
		<?php echo $form->textField($model,'nm_dsn',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ds_kel'); ?>
		<?php echo $form->textField($model,'ds_kel',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_wil'); ?>
		<?php echo $form->textField($model,'id_wil',array('size'=>8,'maxlength'=>8)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kode_pos'); ?>
		<?php echo $form->textField($model,'kode_pos',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_jns_tinggal'); ?>
		<?php echo $form->textField($model,'id_jns_tinggal',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_alat_transport'); ?>
		<?php echo $form->textField($model,'id_alat_transport',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telepon_rumah'); ?>
		<?php echo $form->textField($model,'telepon_rumah',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telepon_seluler'); ?>
		<?php echo $form->textField($model,'telepon_seluler',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stat_pd'); ?>
		<?php echo $form->textField($model,'stat_pd',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nm_ayah'); ?>
		<?php echo $form->textField($model,'nm_ayah',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_lahir_ayah'); ?>
		<?php echo $form->textField($model,'tgl_lahir_ayah'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_jenjang_pendidikan_ayah'); ?>
		<?php echo $form->textField($model,'id_jenjang_pendidikan_ayah',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_pekerjaan_ayah'); ?>
		<?php echo $form->textField($model,'id_pekerjaan_ayah'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nm_ibu_kandung'); ?>
		<?php echo $form->textField($model,'nm_ibu_kandung',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_lahir_ibu'); ?>
		<?php echo $form->textField($model,'tgl_lahir_ibu'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_jenjang_pendidikan_ibu'); ?>
		<?php echo $form->textField($model,'id_jenjang_pendidikan_ibu',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_pekerjaan_ibu'); ?>
		<?php echo $form->textField($model,'id_pekerjaan_ibu'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kewarganegaraan'); ?>
		<?php echo $form->textField($model,'kewarganegaraan',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->