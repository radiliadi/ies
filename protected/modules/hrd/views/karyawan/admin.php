<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Employee' ,
        // 'Semua' => array('Semua'=>'admin', 
        // 'Lalai'=>array('asd'
          // )
        ),
    )
);
?>
<div class="row">

	<div class="page-title">                    
                    <h2><span class="fa fa-users"></span> Manage Employees</h2>
                </div>
<div class="col-md-12">
<div class="panel panel-default">
                                <div class="panel-heading">
									<div class="btn-group pull-right">
                                        <?php echo CHtml::link('<i class="fa fa-plus" title="Create"></i>', 'create', array('class'=>'btn btn btn-primary pull-right')); ?>
	
                                    </div> 
                                </div>
    <div class="panel-body">

<?php echo CHtml::beginForm(array('karyawan/exportExcel')); ?>


<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'mahasiswa-grid',
// 'type' => ' condensed',
'dataProvider'=>$model->search(), 
// array(
// 	// 'pagination' => array('pageSize' => 100),
// 	),
'filter'=>$model,
'summaryText' => '',
// 'emptyText'=>'Belum ada thread pada kategori ini',
'columns'=>array(
		array(
		'header' => 'No.',
		'htmlOptions' => array(
			'width' => '20px',
			'style' => 'text-align:center'
		),
		'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		// 'class'=>'booster.widgets.TbTotalSumColumn',
		),
		'id_pd',
		'nm_pd',
		// 'id_jabatan',
		array(
                   'name' => 'id_jabatan',
                   'filter' => Karyawan::listJabatan(),
                   'value' => '$data->id_jabatan == "10" ? "Engineer On Site" : 
                   ($data->id_jabatan == 9 ? "Engineer Warehouse" : 
                   ($data->id_jabatan == 8 ? "Field Engineer" : 
                   ($data->id_jabatan == 7 ? "Insurance & Tax" : 
                   ($data->id_jabatan == 6 ? "Manager Finance & Accounting" : 
                   ($data->id_jabatan == 5 ? "Formalities Insurance" : 
                   ($data->id_jabatan == 4 ? "Kontrak, Tender & HSE Manager" : 
                   ($data->id_jabatan == 3 ? "Manager HR" : 
                   ($data->id_jabatan == 2 ? "Direktur" : 
                   ($data->id_jabatan == 1 ? "Direktur Utama" : 
                   ($data->id_jabatan == 13 ? "Helpdesk" : 
                   ($data->id_jabatan == 12 ? "Technical Support" : 
                   ($data->id_jabatan == 11 ? "Asst. Project Manager" : 
                   "Laki-Laki"))))))))))))',
        ),
		array(
                   'name' => 'divisi',
                   'filter' => Karyawan::listDivisi()
        ),
		// array(
  //                  'name' => 'divisi',
  //                  'filter' => Karyawan::listDivisi()
  //       ),
		// 'lokasi_kerja',
		array(
                   'name' => 'lokasi_kerja',
                   'filter' => Karyawan::listLokker()
        ),
        // 'tgl_lahir',
        
		// array(
		// 	'name'=>'jk',
		// 	'value'=>'$data->jk == "P" ? "Perempuan" :  "Laki-Laki"',
		// 	 'filter'=>array(    
		// 			'L'=>'laki-laki',
  //                   'P'=>'Perempuan',
  //               )
		// ),
    // array(
    //  'name' => 'stat_pd',
    //  'filter' => Karyawan::listStatus()
    //     ),
		// array(
		// 	'name'=>'jk',
		// 	'value'=>'$data->jk == "P" ? "Perempuan" :  "Laki-Laki"',
		// 	 'filter'=>array(    
		// 			'L'=>'laki-laki',
  //                   'P'=>'Perempuan',
  //               )
		// ),
// array(
// 'class' => 'CButtonColumn',
// 'template' => '{gaji}',
// 'header' => '',
// 'htmlOptions' => array(
//     'style' => 'white-space: nowrap'
// ),
// 'buttons' => array(
//     'gaji' => array(
//         'label' => '',
//         'url' => 'Yii::app()->createAbsoluteUrl("operator/gaji/admin/", array(
//             "id" => $data->id_pd
//         ))',
//         'options' => array(
//             'class' => 'btn btn-info bola fa fa-search tooltips',
//             'title' => 'Gaji'
//         ),
//     ),
// ),
// ),
array(
'class'=>'booster.widgets.TbButtonColumn',
'template'=>'{view}',
),
array(
'class'=>'booster.widgets.TbButtonColumn',
'template'=>'{update}',
),
array(
'class'=>'booster.widgets.TbButtonColumn',
'afterDelete'=>'function(link,success,data){ if(success) alert("Delete completed successfully"); }',
'template'=>'{delete}',
),
),
)); ?>



    <hr></hr>
	<div class="col-md-6">
		<div class="col-md-3">
		<select name="fileType" class="form-control">
			<option value="Excel">EXCEL 5 (xls)</option>
			<!-- <option value="CSV">CSV</option> -->
		</select>
		</div>
		<div class="col-md-3">
		<?php echo CHtml::submitButton('Export',array(
					'buttonType' => 'submit',
					'context'=>'primary',
					'label'=>'Export Excel',
					'class'=>'btn btn-primary',
				)); ?>
		<?php echo CHtml::endForm(); ?>
		</div>
	</div>


			</div>
		</div>
	</div>
  </div>