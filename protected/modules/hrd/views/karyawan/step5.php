<?php
/* @var $this MahasiswaController */
/* @var $model Mahasiswa */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mahasiswa-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	
	<?php echo $form->errorSummary($model); ?>

	
	                
              <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form class="form-horizontal">
							<!-- STEP 1:START -->
							<?php echo $form->hiddenField($model,'id_pd',array('value'=>$model->id_pd)); ?>
						  	<?php echo $form->hiddenField($model,'nm_pd',array('value'=>$model->nm_pd)); ?>
						   	<?php echo $form->hiddenField($model,'id_jabatan',array('value'=>$model->id_jabatan)); ?>
						   	<?php echo $form->hiddenField($model,'divisi',array('value'=>$model->divisi)); ?>
						   	<?php echo $form->hiddenField($model,'lokasi_kerja',array('value'=>$model->lokasi_kerja)); ?>
						   	<?php echo $form->hiddenField($model,'jk',array('value'=>$model->jk)); ?>
						   	<?php echo $form->hiddenField($model,'id_agama',array('value'=>$model->id_agama)); ?>
						   	<?php echo $form->hiddenField($model,'tmpt_lahir',array('value'=>$model->tmpt_lahir)); ?>
						    <?php echo $form->hiddenField($model,'tgl_lahir',array('value'=>$model->tgl_lahir)); ?>
							<?php echo $form->hiddenField($model,'kewarganegaraan',array('value'=>$model->kewarganegaraan)); ?>
							<?php echo $form->hiddenField($model,'stat_pd',array('value'=>$model->stat_pd)); ?>
							<?php echo $form->hiddenField($model,'tender_id',array('value'=>$model->tender_id)); ?>
							
							<!-- STEP 1:END -->
							<!-- STEP 2:START -->
							<?php echo $form->hiddenField($model,'nik',array('value'=>$model->nik)); ?>
							<?php echo $form->hiddenField($model,'npwp',array('value'=>$model->npwp)); ?>
					  		<?php echo $form->hiddenField($model,'jln',array('value'=>$model->jln)); ?>
					  		<?php echo $form->hiddenField($model,'nm_dsn',array('value'=>$model->nm_dsn)); ?>
					   		<?php echo $form->hiddenField($model,'ds_kel',array('value'=>$model->ds_kel)); ?>
					    	<?php echo $form->hiddenField($model,'id_wil',array('value'=>$model->id_wil)); ?>
							<?php echo $form->hiddenField($model,'id_jns_tinggal',array('value'=>$model->id_jns_tinggal)); ?>
					  		<?php echo $form->hiddenField($model,'telepon_rumah',array('value'=>$model->telepon_rumah)); ?>
					  		<?php echo $form->hiddenField($model,'telepon_seluler',array('value'=>$model->telepon_seluler)); ?>
					   		<?php echo $form->hiddenField($model,'email',array('value'=>$model->email)); ?>
					    	<?php echo $form->hiddenField($model,'id_alat_transport',array('value'=>$model->id_alat_transport)); ?>
							<?php echo $form->hiddenField($model,'rt',array('value'=>$model->rt)); ?>
						  	<?php echo $form->hiddenField($model,'rw',array('value'=>$model->rw)); ?>
						   	<?php echo $form->hiddenField($model,'kode_pos',array('value'=>$model->kode_pos)); ?>
							<!-- STEP 2:END -->
							<!-- STEP 3:START -->
							<?php echo $form->hiddenField($model,'nm_ayah',array('value'=>$model->nm_ayah)); ?>
							<?php echo $form->hiddenField($model,'tgl_lahir_ayah',array('value'=>$model->tgl_lahir_ayah)); ?>
							<?php echo $form->hiddenField($model,'id_jenjang_pendidikan_ayah',array('value'=>$model->id_jenjang_pendidikan_ayah)); ?>
							<?php echo $form->hiddenField($model,'id_pekerjaan_ayah',array('value'=>$model->id_pekerjaan_ayah)); ?>
							<?php echo $form->hiddenField($model,'nm_ibu_kandung',array('value'=>$model->nm_ibu_kandung)); ?>
							<?php echo $form->hiddenField($model,'tgl_lahir_ibu',array('value'=>$model->tgl_lahir_ibu)); ?>
							<?php echo $form->hiddenField($model,'id_jenjang_pendidikan_ibu',array('value'=>$model->id_jenjang_pendidikan_ayah)); ?>
							<?php echo $form->hiddenField($model,'id_pekerjaan_ibu',array('value'=>$model->id_pekerjaan_ibu)); ?>
							<?php echo $form->hiddenField($model,'id_penghasilan_ibu',array('value'=>$model->id_penghasilan_ibu)); ?>
							<!-- STEP 3:END -->
									 
							<!-- STEP 4:START -->
							<?php echo $form->hiddenField($model,'id_ptkp',array('value'=>$model->id_ptkp)); ?>
							<!-- STEP 4:END -->
							
							<!-- STEP 5:START -->
							<?php echo $form->hiddenField($model,'tgl_masuk',array('value'=>$model->tgl_masuk)); ?>
							<?php echo $form->hiddenField($model,'regpd_id_jns_daftar',array('value'=>$model->regpd_id_jns_daftar)); ?>
							<!-- STEP 5:END -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
								</div>
                               
                                <div class="panel-body">                                                                        
                                    
                                    <div class="row">
                                        
                                         <div class="col-md-6">
                            <div class="panel panel-default form-horizontal">
                                <div class="panel-body">
                                    <h3><span></span>5. Done! </h3>
                                    <p>Berikut data yang Anda inputkan :</p>
                                </div>
                                <div class="panel-body form-group-separated">                                    
                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-5 control-label">NIK</label>
                                        <div class="col-md-8 col-xs-7 line-height-30"><?php echo $model->id_pd;?></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-5 control-label">Nama Lengkap</label>
                                        <div class="col-md-8 col-xs-7 line-height-30"><?php echo $model->nm_pd;?></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-5 control-label">Jabatan</label>
										<?php	
										$modelJabatan=Jabatan::model()->findAll('id_jabatan=:id_jabatan',array(':id_jabatan'=>$model->id_jabatan));
										foreach($modelJabatan as $db)
										{
											echo '<div class="col-md-8 col-xs-7 line-height-30">'.$db->nm_jabatan.'</div>'; 
										}
										?>
                                    </div>
									<div class="form-group">
                                        <label class="col-md-4 col-xs-5 control-label">Divisi</label>
                                        <div class="col-md-8 col-xs-7"><?php echo $model->divisi; ?></div>
                                    </div>
									<div class="form-group">
                                        <label class="col-md-4 col-xs-5 control-label">Lokasi Kerja</label>
                                        <div class="col-md-8 col-xs-7"><?php echo $model->lokasi_kerja; ?></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-5 control-label">No Tender</label>
										<?php	
										$modelTender=Tender::model()->findAll('id_tender=:id_tender',array(':id_tender'=>$model->tender_id));
										foreach($modelTender as $db)
										{
											echo '<div class="col-md-8 col-xs-7 line-height-30">'.$db->tender_no.' ('.($db->client_name).')</div>'; 
										}
										?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-5 control-label">Agama</label>
										<?php	
										$modelAgama=Agama::model()->findAll('id_agama=:id_agama',array(':id_agama'=>$model->id_agama));
										foreach($modelAgama as $db)
										{
											echo '<div class="col-md-8 col-xs-7 line-height-30">'.$db->nm_agama.'</div>'; 
										}
										?>
                                    </div>
									<div class="form-group">
                                        <label class="col-md-4 col-xs-5 control-label">Email</label>
                                        <div class="col-md-8 col-xs-7 line-height-30"><?php echo $model->email; ?></div>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
						<div>
                            <div class="panel panel-default form-horizontal" style="float:left;width:50%;background:#33414E;">
                                <div class="panel-body">
									<?php echo' '.CHtml::Image(Yii::app()->request->baseUrl.'/images/bendera/'.$model->kewarganegaraan.'.png','Image',array("width"=>'25%','style'=>'float:left',"margin:0px auto")).'';
									?>
								</div>
							</div>
                        </div>
						</div>
                        </div>
                        </div>
						</div>
                        <div class="panel-footer">                             
                        <?php echo CHtml::link('<i class="fa fa-check"></i> Done', array('admin'), array('class'=>'btn btn btn-primary pull-right')); ?>
                        </div>
                        </div>
							</form>
                           
                        </div>
                <!-- END PAGE CONTENT WRAPPER -->           

<?php $this->endWidget(); ?>

</div><!-- form -->

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mahasiswa-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
							<!-- STEP 1:START -->
							<?php echo $form->hiddenField($model,'id_pd',array('value'=>$model->id_pd)); ?>
						  	<?php echo $form->hiddenField($model,'nm_pd',array('value'=>$model->nm_pd)); ?>
						   	<?php echo $form->hiddenField($model,'id_jabatan',array('value'=>$model->id_jabatan)); ?>
						   	<?php echo $form->hiddenField($model,'divisi',array('value'=>$model->divisi)); ?>
						   	<?php echo $form->hiddenField($model,'lokasi_kerja',array('value'=>$model->lokasi_kerja)); ?>
						   	<?php echo $form->hiddenField($model,'jk',array('value'=>$model->jk)); ?>
						   	<?php echo $form->hiddenField($model,'id_agama',array('value'=>$model->id_agama)); ?>
						   	<?php echo $form->hiddenField($model,'tmpt_lahir',array('value'=>$model->tmpt_lahir)); ?>
						    <?php echo $form->hiddenField($model,'tgl_lahir',array('value'=>$model->tgl_lahir)); ?>
							<?php echo $form->hiddenField($model,'kewarganegaraan',array('value'=>$model->kewarganegaraan)); ?>
							<?php echo $form->hiddenField($model,'stat_pd',array('value'=>$model->stat_pd)); ?>
							<?php echo $form->hiddenField($model,'tender_id',array('value'=>$model->tender_id)); ?>
							
							<!-- STEP 1:END -->
							<!-- STEP 2:START -->
							<?php echo $form->hiddenField($model,'nik',array('value'=>$model->nik)); ?>
							<?php echo $form->hiddenField($model,'npwp',array('value'=>$model->npwp)); ?>
					  		<?php echo $form->hiddenField($model,'jln',array('value'=>$model->jln)); ?>
					  		<?php echo $form->hiddenField($model,'nm_dsn',array('value'=>$model->nm_dsn)); ?>
					   		<?php echo $form->hiddenField($model,'ds_kel',array('value'=>$model->ds_kel)); ?>
					    	<?php echo $form->hiddenField($model,'id_wil',array('value'=>$model->id_wil)); ?>
							<?php echo $form->hiddenField($model,'id_jns_tinggal',array('value'=>$model->id_jns_tinggal)); ?>
					  		<?php echo $form->hiddenField($model,'telepon_rumah',array('value'=>$model->telepon_rumah)); ?>
					  		<?php echo $form->hiddenField($model,'telepon_seluler',array('value'=>$model->telepon_seluler)); ?>
					   		<?php echo $form->hiddenField($model,'email',array('value'=>$model->email)); ?>
					    	<?php echo $form->hiddenField($model,'id_alat_transport',array('value'=>$model->id_alat_transport)); ?>
							<?php echo $form->hiddenField($model,'rt',array('value'=>$model->rt)); ?>
						  	<?php echo $form->hiddenField($model,'rw',array('value'=>$model->rw)); ?>
						   	<?php echo $form->hiddenField($model,'kode_pos',array('value'=>$model->kode_pos)); ?>
							<!-- STEP 2:END -->
							<!-- STEP 3:START -->
							<?php echo $form->hiddenField($model,'nm_ayah',array('value'=>$model->nm_ayah)); ?>
							<?php echo $form->hiddenField($model,'tgl_lahir_ayah',array('value'=>$model->tgl_lahir_ayah)); ?>
							<?php echo $form->hiddenField($model,'id_jenjang_pendidikan_ayah',array('value'=>$model->id_jenjang_pendidikan_ayah)); ?>
							<?php echo $form->hiddenField($model,'id_pekerjaan_ayah',array('value'=>$model->id_pekerjaan_ayah)); ?>
							<?php echo $form->hiddenField($model,'nm_ibu_kandung',array('value'=>$model->nm_ibu_kandung)); ?>
							<?php echo $form->hiddenField($model,'tgl_lahir_ibu',array('value'=>$model->tgl_lahir_ibu)); ?>
							<?php echo $form->hiddenField($model,'id_jenjang_pendidikan_ibu',array('value'=>$model->id_jenjang_pendidikan_ayah)); ?>
							<?php echo $form->hiddenField($model,'id_pekerjaan_ibu',array('value'=>$model->id_pekerjaan_ibu)); ?>
							<?php echo $form->hiddenField($model,'id_penghasilan_ibu',array('value'=>$model->id_penghasilan_ibu)); ?>
							<!-- STEP 3:END -->
									 
							<!-- STEP 4:START -->
							<?php echo $form->hiddenField($model,'id_ptkp',array('value'=>$model->id_ptkp)); ?>
							<!-- STEP 4:END -->
							
							<!-- STEP 5:START -->
							<?php echo $form->hiddenField($model,'tgl_masuk',array('value'=>$model->tgl_masuk)); ?>
							<?php echo $form->hiddenField($model,'regpd_id_jns_daftar',array('value'=>$model->regpd_id_jns_daftar)); ?>
							<!-- STEP 5:END -->
	<?php $this->endWidget(); ?>

</div><!-- form -->
