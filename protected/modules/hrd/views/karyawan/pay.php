<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Payroll' ,
        // 'Semua' => array('Semua'=>'admin', 
        // // 'Lalai'=>array('asd'
        //   )
        ),
    )
);
?>
<div class="row">

	<div class="page-title">                    
                    <h2><span class="fa fa-money"></span> Payroll</h2>
                </div>
<div class="col-md-12">
<div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
    <div class="panel-body">

<?php echo CHtml::beginForm(array('karyawan/exportExcel')); ?>


<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'mahasiswa-grid',
// 'type' => ' condensed',
'dataProvider'=>$model->search(), 
// array(
// 	// 'pagination' => array('pageSize' => 100),
// 	),
'filter'=>$model,
// 'emptyText'=>'Belum ada thread pada kategori ini',
'columns'=>array(
		array(
		'header' => 'No.',
		'htmlOptions' => array(
			'width' => '20px',
			'style' => 'text-align:center'
		),
		'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		// 'class'=>'booster.widgets.TbTotalSumColumn',
		),
		'id_pd',
		'nm_pd',
		// 'id_jabatan',
		array(
                   'name' => 'id_jabatan',
                   'filter' => Karyawan::listJabatan()
        ),
		array(
                   'name' => 'divisi',
                   'filter' => Karyawan::listDivisi()
        ),
		// array(
  //                  'name' => 'divisi',
  //                  'filter' => Karyawan::listDivisi()
  //       ),
		// 'lokasi_kerja',
		array(
                   'name' => 'lokasi_kerja',
                   'filter' => Karyawan::listLokker()
        ),
    array(
     'name' => 'stat_pd',
     'filter' => Karyawan::listStatus()
        ),
		// array(
		// 	'name'=>'jk',
		// 	'value'=>'$data->jk == "P" ? "Perempuan" :  "Laki-Laki"',
		// 	 'filter'=>array(    
		// 			'L'=>'laki-laki',
  //                   'P'=>'Perempuan',
  //               )
		// ),
array(
'class' => 'CButtonColumn',
'template' => '{gaji}',
'header' => '',
'htmlOptions' => array(
    'style' => 'white-space: nowrap'
),
'buttons' => array(
    'gaji' => array(
        'label' => '',
        'url' => 'Yii::app()->createAbsoluteUrl("operator/gaji/admin/", array(
            "id" => $data->id_pd
        ))',
        'options' => array(
            'class' => 'btn btn-primary bola fa fa-dollar tooltips',
            'title' => 'Gaji'
        ),
    ),
),
),
),
)); ?>



			</div>
		</div>
	</div>
  </div>