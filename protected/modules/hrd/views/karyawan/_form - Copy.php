<div class="form">

	<?php $form=$this->beginWidget('CActiveForm',array(
	'id'=>'karyawan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array(
        'enctype'=>'multipart/form-data',
		'action'=>'javascript:alert("Validated!");',
		'role'=>'form',
		'class'=>'',
		'id'=>"wizard-validation"
    ),
)); ?>
<!-- PAGE CONTENT WRAPPER -->
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-body">
	<!-- START WIZARD WITH VALIDATION -->
            <div class="block">
			<div style="color:red;"><?php echo $form->errorSummary($model); ?></div>
            <h4>Data Karyawan</h4>                                
				<div class="wizard show-submit wizard-validation">
                    <ul>
						<li>
                            <a href="#step-1">
                                <span class="stepNumber">1</span>
                                <span class="stepDesc"><div style="min-height:20px;">Informasi Personal</div><br /><small>Step 1</small></span>
                            </a>
                        </li>
						<li>
                            <a href="#step-2">
                                <span class="stepNumber">2</span>
                                <span class="stepDesc"><div style="min-height:20px;">Alamat & Kontak</div><br /><small>Step 2</small></span>
                            </a>
                        </li>
						
                        <li>
                            <a href="#step-3">
                                <span class="stepNumber">3</span>
                                <span class="stepDesc"><div style="min-height:20px;">Orang Tua</div><br /><small>Informasi</small></span>
                            </a>
                        </li>
						<li>
                            <a href="#step-4">
                                <span class="stepNumber">4</span>
                                <span class="stepDesc"><div style="min-height:20px;">Wali</div><br /><small>Informasi</small></span>
                            </a>
                        </li>
						<li>
                            <a href="#step-5">
                                <span class="stepNumber">5</span>
                                <span class="stepDesc"><div style="min-height:20px;">Lainnya</div><br /><small>Informasi</small></span>
                            </a>
                        </li> 
						
                    </ul>
					<div id="step-1">   
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>1. Informasi Personal</strong> </h3>
                                    
                                </div>
                                
                                <div class="panel-body">                                                                        
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">NIP/NIK<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                                        <?php 
															if(isset($_SESSION['sessiondrz']))
															{
																echo $form->textField($model,'id_pd',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'value'=>$vari[0], 'readonly'=>'readonly', 'required'=>'required'));
															}else{
																echo $form->textField($model,'id_pd',array('size'=>50,'maxlength'=>50,'class'=>"form-control", 'required'=>'required','placeholder'=>"Masukkan Nomor Induk Pegawai / Karyawan",'required'=>"required"));
															}
														?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Nama<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php 
															if(isset($_SESSION['sessiondrz']))
															{
																echo $form->textField($model,'nm_pd',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'value'=>$vari[4], 'readonly'=>'readonly', 'required'=>'required'));
															}else{
																echo $form->textField($model,'nm_pd',array('size'=>50,'maxlength'=>50,'class'=>"form-control", 'required'=>'required','placeholder'=>"Masukkan Nama Lengkap Mahasiswa",'required'=>"required"));
																
															}
														?>
														
                                                    </div>      
															
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Jabatan<a style="color:red;"><blink>*</blink></a></label>
												
												<div class="col-md-9">
													<?php echo $form->dropDownList($model,'id_jabatan',CHtml::listData(Jabatan::model()->findAll(),'id_jabatan','nm_jabatan'),array('empty'=>'Pilih Jabatan','class'=>"form-control select", 'required'=>'required')); ?>
													<span class="help-block"></span>
												</div>
											</div>
                                            
											<div class="form-group">
                                                <label class="col-md-3 control-label">Lokasi Kerja<a style="color:red;"><blink>*</blink></a></label>
												
												<div class="col-md-9">
													<?php echo $form->dropDownList($model,'id_lokasi_kerja',CHtml::listData(LokasiKerja::model()->findAll(),'id_lokasi_kerja','nm_lokasi_kerja'),array('empty'=>'Pilih Lokasi Kerja','class'=>"form-control select", 'required'=>'required')); ?>
													<span class="help-block"></span>
												</div>
											</div>
											
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Status <a style="color:red;"><blink>*</blink></a></label>
												
												<div class="col-md-9">
													<?php echo $form->dropDownList($model,'stat_pd',CHtml::listData(StatusMahasiswa::model()->findAll(),'id_stat_mhs','nm_stat_mhs'),array('empty'=>'Pilih Status','class'=>"form-control select", 'required'=>'required')); ?>
													
													<span class="help-block"></span>
													
												</div>
											</div>
											
                                            <div class="form-group">
											
                                                <label class="col-md-3 control-label">Jenis Kelamin<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9 col-xs-12">
                                                    
													<?php echo $form->radioButton($model,'jk',array('value'=>'L','uncheckValue'=>null,'required'=>"required")); ?>Laki-Laki										
													<?php echo $form->radioButton($model,'jk',array('value'=>'P','uncheckValue'=>null,'required'=>"required")); ?>Perempuan											
																			
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
											
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Agama<a style="color:red;"><blink>*</blink></a></label>
												
												<div class="col-md-9">
													<?php echo $form->dropDownList($model,'id_agama',CHtml::listData(Agama::model()->findAll(),'id_agama','nm_agama'),array('empty'=>'Pilih Agama','class'=>"form-control select", 'required'=>'required')); ?>
													<span class="help-block"></span>
												</div>
											</div>
                                            
                                            
                                        </div>
                                        <div class="col-md-6">
                                            
											<div class="form-group">
                                                <label class="col-md-3 control-label">Tempat Lahir<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php echo $form->textField($model,'tmpt_lahir',array('size'=>50,'maxlength'=>50,'class'=>"form-control", 'required'=>'required','placeholder'=>"Masukkan Tempat Lahir Mahasiswa",'required'=>"required"));?>
                                                    </div>            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
										
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Tanggal Lahir <a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php 	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
																				'name'=>'Mahasiswa[tgl_lahir]',
																				'value'=> $model->tgl_lahir,
																				'options'=>array(
																					'showAnim'=>'fold',
																					'dateFormat'=>'yy-mm-dd',
																				),
																				'htmlOptions' => array(
																					'class' => 'form-control',
																					'placeholder'=>'Masukkan Tanggal Lahir Mahasiswa'
																				),
																			));
														?>
                                                    </div>  
											
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
	
											<div class="form-group">
                                                <label class="col-md-3 control-label">Kewarganegaraan<a style="color:red;"><blink>*</blink></a></label>
												
												<div class="col-md-9">
													<?php echo $form->dropDownList($model,'kewarganegaraan',CHtml::listData(Negara::model()->findAll(),'id_negara','nm_negara'),array('empty'=>'Pilih Kewarganegaraan','class'=>"form-control select", 'required'=>'required')); ?>
													
													<span class="help-block"></span>
													</div>
											</div>
											
										</div>
									</div>
                                </div>
                            </div>
							<a style="color:red;margin-left:3%"><blink>*: Data wajib diisi</blink></a>  
                    </div>
                           			
					<div id="step-2">
						<div class="panel panel-default">
                                <div class="panel-heading">
                                   <h3 class="panel-title"><strong>2. Alamat & Kontak </strong> </h3>
                                </div>
                                <div class="panel-body">                                                                          
                                    <div class="row">         
                                        <div class="col-md-6">           
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">No KTP</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                       <?php echo $form->textField($model,'nik',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Masukkan NIK" ));?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            
                                           <div class="form-group">
                                                <label class="col-md-3 control-label">Jalan<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                       <?php echo $form->textField($model,'jln',array('size'=>50,'maxlength'=>50,'class'=>"form-control", 'required'=>'required','placeholder'=>"Masukkan Jalan",'required'=>"required"));?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Dusun<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                      <?php echo $form->textField($model,'nm_dsn',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Masukkan Dusun",'required'=>"required")); ?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="col-md-3 control-label">Kelurahan<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php echo $form->textField($model,'ds_kel',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Masukkan kelurahan",'required'=>"required")); ?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="col-md-3 control-label">Kecamatan</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
														   <?php $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
																'name'=>'id_wil',
																// additional javascript options for the autocomplete plugin
																'options'=>array(
																	'minLength'=>'1',
																),
																'source'=>$this->createUrl("mahasiswa/ajax"),
																'htmlOptions'=>array(
																	'class'=>"form-control",
																	'placeholder'=>"Masukkan kelurahan",
								
																),
															)); ?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="col-md-3 control-label">Jenis Tinggal<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9">                                            
													<?php echo $form->dropDownList($model,'id_jns_tinggal',CHtml::listData(JenisTinggal::model()->findAll(),'id_jns_tinggal','nm_jns_tinggal'),array('empty'=>'Pilih Jenis Tinggal','class'=>"form-control select", 'required'=>'required', 'style'=>'z-index:1000;')); ?>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="col-md-3 control-label">Telepon</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                       <?php echo $form->textField($model,'telepon_rumah',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Masukkan No telepon")); ?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="col-md-3 control-label">Email</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Masukkan Email")); ?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Alat Transportasi<a style="color:red;"><blink>*</blink></a></label>
												
												<div class="col-md-9">
												<?php echo $form->dropDownList($model,'id_alat_transport',CHtml::listData(AlatTransport::model()->findAll(),'id_alat_transport','nm_alat_transport'),array('empty'=>'Pilih Alat Transportasi','class'=>"form-control select",'required'=>"required")); ?>
                                                <span class="help-block"></span>
                                            </div>
											</div>
                                        </div>
                                        <div class="col-md-6">
                                            <br>
											</br>
											<br>
											</br>
											<br>
											</br>
											
											
											
											
											<div class="form-group">                                        
                                                <label class="col-md-2 control-label">RT</label>
                                                <div class="col-md-4 col-xs-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                      <?php echo $form->textField($model,'rt',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Masukkan RT")); ?>
                                                    </div>            
                                                    <span class="help-block"></span>
                                                </div>
                                            
											
											                                     
                                                <label class="col-md-1 control-label">RW</label>
                                                <div class="col-md-4 col-xs-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php echo $form->textField($model,'rw',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Masukkan RW")); ?>
                                                    </div>            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
											
											
											
                                            <div class="form-group">                                        
                                                <label class="col-md-2 control-label">Kodepos</label>
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                          <?php echo $form->textField($model,'kode_pos',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Masukkan Kode pos")); ?>
                                                    </div>            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
											<div class="form-group">                                        
                                                <label class="col-md-1 control-label">No HP</label>
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                          <?php echo $form->textField($model,'telepon_seluler',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Masukkan No HP")); ?>
                                                    </div>            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
											
                                            
                                           
											<br>
											</br>
											
                                        </div>
                                        
                                    </div>
                                </div>
								

                        </div>
						<a style="color:red;margin-left:3%"><blink>*: Data wajib diisi</blink></a>  
                    </div>
								
					<div id="step-3">   
					<div class="panel panel-default">
					<div class="panel-heading">
                        <h3 class="panel-title"><strong>3. Orang Tua</strong></h3>
                    </div>
                            <div class="panel-body">                                                                        
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
											<label class="col-md-4 control-label">Nama Ayah &nbsp; <a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-8">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php echo $form->textField($model,'nm_ayah',array('size'=>60,'maxlength'=>60,'class'=>"form-control",'placeholder'=>"Masukkan nama Ayah")); ?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                        </div>
										
										<div class="form-group">
                                                <label class="col-md-4 control-label">Tanggal Lahir<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-8 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php 	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
																				'name'=>'Mahasiswa[tgl_lahir_ayah]',
																				'value'=>$model->tgl_lahir_ayah,
																				'options'=>array(
																					'showAnim'=>'fold',
																					'dateFormat'=>'yy-mm-dd',
																				),
																				'htmlOptions' => array(
																					'class' => 'form-control',
																					'placeholder'=>'Masukkan Tanggal Lahir ayah'
																				),
																			));
														?>
                                                    </div>  
											
                                                    <span class="help-block"></span>
                                                </div>
                                          </div>
										
										<div class="form-group">
                                             <label class="col-md-4 control-label">Pendidikan Ayah &nbsp; <a style="color:red;"><blink>*</blink></a></label>
												
												<div class="col-md-8">
												<?php echo $form->dropDownList($model,'id_jenjang_pendidikan_ayah',CHtml::listData(JenjangPendidikan::model()->findAll(),'id_jenj_didik','nm_jenj_didik'),array('empty'=>'Pilih jenjang pendidikan Ayah','class'=>"form-control select")); ?>
                                                <span class="help-block"></span>
												</div>
										</div>
											
										<div class="form-group">
                                            <label class="col-md-4 control-label">Pekerjaan Ayah &nbsp; <a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-8">
												<?php echo $form->dropDownList($model,'id_pekerjaan_ayah',CHtml::listData(Pekerjaan::model()->findAll(),'id_pekerjaan','nm_pekerjaan'),array('empty'=>'Pilih pekerjaan Ayah','class'=>"form-control select")); ?>
                                               <span class="help-block"></span>
												</div>
                                        </div>
											
										    
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
											<label class="col-md-4 control-label">Nama Ibu &nbsp; <a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-8">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php echo $form->textField($model,'nm_ibu_kandung',array('size'=>60,'maxlength'=>60,'class'=>"form-control",'placeholder'=>"Masukkan nama Ibu")); ?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                        </div>
                                            
										<div class="form-group">
                                                <label class="col-md-4 control-label">Tanggal Lahir<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-8 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php 	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
																				'name'=>'Mahasiswa[tgl_lahir_ibu]',
																				'value'=>$model->tgl_lahir_ibu,
																				'options'=>array(
																					'showAnim'=>'fold',
																					'dateFormat'=>'yy-mm-dd',
																				),
																				'htmlOptions' => array(
																					'class' => 'form-control',
																					'placeholder'=>'Masukkan Tanggal Lahir Ibu'
																				),
																			));
														?>
                                                    </div>  
											
                                                    <span class="help-block"></span>
                                                </div>
                                          </div>
										<div class="form-group">
                                             <label class="col-md-4 control-label">Pendidikan Ibu &nbsp; <a style="color:red;"><blink>*</blink></a></label>
												
												<div class="col-md-8">
												<?php echo $form->dropDownList($model,'id_jenjang_pendidikan_ibu',CHtml::listData(JenjangPendidikan::model()->findAll(),'id_jenj_didik','nm_jenj_didik'),array('empty'=>'Pilih jenjang pendidikan Ibu','class'=>"form-control select")); ?>
                                                <span class="help-block"></span>
												</div>
										</div>
											
										<div class="form-group">
                                            <label class="col-md-4 control-label">Pekerjaan Ibu &nbsp; <a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-8">
												<?php echo $form->dropDownList($model,'id_pekerjaan_ibu',CHtml::listData(Pekerjaan::model()->findAll(),'id_pekerjaan','nm_pekerjaan'),array('empty'=>'Pilih pekerjaan Ibu','class'=>"form-control select")); ?>
                                               <span class="help-block"></span>
												</div>
                                        </div>
											
                                            
                                            
                                    </div>
                                  
                                </div>

                            </div>
							  
                    </div>
					 <a style="color:red;"><blink>*: Data wajib diisi</blink></a>  
                    </div>
					
					<div id="step-4">   

                    </div>
										<div id="step-5">   

                    </div>
      
				
                </div>
            </div>                        
    <!-- END WIZARD WITH VALIDATION -->					
        </div>
    </div>                    
                    
</div>
<!-- END PAGE CONTENT WRAPPER -->        				
<?php $this->endWidget(); ?>
</div><!-- form -->
