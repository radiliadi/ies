<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_pesan')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_pesan),array('view','id'=>$data->id_pesan)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pengirim')); ?>:</b>
	<?php echo CHtml::encode($data->pengirim); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penerima')); ?>:</b>
	<?php echo CHtml::encode($data->penerima); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('judul')); ?>:</b>
	<?php echo CHtml::encode($data->judul); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isi')); ?>:</b>
	<?php echo CHtml::encode($data->isi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('file')); ?>:</b>
	<?php echo CHtml::encode($data->file); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datetime')); ?>:</b>
	<?php echo CHtml::encode($data->datetime); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('sudahbaca')); ?>:</b>
	<?php echo CHtml::encode($data->sudahbaca); ?>
	<br />

	*/ ?>

</div>