<?php
$this->breadcrumbs=array(
	'Pengumumen'=>array('index'),
	$model->id_pesan,
);

$this->menu=array(
array('label'=>'List Pengumuman','url'=>array('index')),
array('label'=>'Create Pengumuman','url'=>array('create')),
array('label'=>'Update Pengumuman','url'=>array('update','id'=>$model->id_pesan)),
array('label'=>'Delete Pengumuman','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id_pesan),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Pengumuman','url'=>array('admin')),
);
?>

<h1>View Pengumuman #<?php echo $model->id_pesan; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id_pesan',
		'pengirim',
		'penerima',
		'judul',
		'isi',
		'file',
		'datetime',
		'sudahbaca',
),
)); ?>
