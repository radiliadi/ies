<?php			
	
$this->Widget('ext.highcharts.HighchartsWidget', array(
	'options' => array(
	  // 'colors'=>array('#6AC36A', '#FFD148', '#0563FE', '#FF2F2F', '#000000'),
	  'colors'=>array('#FFD148', '#0563FE', '#FF2F2F', '#000000'),
	  'gradient' => array('enabled'=> true),
	  'credits' => array('enabled' => false),
	  'exporting' => array('enabled' => false),
	  'chart' => array(
		'plotBackgroundColor' => '#ffffff',
		'plotBorderWidth' => null,
		'plotShadow' => false,
		'height' => 400,
	  ),
	  'title' => 'false',
	  'tooltip' => array(
		// 'pointFormat' => '{series.name}: <b>{point.percentage}%</b>',
		// 'percentageDecimals' => 1,
		'formatter'=> 'js:function() { return this.point.name+":  <b>"+Math.round(this.point.percentage)+"</b>%"; }',
			//the reason it didnt work before was because you need to use javascript functions to round and refrence the JSON as this.<array>.<index> ~jeffrey
	  ),
	  'plotOptions' => array(
		'pie' => array(
		  'allowPointSelect' => true,
		  'cursor' => 'pointer',
		  'dataLabels' => array(
			'enabled' => true,
			'color' => '#AAAAAA',
			'connectorColor' => '#AAAAAA',
		  ),
		  'showInLegend'=>true,
		)
	  ),
	  'series' => array(
		array(
		  'type' => 'pie',
		  'name' => 'Percentage',
		  'data' => array(
			// array('Ready / Deployable', 15),
			array($labelA, $dataa),
			array($labelB, $datab),
		  ),
		),
	  ),
	)
  ));
							
?>