<meta http-equiv="refresh" content="60" >
<?php
$judul="Chat";
$this->breadcrumbs=array(
	$judul,
);
?>
	<div class="col-md-12">		
		<!-- START NEW RECORD -->
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'login-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
		)); ?>
		<div class="panel panel-default">
			<div class="panel-body">
				<h3>What happened?</h3>
				<form class="form-horizontal" role="form">
				<div class="form-group">
					<div class="col-md-12">
						<div class="input-group">
							<span class="input-group-addon"><span class="fa fa-pencil-square-o"></span></span>
							<?php echo $form->textField($model,'chat_text',array('autofocus'=>'autofocus','placeholder'=>'Mulai percakapan..','class'=>"form-control")); ?>
						</div>  
							<br>
					</div>                                        
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<div class="pull-right">
							<?php echo CHtml::submitButton("Kirim",array('class'=>"btn btn-primary")); ?>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
		<?php $this->endWidget(); ?>
		<!-- END NEW RECORD -->
		
	</div>	
	<div class="col-md-12">
		
		<!-- START TIMELINE -->
		<div class="timeline">
			<!-- START TIMELINE ITEM -->
			<?php
				foreach($modelTahun as $thn)
				{
					echo '
					<div class="timeline-item timeline-main">
						<div class="timeline-date">'.$thn->year.'</div>
					</div>
					';
						$no=1;						
						$criteria=new CDbCriteria;
						$criteria->order = "chat_date DESC";
						$criteria->condition = "DATE_FORMAT(chat_date,'%Y')=:date";
						$criteria->params = array (	
						':date' => $thn->year,
						);
						$model=Chat::model()->findAll($criteria);
						foreach($model as $db)
						{
							//Pengaturan posisi kiri atau kanan
							if($no%2==1)
							{
								$position="";
							}else{
								$position="timeline-item-right";
							}
							echo'
							<!-- START TIMELINE ITEM -->
							<div class="timeline-item '.$position.'">
								<div class="timeline-item-info" style="background:#656d78;"><font color="white">'.$db->chat_date.'</font></div>
								<div class="timeline-item-icon"><span class="fa fa-globe"></span></div>
								<div class="timeline-item-content">
									<div class="timeline-heading">
										';
											if($db->user['user_foto']!="") :
											echo CHtml::image(Yii::app()->request->baseUrl.'/images/user/'.$db->user['username'].'.jpg',"Pilih Foto",array("width"=>'250px','class'=>'media-object img-thumbnail user-img')); // Image shown here if page is update page
											else:
												echo '<img src="'.Yii::app()->request->baseUrl.'/images/no-image.jpg"/>';
											endif;
										echo '
										<a>'.$db->user['username'].'</a> as<a> '.CHtml::encode(Yii::app()->session->get('level')).' </a>
									</div>
									<div class="timeline-body">
										<p>'.$db->chat_text.'</p>
									</div>
								</div>
							</div>       
							<!-- END TIMELINE ITEM -->
							';
						$no++;
						}
				}
			?>
			<!-- END TIMELINE ITEM -->
			<!-- START TIMELINE ITEM -->
			<div class="timeline-item timeline-main">
				<div class="timeline-date"><a href="#"><span class="fa fa-arrow-up"></span></a></div>
			</div>                                
			<!-- END TIMELINE ITEM -->
		</div>
		<!-- END TIMELINE -->
		
	</div>