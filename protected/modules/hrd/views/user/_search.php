<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldGroup($model,'username',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>25)))); ?>
		
		

		<?php echo $form->dropDownListGroup($model,'level',array(
				'widgetOptions' => array(
					'data' => array(                  
                    'operator'=>'operator',
                    'mahasiswa'=>'mahasiswa',
					'admin'=>'admin',
                ),
					'htmlOptions' => array(),
				)));
		?>

	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
