<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('User' ,
        // 'Semua' => array('Semua'=>'admin', 
        // 'Lalai'=>array('asd'
          // )
        ),
    )
);
?>
<div class="row">

	<div class="page-title">                    
                    <h2><span class="fa fa-laptop"></span> Manage User</h2>
                </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
				<div class="search-form" style="display:none">
					<?php $this->renderPartial('_search',array(
					'model'=>$model,
				)); ?>
				</div>
			</div>
		</div>
	</div>



<div class="col-md-12">
<div class="panel panel-default">
                                <div class="panel-heading">
									<div class="btn-group pull-right">
                                        <?php echo CHtml::link('<i class="fa fa-plus" title="Create"></i>', 'create', array('class'=>'btn btn btn-primary pull-right')); ?>
	
                                    </div> 
									
                                    
                                </div>

   
    <div class="panel-body">

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'user-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'pager' => array(
                            'header' => '',
                            'footer' => '',
                            'cssFile' => false,
                            'selectedPageCssClass' => 'active',
                            'hiddenPageCssClass' => 'disabled',
                            'firstPageCssClass' => 'previous',
                            'lastPageCssClass' => 'next',
                            'maxButtonCount' => 5,
                            'firstPageLabel'=> '<i class="fa fa-angle-double-left"></i>',
                            'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
                            'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
                            'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
                            'htmlOptions' => array(
                                'class' => 'dataTables_paginate paging_bootstrap pagination pull-right',
                                'align' => 'center',
                            )
                        ),
'columns'=>array(
		'username',
		'password',
		array(
                   'name' => 'level',
                   'filter' => User::listUser()
        ),
		// array(
  //           'name'=>'level',
  //           'value'=>'$data->level', 
  //           'filter'=>array(                  
  //                   'operator'=>'operator',
  //               )
           
  //       ),
	
// 	array(
// 'class'=>'booster.widgets.TbButtonColumn',
// 'template'=>'{view}',
// ),		
array(
'class'=>'booster.widgets.TbButtonColumn',
'template'=>'{update}',
),
array(
'class'=>'booster.widgets.TbButtonColumn',
'afterDelete'=>'function(link,success,data){ if(success) alert("Delete completed successfully"); }',
'template'=>'{delete}',
),
),
)); ?>
