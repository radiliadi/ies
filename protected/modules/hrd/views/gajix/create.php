<?php
/* @var $this GajiController */
/* @var $model Gaji */

$this->breadcrumbs=array(
	'Gajis'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Gaji', 'url'=>array('index')),
	array('label'=>'Manage Gaji', 'url'=>array('admin')),
);
?>

<h1>Create Gaji</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>