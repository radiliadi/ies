<?php
/* @var $this GajiController */
/* @var $model Gaji */

$this->breadcrumbs=array(
	'Gajis'=>array('index'),
	$model->id_gaji,
);

$this->menu=array(
	array('label'=>'List Gaji', 'url'=>array('index')),
	array('label'=>'Create Gaji', 'url'=>array('create')),
	array('label'=>'Update Gaji', 'url'=>array('update', 'id'=>$model->id_gaji)),
	array('label'=>'Delete Gaji', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_gaji),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Gaji', 'url'=>array('admin')),
);
?>

<h1>View Gaji #<?php echo $model->id_gaji; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_gaji',
		'id_pd',
		'tgl',
		'bulan',
		'tahun',
		'gj_pokok',
		'tunjangan',
		'potongan',
		'tot_gaji',
		'neto_setahun',
		'wp_pribadi',
		'wp_menikah',
		'tanggungan',
		'pajak_setahun',
		'pph_tahun',
		'pph_bulan',
		'tot_pinjaman',
	),
)); ?>
