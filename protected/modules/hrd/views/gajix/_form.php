<?php
/* @var $this GajiController */
/* @var $model Gaji */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gaji-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_pd'); ?>
		<?php echo $form->textField($model,'id_pd'); ?>
		<?php echo $form->error($model,'id_pd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl'); ?>
		<?php echo $form->textField($model,'tgl'); ?>
		<?php echo $form->error($model,'tgl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bulan'); ?>
		<?php echo $form->textField($model,'bulan',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'bulan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tahun'); ?>
		<?php echo $form->textField($model,'tahun'); ?>
		<?php echo $form->error($model,'tahun'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gj_pokok'); ?>
		<?php echo $form->textField($model,'gj_pokok'); ?>
		<?php echo $form->error($model,'gj_pokok'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tunjangan'); ?>
		<?php echo $form->textField($model,'tunjangan'); ?>
		<?php echo $form->error($model,'tunjangan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'potongan'); ?>
		<?php echo $form->textField($model,'potongan'); ?>
		<?php echo $form->error($model,'potongan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tot_gaji'); ?>
		<?php echo $form->textField($model,'tot_gaji'); ?>
		<?php echo $form->error($model,'tot_gaji'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'neto_setahun'); ?>
		<?php echo $form->textField($model,'neto_setahun'); ?>
		<?php echo $form->error($model,'neto_setahun'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wp_pribadi'); ?>
		<?php echo $form->textField($model,'wp_pribadi'); ?>
		<?php echo $form->error($model,'wp_pribadi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wp_menikah'); ?>
		<?php echo $form->textField($model,'wp_menikah'); ?>
		<?php echo $form->error($model,'wp_menikah'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tanggungan'); ?>
		<?php echo $form->textField($model,'tanggungan'); ?>
		<?php echo $form->error($model,'tanggungan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pajak_setahun'); ?>
		<?php echo $form->textField($model,'pajak_setahun'); ?>
		<?php echo $form->error($model,'pajak_setahun'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pph_tahun'); ?>
		<?php echo $form->textField($model,'pph_tahun'); ?>
		<?php echo $form->error($model,'pph_tahun'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pph_bulan'); ?>
		<?php echo $form->textField($model,'pph_bulan'); ?>
		<?php echo $form->error($model,'pph_bulan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tot_pinjaman'); ?>
		<?php echo $form->textField($model,'tot_pinjaman'); ?>
		<?php echo $form->error($model,'tot_pinjaman'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->