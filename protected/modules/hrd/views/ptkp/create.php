<?php
/* @var $this PtkpController */
/* @var $model Ptkp */

$this->breadcrumbs=array(
	'Ptkps'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Ptkp', 'url'=>array('index')),
	array('label'=>'Manage Ptkp', 'url'=>array('admin')),
);
?>

<h1>Create Ptkp</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>