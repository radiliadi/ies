<?php
/* @var $this PtkpController */
/* @var $model Ptkp */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ptkp-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'wp_pribadi'); ?>
		<?php echo $form->textField($model,'wp_pribadi'); ?>
		<?php echo $form->error($model,'wp_pribadi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wp_menikah'); ?>
		<?php echo $form->textField($model,'wp_menikah'); ?>
		<?php echo $form->error($model,'wp_menikah'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wp_tanggungan'); ?>
		<?php echo $form->textField($model,'wp_tanggungan'); ?>
		<?php echo $form->error($model,'wp_tanggungan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ptkp_ket'); ?>
		<?php echo $form->textField($model,'ptkp_ket',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ptkp_ket'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->