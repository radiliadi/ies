<?php
/* @var $this PtkpController */
/* @var $model Ptkp */

$this->breadcrumbs=array(
	'Ptkps'=>array('index'),
	$model->id_ptkp,
);

$this->menu=array(
	array('label'=>'List Ptkp', 'url'=>array('index')),
	array('label'=>'Create Ptkp', 'url'=>array('create')),
	array('label'=>'Update Ptkp', 'url'=>array('update', 'id'=>$model->id_ptkp)),
	array('label'=>'Delete Ptkp', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_ptkp),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ptkp', 'url'=>array('admin')),
);
?>

<h1>View Ptkp #<?php echo $model->id_ptkp; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_ptkp',
		'wp_pribadi',
		'wp_menikah',
		'wp_tanggungan',
		'ptkp_ket',
	),
)); ?>
