<?php
/* @var $this PtkpController */
/* @var $model Ptkp */

$this->breadcrumbs=array(
	'Ptkps'=>array('index'),
	$model->id_ptkp=>array('view','id'=>$model->id_ptkp),
	'Update',
);

$this->menu=array(
	array('label'=>'List Ptkp', 'url'=>array('index')),
	array('label'=>'Create Ptkp', 'url'=>array('create')),
	array('label'=>'View Ptkp', 'url'=>array('view', 'id'=>$model->id_ptkp)),
	array('label'=>'Manage Ptkp', 'url'=>array('admin')),
);
?>

<h1>Update Ptkp <?php echo $model->id_ptkp; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>