<?php
/* @var $this PtkpController */
/* @var $data Ptkp */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_ptkp')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_ptkp), array('view', 'id'=>$data->id_ptkp)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wp_pribadi')); ?>:</b>
	<?php echo CHtml::encode($data->wp_pribadi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wp_menikah')); ?>:</b>
	<?php echo CHtml::encode($data->wp_menikah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wp_tanggungan')); ?>:</b>
	<?php echo CHtml::encode($data->wp_tanggungan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ptkp_ket')); ?>:</b>
	<?php echo CHtml::encode($data->ptkp_ket); ?>
	<br />


</div>