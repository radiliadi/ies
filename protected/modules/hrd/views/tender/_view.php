<?php
/* @var $this TenderController */
/* @var $data Tender */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tender')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tender), array('view', 'id'=>$data->id_tender)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('info_source')); ?>:</b>
	<?php echo CHtml::encode($data->info_source); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('client_code')); ?>:</b>
	<?php echo CHtml::encode($data->client_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('client_name')); ?>:</b>
	<?php echo CHtml::encode($data->client_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tender_no')); ?>:</b>
	<?php echo CHtml::encode($data->tender_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tender_status')); ?>:</b>
	<?php echo CHtml::encode($data->tender_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tender_type')); ?>:</b>
	<?php echo CHtml::encode($data->tender_type); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('detail_task')); ?>:</b>
	<?php echo CHtml::encode($data->detail_task); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ref_address')); ?>:</b>
	<?php echo CHtml::encode($data->ref_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deadline')); ?>:</b>
	<?php echo CHtml::encode($data->deadline); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bid_bond')); ?>:</b>
	<?php echo CHtml::encode($data->bid_bond); ?>
	<br />

	*/ ?>

</div>