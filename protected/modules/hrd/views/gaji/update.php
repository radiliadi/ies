<?php
/* @var $this GajiController */
/* @var $model Gaji */

$this->breadcrumbs=array(
	'Gajis'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Gaji', 'url'=>array('index')),
	array('label'=>'Create Gaji', 'url'=>array('create')),
	array('label'=>'View Gaji', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Gaji', 'url'=>array('admin')),
);
?>

<h1>Update Gaji <?php echo $model->nip; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>