<?php
/* @var $this GajiController */
/* @var $model Gaji */


echo '<div class="box col-md-12" style="width:1100px; background-color:white">
        <div class="box-inner">
                <h2><i class="glyphicon glyphicon-info-sign"></i> Introduction</h2>
			</div>
			';


$this->breadcrumbs=array(
	'Gajis'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Gaji', 'url'=>array('index')),
	array('label'=>'Manage Gaji', 'url'=>array('admin')),
);
?>

<h1>Create Gaji</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'modelpegawai'=>$modelpegawai)); ?>