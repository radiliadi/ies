<?php
/* @var $this GajiController */
/* @var $data Gaji */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nip')); ?>:</b>
	<?php echo CHtml::encode($data->nip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl')); ?>:</b>
	<?php echo CHtml::encode($data->tgl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gj_pokok')); ?>:</b>
	<?php echo CHtml::encode($data->gj_pokok); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tunjangan')); ?>:</b>
	<?php echo CHtml::encode($data->tunjangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('potongan')); ?>:</b>
	<?php echo CHtml::encode($data->potongan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tot_gaji')); ?>:</b>
	<?php echo CHtml::encode($data->tot_gaji); ?>
	<br />


</div>