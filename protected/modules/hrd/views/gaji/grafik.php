<?php
/* @var $this GajiController */
/* @var $dataProvider CActiveDataProvider */

echo '<div class="box col-md-12" style="width:1100px; background-color:white">
        <div class="box col-md-12 alert alert-success" style="width:1070px">
            <h2><i class="glyphicon icon-copy"></i> Statistik Penggajian</h2>
		</div>
			';

?>

<div class="row">
	<div class="box col-md-12 alert alert-warning">
		<div class="box-content">
			<div class="col-md-12">
				<br>
				<h2><b>Statistik Berdasarkan Golongan</b></h2>
				<br>
					<?php
						$this->breadcrumbs=array(
						'Chart'=>array('grafik'),
						'Statistik Gaji Karyawan',
						);
						
							$form=$this->beginWidget('CActiveForm', array(
							'id'=>'tinstrument-form',
							'enableAjaxValidation'=>false,
					)); 

							$label=array();
							$nilai=array();
								foreach($dataProvider->getData() as $i=>$ii)
									{
										$label[$i]=$ii['jabatan'];
										$nilai[$i]=(int)$ii['total'];
									}
									
							$this->widget('application.extensions.highcharts.HighchartsWidget', array(
								'options'=>array(
								'chart'=> array('defaultSeriesType'=>'column',),
								'title' => array('text' => ''),
								'legend'=>array('enabled'=>false),
								'xAxis'=>array('categories'=>$label,
								'title'=>array('text'=>''),),
								'yAxis'=> array(
									'min'=> 0,
								'title'=> array(
									'text'=>'Jumlah'
								),
							),
								'series' => array(
									array('data' => $nilai)
								),
								
								'tooltip' => array('formatter' => 'js:function(){ return "<b>"+this.point.name+"</b>:"+this.y;}'),
								'tooltip' => array(
								'formatter' => 'js:function() {return "<b>"+ this.x +"</b><br/>"+" Jumlah: "+this.y;}'
								),
									'plotOptions'=>array('pie'=>(array(
										'allowPointSelect'=>true,
										'showInLegend'=>true,
										'cursor'=>'pointer',
											)
										)
									),
									'credits'=>array('enabled'=>false),
									)
								));
								
					?>
				</div>
			</div>
		</div>
	</div>	
<?php $this->endWidget(); ?>
</div>