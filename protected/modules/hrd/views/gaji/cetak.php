<?php
/* @var $this GajiController */
/* @var $dataProvider CActiveDataProvider */

?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pegawai-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data',
		'role'=>'form',
	),
)); 

echo '<div class="box col-md-12" style="width:1100px; background-color:white">
        <div class="box-inner">
                <h2><i class="glyphicon icon-copy"></i> Laporan Penggajian</h2>
		</div>
			';
						

$this->breadcrumbs=array(
	'Pegawais'=>array('cetak'),
	'Manage',
);

?>

<div class="box-inner alert alert-info"  style="background-color:white">        			
			<div class="alert alert-danger">	
			<h3>Lihat Gaji Pegawai pada Tahun </h3>
			<table>
				<tbody>
					<tr>
						<td><!--?php echo $tahun?--> &nbsp;</td>
						<td class="center">
							<?php
								for($i=2000;$i<=date('Y');$i++){
								$thn[$i]=$i;
								}
							?>
							<div>
								<div style="float:left;margin:5px;">
								<?php 
								echo $form->dropDownList($modelbulan,'tahun',$thn,array('empty'=>'Pilih Tahun','class'=>"form-control", 'style'=>"float:left;")); 
								?>															
								</div>
							</div>	
						</td>
						<td>	
							<?php echo CHtml::submitButton('Lihat', array('class'=>'btn btn-default', 'style'=>'width:100px')); ?>					
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>



<?php $this->endWidget(); ?>

</div>