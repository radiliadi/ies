<?php
/* @var $this PagesizeController */
/* @var $data Pagesize */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_ps')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_ps), array('view', 'id'=>$data->id_ps)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jml_ps')); ?>:</b>
	<?php echo CHtml::encode($data->jml_ps); ?>
	<br />


</div>