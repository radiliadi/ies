<?php
/* @var $this PagesizeController */
/* @var $model Pagesize */

$this->breadcrumbs=array(
	'Pagesizes'=>array('index'),
	$model->id_ps,
);

$this->menu=array(
	array('label'=>'List Pagesize', 'url'=>array('index')),
	array('label'=>'Create Pagesize', 'url'=>array('create')),
	array('label'=>'Update Pagesize', 'url'=>array('update', 'id'=>$model->id_ps)),
	array('label'=>'Delete Pagesize', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_ps),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Pagesize', 'url'=>array('admin')),
);
?>

<h1>View Pagesize #<?php echo $model->id_ps; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_ps',
		'jml_ps',
	),
)); ?>
