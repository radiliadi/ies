<?php
/* @var $this PagesizeController */
/* @var $model Pagesize */

$this->breadcrumbs=array(
	'Pagesizes'=>array('index'),
	$model->id_ps=>array('view','id'=>$model->id_ps),
	'Update',
);

$this->menu=array(
	array('label'=>'List Pagesize', 'url'=>array('index')),
	array('label'=>'Create Pagesize', 'url'=>array('create')),
	array('label'=>'View Pagesize', 'url'=>array('view', 'id'=>$model->id_ps)),
	array('label'=>'Manage Pagesize', 'url'=>array('admin')),
);
?>

<h1>Update Pagesize <?php echo $model->id_ps; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>