<?php

class CvController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','admin', 'adminn', 'delete','create','update'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Cv;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Cv']))
		{
			$model->attributes=$_POST['Cv'];
			$model->link_cv=CUploadedFile::getInstance($model, 'link_cv');
			if($model->save())
            {
                $model->link_cv->saveAs(Yii::app()->basePath.'/../cv/'.$model->nm_cv.'.pdf');
                $this->redirect(array('admin'));
            }
        }

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Cv']))
		{
			$model->attributes=$_POST['Cv'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_cv));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Cv');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Cv('search');

		// $criteria = new CDbcriteria();
		// $criteria->condition='stat_cv = :stat_cv';
		// $criteria->params=(array(':stat_cv'=>'1'));
		// $model=Cv::model()->findAll(array(
		// 	'condition'=>'stat_cv = :stat_cv',
		// 	'params'=>array(
		// 	':stat_cv'=>1),
		// 	));
		// $sql='SELECT * FROM cv WHERE nm_cv="Firman Alvianto"';
		
		// $dataProvider=new CSqlDataProvider($sql);
		// $criteria=new CDbCriteria;
		// $criteria->select = "* as cv_tetap";
		// $criteria->condition = "stat_cv=:stat_cv";
		// $criteria->params = array (	
		// 	':stat_cv'=>"1",
		// );
		// $cv=Cv::model()->find($criteria);
		// $dataProvider=new CActiveDataProvider('Cv',array('criteria'=>$criteria));
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Cv']))
			$model->attributes=$_GET['Cv'];

		$this->render('admin',array(
			'model'=>$model,
			// 'cv'=>$cv,
			// 'dataProvider'=>$dataProvider,
		));
	}

	public function actionAdminn()
	{
		$model=new Cv('searchx');

		// $criteria = new CDbcriteria();
		// $criteria->condition='stat_cv = :stat_cv';
		// $criteria->params=(array(':stat_cv'=>'1'));
		// $model=Cv::model()->findAll(array(
		// 	'condition'=>'stat_cv = :stat_cv',
		// 	'params'=>array(
		// 	':stat_cv'=>1),
		// 	));
		// $sql='SELECT * FROM cv WHERE nm_cv="Firman Alvianto"';
		
		// $dataProvider=new CSqlDataProvider($sql);
		// $criteria=new CDbCriteria;
		// $criteria->select = "* as cv_tetap";
		// $criteria->condition = "stat_cv=:stat_cv";
		// $criteria->params = array (	
		// 	':stat_cv'=>"1",
		// );
		// $cv=Cv::model()->find($criteria);
		// $dataProvider=new CActiveDataProvider('Cv',array('criteria'=>$criteria));
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Cv']))
			$model->attributes=$_GET['Cv'];

		$this->render('adminn',array(
			'model'=>$model,
			// 'cv'=>$cv,
			// 'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Cv the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Cv::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Cv $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='cv-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
