<?php

class OperatorModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'operator.models.*',
			'operator.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			//$user = Yii::app()->user;
			$getlevel=Yii::app()->session->get('level');
			if($getlevel == "operator"){
				return true;
			}else{
				throw new CHttpException('403', 'Anda tidak berhak mengakses halaman ini');
			}
		}
		else
			return false;
	}
}
