<?php

class ChartOfAccountDetailController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','menu','checkSaldo','add','changeColor','aigo','adminx','jurnal','submitJurnal', 'unsubmitJurnal'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ChartOfAccountDetail;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ChartOfAccountDetail']))
		{
			$model->attributes=$_POST['ChartOfAccountDetail'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->coad_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionAdd($id, $type)
	{
		$id_pd=Yii::app()->session->get('username');
		$coaRule=ChartOfAccountRule::model()->find('coar_status=1');

		$model=new ChartOfAccountDetail;
		$model->coad_coa_id 			= $id;
		$model->coad_coar_id 			= $coaRule->coar_id;
		$model->coad_coat_id 			= $type;
		$model->coad_id_pd 				= $id_pd;
		$model->coad_datetime_insert 	= date('Y-m-d H:i:s');

		$coaMenu = ChartOfAccount::model()->find('coa_id=:coa_id AND coa_status=1', array(':coa_id'=>$id));
		$coaType = ChartOfAccountType::model()->find(array('select'=>'coat_id, coat_desc', 'condition'=>'coat_id=:coat_id AND coat_status=1', 'params'=>array(':coat_id'=>$type)));

		if(isset($_POST['ChartOfAccountDetail']))
		{
			$model->attributes=$_POST['ChartOfAccountDetail'];
			if($model->save())
				$this->redirect(array('chartOfAccountDetail/menu/id/'.$id.'/type/'.$type.''));
		}

		$this->render('create',array(
			'model'=>$model,
			'coaMenu'=>$coaMenu,
			'coaType'=>$coaType,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id, $coaId, $type)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$id_pd=Yii::app()->session->get('username');

		if(isset($_POST['ChartOfAccountDetail']))
		{
			$model->attributes=$_POST['ChartOfAccountDetail'];
			$model->coad_id_pd = $id_pd;
			if($model->save())
				$this->redirect(array('chartOfAccountDetail/menu/id/'.$coaId.'/type/'.$type.''));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ChartOfAccountDetail');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		if(isset($_POST['ChartOfAccountDetail']['bulan1']))
		{
			if($_POST['ChartOfAccountDetail']['bulan1']!=''){
				$model=new ChartOfAccountDetail('search');
				$model->unsetAttributes();  // clear any default values
				if(isset($_GET['ChartOfAccountDetail']))
					$model->attributes=$_GET['ChartOfAccountDetail'];
				$id = 1;//ngopas dari actionMenu
				$type = 6;//ngopas dari actionMenu
				$model->coad_coa_id=$id;//ngopas dari actionMenu
				$model->coad_coat_id=$type;//ngopas dari actionMenu
				// $model->findByAttributes(array('coa_coat_id'=>$id));
				$coaRule = ChartOfAccountRule::model()->find('coar_status=1');//ngopas dari actionMenu
				$coaMenu = ChartOfAccount::model()->find('coa_id=:coa_id AND coa_status=1', array(':coa_id'=>$id));//ngopas dari actionMenu

				// $coaDetail=ChartOfAccountDetail::model()->findAll('coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1', array(':coad_coat_id'=>$id, 'coad_coa_id'=>$coaMenu->coa_id));

				$coaDetailfilter = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_datetime ASC, coad_coa_id ASC', 'group'=>'coad_coa_id ASC', 'condition'=>'coad_status=1 AND coad_is_submit=1'));//ngopas dari actionMenu TAPI DIPAKE
				$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_coa_id ASC, coad_datetime DESC', 'condition'=>'coad_status=1 AND coad_is_submit=1'));//ngopas dari actionMenu TAPI DIPAKE
				// foreach($coaDetailfilter as $kambing){
				// // 	// $count = 30;
				// // 	// for($i=0; $i<$count; $i++){
				// 		$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_datetime DESC', 'condition'=>'coad_status=1 AND coad_is_submit=1 AND coad_coa_id=:coad_coa_id', 'params'=>array(':coad_coa_id'=>$kambing->coad_coa_id)));//ngopas dari actionMenu TAPI DIPAKE
				// 		foreach($coaDetail as $key => $value){
				// 			echo '<pre>';
				// 			print_r($value->coad_id);
				// 			echo ' ';
				// 			print_r($value->coad_coa_id);
				// 			echo '<pre>';
				// 			echo '<br/>';
				// 		}
				// 		// echo '<pre>';
				// 		// print_r($kambing->coad_coa_id);
				// 		// echo '<pre>';
				// 	// }
						
					
				// }
				
				
				// die;
				if(empty($coaDetail)){//ngopas dari actionMenu
					// dd('rere');
					$newCoaDetail = new ChartOfAccountDetail;
					$newCoaDetail->coad_coa_id 			= $id;
					$newCoaDetail->coad_coar_id 		= $coaRule->coar_id;
					$newCoaDetail->coad_coat_id 		= $type;
					$newCoaDetail->coad_desc 			= "has been created";
					$newCoaDetail->coad_debet 			= 0;
					$newCoaDetail->coad_credit 			= 0;
					$newCoaDetail->coad_is_saldo 		= 1;
					$newCoaDetail->coad_datetime 		= date('Y-m-d');
					$newCoaDetail->coad_datetime_insert = date('Y-m-d H:i:s');
					if($newCoaDetail->save(false)){
						print_r($newCoaDetail->getErrors());
						// throw new CHttpException('Please try again, You are fine!');
						return $this->redirect(Yii::app()->request->urlReferrer);
					}
				}
				$saldoAkhir = ChartOfAccountDetail::model()->findAll(array('select'=>'coad_id','order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1', 'params'=>array(':coad_coa_id'=>$id, ':coad_coat_id'=>$type)));//ngopas dari actionMenu
				$coaDetailx = ChartOfAccountDetail::model()->find('coad_coa_id=:coad_coa_id AND coad_status=1', array(':coad_coa_id'=>$id));//ngopas dari actionMenu

				$coaType = ChartOfAccountType::model()->find('coat_id=:coat_id AND coat_status=1', array(':coat_id'=>$type));//ngopas dari actionMenu

				$bankAll = Bank::model()->findAll(array('condition'=>'bnk_status=1'));

				$bankSaldoCurrent = ChartOfAccountDetail::model()->findAll(array('group'=>'coad_coa_id','condition'=>'coad_coat_id=:coad_coat_id AND coad_status=1', 'params'=>array(':coad_coat_id'=>8)));
				$bankSaldoCurren = ChartOfAccountDetail::model()->find(array('group'=>'coad_coa_id','condition'=>'coad_coat_id=:coad_coat_id AND coad_status=1', 'params'=>array(':coad_coat_id'=>8)));
				$bankBB = ChartOfAccount::model()->findAll(array('condition'=>'coa_coat_id=:coa_coat_id AND coa_status=1', 'params'=>array(':coa_coat_id'=>8)));

				$getBankId = Bank::model()->findAll(array('condition'=>'bnk_coa_id=:bnk_coa_id AND bnk_status=1', 'params'=>array(':bnk_coa_id'=>$bankSaldoCurren->coad_coa_id)));

				$bniIDR = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC','condition'=>'coad_coa_id=:coad_coa_id AND coad_status=1 AND coad_is_submit=1 AND coad_bb_is_saldo=1', 'params'=>array(':coad_coa_id'=>101)));
				$bniGIRO = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC','condition'=>'coad_coa_id=:coad_coa_id AND coad_status=1 AND coad_is_submit=1 AND coad_bb_is_saldo=1', 'params'=>array(':coad_coa_id'=>102)));
				$bniKSO = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC','condition'=>'coad_coa_id=:coad_coa_id AND coad_status=1 AND coad_is_submit=1 AND coad_bb_is_saldo=1', 'params'=>array(':coad_coa_id'=>103)));
			}
		}

		$tahun=date('Y');
		$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_coa_id ASC, coad_datetime DESC', 'condition'=>'coad_status=1 AND coad_is_submit=1 AND coad_is_jurnal=1'));


		if(isset($_POST['ChartOfAccountDetail']['coad_datetime'])) //filter tahun
		{	
			if($_POST['ChartOfAccountDetail']['coad_datetime']!=''){
				$tahun= $_POST['ChartOfAccountDetail']['coad_datetime'];
				$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_coa_id ASC, coad_datetime DESC', 'condition'=>'coad_status=1 AND coad_is_submit=1 AND coad_is_jurnal=1 AND DATE_FORMAT (coad_datetime,("%Y"))=:thn', 'params'=>array(':thn'=>$tahun)));
			}
		}

		// $bulan=date('m');
		$bulan='';
		if(isset($_POST['ChartOfAccountDetail']['coad_datetime_insert'])) //filter bulan + tahun
		{	
			if($_POST['ChartOfAccountDetail']['coad_datetime_insert']!=''){
				$bulan= $_POST['ChartOfAccountDetail']['coad_datetime_insert'];
				$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_coa_id ASC, coad_datetime DESC', 'condition'=>'coad_status=1 AND coad_is_submit=1 AND coad_is_jurnal=1 AND DATE_FORMAT (coad_datetime,("%Y"))=:thn AND DATE_FORMAT (coad_datetime,("%c"))=:bln', 'params'=>array(':thn'=>$tahun, ':bln'=>$bulan)));
			}
		}else{
			// $bulan=date('m');
			$bulan='';
		}

		$coa = '';
		if(isset($_POST['ChartOfAccountDetail']['coad_coa_id'])) //filter coa + tahun
		{	
			if($_POST['ChartOfAccountDetail']['coad_coa_id']!=''){
				$coa= $_POST['ChartOfAccountDetail']['coad_coa_id'];
				$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_coa_id ASC, coad_datetime DESC', 'condition'=>'coad_status=1 AND coad_is_submit=1 AND coad_is_jurnal=1 AND DATE_FORMAT (coad_datetime,("%Y"))=:thn AND coad_coa_id=:coad_coa_id', 'params'=>array(':thn'=>$tahun, ':coad_coa_id'=>$coa)));
			}
		}

		if(isset($_POST['ChartOfAccountDetail']['coad_datetime_insert']) && isset($_POST['ChartOfAccountDetail']['coad_coa_id'])) //filter bulan + coa - tahun
		{	
			if($_POST['ChartOfAccountDetail']['coad_datetime_insert']!='' && $_POST['ChartOfAccountDetail']['coad_coa_id']!=''){
				$bulan= $_POST['ChartOfAccountDetail']['coad_datetime_insert'];
				$coa= $_POST['ChartOfAccountDetail']['coad_coa_id'];
				$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_coa_id ASC, coad_datetime DESC', 'condition'=>'coad_status=1 AND coad_is_submit=1 AND coad_is_jurnal=1 AND DATE_FORMAT (coad_datetime,("%c"))=:bln AND coad_coa_id=:coad_coa_id', 'params'=>array(':bln'=>$bulan, ':coad_coa_id'=>$coa)));
			}
		}else{
			// $bulan=date('m');
			$bulan='';
		}

		if(isset($_POST['ChartOfAccountDetail']['coad_datetime_insert']) && isset($_POST['ChartOfAccountDetail']['coad_coa_id']) && isset($_POST['ChartOfAccountDetail']['coad_datetime'])) //filter bulan + coa - tahun
		{	
			if($_POST['ChartOfAccountDetail']['coad_datetime_insert']!='' && $_POST['ChartOfAccountDetail']['coad_coa_id']!=''){
				$tahun= $_POST['ChartOfAccountDetail']['coad_datetime'];
				$bulan= $_POST['ChartOfAccountDetail']['coad_datetime_insert'];
				$coa= $_POST['ChartOfAccountDetail']['coad_coa_id'];
				$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_coa_id ASC, coad_datetime DESC', 'condition'=>'coad_status=1 AND coad_is_submit=1 AND coad_is_jurnal=1 AND DATE_FORMAT (coad_datetime,("%Y"))=:thn AND DATE_FORMAT (coad_datetime,("%c"))=:bln AND coad_coa_id=:coad_coa_id', 'params'=>array(':thn'=>$tahun, ':bln'=>$bulan, ':coad_coa_id'=>$coa)));
			}
		}else{
			// $bulan=date('m');
			$bulan='';
		}

		$coaDetailfilter = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_datetime ASC, coad_coa_id ASC', 'group'=>'coad_coa_id ASC', 'condition'=>'coad_status=1 AND coad_is_submit=1'));//ngopas dari actionMenu TAPI DIPAKE
		// $model=Gaji::model()->findAll('id_pd=:id_pd AND DATE_FORMAT (tgl,("%Y"))=:thn', array(':id_pd'=>$id , ':thn'=>$tahun));

		$bankAll = Bank::model()->findAll(array('condition'=>'bnk_status=1'));
		$bankBB = ChartOfAccount::model()->findAll(array('condition'=>'coa_coat_id=:coa_coat_id AND coa_status=1', 'params'=>array(':coa_coat_id'=>8)));
		$bankSaldoCurrent = ChartOfAccountDetail::model()->findAll(array('group'=>'coad_coa_id','condition'=>'coad_coat_id=:coad_coat_id AND coad_status=1', 'params'=>array(':coad_coat_id'=>8)));
		$bankSaldoCurren = ChartOfAccountDetail::model()->find(array('group'=>'coad_coa_id','condition'=>'coad_coat_id=:coad_coat_id AND coad_status=1', 'params'=>array(':coad_coat_id'=>8)));
		$getBankId = Bank::model()->findAll(array('condition'=>'bnk_coa_id=:bnk_coa_id AND bnk_status=1', 'params'=>array(':bnk_coa_id'=>$bankSaldoCurren->coad_coa_id)));
		$bniIDR = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC','condition'=>'coad_coa_id=:coad_coa_id AND coad_status=1 AND coad_is_submit=1 AND coad_bb_is_saldo=1', 'params'=>array(':coad_coa_id'=>101)));
		$bniGIRO = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC','condition'=>'coad_coa_id=:coad_coa_id AND coad_status=1 AND coad_is_submit=1', 'params'=>array(':coad_coa_id'=>102)));
		$bniKSO = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC','condition'=>'coad_coa_id=:coad_coa_id AND coad_status=1 AND coad_is_submit=1', 'params'=>array(':coad_coa_id'=>103)));
		$modeltahun=new ChartOfAccountDetail;
		$modelbulan=new ChartOfAccountDetail;
		$modelcoa=new ChartOfAccountDetail;
		// echo '<pre>';
		// print_r($bankAll);
		// echo '</pre>';
		// die;

		$this->render('admin',array(
			// 'model'=>$model,
			'bankAll'=>$bankAll,
			'bankBB'=>$bankBB,
			'bankSaldoCurrent'=>$bankSaldoCurrent,
			'getBankId'=>$getBankId,
			'bniIDR'=>$bniIDR,
			'bniGIRO'=>$bniGIRO,
			'bniKSO'=>$bniKSO,
			'tahun'=>$tahun,
			'bulan'=>$bulan,
			'coa'=>$coa,
			'modeltahun'=>$modeltahun,
			'modelbulan'=>$modelbulan,
			'modelcoa'=>$modelcoa,

			//begin ngopas dari actionMenu
			// 'coaMenu'=>$coaMenu,
			// 'coaType'=>$coaType,
			'coaDetail'=>$coaDetail,
			// 'coaDetailfilter'=>$coaDetailfilter,
			// 'coaDetailx'=>$coaDetailx,
			// 'saldoAkhir'=>$saldoAkhir,
			//end ngopas dari actionMenu
		));
	}

	public function actionAdminx()
	{
		$model=new ChartOfAccountDetail('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ChartOfAccountDetail']))
			$model->attributes=$_GET['ChartOfAccountDetail'];

		$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_datetime DESC', 'condition'=>'coad_status=0 AND coad_is_submit=1'));

		$bankAll = Bank::model()->findAll(array('condition'=>'bnk_status=1'));

		$bankSaldoCurrent = ChartOfAccountDetail::model()->findAll(array('group'=>'coad_coa_id','condition'=>'coad_coat_id=:coad_coat_id AND coad_status=1', 'params'=>array(':coad_coat_id'=>8)));
		$bankSaldoCurren = ChartOfAccountDetail::model()->find(array('group'=>'coad_coa_id','condition'=>'coad_coat_id=:coad_coat_id AND coad_status=1', 'params'=>array(':coad_coat_id'=>8)));
		$bankBB = ChartOfAccount::model()->findAll(array('condition'=>'coa_coat_id=:coa_coat_id AND coa_status=1', 'params'=>array(':coa_coat_id'=>8)));

		$getBankId = Bank::model()->findAll(array('condition'=>'bnk_coa_id=:bnk_coa_id AND bnk_status=1', 'params'=>array(':bnk_coa_id'=>$bankSaldoCurren->coad_coa_id)));

		$bniIDR = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC','condition'=>'coad_coa_id=:coad_coa_id AND coad_status=1 AND coad_is_submit=1 AND coad_bb_is_saldo=1', 'params'=>array(':coad_coa_id'=>101)));
		$bniGIRO = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC','condition'=>'coad_coa_id=:coad_coa_id AND coad_status=1 AND coad_is_submit=1', 'params'=>array(':coad_coa_id'=>102)));
		$bniKSO = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC','condition'=>'coad_coa_id=:coad_coa_id AND coad_status=1 AND coad_is_submit=1', 'params'=>array(':coad_coa_id'=>103)));

		$this->render('adminx',array(
			'model'=>$model,
			'bankAll'=>$bankAll,
			'bankBB'=>$bankBB,
			'bankSaldoCurrent'=>$bankSaldoCurrent,
			'getBankId'=>$getBankId,
			'bniIDR'=>$bniIDR,
			'bniGIRO'=>$bniGIRO,
			'bniKSO'=>$bniKSO,
			'coaDetail'=>$coaDetail,
		));
	}

	// public function actionAdminx()
	// {
	// 	if(isset($_POST['ChartOfAccountDetail']['bulan1']))
	// 	{
	// 		if($_POST['ChartOfAccountDetail']['bulan1']!=''){
	// 			$model=new ChartOfAccountDetail('search');
	// 			$model->unsetAttributes();  // clear any default values
	// 			if(isset($_GET['ChartOfAccountDetail']))
	// 				$model->attributes=$_GET['ChartOfAccountDetail'];
	// 			$id = 1;//ngopas dari actionMenu
	// 			$type = 6;//ngopas dari actionMenu
	// 			$model->coad_coa_id=$id;//ngopas dari actionMenu
	// 			$model->coad_coat_id=$type;//ngopas dari actionMenu
	// 			// $model->findByAttributes(array('coa_coat_id'=>$id));
	// 			$coaRule = ChartOfAccountRule::model()->find('coar_status=1');//ngopas dari actionMenu
	// 			$coaMenu = ChartOfAccount::model()->find('coa_id=:coa_id AND coa_status=1', array(':coa_id'=>$id));//ngopas dari actionMenu

	// 			// $coaDetail=ChartOfAccountDetail::model()->findAll('coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=0', array(':coad_coat_id'=>$id, 'coad_coa_id'=>$coaMenu->coa_id));

	// 			$coaDetailfilter = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_datetime ASC, coad_coa_id ASC', 'group'=>'coad_coa_id ASC', 'condition'=>'coad_status=0 AND coad_is_submit=1'));//ngopas dari actionMenu TAPI DIPAKE
	// 			$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_coa_id ASC, coad_datetime DESC', 'condition'=>'coad_status=0 AND coad_is_submit=1'));//ngopas dari actionMenu TAPI DIPAKE
	// 			// foreach($coaDetailfilter as $kambing){
	// 			// // 	// $count = 30;
	// 			// // 	// for($i=0; $i<$count; $i++){
	// 			// 		$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_datetime DESC', 'condition'=>'coad_status=1 AND coad_is_submit=1 AND coad_coa_id=:coad_coa_id', 'params'=>array(':coad_coa_id'=>$kambing->coad_coa_id)));//ngopas dari actionMenu TAPI DIPAKE
	// 			// 		foreach($coaDetail as $key => $value){
	// 			// 			echo '<pre>';
	// 			// 			print_r($value->coad_id);
	// 			// 			echo ' ';
	// 			// 			print_r($value->coad_coa_id);
	// 			// 			echo '<pre>';
	// 			// 			echo '<br/>';
	// 			// 		}
	// 			// 		// echo '<pre>';
	// 			// 		// print_r($kambing->coad_coa_id);
	// 			// 		// echo '<pre>';
	// 			// 	// }
						
					
	// 			// }
				
				
	// 			// die;
	// 			if(empty($coaDetail)){//ngopas dari actionMenu
	// 				// dd('rere');
	// 				$newCoaDetail = new ChartOfAccountDetail;
	// 				$newCoaDetail->coad_coa_id 			= $id;
	// 				$newCoaDetail->coad_coar_id 		= $coaRule->coar_id;
	// 				$newCoaDetail->coad_coat_id 		= $type;
	// 				$newCoaDetail->coad_desc 			= "has been created";
	// 				$newCoaDetail->coad_debet 			= 0;
	// 				$newCoaDetail->coad_credit 			= 0;
	// 				$newCoaDetail->coad_is_saldo 		= 1;
	// 				$newCoaDetail->coad_datetime 		= date('Y-m-d');
	// 				$newCoaDetail->coad_datetime_insert = date('Y-m-d H:i:s');
	// 				if($newCoaDetail->save(false)){
	// 					print_r($newCoaDetail->getErrors());
	// 					// throw new CHttpException('Please try again, You are fine!');
	// 					return $this->redirect(Yii::app()->request->urlReferrer);
	// 				}
	// 			}
	// 			$saldoAkhir = ChartOfAccountDetail::model()->findAll(array('select'=>'coad_id','order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=0', 'params'=>array(':coad_coa_id'=>$id, ':coad_coat_id'=>$type)));//ngopas dari actionMenu
	// 			$coaDetailx = ChartOfAccountDetail::model()->find('coad_coa_id=:coad_coa_id AND coad_status=0', array(':coad_coa_id'=>$id));//ngopas dari actionMenu

	// 			$coaType = ChartOfAccountType::model()->find('coat_id=:coat_id AND coat_status=1', array(':coat_id'=>$type));//ngopas dari actionMenu

	// 			$bankAll = Bank::model()->findAll(array('condition'=>'bnk_status=1'));

	// 			$bankSaldoCurrent = ChartOfAccountDetail::model()->findAll(array('group'=>'coad_coa_id','condition'=>'coad_coat_id=:coad_coat_id AND coad_status=0', 'params'=>array(':coad_coat_id'=>8)));
	// 			$bankSaldoCurren = ChartOfAccountDetail::model()->find(array('group'=>'coad_coa_id','condition'=>'coad_coat_id=:coad_coat_id AND coad_status=0', 'params'=>array(':coad_coat_id'=>8)));
	// 			$bankBB = ChartOfAccount::model()->findAll(array('condition'=>'coa_coat_id=:coa_coat_id AND coa_status=1', 'params'=>array(':coa_coat_id'=>8)));

	// 			$getBankId = Bank::model()->findAll(array('condition'=>'bnk_coa_id=:bnk_coa_id AND bnk_status=1', 'params'=>array(':bnk_coa_id'=>$bankSaldoCurren->coad_coa_id)));

	// 			$bniIDR = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC','condition'=>'coad_coa_id=:coad_coa_id AND coad_status=0 AND coad_is_submit=1 AND coad_bb_is_saldo=1', 'params'=>array(':coad_coa_id'=>101)));
	// 			$bniGIRO = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC','condition'=>'coad_coa_id=:coad_coa_id AND coad_status=0 AND coad_is_submit=1 AND coad_bb_is_saldo=1', 'params'=>array(':coad_coa_id'=>102)));
	// 			$bniKSO = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC','condition'=>'coad_coa_id=:coad_coa_id AND coad_status=0 AND coad_is_submit=1 AND coad_bb_is_saldo=1', 'params'=>array(':coad_coa_id'=>103)));
	// 		}
	// 	}

	// 	$tahun=date('Y');
	// 	$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_coa_id ASC, coad_datetime DESC', 'condition'=>'coad_status=0 AND coad_is_submit=1 AND coad_is_jurnal=1'));


	// 	if(isset($_POST['ChartOfAccountDetail']['coad_datetime'])) //filter tahun
	// 	{	
	// 		if($_POST['ChartOfAccountDetail']['coad_datetime']!=''){
	// 			$tahun= $_POST['ChartOfAccountDetail']['coad_datetime'];
	// 			$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_coa_id ASC, coad_datetime DESC', 'condition'=>'coad_status=0 AND coad_is_submit=1 AND coad_is_jurnal=1 AND DATE_FORMAT (coad_datetime,("%Y"))=:thn', 'params'=>array(':thn'=>$tahun)));
	// 		}
	// 	}

	// 	// $bulan=date('m');
	// 	$bulan='';
	// 	if(isset($_POST['ChartOfAccountDetail']['coad_datetime_insert'])) //filter bulan + tahun
	// 	{	
	// 		if($_POST['ChartOfAccountDetail']['coad_datetime_insert']!=''){
	// 			$bulan= $_POST['ChartOfAccountDetail']['coad_datetime_insert'];
	// 			$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_coa_id ASC, coad_datetime DESC', 'condition'=>'coad_status=0 AND coad_is_submit=1 AND coad_is_jurnal=1 AND DATE_FORMAT (coad_datetime,("%Y"))=:thn AND DATE_FORMAT (coad_datetime,("%c"))=:bln', 'params'=>array(':thn'=>$tahun, ':bln'=>$bulan)));
	// 		}
	// 	}else{
	// 		// $bulan=date('m');
	// 		$bulan='';
	// 	}

	// 	$coa = '';
	// 	if(isset($_POST['ChartOfAccountDetail']['coad_coa_id'])) //filter coa + tahun
	// 	{	
	// 		if($_POST['ChartOfAccountDetail']['coad_coa_id']!=''){
	// 			$coa= $_POST['ChartOfAccountDetail']['coad_coa_id'];
	// 			$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_coa_id ASC, coad_datetime DESC', 'condition'=>'coad_status=0 AND coad_is_submit=1 AND coad_is_jurnal=1 AND DATE_FORMAT (coad_datetime,("%Y"))=:thn AND coad_coa_id=:coad_coa_id', 'params'=>array(':thn'=>$tahun, ':coad_coa_id'=>$coa)));
	// 		}
	// 	}

	// 	if(isset($_POST['ChartOfAccountDetail']['coad_datetime_insert']) && isset($_POST['ChartOfAccountDetail']['coad_coa_id'])) //filter bulan + coa - tahun
	// 	{	
	// 		if($_POST['ChartOfAccountDetail']['coad_datetime_insert']!='' && $_POST['ChartOfAccountDetail']['coad_coa_id']!=''){
	// 			$bulan= $_POST['ChartOfAccountDetail']['coad_datetime_insert'];
	// 			$coa= $_POST['ChartOfAccountDetail']['coad_coa_id'];
	// 			$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_coa_id ASC, coad_datetime DESC', 'condition'=>'coad_status=0 AND coad_is_submit=1 AND coad_is_jurnal=1 AND DATE_FORMAT (coad_datetime,("%c"))=:bln AND coad_coa_id=:coad_coa_id', 'params'=>array(':bln'=>$bulan, ':coad_coa_id'=>$coa)));
	// 		}
	// 	}else{
	// 		// $bulan=date('m');
	// 		$bulan='';
	// 	}

	// 	if(isset($_POST['ChartOfAccountDetail']['coad_datetime_insert']) && isset($_POST['ChartOfAccountDetail']['coad_coa_id']) && isset($_POST['ChartOfAccountDetail']['coad_datetime'])) //filter bulan + coa - tahun
	// 	{	
	// 		if($_POST['ChartOfAccountDetail']['coad_datetime_insert']!='' && $_POST['ChartOfAccountDetail']['coad_coa_id']!=''){
	// 			$tahun= $_POST['ChartOfAccountDetail']['coad_datetime'];
	// 			$bulan= $_POST['ChartOfAccountDetail']['coad_datetime_insert'];
	// 			$coa= $_POST['ChartOfAccountDetail']['coad_coa_id'];
	// 			$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_coa_id ASC, coad_datetime DESC', 'condition'=>'coad_status=0 AND coad_is_submit=1 AND coad_is_jurnal=1 AND DATE_FORMAT (coad_datetime,("%Y"))=:thn AND DATE_FORMAT (coad_datetime,("%c"))=:bln AND coad_coa_id=:coad_coa_id', 'params'=>array(':thn'=>$tahun, ':bln'=>$bulan, ':coad_coa_id'=>$coa)));
	// 		}
	// 	}else{
	// 		// $bulan=date('m');
	// 		$bulan='';
	// 	}

	// 	$coaDetailfilter = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_datetime ASC, coad_coa_id ASC', 'group'=>'coad_coa_id ASC', 'condition'=>'coad_status=0 AND coad_is_submit=1'));//ngopas dari actionMenu TAPI DIPAKE
	// 	// $model=Gaji::model()->findAll('id_pd=:id_pd AND DATE_FORMAT (tgl,("%Y"))=:thn', array(':id_pd'=>$id , ':thn'=>$tahun));

	// 	$bankAll = Bank::model()->findAll(array('condition'=>'bnk_status=1'));
	// 	$bankBB = ChartOfAccount::model()->findAll(array('condition'=>'coa_coat_id=:coa_coat_id AND coa_status=1', 'params'=>array(':coa_coat_id'=>8)));
	// 	$bankSaldoCurrent = ChartOfAccountDetail::model()->findAll(array('group'=>'coad_coa_id','condition'=>'coad_coat_id=:coad_coat_id AND coad_status=0', 'params'=>array(':coad_coat_id'=>8)));
	// 	$bankSaldoCurren = ChartOfAccountDetail::model()->find(array('group'=>'coad_coa_id','condition'=>'coad_coat_id=:coad_coat_id AND coad_status=0', 'params'=>array(':coad_coat_id'=>8)));
	// 	$getBankId = Bank::model()->findAll(array('condition'=>'bnk_coa_id=:bnk_coa_id AND bnk_status=1', 'params'=>array(':bnk_coa_id'=>$bankSaldoCurren->coad_coa_id)));
	// 	$bniIDR = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC','condition'=>'coad_coa_id=:coad_coa_id AND coad_status=0 AND coad_is_submit=1 AND coad_bb_is_saldo=1', 'params'=>array(':coad_coa_id'=>101)));
	// 	$bniGIRO = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC','condition'=>'coad_coa_id=:coad_coa_id AND coad_status=0 AND coad_is_submit=1', 'params'=>array(':coad_coa_id'=>102)));
	// 	$bniKSO = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC','condition'=>'coad_coa_id=:coad_coa_id AND coad_status=0 AND coad_is_submit=1', 'params'=>array(':coad_coa_id'=>103)));
	// 	$modeltahun=new ChartOfAccountDetail;
	// 	$modelbulan=new ChartOfAccountDetail;
	// 	$modelcoa=new ChartOfAccountDetail;
	// 	// echo '<pre>';
	// 	// print_r($bankAll);
	// 	// echo '</pre>';
	// 	// die;

	// 	$this->render('admin',array(
	// 		// 'model'=>$model,
	// 		'bankAll'=>$bankAll,
	// 		'bankBB'=>$bankBB,
	// 		'bankSaldoCurrent'=>$bankSaldoCurrent,
	// 		'getBankId'=>$getBankId,
	// 		'bniIDR'=>$bniIDR,
	// 		'bniGIRO'=>$bniGIRO,
	// 		'bniKSO'=>$bniKSO,
	// 		'tahun'=>$tahun,
	// 		'bulan'=>$bulan,
	// 		'coa'=>$coa,
	// 		'modeltahun'=>$modeltahun,
	// 		'modelbulan'=>$modelbulan,
	// 		'modelcoa'=>$modelcoa,

	// 		//begin ngopas dari actionMenu
	// 		// 'coaMenu'=>$coaMenu,
	// 		// 'coaType'=>$coaType,
	// 		'coaDetail'=>$coaDetail,
	// 		// 'coaDetailfilter'=>$coaDetailfilter,
	// 		// 'coaDetailx'=>$coaDetailx,
	// 		// 'saldoAkhir'=>$saldoAkhir,
	// 		//end ngopas dari actionMenu
	// 	));
	// }

	public function actionJurnal($id, $type)
	{
		$model 	= new ChartOfAccountDetail;
		$model->coad_coa_id=$id;
		$model->coad_coat_id=$type;
		// $model->findByAttributes(array('coa_coat_id'=>$id));
		$coaRule = ChartOfAccountRule::model()->find('coar_status=1');
		$coaMenu = ChartOfAccount::model()->find('coa_id=:coa_id AND coa_status=1', array(':coa_id'=>$id));
		// echo '<pre>';
		// print_r($coaMenu->coa_id);
		// echo '<pre>';
		// die;

		// $coaDetail=ChartOfAccountDetail::model()->findAll('coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1', array(':coad_coat_id'=>$id, 'coad_coa_id'=>$coaMenu->coa_id));

		$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_datetime DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND coad_is_jurnal=1 AND coad_is_submit=0', 'params'=>array(':coad_coa_id'=>$id, ':coad_coat_id'=>$type)));
		// if(empty($coaDetail)){
		// 	// echo '<pre>';
		// 	// print_r('rere');
		// 	// echo '<pre>';
		// 	// die;
		// 	$newCoaDetail = new ChartOfAccountDetail;
		// 	$newCoaDetail->coad_coa_id 			= $id;
		// 	$newCoaDetail->coad_coar_id 		= $coaRule->coar_id;
		// 	$newCoaDetail->coad_coat_id 		= $type;
		// 	$newCoaDetail->coad_desc 			= "has been created";
		// 	$newCoaDetail->coad_debet 			= 0;
		// 	$newCoaDetail->coad_credit 			= 0;
		// 	$newCoaDetail->coad_datetime 		= date('Y-m-d');
		// 	$newCoaDetail->coad_datetime_insert = date('Y-m-d H:i:s');
		// 	if($newCoaDetail->save(false)){
		// 		print_r($newCoaDetail->getErrors());
		// 		// throw new CHttpException('Please try again, You are fine!');
		// 		return $this->redirect(Yii::app()->request->urlReferrer);
		// 	}
		// }
		$saldoAkhir = ChartOfAccountDetail::model()->findAll(array('select'=>'coad_id','order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1', 'params'=>array(':coad_coa_id'=>$id, ':coad_coat_id'=>$type)));
		$coaDetailx = ChartOfAccountDetail::model()->find('coad_coa_id=:coad_coa_id AND coad_status=1', array(':coad_coa_id'=>$id));

		$coaType = ChartOfAccountType::model()->find('coat_id=:coat_id AND coat_status=1', array(':coat_id'=>$type));
		

		if(isset($_POST['ChartOfAccount']['tahun']))
		{	
			if($_POST['ChartOfAccount']['tahun']!=''){
			$tahun= $_POST['ChartOfAccount']['tahun'];
			}
			else{
			$tahun = date('Y');
			}
		}
		else{
		$tahun = date('Y');
		}

		$this->render('jurnal',array(
			'model'=>$model,
			'coaRule'=>$coaRule,
			'coaMenu'=>$coaMenu,
			'coaType'=>$coaType,
			'coaDetail'=>$coaDetail,
			'coaDetailx'=>$coaDetailx,
			'saldoAkhir'=>$saldoAkhir,
			'tahun'=>$tahun,
			'title'=>'J U R N A L'
		));
	}

	public function actionMenu($id, $type) //pas di menu emg iya ngecreate defaut
	{
		$model 	= new ChartOfAccountDetail;
		$model->coad_coa_id=$id;
		$model->coad_coat_id=$type;
		// $model->findByAttributes(array('coa_coat_id'=>$id));
		$coaRule = ChartOfAccountRule::model()->find('coar_status=1');
		$coaMenu = ChartOfAccount::model()->find('coa_id=:coa_id AND coa_status=1', array(':coa_id'=>$id));
		// echo '<pre>';
		// print_r($coaMenu->coa_id);
		// echo '<pre>';
		// die;

		// $coaDetail=ChartOfAccountDetail::model()->findAll('coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1', array(':coad_coat_id'=>$id, 'coad_coa_id'=>$coaMenu->coa_id));

		$coaDetail = ChartOfAccountDetail::model()->findAll(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1', 'params'=>array(':coad_coa_id'=>$id, ':coad_coat_id'=>$type)));
		if(empty($coaDetail)){
			// echo '<pre>';
			// print_r('rere');
			// echo '<pre>';
			// die;
			$newCoaDetail = new ChartOfAccountDetail;
			$newCoaDetail->coad_coa_id 			= $id;
			$newCoaDetail->coad_coar_id 		= $coaRule->coar_id;
			$newCoaDetail->coad_coat_id 		= $type;
			$newCoaDetail->coad_desc 			= "has been created";
			$newCoaDetail->coad_debet 			= 0;
			$newCoaDetail->coad_credit 			= 0;
			$newCoaDetail->coad_is_saldo 		= 1;
			$newCoaDetail->coad_datetime 		= date('Y-m-d');
			$newCoaDetail->coad_datetime_insert = date('Y-m-d H:i:s');
			if($newCoaDetail->save(false)){
				print_r($newCoaDetail->getErrors());
				// throw new CHttpException('Please try again, You are fine!');
				return $this->redirect(Yii::app()->request->urlReferrer);
			}
		}
		$saldoAkhir = ChartOfAccountDetail::model()->findAll(array('select'=>'coad_id','order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1', 'params'=>array(':coad_coa_id'=>$id, ':coad_coat_id'=>$type)));
		$coaDetailx = ChartOfAccountDetail::model()->find('coad_coa_id=:coad_coa_id AND coad_status=1', array(':coad_coa_id'=>$id));

		$coaType = ChartOfAccountType::model()->find('coat_id=:coat_id AND coat_status=1', array(':coat_id'=>$type));
		

		if(isset($_POST['ChartOfAccount']['tahun']))
		{	
			if($_POST['ChartOfAccount']['tahun']!=''){
			$tahun= $_POST['ChartOfAccount']['tahun'];
			}
			else{
			$tahun = date('Y');
			}
		}
		else{
		$tahun = date('Y');
		}

		$this->render('menu',array(
			'model'=>$model,
			'coaMenu'=>$coaMenu,
			'coaType'=>$coaType,
			'coaDetail'=>$coaDetail,
			'coaDetailx'=>$coaDetailx,
			'saldoAkhir'=>$saldoAkhir,
			'tahun'=>$tahun,
		));
	}

	public function actionCheckSaldo($id, $coaId, $prevId, $type)
	{
		// $num = 0;
		// for ($i=0;$i<100000;$i++){
		// 	$num .= $i;
		// }
		// $before = ($id-$num);
		// $before = ($id-1);

		
		// $before = ($id-2);
		// $before = ($id-3);
		// $before = ($id-4);
		// $before = ($id-5);
		// $model = ChartOfAccountDetail::model()->findByAttributes(array('coad_id'=>$id));
		// $modelx = ChartOfAccountDetail::model()->findByAttributes(array('coad_id'=>$id-1));
		$coad = ChartOfAccountDetail::model()->find('coad_id=:coad_id AND coad_coa_id=:coad_coa_id AND coad_status=1', array(':coad_id'=>$id, ':coad_coa_id'=>$coaId));

		// $virt = sprintf("%03d", $coad->coad_coat_id);
		// // $virt .= 1;
		// // $virt .= 2;
		// $virt .= 7;
		// $virt = $virt.$coad->coad_coa_id;
		// $virt .= 7;
		// $virt .= $id;
		$coad->coad_current_saldo = $prevId;
		$coad->SaveAttributes(array('coad_current_saldo'));
		// echo '<pre>';
		// echo '</br>';
		// print_r($coad->coad_current_saldo);
		// echo '</br>';
		// // print_r($virt);
		// echo '</pre>';
		// die;
		// if($coad->SaveAttributes(array('coad_current_saldo'))){
		// 	return $this->redirect(Yii::app()->request->urlReferrer);
		// }else{
		// 	print_r($coad->getErrors());
		// 	throw new CHttpException('CNTRL 0 :' ,'ERROR CURRENT SALDO');
		// }

		$this->actionAigo($id, $coaId, $prevId, $type);
	}

	public function actionAigo($id, $coaId, $prevId, $type)
	{
		
		$logAction = 'check-saldo';
		$coad = ChartOfAccountDetail::model()->find('coad_id=:coad_id AND coad_coa_id=:coad_coa_id AND coad_status=1', array(':coad_id'=>$id, ':coad_coa_id'=>$coaId));
		// $now = $coad->coad_current_saldo;
		$coad->coad_current_saldo = $prevId;
		$before = $coad->coad_current_saldo;
		// $before = ($coad->coad_current_saldo)-$prevId;
		// echo '<pre>';
		// echo '</br>';
		// print_r($before);
		// echo '<br>';
		// print_r($coad->coad_current_saldo);
		// echo '<br>';
		// print_r($coad->coad_saldo);
		// echo '<br>';
		// print_r($coad->coad_debet);
		// echo '<br>';
		// echo '</pre>';
		// die;
		// $coadx=ChartOfAccountDetail::model()->find('coad_status=1 AND coad_current_saldo=:before', array(':before'=>$now));
		// $coadz=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_current_saldo>7730178 AND coad_current_saldo<=77301711 AND coad_status=1'));
		// echo '<pre>';
		// print_r($coadz->coad_id);
		// echo '</pre>';
		// die;
		$coadx = ChartOfAccountDetail::model()->find(array('condition'=>'coad_id=:before AND coad_status=1', 'limit'=>'1', 'params'=>array(':before'=>$before)));
		// $saldoAkhir=ChartOfAccountDetail::model()->find(array('select'=>'coad_id', 'condition'=>'coad_id=:before AND coad_status=1', 'limit'=>'1', 'params'=>array(':before'=>$before)));
		// if(!empty($coadx)){
		// 	echo '<pre>';
		// 	print_r('rere');
		// 	echo '</pre>';
		// }
		// echo '<pre>';
		// print_r(current($coadx));
		// echo '</pre>';
		// die;

		if($coadx->coad_coa_id == $coaId){
			$coad->coad_saldo = ($coadx->coad_saldo + $coad->coad_debet) - $coad->coad_credit;
		}else{
			$coad->coad_saldo = $coad->coad_debet - $coad->coad_credit;
			// $coad->coad_saldo = ($coadx->coad_saldo + $coad->coad_debet) - $coad->coad_credit;
		}
		// echo '<pre>';
		// print_r('coad_current_saldo '.$coad->coad_current_saldo);
		// echo '<br>';
		// print_r('id '.intval($id));
		// echo '<br>';
		// echo '<pre>';
		// // print_r('now '.$now);
		// echo '<br>';
		// print_r('before '.$before);
		// echo '<br>';
		// echo '</br>';
		// print_r($coadx->coad_id);
		// echo '<br>';
		// print_r($coadx->coad_current_saldo);
		// echo '<br>';
		// print_r($coadx->coad_saldo);
		// echo '<br>';
		// print_r($coadx->coad_debet);
		// echo '<br>';
		// // print_r($coaId);
		
		// echo '</br>';
		// print_r($coad->coad_id);
		// echo '<br>';
		// print_r($coad->coad_current_saldo);
		// echo '<br>';
		// print_r($coad->coad_saldo);
		// echo '<br>';
		// print_r($coad->coad_debet);
		// echo '<br>';
		// echo '</pre>';
		// die;
		if($coad->coad_is_saldo == 0){
			$coad->coad_is_saldo = 1;
		}else{
			$coad->coad_is_saldo = $coad->coad_is_saldo+1;
		}

		if($type == 8) {
			$id_pd=Yii::app()->session->get('username');
			// echo '<pre>';
			// print_r('rerexx');
			// echo '</pre>';
			// die;
			// $saveBankLog = Yii::app()->urlManager->createUrl('operator/bankLog/saveLog');
			// print_r($saveBankLog);die;
			// return $saveBankLog;
			
			$bank = Bank::model()->find(array('condition'=>'bnk_coa_id=:bnk_coa_id AND bnk_status=1', 'params'=>array(':bnk_coa_id'=>$coaId)));
			if($coaId == 101) {
				$noCoaBNIIDR 			= 101;

				$BNIIDR = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBNIIDR, ':coad_coat_id'=>$type)));

				if(!empty($BNIIDR)) {
					if($bank->bnk_saldo_current !== $BNIIDR->coad_saldo) {
						$bank->bnk_saldo_current = $BNIIDR->coad_saldo;
						// echo '<pre>';
						// print_r($bank->bnk_saldo_current);
						// echo '</pre>';
						// die;
						// $this->redirect(array('bankLog/saveLog/bnkId/'.$bank->bnk_id.'/coaId/'.$noCoaBNIIDR.'/type/'.$type.'/idPd/'.$id_pd.'/log/'.$logAction.'/current/'.$bank->bnk_saldo_current));
							// echo '<pre>';
							// print_r('kreredo');
							// echo '</pre>';
							// die;
						if($bank->SaveAttributes(array('bnk_saldo_current'))) {
							// $saveBankLog = BankLog::actionSaveLog();
							// echo '<pre>';
							// print_r('rerexx');
							// echo '</pre>';
							// die;
							$return = true;
						}else{
							$return = false;
							print_r($bank->getErrors());
							throw new CHttpException('CNTRL 2 :' ,'error generate BNIIDR');
						}
					}else{
						$return = true;
					}
					$saveLog = new BankLog;
					$saveLog->bnkl_bnk_id 			= $bank->bnk_id;
					$saveLog->bnkl_coa_id 			= $noCoaBNIIDR;
					$saveLog->bnkl_coat_id 			= $type;
					$saveLog->bnkl_id_pd 			= $id_pd;
					$saveLog->bnkl_log 				= $logAction;
					$saveLog->bnkl_saldo_current 	= $bank->bnk_saldo_current;
					$saveLog->bnkl_datetime_insert 	= date('Y-m-d H:i:s');
					$saveLog->save();
				}else{
					$return = false;
					print_r($bank->getErrors());
					throw new CHttpException('CNTRL 1 :' ,'BNIIDR is empty');
				}
			}

			if($coaId == 102) {
				$noCoaBNIGIRO 			= 102;

				$BNIGIRO = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBNIGIRO, ':coad_coat_id'=>$type)));

				if(!empty($BNIGIRO)) {
					if($bank->bnk_saldo_current !== $BNIGIRO->coad_saldo) {
						$bank->bnk_saldo_current = $BNIGIRO->coad_saldo;
						if($bank->SaveAttributes(array('bnk_saldo_current'))) {
							$return = true;
						}else{
							$return = false;
							print_r($bank->getErrors());
							throw new CHttpException('CNTRL 2 :' ,'error generate BNIGIRO');
						}
					}else{
						$return = true;
					}
					$saveLog = new BankLog;
					$saveLog->bnkl_bnk_id 			= $bank->bnk_id;
					$saveLog->bnkl_coa_id 			= $noCoaBNIGIRO;
					$saveLog->bnkl_coat_id 			= $type;
					$saveLog->bnkl_id_pd 			= $id_pd;
					$saveLog->bnkl_log 				= $logAction;
					$saveLog->bnkl_saldo_current 	= $bank->bnk_saldo_current;
					$saveLog->bnkl_datetime_insert 	= date('Y-m-d H:i:s');
					$saveLog->save();
				}else{
					$return = false;
					print_r($bank->getErrors());
					throw new CHttpException('CNTRL 1 :' ,'BNIGIRO is empty');
				}
			}

			if($coaId == 103) {
				$noCoaBNIKSO 			= 103;

				$BNIKSO = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBNIKSO, ':coad_coat_id'=>$type)));

				if(!empty($BNIKSO)) {
					if($bank->bnk_saldo_current !== $BNIKSO->coad_saldo) {
						$bank->bnk_saldo_current = $BNIKSO->coad_saldo;
						if($bank->SaveAttributes(array('bnk_saldo_current'))) {
							$return = true;
						}else{
							$return = false;
							print_r($bank->getErrors());
							throw new CHttpException('CNTRL 2 :' ,'error generate BNIKSO');
						}
					}else{
						$return = true;
					}
					$saveLog = new BankLog;
					$saveLog->bnkl_bnk_id 			= $bank->bnk_id;
					$saveLog->bnkl_coa_id 			= $noCoaBNIKSO;
					$saveLog->bnkl_coat_id 			= $type;
					$saveLog->bnkl_id_pd 			= $id_pd;
					$saveLog->bnkl_log 				= $logAction;
					$saveLog->bnkl_saldo_current 	= $bank->bnk_saldo_current;
					$saveLog->bnkl_datetime_insert 	= date('Y-m-d H:i:s');
					$saveLog->save();
				}else{
					$return = false;
					print_r($bank->getErrors());
					throw new CHttpException('CNTRL 1 :' ,'BNIKSO is empty');
				}
			}
			


		}
		
		if($coad->SaveAttributes(array('coad_is_saldo','coad_saldo'))) {

			return $this->redirect(Yii::app()->request->urlReferrer);
			
		}else{
			print_r($coad->getErrors());
			throw new CHttpException('CNTRL 0 :' ,'ERROR CHECK SALDO');
		}

	}

	public function actionSubmit($rule, $type, $coaId)
	{
		// echo '<pre>';
		// print_r($rule); echo '<br/>';
		// print_r($type); echo '<br/>';
		// print_r($coaId);
		// echo '</pre>';
		// die;
		// $coad = ChartOfAccountDetail::model()->findAll('coad_coar_id=:coad_coar_id AND coad_coat_id=:coad_coat_id AND coad_coa_id=:coad_coa_id AND coad_status=1 AND coad_is_jurnal=1', array(':coad_coar_id'=>$rule, ':coad_coat_id'=>$type, ':coad_coa_id'=>$coaId));
		// ChartOfAccountDetail::model()->updateAll(array('coad_is_submit' => 77));
		// ChartOfAccountDetail::model()->updateAll(array('coad_is_submit' => 9, 'coad_is_jurnal=1'));
		ChartOfAccountDetail::model()->updateAll(array( 'coad_is_submit' => 1), 'coad_coar_id ='.$rule.' AND coad_coat_id ='.$type.' AND coad_coa_id ='.$coaId.' AND coad_is_jurnal = 1');
		// echo '<pre>';
		// print_r($coad);
		// echo '</pre>';
		// die;
		// $coad->coad_is_submit = 1;
		// if($coad->SaveAttributes(array('coad_is_submit'))){
		return $this->redirect(Yii::app()->request->urlReferrer);
		// }else{
		// 	print_r($coad->getErrors());
		// 	throw new CHttpException('CNTRL 0 :' ,'ERROR SUBMIT TO LEDGER');
		// }

		// echo 'rere';
	}

	public function actionSubmitJurnal($id)
	{
		
		$coad = ChartOfAccountDetail::model()->find('coad_id=:coad_id AND coad_status=1', array(':coad_id'=>$id));
		if($coad->coad_is_jurnal == 0){
			$coad->coad_is_jurnal = 1;
			if($coad->SaveAttributes(array('coad_is_jurnal'))){
				return $this->redirect(Yii::app()->request->urlReferrer);
			}else{
				print_r($coad->getErrors());
				throw new CHttpException('CNTRL 0 :' ,'ERROR SUBMIT TO JURNAL');
			}
		}else{
			Yii::app()->user->setFlash('notice', "Already submitted!");
			foreach(Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
            }
		}
		
		
	}

	public function actionUnsubmitJurnal($id)
	{
		
		$coad = ChartOfAccountDetail::model()->find('coad_id=:coad_id AND coad_status=1', array(':coad_id'=>$id));
		if($coad->coad_is_jurnal > 0){
			$coad->coad_is_jurnal = 0;
			if($coad->SaveAttributes(array('coad_is_jurnal'))){
				return $this->redirect(Yii::app()->request->urlReferrer);
			}else{
				print_r($coad->getErrors());
				throw new CHttpException('CNTRL 0 :' ,'ERROR UNSUBMIT TO JURNAL');
			}
		}else{
			Yii::app()->user->setFlash('notice', "Already unsubmitted!");
			foreach(Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
            }
		}
		
		
	}

	public function actionChangeColor($id)
	{
		
		$coad = ChartOfAccountDetail::model()->find('coad_id=:coad_id AND coad_status=1', array(':coad_id'=>$id));
		$coad->coad_color = " ";
		if($coad->SaveAttributes(array('coad_color'))){
		return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			print_r($coad->getErrors());
			throw new CHttpException('CNTRL 0 :' ,'ERROR CHANGE COLOR');
		}
	}

	public function bulan($i)
	{
		switch($i)
		{
			case 1:
			$bulan="Januari";
			break;
			
			case 2:
			$bulan="Februari";
			break;
			
			case 3:
			$bulan="Maret";
			break;
			
			case 4:
			$bulan="April";
			break;
			
			case 5:
			$bulan="Mei";
			break;
			
			case 6:
			$bulan="Juni";
			break;
			
			case 7:
			$bulan="Juli";
			break;
			
			case 8:
			$bulan="Agustus";
			break;
			
			case 9:
			$bulan="September";
			break;
			
			case 10:
			$bulan="Oktober";
			break;
			
			case 11:
			$bulan="November";
			break;
			
			case 12:
			$bulan="Desember";
			break;
		}
			return $bulan;
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ChartOfAccountDetail the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ChartOfAccountDetail::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ChartOfAccountDetail $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='chart-of-account-detail-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
