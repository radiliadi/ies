<?php

class AgamaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('exportExcel','admin','delete','create','update','updateStatus'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Agama;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Agama']))
		{
			$model->attributes=$_POST['Agama'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Agama']))
		{
			$model->attributes=$_POST['Agama'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Agama');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Agama('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Agama']))
			$model->attributes=$_GET['Agama'];

		$this->render('admin',array(
			'title'=>'Religion',
			'model'=>$model,
		));
	}

	public function actionExportExcel()
	{ 	
		if(isset($_POST['fileType']) && $_POST['Agama']){
			$model = new Agama();
			$model->attributes = $_POST['Agama'];
			$date = date('Y-m-d H:i:s');
			if($_POST['fileType']== "Excel"){
				$this->widget('ext.EExcelView', array(
					'title'=>'Daftar Religion',
					'filename'=>'Religion - '.$date,
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->search(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'Excel2007',
					'columns' => array(
						'agm_id',
						'agm_desc',
					),
				));
			} 
			$date = date('dMY');
			if($_POST['fileType']== "CSV"){
				$this->widget('ext.tcPDF', array(
					'title'=>'Daftar Religion',
					'filename'=>'Religion'.$date,
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->search(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'CSV',
					'columns' => array(
						'agm_id',
						'agm_desc',
					),
				));
			}
		}
	}

	public function actionUpdateStatus(){
        $postId	   	= Yii::app()->request->getPost('id');
        $return 	= false;
        $msg    	= 'Success';
        $error  	= (object)[];
        $data   	= (object)[];
        $id_pd 		= Yii::app()->session->get('username');
        
        if(!empty($postId)){
            $checkAgama = Agama::model()->find('agm_id=:agm_id', array(':agm_id' => $postId));
            if(!empty($checkAgama)){
            	if($checkAgama->agm_status == 1){
            		$changeStatus = 0;
            	}else if($checkAgama->agm_status !== 1 ){
            		$changeStatus = 1;
            	}
            	$checkAgama->agm_datetime_delete = date('Y-m-d H:i:s');
            	$checkAgama->agm_delete_by = $id_pd;
                $checkAgama->agm_status = $changeStatus;
                if($checkAgama->save()){
                    $return = true;
                    $msg = "Succes";
                }else{
                	print_r($checkAgama->getErrors());
                	$return = false;
                    $msg = "Failed to save data";
                }
            }else{
                $msg = "Data is Empty";
            }
        }
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return CJSON::encode($result);
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Agama the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Agama::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Agama $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='agama-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
