<?php

class AccountingController extends Controller
{
	
	
	public function  actionIndex()
	{
		$coaTypeCommon = ChartOfAccountType::model()->findAll(array('order'=>'coat_id ASC', 'condition'=>'coat_coac_id=:coatCoac AND coat_status=1', 'params'=>array(':coatCoac'=>1)));

		$coaTypeCustom = ChartOfAccountType::model()->findAll(array('order'=>'coat_id ASC', 'condition'=>'coat_coac_id=:coatCoac AND coat_status=1', 'params'=>array(':coatCoac'=>2)));

		$coaRule = ChartOfAccountRule::model()->find(array('order'=>'coar_id ASC', 'condition'=>'coar_status=1'));

		$countCoad = ChartOfAccountDetail::model()->findAll(array('select'=>'coad_id', 'condition'=>'coad_status=1'));

		$criteria=new CDbCriteria;
		$criteria->select = "count(coa_id) as coa_id";
		$criteria->addCondition("coa_status = 1");
		$countCoa=ChartOfAccount::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->select = "count(coad_id) as coad_id";
		$criteria->addCondition("coad_status = 1 AND coad_is_submit = 1 AND coad_is_jurnal=1 AND coad_is_saldo != 0");
		$bukuBesar=ChartOfAccountDetail::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->select = "count(coad_id) as coad_id";
		$criteria->addCondition("coad_status = 0 AND coad_is_submit = 1 AND coad_is_jurnal=1 AND coad_is_saldo != 0");
		$bukuBesarInactive=ChartOfAccountDetail::model()->find($criteria);

		$this->render('index',array(
			'coaTypeCommon'=>$coaTypeCommon,
			'coaTypeCustom'=>$coaTypeCustom,
			'countCoa'=>$countCoa,
			'coaRule'=>$coaRule,
			'bukuBesar'=>$bukuBesar,
			'bukuBesarInactive'=>$bukuBesarInactive,
			'countCoad'=>$countCoad,
		));
	}
	
	//Hitung hari pembayaran kantor di laporan
	public function timeDiff($firstTime,$lastTime){
	   // convert to unix timestamps
	   $firstTime=strtotime($firstTime);
	   $lastTime=strtotime($lastTime);

	   // perform subtraction to get the difference (in seconds) between times
	   //hari
	   $timeDiff=($lastTime-$firstTime)/(3600*24);
	   //bulan
	   if($timeDiff>30):
			$total=$timeDiff;
			$bulan=number_format($timeDiff/30);
			$hari=$timeDiff%30;
			$timeDiff=$bulan.' bulan, '.$hari.' hari ('.$total.')';
		else:
			$timeDiff.='';
	   endif;

	   // return the difference
	   return $timeDiff;
	}
}

