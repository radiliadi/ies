<?php

class MedicalClaimController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('update','admin','updateSaldo'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MedicalClaim;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MedicalClaim']))
		{
			$model->attributes=$_POST['MedicalClaim'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->mc_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionExportExcel()
	{ 	
		if(isset($_POST['fileType']) && $_POST['MedicalClaim']){
			$model = new MedicalClaim();
			$model->attributes = $_POST['MedicalClaim'];
			$date = date('Y-m-d H:i:s');
			if($_POST['fileType']== "Excel"){
				$this->widget('ext.EExcelView', array(
					'title'=>'Daftar MedicalClaim',
					'filename'=>'MedicalClaim - '.$date,
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->search(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'Excel2007',
					'columns' => array(
						'mc_id_pd',
						'mc_bpjs_kete',
						'mc_bpjs_kese',
						'mc_bpjs_pensi',
						'mc_tunj_kese',
					),
				));
			} 
			$date = date('dMY');
			if($_POST['fileType']== "CSV"){
				$this->widget('ext.tcPDF', array(
					'title'=>'Daftar MedicalClaim',
					'filename'=>'MedicalClaim'.$date,
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->search(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'CSV',
					'columns' => array(
						'mc_id_pd',
						'mc_bpjs_kete',
						'mc_bpjs_kese',
						'mc_bpjs_pensi',
						'mc_tunj_kese',
					),
				));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MedicalClaim']))
		{
			$model->attributes=$_POST['MedicalClaim'];
			if($model->save())
				$this->redirect(array('admin'));
		}else{
			// print_r($model->getErrors());
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionUpdateSaldo($id)
	{
		$model = MedicalClaim::model()->findByAttributes(array('mc_id'=>$id));
		$model->mc_tunj_current = 0;
		$model->mc_tunj_current = $model->mc_tunj_current + 3000000;
		if($model->SaveAttributes(array('mc_tunj_current'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			echo 'CNTRL 0 : SALDO REACH MAX TOPUP (Rp. 3.000.000) / <small>ERROR UPDATE SALDO MEDICAL CLAIM</small>';
		}
		// return $this->redirect(Yii::app()->request->urlReferrer);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('MedicalClaim');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MedicalClaim('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MedicalClaim']))
			$model->attributes=$_GET['MedicalClaim'];

		$this->render('admin',array(
			'title'=>'Medical Claim',
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MedicalClaim the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MedicalClaim::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MedicalClaim $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='medical-claim-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
