<?php

class LabaDetailController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','generate','lock','unlock','print'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new LabaDetail;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['LabaDetail']))
		{
			$model->attributes=$_POST['LabaDetail'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->lbd_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		// $model->lbd_pendapatan = number_format($model->lbd_pendapatan);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['LabaDetail']))
		{
			$model->attributes=$_POST['LabaDetail'];
			if($model->save())
				$this->redirect(array('admin','id'=>$model->lbd_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('LabaDetail');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id)
	{
		$model=new LabaDetail;
		// $id=1;
		$model->lbd_lb_id=$id;
		// $modelkaryawan=Karyawan::model()->find('id_pd=:id_pd', array(':id_pd'=>'SEA-001-ADI'));
		$labaDetailId=LabaDetail::model()->find('lbd_lb_id=:lbd_lb_id AND lbd_status=1', array(':lbd_lb_id'=>$id));
		$labaDetail=LabaDetail::model()->findAll('lbd_lb_id=:lbd_lb_id AND lbd_status=1', array(':lbd_lb_id'=>$id));
		// $karyawan=Karyawan::model()->find('id_pd=:id_pd', array(':id_pd'=>$expensesDetailId->expd_id_pd));
		$laba=Laba::model()->findByAttributes(array('lb_id'=>$id, 'lb_status'=>1));
		$model->findByAttributes(array('lbd_lb_id'=>$id));
		// echo '<pre>';
		// // print_r($modelkaryawan->id_pd);
		// print_r($modeled->expd_exp_id).'</br>';
		// echo '<br>';
		// print_r($modeled->expd_id_pd).'</br>';
		// echo '</pre>';
		// die;
		// $model->findAll(array('condition'=>'expd_status = 2'));
		// $model->expd_exp_id=$id;
		// $modeled->unsetAttributes();  // clear any default values
		// if(isset($_GET['ExpensesDetail']))
		// 	$modeled->attributes=$_GET['ExpensesDetail'];


		$this->render('admin',array(
			'model'=>$model,
			'labaDetail'=>$labaDetail,
			'labaDetailId'=>$labaDetailId,
			'laba'=>$laba,
			// 'karyawan'=>$karyawan,
		));
	}

	public function actionGenerate($id, $date)
	{
		// $post = Yii::app()->request->getPost();
		// echo '<pre>';
		// print_r($post->id);
		// echo '</pre>';
		// die;

		$labaDetailId=LabaDetail::model()->find('lbd_lb_id=:lbd_lb_id AND lbd_status=1', array(':lbd_lb_id'=>$id));

		$labaDetailId->lbd_is_generate = $labaDetailId->lbd_is_generate+1;
		$labaDetailId->SaveAttributes(array('lbd_is_generate'));
		// if($labaDetailId->SaveAttributes(array('lbd_is_generate'))){
		// 	return $this->redirect(Yii::app()->request->urlReferrer);
		// }else{
		// 	print_r($labaDetailId->getErrors());
		// 	throw new CHttpException('CNTRL 0 :' ,'IS GENERATE DIDNT SAVE');
		// }
		$this->actionGenerateDetail($id, $date);
	}

	public function actionGenerateDetail($id, $date)
	{
		$return = false;
		// $date="2016-08-08";
		$time=strtotime($date);
		$year=date("Y", $time);
		$noCoaBebanTransaksi = 301;
		$noTypeBebanTransaksi = 7;
		$noCoaBiayaGaji = 311;
		$noTypeBiayaGaji = 7;
		$noCoaBebanLembur = 312;
		$noTypeBebanLembur = 7;
		$noCoaBebanPengobatan = 313;
		$noTypeBebanPengobatan = 7;
		$noCoaBpjsTkk = 314;
		$noTypeBpjsTkk = 7;
		$noCoaBiySewaKantor = 315;
		$noTypeBiySewaKantor = 7;
		$noCoaBebanAtk = 316;
		$noTypeBebanAtk = 7;
		$noCoaBiyTfi = 317;
		$noTypeBiyTfi = 7;
		$noCoaBebanParkir = 318;
		$noTypeBebanParkir = 7;
		$noCoaBebanListrik = 318;
		$noTypeBebanListrik = 7;
		$noCoaBpBk = 319;
		$noTypeBpBk = 7;
		$noCoaBebanTrans = 320;
		$noTypeBebanTrans = 7;
		$noCoaBebanEntertain = 321;
		$noTypeBebanEntertain = 7;
		$noCoaKRT = 322;
		$noTypeKRT = 7;
		$noCoaBiyPS = 323;
		$noTypeBiyPS = 7;
		$noCoaBiyAK = 324;
		$noTypeBiyAK = 7;
		$noCoaBppKantor = 325;
		$noTypeBppKantor = 7;
		$noCoaBppKendaraan = 326;
		$noTypeBppKendaraan = 7;
		$noCoaBppPeralatanKantor = 333;
		$noTypeBppPeralatanKantor = 7;
		$noCoaBiyAdmTndr = 327;
		$noTypeBiyAdmTndr = 7;
		$noCoaBiyAdmBank = 328;
		$noTypeBiyAdmBank = 7;
		$noCoaBebanAskes = 330;
		$noTypeBebanAskes = 7;
		$noCoaBebanDinas = 331;
		$noTypeBebanDinas = 7;
		$noCoaBebanLl = 329;
		$noTypeBebanLl = 7;

		$noCoaPll = 402;
		$noTypePll = 14;
		$noCoaJasaGiro = 403;
		$noTypeJasaGiro = 14;


		if(!empty($year)){
			$bebanTransaksi=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBebanTransaksi, ':coad_coat_id'=>$noTypeBebanTransaksi, 'coad_datetime'=>$year)));
			$biayaGaji=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBiayaGaji, ':coad_coat_id'=>$noTypeBiayaGaji, 'coad_datetime'=>$year)));
			// echo '<pre>';
			// print_r($biayaGaji);
			// echo '</pre>';
			// die;
			$bebanLembur=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBebanLembur, ':coad_coat_id'=>$noTypeBebanLembur, 'coad_datetime'=>$year)));
			// echo '<pre>';
			// print_r($bebanLembur);
			// echo '</pre>';
			// die;
			$bebanPengobatan=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBebanPengobatan, ':coad_coat_id'=>$noTypeBebanPengobatan, 'coad_datetime'=>$year)));
			$bpjsTkk=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBpjsTkk, ':coad_coat_id'=>$noTypeBpjsTkk, 'coad_datetime'=>$year)));
			$biySewaKantor=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBiySewaKantor, ':coad_coat_id'=>$noTypeBiySewaKantor, 'coad_datetime'=>$year)));
			$bebanAtk=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBebanAtk, ':coad_coat_id'=>$noTypeBebanAtk, 'coad_datetime'=>$year)));
			$biyTfi=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBiyTfi, ':coad_coat_id'=>$noTypeBiyTfi, 'coad_datetime'=>$year)));
			$bebanParkir=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBebanParkir, ':coad_coat_id'=>$noTypeBebanParkir, 'coad_datetime'=>$year)));
			$bebanListrik=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBebanListrik, ':coad_coat_id'=>$noTypeBebanListrik, 'coad_datetime'=>$year)));
			$bpBk=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBpBk, ':coad_coat_id'=>$noTypeBpBk, 'coad_datetime'=>$year)));
			$bebanTrans=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBebanTrans, ':coad_coat_id'=>$noTypeBebanTrans, 'coad_datetime'=>$year)));
			$bebanEntertain=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBebanEntertain, ':coad_coat_id'=>$noTypeBebanEntertain, 'coad_datetime'=>$year)));
			$KRT=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaKRT, ':coad_coat_id'=>$noTypeKRT, 'coad_datetime'=>$year)));
			$biyPS=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBiyPS, ':coad_coat_id'=>$noTypeBiyPS, 'coad_datetime'=>$year)));
			$biyAK=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBiyAK, ':coad_coat_id'=>$noTypeBiyAK, 'coad_datetime'=>$year)));
			$bppKantor=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBppKantor, ':coad_coat_id'=>$noTypeBppKantor, 'coad_datetime'=>$year)));
			$bppKendaraan=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBppKendaraan, ':coad_coat_id'=>$noTypeBppKendaraan, 'coad_datetime'=>$year)));
			$bppPeralatanKantor=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBppPeralatanKantor, ':coad_coat_id'=>$noTypeBppPeralatanKantor, 'coad_datetime'=>$year)));
			$biyAdmTndr=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBiyAdmTndr, ':coad_coat_id'=>$noTypeBiyAdmTndr, 'coad_datetime'=>$year)));
			$biyAdmBank=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBiyAdmBank, ':coad_coat_id'=>$noTypeBiyAdmBank, 'coad_datetime'=>$year)));
			$bebanAskes=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBebanAskes, ':coad_coat_id'=>$noTypeBebanAskes, 'coad_datetime'=>$year)));
			$bebanDinas=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBebanDinas, ':coad_coat_id'=>$noTypeBebanDinas, 'coad_datetime'=>$year)));
			$bebanLl=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBebanLl, ':coad_coat_id'=>$noTypeBebanLl, 'coad_datetime'=>$year)));
			// echo '<pre>';
			// print_r($biayaGaji->coad_saldo);
			// echo '</pre>';
			// die;
			$Pll=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaPll, ':coad_coat_id'=>$noTypePll, 'coad_datetime'=>$year)));
			$JasaGiro=ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaJasaGiro, ':coad_coat_id'=>$noTypeJasaGiro, 'coad_datetime'=>$year)));
			// echo '<pre>';
			// print_r($JasaGiro->coad_saldo);
			// echo '</pre>';
			// die;
			$labaDetailId=LabaDetail::model()->find('lbd_lb_id=:lbd_lb_id AND lbd_status=1', array(':lbd_lb_id'=>$id));
			if(!empty($bebanTransaksi)){
				if($labaDetailId->lbd_bbn_transaksi !== $bebanTransaksi->coad_saldo){
					$labaDetailId->lbd_bbn_transaksi = $bebanTransaksi->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bbn_transaksi'))){
						$return = true;
						// return $this->redirect(Yii::app()->request->urlReferrer);
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bebanTransaksi');
					}
				}else{
					$return = true;
					// return $this->redirect(Yii::app()->request->urlReferrer);
				}
			}else{
				$return = false;
				print_r($bebanTransaksi->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bebanTransaksi is empty');
			}
			if(!empty($biayaGaji)){
				if($labaDetailId->lbd_biy_gaji !== $biayaGaji->coad_saldo){
					$labaDetailId->lbd_biy_gaji = $biayaGaji->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_biy_gaji'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate biayaGaji');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($biayaGaji->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'biayaGaji is empty');
			}
			if(!empty($bebanLembur)){
				if($labaDetailId->lbd_bbn_lembur !== $bebanLembur->coad_saldo){
					$labaDetailId->lbd_bbn_lembur = $bebanLembur->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bbn_lembur'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bebanLembur');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($bebanLembur->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bebanLembur is empty');
			}
			if(!empty($bebanPengobatan)){
				if($labaDetailId->lbd_bbn_pengobatan_kyw !== $bebanPengobatan->coad_saldo){
					$labaDetailId->lbd_bbn_pengobatan_kyw = $bebanPengobatan->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bbn_pengobatan_kyw'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bebanPengobatan');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($bebanPengobatan->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bebanPengobatan is empty');
			}
			if(!empty($bpjsTkk)){
				if($labaDetailId->lbd_bpjs_tkk !== $bpjsTkk->coad_saldo){
					$labaDetailId->lbd_bpjs_tkk = $bpjsTkk->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bpjs_tkk'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bpjsTkk');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($bpjsTkk->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bpjsTkk is empty');
			}
			if(!empty($biySewaKantor)){
				if($labaDetailId->lbd_biy_sewa_kantor !== $biySewaKantor->coad_saldo){
					$labaDetailId->lbd_biy_sewa_kantor = $biySewaKantor->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_biy_sewa_kantor'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate biySewaKantor');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($biySewaKantor->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'biySewaKantor is empty');
			}
			if(!empty($bebanAtk)){
				if($labaDetailId->lbd_bbn_atk !== $bebanAtk->coad_saldo){
					$labaDetailId->lbd_bbn_atk = $bebanAtk->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bbn_atk'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bebanAtk');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($bebanAtk->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bebanAtk is empty');
			}
			if(!empty($biyTfi)){
				if($labaDetailId->lbd_biy_tfi !== $biyTfi->coad_saldo){
					$labaDetailId->lbd_biy_tfi = $biyTfi->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_biy_tfi'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate biyTfi');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($biyTfi->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'biyTfi is empty');
			}
			if(!empty($bebanParkir)){
				if($labaDetailId->lbd_bbn_prkr !== $bebanParkir->coad_saldo){
					$labaDetailId->lbd_bbn_prkr = $bebanParkir->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bbn_prkr'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bebanParkir');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($bebanParkir->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bebanParkir is empty');
			}
			if(!empty($bebanListrik)){
				if($labaDetailId->lbd_bbn_lstrk !== $bebanListrik->coad_saldo){
					$labaDetailId->lbd_bbn_lstrk = $bebanListrik->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bbn_lstrk'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bebanListrik');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($bebanListrik->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bebanListrik is empty');
			}
			if(!empty($bpBk)){
				if($labaDetailId->lbd_bp_bk !== $bpBk->coad_saldo){
					$labaDetailId->lbd_bp_bk = $bpBk->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bp_bk'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bpBk');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($bpBk->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bpBk is empty');
			}
			if(!empty($bebanTrans)){
				if($labaDetailId->lbd_bbn_trans !== $bebanTrans->coad_saldo){
					$labaDetailId->lbd_bbn_trans = $bebanTrans->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bbn_trans'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bebanTrans');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($bebanTrans->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bebanTrans is empty');
			}
			if(!empty($bebanEntertain)){
				if($labaDetailId->lbd_bbn_entertain !== $bebanEntertain->coad_saldo){
					$labaDetailId->lbd_bbn_entertain = $bebanEntertain->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bbn_entertain'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bebanEntertain');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($bebanEntertain->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bebanEntertain is empty');
			}
			if(!empty($KRT)){
				if($labaDetailId->lbd_k_rt !== $KRT->coad_saldo){
					$labaDetailId->lbd_k_rt = $KRT->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_k_rt'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate KRT');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($KRT->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'KRT is empty');
			}
			if(!empty($biyPS)){
				if($labaDetailId->lbd_biy_ps !== $biyPS->coad_saldo){
					$labaDetailId->lbd_biy_ps = $biyPS->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_biy_ps'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate biyPS');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($biyPS->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'biyPS is empty');
			}
			if(!empty($biyAK)){
				if($labaDetailId->lbd_biy_ak !== $biyAK->coad_saldo){
					$labaDetailId->lbd_biy_ak = $biyAK->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_biy_ak'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate biyAK');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($biyAK->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'biyAK is empty');
			}
			if(!empty($bppKantor)){
				if($labaDetailId->lbd_bpp_kantor !== $bppKantor->coad_saldo){
					$labaDetailId->lbd_bpp_kantor = $bppKantor->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bpp_kantor'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bppKantor');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($bppKantor->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bppKantor is empty');
			}
			if(!empty($bppKendaraan)){
				if($labaDetailId->lbd_bpp_kendaraan !== $bppKendaraan->coad_saldo){
					$labaDetailId->lbd_bpp_kendaraan = $bppKendaraan->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bpp_kendaraan'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bppKendaraan');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($bppKendaraan->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bppKendaraan is empty');
			}
			if(!empty($bppPeralatanKantor)){
				if($labaDetailId->lbd_bpp_alat_kantor !== $bppPeralatanKantor->coad_saldo){
					$labaDetailId->lbd_bpp_alat_kantor = $bppPeralatanKantor->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bpp_alat_kantor'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bppPeralatanKantor');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($bppPeralatanKantor->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bppPeralatanKantor is empty');
			}
			if(!empty($biyAdmTndr)){
				if($labaDetailId->lbd_biy_adm_tndr !== $biyAdmTndr->coad_saldo){
					$labaDetailId->lbd_biy_adm_tndr = $biyAdmTndr->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_biy_adm_tndr'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate biyAdmTndr');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($biyAdmTndr->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'biyAdmTndr is empty');
			}
			if(!empty($biyAdmBank)){
				if($labaDetailId->lbd_biy_adm_bank !== $biyAdmBank->coad_saldo){
					$labaDetailId->lbd_biy_adm_bank = $biyAdmBank->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_biy_adm_bank'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate biyAdmBank');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($biyAdmBank->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'biyAdmBank is empty');
			}
			if(!empty($bebanAskes)){
				if($labaDetailId->lbd_bbn_askes !== $bebanAskes->coad_saldo){
					$labaDetailId->lbd_bbn_askes = $bebanAskes->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bbn_askes'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bebanAskes');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($bebanAskes->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bebanAskes is empty');
			}
			if(!empty($bebanDinas)){
				if($labaDetailId->lbd_bbn_dinas !== $bebanDinas->coad_saldo){
					$labaDetailId->lbd_bbn_dinas = $bebanDinas->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bbn_dinas'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bebanDinas');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($bebanDinas->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bebanDinas is empty');
			}
			if(!empty($bebanLl)){
				if($labaDetailId->lbd_bbn_ll !== $bebanLl->coad_saldo){
					$labaDetailId->lbd_bbn_ll = $bebanLl->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_bbn_ll'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate bebanLl');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($bebanLl->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'bebanLl is empty');
			}
			if(!empty($Pll)){
				if($labaDetailId->lbd_pll !== $Pll->coad_saldo){
					$labaDetailId->lbd_pll = $Pll->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_pll'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate Pll');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($Pll->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'Pll is empty');
			}
			// echo '<pre>';
			// print_r('sakata');
			// echo '</pre>';
			// die;
			if(!empty($JasaGiro)){
				if($labaDetailId->lbd_jasa_giro !== $JasaGiro->coad_saldo){
					$labaDetailId->lbd_jasa_giro = $JasaGiro->coad_saldo;
					if($labaDetailId->SaveAttributes(array('lbd_jasa_giro'))){
						$return = true;
					}else{
						$return = false;
						print_r($labaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate JasaGiro');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($JasaGiro->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'JasaGiro is empty');
			}

		}else{
			throw new CHttpException('CNTRL 0 :' ,'year is empty');
		}
		
		// echo '<pre>';
		// print_r($labaDetailId->lbd_bbn_transaksi);
		// echo '</pre>';
		// die;
		// if($labaDetailId->SaveAttributes(array('lbd_bbn_transaksi'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		// }else{
		// 	print_r($labaDetailId->getErrors());
		// 	throw new CHttpException('CNTRL 0 :' ,'ERROR GENERATE');
		// }
	}

	public function actionLock($id)
	{
		$model = Laba::model()->findByAttributes(array('lb_id'=>$id));
		$model->lb_is_locked = 1;
		if($model->SaveAttributes(array('lb_is_locked'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR LOCK LABA DETAIL');
		}
	}

	public function actionUnlock($id)
	{
		$model = Laba::model()->findByAttributes(array('lb_id'=>$id));
		$model->lb_is_locked = 0;
		if($model->SaveAttributes(array('lb_is_locked'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR LOCK LABA DETAIL');
		}
	}

	public function actionPrint($id)
	{
		// $expensesDetailId=ExpensesDetail::model()->find('expd_exp_id=:expd_exp_id AND expd_status=1', array(':expd_exp_id'=>$id));
		// $expensesDetail=ExpensesDetail::model()->findAll('expd_exp_id=:expd_exp_id AND expd_status=1', array(':expd_exp_id'=>$id));
		// $expenses=Expenses::model()->findByAttributes(array('exp_id'=>$id, 'exp_status'=>1));
		// $karyawan=Karyawan::model()->find('id_pd=:id_pd', array(':id_pd'=>$expensesDetailId->expd_id_pd));
		// $jabatan=Jabatan::model()->find('id_jabatan=:id_jabatan', array(':id_jabatan'=>$karyawan->id_jabatan));
		$labaDetailId = LabaDetail::model()->find('lbd_lb_id=:lbd_lb_id AND lbd_status=1', array(':lbd_lb_id'=>$id));
		$labaDetail = LabaDetail::model()->findAll('lbd_lb_id=:lbd_lb_id AND lbd_status=1', array(':lbd_lb_id'=>$id));
		$laba = Laba::model()->findByAttributes(array('lb_id'=>$id, 'lb_status'=>1));
		$company = Company::model()->find('cpy_id=:cpy_id', array(':cpy_id'=>1));
		// echo '<pre>';
		// print_r($labaDetailId);
		// echo '</pre>';
		// die;

		$pdf = new fpdf();

		$size="a4";
		$header0="PT. SPARTAN ERAGON ASIA";
		// $header1="Bona Bisnis Center No. 8J Lt.2,";
		// $header2="Jl. Karang Tengah Raya, Jakarta Selatan";
		$dateLaba = LabaDetail::model()->find('lbd_id=:lbd_id', array(':lbd_id'=>$labaDetailId->lbd_id));
		$judul="LAPORAN LABA RUGI";

		$this->renderPartial('print',array(
			'pdf'=>$pdf,
			'judul'=>$judul,
			'header0'=>$header0,
			'dateLaba'=>$dateLaba,
			'company'=>$company,
			// 'header1'=>$header1,
			// 'header2'=>$header2,
			'size'=>$size,
			'labaDetailId'=>$labaDetailId,
			'labaDetail'=>$labaDetail,
			'laba'=>$laba,
			
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return LabaDetail the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=LabaDetail::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param LabaDetail $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='laba-detail-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
