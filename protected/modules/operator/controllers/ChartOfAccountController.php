<?php

class ChartOfAccountController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','index','view','menu','exportExcel','updateStatus'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ChartOfAccount;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ChartOfAccount']))
		{
			$model->attributes=$_POST['ChartOfAccount'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ChartOfAccount']))
		{
			$model->attributes=$_POST['ChartOfAccount'];
			// print_r($model->coa_code);die;
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = ChartOfAccount::model()->findByAttributes(array('coa_id'=>$id));
		$model->coa_status = 0;
		if($model->SaveAttributes(array('coa_status'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR DELETED COA');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ChartOfAccount');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ChartOfAccount('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ChartOfAccount']))
			$model->attributes=$_GET['ChartOfAccount'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionMenu($id, $type)
	{
		$model=new ChartOfAccount;
		$model->coa_id=$id;
		// $model->findByAttributes(array('coa_coat_id'=>$id));

		$coaMenu=ChartOfAccount::model()->findAll('coa_id=:coa_id AND coa_status=1', array(':coa_id'=>$id));
		// echo '<pre>';
		// print_r($coaMenu->coa_id);
		// echo '<pre>';
		// die;

		// $coaDetail=ChartOfAccountDetail::model()->findAll('coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1', array(':coad_coat_id'=>$id, 'coad_coa_id'=>$coaMenu->coa_id));

		// $coaDetail=ChartOfAccountDetail::model()->findAll('coad_coa_id=:coad_coa_id AND coad_status=1', array(':coad_coa_id'=>$id));

		$coaType=ChartOfAccountType::model()->find('coat_id=:coat_id AND coat_status=1', array(':coat_id'=>$type));
		

		if(isset($_POST['ChartOfAccount']['tahun']))
		{	
			if($_POST['ChartOfAccount']['tahun']!=''){
			$tahun= $_POST['ChartOfAccount']['tahun'];
			}
			else{
			$tahun=date('Y');
			}
		}
		else{
		$tahun=date('Y');
		}

		$this->render('menu',array(
			'model'=>$model,
			'coaMenu'=>$coaMenu,
			'coaType'=>$coaType,
			// 'coaDetail'=>$coaDetail,
			'tahun'=>$tahun,
		));
	}

	public function actionList($id)
	{
		$model=new ChartOfAccount;
		$model->coa_coat_id=$id;
		// $model->findByAttributes(array('coa_coat_id'=>$id));

		$coaMenu=ChartOfAccount::model()->findAll('coa_coat_id=:coa_coat_id AND coa_status=1', array(':coa_coat_id'=>$id));
		// echo '<pre>';
		// print_r($coaMenu->coa_id);
		// echo '<pre>';
		// die;

		// $coaDetail=ChartOfAccountDetail::model()->findAll('coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1', array(':coad_coat_id'=>$id, 'coad_coa_id'=>$coaMenu->coa_id));

		$coaDetail=ChartOfAccountDetail::model()->findAll('coad_coat_id=:coad_coat_id AND coad_status=1', array(':coad_coat_id'=>$id));

		$coaType=ChartOfAccountType::model()->find('coat_id=:coat_id AND coat_status=1', array(':coat_id'=>$id));
		

		if(isset($_POST['ChartOfAccount']['tahun']))
		{	
			if($_POST['ChartOfAccount']['tahun']!=''){
			$tahun= $_POST['ChartOfAccount']['tahun'];
			}
			else{
			$tahun=date('Y');
			}
		}
		else{
		$tahun=date('Y');
		}

		$this->render('list',array(
			'model'=>$model,
			'coaMenu'=>$coaMenu,
			'coaType'=>$coaType,
			'coaDetail'=>$coaDetail,
			'tahun'=>$tahun,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ChartOfAccount the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ChartOfAccount::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ChartOfAccount $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='chart-of-account-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionExportExcel()
	{ 	
		if(isset($_POST['fileType']) && $_POST['ChartOfAccount']){
			$model = new ChartOfAccount();
			$model->attributes = $_POST['ChartOfAccount'];
			$date = date('Y-m-d H:i:s');
			if($_POST['fileType']== "Excel"){
				$this->widget('ext.EExcelView', array(
					'title'=>'Daftar ChartOfAccount',
					'filename'=>'ChartOfAccount - '.$date,
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->search(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'Excel2007',
					'columns' => array(
						'coa_id',
						'coa_coat_id',
						'coa_coar_id',
						'coa_desc',
					),
				));
			} 
			$date = date('dMY');
			if($_POST['fileType']== "CSV"){
				$this->widget('ext.tcPDF', array(
					'title'=>'Daftar ChartOfAccount',
					'filename'=>'ChartOfAccount'.$date,
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->search(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'CSV',
					'columns' => array(
						'coa_id',
						'coa_coat_id',
						'coa_coar_id',
						'coa_desc',
					),
				));
			}
		}
	}

	public function actionUpdateStatus(){
        $postId	   	= Yii::app()->request->getPost('id');
        $return 	= true;
        $msg    	= 'Success';
        $error  	= (object)[];
        $data   	= (object)[];
        
        if(!empty($postId)){
            $checkCoa = ChartOfAccount::model()->find('coa_id=:coa_id', array(':coa_id' => $postId));
            if(!empty($checkCoa)){
            	if($checkCoa->coa_status == 1){
            		$changeStatus = 0;
            	}else if($checkCoa->coa_status !== 1 ){
            		$changeStatus = 1;
            	}
                $checkCoa->coa_status = $changeStatus;
                if($checkCoa->save()){
                    $return = true;
                    $msg = "Succes";

                }else{
                    $msg = "Failed to save data";
                }
          //       echo '<pre>';
		        // print_r($checkCoa->coa_status);
		        // echo '<pre>';
		        // die;
	                
            }else{
                $msg = "Data is Empty";
            }
        }
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return CJSON::encode($result);
    }
}