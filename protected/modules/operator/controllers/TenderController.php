<?php

class TenderController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array(),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','delete','create','update','exportExcel','member'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array(),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	// public function actionView($id)
	// {
	// 	$this->render('view',array(
	// 		'model'=>$this->loadModel($id),
	// 	));
	// }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Tender;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Tender']))
		{
			$model->attributes=$_POST['Tender'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionExportExcel()
    { 	
		if(isset($_POST['fileType']) && $_POST['Tender']){
			$model = new Tender();
			$model->attributes = $_POST['Tender'];
			if($_POST['fileType']== "Excel"){
				$this->widget('ext.EExcelView', array(
					'title'=>'Daftar Tender',
					'filename'=>'Tender',
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->search(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'Excel2007',
					'columns' => array(
						'id_tender',  
						'info_source',
						'client_code',
						'client_name',
						'tender_no',
						'tender_status',
						'tender_type',
						'detail_task',
						'ref_address',
						'deadline',
						'bid_bond',
					),
				));
				 
			} 
			if($_POST['fileType']== "CSV"){
				$this->widget('ext.tcPDF', array(
					'title'=>'Daftar Karyawan',
					'filename'=>'Karyawan',
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->search(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'CSV',
					'columns' => array(
						'id_pd',  
						'nm_pd',
						'divisi',
						'lokasi_kerja',
						'jk',
						'stat_pd',
					),
				));
				 
			}
		}
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Tender']))
		{
			$model->attributes=$_POST['Tender'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	// public function actionDelete($id)
	// {
	// 	$this->loadModel($id)->delete();

	// 	// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
	// 	if(!isset($_GET['ajax']))
	// 		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	// }

	public function actionDelete($id)
	{
		$model=Tender::model()->findByPk($id);
		$model->tndr_status = 2;
		if($model->SaveAttributes(array('tndr_status'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR DELETED KARYAWAN');
		}
	}

	/**
	 * Lists all models.
	 */
	// public function actionIndex()
	// {
	// 	$dataProvider=new CActiveDataProvider('Tender');
	// 	$this->render('index',array(
	// 		'dataProvider'=>$dataProvider,
	// 	));
	// }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		unset(Yii::app()->request->cookies['from_date']);  // first unset cookie for dates
        unset(Yii::app()->request->cookies['to_date']);
		$model=new Tender('search');
		if(!empty($_POST))
          {
            Yii::app()->request->cookies['from_date'] = new CHttpCookie('from_date', $_POST['from_date']);  // define cookie for from_date
            Yii::app()->request->cookies['to_date'] = new CHttpCookie('to_date', $_POST['to_date']);
            // Yii::app()->request->cookies['from_date_s'] = new CHttpCookie('from_date', $_POST['from_date_s']);  // define cookie for from_date
            // Yii::app()->request->cookies['to_date_s'] = new CHttpCookie('to_date', $_POST['to_date_s']);
            $model->from_date = $_POST['from_date'];
            $model->to_date = $_POST['to_date'];
            // $model->from_date = $_POST['from_date_s'];
            // $model->to_date = $_POST['to_date_s'];
        }
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Tender']))
			$model->attributes=$_GET['Tender'];

		$this->render('admin',array(
			'title'=>'Tender',
			'model'=>$model,
		));
	}

	public function actionMember($id)
	{
        unset(Yii::app()->request->cookies['from_date']);  // first unset cookie for dates
        unset(Yii::app()->request->cookies['to_date']);
		$model=new Tender('searchmember');
		if(!empty($_POST))
          {
            Yii::app()->request->cookies['from_date'] = new CHttpCookie('from_date', $_POST['from_date']);  // define cookie for from_date
            Yii::app()->request->cookies['to_date'] = new CHttpCookie('to_date', $_POST['to_date']);
            // Yii::app()->request->cookies['from_date_s'] = new CHttpCookie('from_date', $_POST['from_date_s']);  // define cookie for from_date
            // Yii::app()->request->cookies['to_date_s'] = new CHttpCookie('to_date', $_POST['to_date_s']);
            $model->from_date = $_POST['from_date'];
            $model->to_date = $_POST['to_date'];
            // $model->from_date = $_POST['from_date_s'];
            // $model->to_date = $_POST['to_date_s'];
        }
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Tender']))
			$model->attributes=$_GET['Tender'];

		$this->render('member',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Tender the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Tender::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Tender $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tender-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
