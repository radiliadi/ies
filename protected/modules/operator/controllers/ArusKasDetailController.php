<?php

class ArusKasDetailController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','generate','lock','unlock','print'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ArusKasDetail;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ArusKasDetail']))
		{
			$model->attributes=$_POST['ArusKasDetail'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->akd_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ArusKasDetail']))
		{
			$model->attributes=$_POST['ArusKasDetail'];
			if($model->save())
				$this->redirect(array('admin','id'=>$model->akd_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ArusKasDetail');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id)
	{
		$model=new ArusKasDetail;
		$model->akd_ak_id=$id;

		$arusKasDetailId=ArusKasDetail::model()->find('akd_ak_id=:akd_ak_id AND akd_status=1', array(':akd_ak_id'=>$id));
		$arusKasDetail=ArusKasDetail::model()->findAll('akd_ak_id=:akd_ak_id AND akd_status=1', array(':akd_ak_id'=>$id));
		$arusKas=ArusKas::model()->findByAttributes(array('ak_id'=>$id, 'ak_status'=>1));
		$model->findByAttributes(array('akd_ak_id'=>$id));

		$this->render('admin',array(
			'model'=>$model,
			'arusKasDetail'=>$arusKasDetail,
			'arusKasDetailId'=>$arusKasDetailId,
			'arusKas'=>$arusKas,
		));
	}

	public function actionGenerate($id, $date)
	{
		$arusKasDetailId = ArusKasDetail::model()->find('akd_ak_id=:akd_ak_id AND akd_status=1', array(':akd_ak_id'=>$id));
		// echo '<pre>';
		// print_r($id);
		// echo '</pre>';
		// die;
		$this->actionGenerateDetail($id, $date, $arusKasDetailId);
	}

	public function actionGenerateDetail($id, $date, $arusKasDetailId)
	{
		$return 	= false;
		$time 		= strtotime($date);
		$year 		= date("Y", $time);
		if(!empty($year)){
			$labaDetailId=LabaDetail::model()->find(array('order'=>'lbd_id DESC', 'condition'=>'lbd_status=:lbd_status AND DATE_FORMAT (lbd_date,("%Y"))>=:lbd_date AND lbd_is_generate!=:lbd_is_generate', 'params'=>array(':lbd_status'=>1, ':lbd_is_generate'=>1, ':lbd_date'=>$year)));
			// echo '<pre>';
			// // print_r($year);
			// // print_r($date);
			// print_r($labaDetailId);
			// echo '</pre>';
			// die;
			if(!empty($arusKasDetailId)){
				$total=0;
				$total = (
							$labaDetailId->lbd_bbn_transaksi + 
							$labaDetailId->lbd_biy_gaji + 
							$labaDetailId->lbd_bbn_lembur + 
							$labaDetailId->lbd_bbn_pengobatan_kyw + 
							$labaDetailId->lbd_bpjs_tkk + 
							$labaDetailId->lbd_biy_sewa_kantor + 
							$labaDetailId->lbd_bbn_atk + 
							$labaDetailId->lbd_biy_tfi + 
							$labaDetailId->lbd_bbn_prkr + 
							$labaDetailId->lbd_bbn_lstrk + 
							$labaDetailId->lbd_bp_bk + 
							$labaDetailId->lbd_bbn_trans + 
							$labaDetailId->lbd_bbn_entertain + 
							$labaDetailId->lbd_k_rt + 
							$labaDetailId->lbd_biy_ps + 
							$labaDetailId->lbd_biy_ak + 
							$labaDetailId->lbd_bpp_kantor + 
							$labaDetailId->lbd_bpp_kendaraan + 
							$labaDetailId->lbd_bpp_alat_kantor + 
							$labaDetailId->lbd_bbn_susut + 
							$labaDetailId->lbd_biy_adm_tndr + 
							$labaDetailId->lbd_biy_adm_bank + 
							$labaDetailId->lbd_bbn_askes + 
							$labaDetailId->lbd_bbn_dinas + 
							$labaDetailId->lbd_bbn_ll
						 );
				$labaKotor = $labaDetailId->lbd_pendapatan + $labaDetailId->lbd_hrg_pokok_penjualan;
				$labaUsaha = $labaKotor - $total;
				$totPll = $labaDetailId->lbd_pll + $labaDetailId->lbd_jasa_giro;
				$sumEnd = $labaUsaha + $totPll; //laba rugi berjalan (gada insert ke db)
				$arusKasDetailId->akd_laba_jalan = $sumEnd;
				$arusKasDetailId->akd_is_generate = $arusKasDetailId->akd_is_generate+1;
				if($arusKasDetailId->SaveAttributes(array('akd_laba_jalan','akd_is_generate'))){
					$return = true;
				}else{
					$return = false;
					print_r($arusKasDetailId->getErrors());
					throw new CHttpException('CNTRL 2 :' ,'error generate akd laba jalan');
				}
			}else{
				$return = false;
				print_r($arusKasDetailId->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'akd laba jalan is empty');
			}
			
		}
		return $this->redirect(Yii::app()->request->urlReferrer);
	}

	public function actionLock($id)
	{
		$model = ArusKas::model()->findByAttributes(array('ak_id'=>$id));
		$model->ak_is_locked = 1;
		if($model->SaveAttributes(array('ak_is_locked'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR LOCK ARUS KAS DETAIL');
		}
	}

	public function actionUnlock($id)
	{
		$model = ArusKas::model()->findByAttributes(array('ak_id'=>$id));
		$model->ak_is_locked = 0;
		if($model->SaveAttributes(array('ak_is_locked'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR LOCK ARUS KAS DETAIL');
		}
	}

	public function actionPrint($id)
	{
		$arusKasDetailId=ArusKasDetail::model()->find('akd_ak_id=:akd_ak_id AND akd_status=1', array(':akd_ak_id'=>$id));
		$arusKasDetail=ArusKasDetail::model()->findAll('akd_ak_id=:akd_ak_id AND akd_status=1', array(':akd_ak_id'=>$id));
		$arusKas=ArusKas::model()->findByAttributes(array('ak_id'=>$id, 'ak_status'=>1));
		$labaDetailId = LabaDetail::model()->find('lbd_lb_id=:lbd_lb_id AND lbd_status=1', array(':lbd_lb_id'=>$id));
		$labaDetail = LabaDetail::model()->findAll('lbd_lb_id=:lbd_lb_id AND lbd_status=1', array(':lbd_lb_id'=>$id));
		$laba = Laba::model()->findByAttributes(array('lb_id'=>$id, 'lb_status'=>1));
		$company = Company::model()->find('cpy_id=:cpy_id', array(':cpy_id'=>1));

		$pdf = new fpdf();

		$size="a4";
		$header0="PT. SPARTAN ERAGON ASIA";
		// $header1="Bona Bisnis Center No. 8J Lt.2,";
		// $header2="Jl. Karang Tengah Raya, Jakarta Selatan";
		$judul="LAPORAN ARUS KAS";

		$this->renderPartial('print',array(
			'pdf'=>$pdf,
			'judul'=>$judul,
			'header0'=>$header0,
			'company'=>$company,
			'arusKasDetailId'=>$arusKasDetailId,
			'arusKasDetail'=>$arusKasDetail,
			'arusKas'=>$arusKas,
			// 'header1'=>$header1,
			// 'header2'=>$header2,
			'size'=>$size,
			'labaDetailId'=>$labaDetailId,
			'labaDetail'=>$labaDetail,
			'laba'=>$laba,
			
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ArusKasDetail the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ArusKasDetail::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ArusKasDetail $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='arus-kas-detail-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
