<?php

class UserController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array(''),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','view','admin', 'delete', 'active', 'inactive', 'inactivex', 'deleted', 'deletedx', 'activex', 'exportExcel', 'update'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('',),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
		'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save())
				$this->redirect(array('active'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User'])){
			$model->attributes=$_POST['User'];
			if($model->save()){
				if($model->attributes=$_POST['User']['user_status'] == 0){
			    	$this->redirect(array('inactive'));
			    }elseif($model->attributes=$_POST['User']['user_status'] == 1){
			    	$this->redirect(array('active'));
			    }elseif($model->attributes=$_POST['User']['user_status'] == 2){
			    	$this->redirect(array('deleted'));
			    }
		    }
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else{
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}

	public function actionInactive()
	{
		$model=new User('searchinactive');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('inactive',array(
		'model'=>$model,
		));
	}

	public function actionInactivex($id)
	{
		$model = User::model()->findByAttributes(array('user_id'=>$id));
		$model->user_status = 0;
		if($model->SaveAttributes(array('user_status'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			echo 'CNTRL 0 : ERROR INACTIVED USER';
		}
	}

	public function actionActive()
	{
		$model=new User('searchactive');
		// $criteria = new CDbCriteria;
		// $criteria->addCondition("user_status = 1");
		// $model->find($criteria);
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('active',array(
		'model'=>$model,
		));
	}

	public function actionActivex($id)
	{
		$model = User::model()->findByAttributes(array('user_id'=>$id));
		$model->user_status = 1;
		if($model->SaveAttributes(array('user_status'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			echo 'CNTRL 0 : ERROR ACTIVED USER';
		}
	}

	public function actionDeleted()
	{
		$model=new User('searchdeleted');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('deleted',array(
		'model'=>$model,
		));
	}

	public function actionDeletedx($id) //YANG DIPAKE
	{
		$id_pd=Yii::app()->session->get('username');
		$model = User::model()->findByAttributes(array('user_id'=>$id));
		$model->user_datetime_delete = date('Y-m-d H:i:s');
		$model->user_delete_by = $id_pd;
		$model->user_status = 2;
		if($model->SaveAttributes(array('user_datetime_delete','user_delete_by','user_status'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR USER DELETEDX');
		}
	}

	public function actionExportExcel()
	{ 	
		if(isset($_POST['fileType']) && $_POST['User']){
			$model = new User();
			$model->attributes = $_POST['User'];
			$date = date('Y-m-d H:i:s');
			if($_POST['fileType']== "Excel"){
				$this->widget('ext.EExcelView', array(
					'title'=>'Daftar User',
					'filename'=>'User - '.$date,
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->searchactive(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'Excel2007',
					'columns' => array(
						'user_id',
						'username',
						'password',
						'level',
					),
				));
			} 
			$date = date('dMY');
			if($_POST['fileType']== "CSV"){
				$this->widget('ext.tcPDF', array(
					'title'=>'Daftar User',
					'filename'=>'User'.$date,
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->searchactive(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'CSV',
					'columns' => array(
						'username',
						'password',
						'level',
					),
				));
			}
		}
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}