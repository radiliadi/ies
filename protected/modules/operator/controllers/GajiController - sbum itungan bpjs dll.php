<?php

class GajiController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','grafik','cetak','laporan','slip'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','grafik','cetak','laporan','slip','deletedx'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id_gaji)
	{
		
		$model=Gaji::model()->find('id_gaji=:id_gaji', array(':id_gaji'=>$id_gaji));
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		
		$model=new Gaji;
		$modelkaryawan=new Karyawan;
		
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Gaji']))
		{
			$model->attributes=$_POST['Gaji'];
			$modelkaryawan->attributes=$_POST['Karyawan'];
			if($model->save())
			
			$this->redirect(array('view','id'=>$model->id_pd));
		}

		$this->render('create',array(
			'model'=>$model,
			'modelkaryawan'=>$modelkaryawan
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Gaji']))
		{
			$model->attributes=$_POST['Gaji'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_pd));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id_gaji)
	{		
		$model=Gaji::model()->findByPk($id_gaji);
		$this->loadModel($id_gaji)->delete();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			
			$this->redirect("../../admin/id/".$model->id_pd);
	}

	public function actionDeletedx($id_gaji)
	{
		$model=Gaji::model()->findByPk($id_gaji);
		$model->gaji_status = 2;
		if($model->SaveAttributes(array('gaji_status'))){
		return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR DELETED GAJI');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($id)
	{
	
		if(isset($_POST['Gaji']['tahun']))
		{	
			if($_POST['Gaji']['tahun']!=''){
			$tahun= $_POST['Gaji']['tahun'];
			}
			else{
			$tahun=date('Y');
			}
		}
		else{
		$tahun=date('Y');
		}
		
		$modelkaryawan=Karyawan::model()->find('id_pd=:id_pd', array(':id_pd'=>$id));
		$model=Gaji::model()->findAll('id_pd=:id_pd AND DATE_FORMAT (tgl,("%Y"))=:thn', array(':id_pd'=>$id , ':thn'=>$tahun));
		
		$modelbulan=new Gaji;
		$this->render('index',array(
			'model'=>$model,
			'modelkaryawan'=>$modelkaryawan,
			'modelbulan'=>$modelbulan,
			'tahun'=>$tahun,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id)
	{
		if(isset($_POST['Gaji']['bulan1']))
		{	
			if($_POST['Gaji']['bulan1']!=''){
			$modelgaji= new Gaji;
			// $modelbugaji= new BuGaji;
			$modelgaji->id_pd=$id;
			$modelkaryawan=Karyawan::model()->findByPk($id);
			$modeljabatan=Jabatan::model()->findByPk($modelkaryawan->id_jabatan);
			$modelptkp=Ptkp::model()->findByPk($modelkaryawan->id_ptkp);
			$modelmc=MedicalClaim::model()->findByAttributes(array('mc_id_pd'=>$id));
			$modelearn=Earnings::model()->findByAttributes(array('earn_id_pd'=>$id));

			$modelgaji->gj_pokok=$modeljabatan->pokok_jabatan;
			$modelgaji->overtime=$modelearn->earn_overtime;
			// echo '<pre>';
			// print_r($modelgaji->overtime);
			// echo '</pre>';
			// die;
			$modelgaji->transport=$modelearn->earn_daily_trans;
			$modelgaji->functional=$modelearn->earn_func_allow;
			$modelgaji->breakfast=$modelearn->earn_breakfast;
			$modelgaji->lunch=$modelearn->earn_lunch;
			$bulan=$_POST['Gaji']['bulan1'];
			$modelgaji->bulan='kjj';			
			$modelgaji->tunjangan=$modeljabatan->tunj_jabatan;
			$modelgaji->bruto=($modelgaji->gj_pokok + $modelgaji->tunjangan + $modelgaji->overtime + $modelgaji->transport + $modelgaji->functional + $modelgaji->breakfast + $modelgaji->lunch);

			if ($modelgaji->bruto > 10000000){
				$modelgaji->potongan=500000;
			} else {
				$modelgaji->potongan=(0.05*$modelgaji->bruto);
			}

			if($modelgaji->bruto <= 4500000){
				$modelgaji->neto_sebulan=0;
				$modelgaji->neto_setahun=0;
			}else{
				$modelgaji->neto_sebulan=($modelgaji->bruto-$modelgaji->potongan);
				$modelgaji->neto_setahun=(12*$modelgaji->neto_sebulan);
			}
			$modelgaji->ptkp_setahun=($modelptkp->wp_pribadi + $modelptkp->wp_menikah + $modelptkp->wp_tanggungan);
			
			if($modelgaji->bruto <= 4500000){
				$modelgaji->pkps=0;
			}else{
				$modelgaji->pkps=($modelgaji->neto_setahun-$modelgaji->ptkp_setahun);
			}
			
				if ($modelgaji->pkps > 0) 
				{
				    if ($modelgaji->pkps > 500000000) 
				    {
				        $modelgaji->pph_setahun = (0.3*$modelgaji->pkps);
				    } elseif ($modelgaji->pkps > 250000000) {
				        $modelgaji->pph_setahun = (0.25*$modelgaji->pkps);
				    } elseif ($modelgaji->pkps > 50000000) {
				        $modelgaji->pph_setahun = (0.15*$modelgaji->pkps);
				    } else {
				        $modelgaji->pph_setahun = (0.05*$modelgaji->pkps);
				    }
				}

				if ($modelgaji->bruto <= 4500000) 
				    {
				        $modelgaji->pph_setahun = 0;
				    }

				if ($modelgaji->bruto <= 4500000) 
					    {
					        $modelgaji->pph_sebulan = 0;
					    } else{
					    	$modelgaji->pph_sebulan=($modelgaji->pph_setahun/12);
					    }
			
			$modelgaji->bpjs_kete = $modelmc->mc_bpjs_kete;
			// echo '<pre>';
			// print_r($modelgaji->bpjs_kete);
			// echo '</pre>';
			// die;
			// if($modelgaji->bpjs_kete == $modelmc->mc_bpjs_kete){
			// 	print_r($modelgaji->getErrors());
   //          	die("mc not saved!");
			// }
			$modelgaji->bpjs_kese = $modelmc->mc_bpjs_kese;
			// echo '<pre>';
			// print_r($modelgaji->bpjs_kese);
			// echo '</pre>';
			// die;
			// if($modelgaji->bpjs_kese == $modelmc->mc_bpjs_kese){
			// 	print_r($modelgaji->getErrors());
   //          	die("mc not saved!");
			// }
			$modelgaji->bpjs_pensi = $modelmc->mc_bpjs_pensi;
			
			$modelgaji->tot_deduc=($modelgaji->pph_sebulan + $modelgaji->deduc_basic + $modelgaji->bpjs_kete + $modelgaji->bpjs_kese + $modelgaji->bpjs_pensi);

			$modelgaji->tot_gaji=($modelgaji->bruto - $modelgaji->tot_deduc);

			if(!empty($modelkaryawan->npwp)){
				$modelgaji->tot_gaji=($modelgaji->tot_gaji*100)/100;
			} else {
				$modelgaji->tot_gaji=($modelgaji->bruto - ((($modelgaji->pph_sebulan*120)/100) + $modelgaji->deduc_basic + $modelgaji->bpjs_kete + $modelgaji->bpjs_kese + $modelgaji->bpjs_pensi));
			}
	
			//tanggal gaji: mulai --------------------------------------------------------------------------------------
				$modelgaji->tgl=$_POST['Gaji']['tahun1'].'-'.$_POST['Gaji']['bulan1'].'-'.$_POST['Gaji']['tanggal1'];
				$modelgaji->bulan=$this->bulan($_POST['Gaji']['bulan1']);	
			//tanggal gaji: selesai ------------------------------------------------------------------------------------
			// $sId=Yii::app()->session->get('id');
			$id_pd=Yii::app()->session->get('username');
			$getlevel=Yii::app()->session->get('level');
			$modelgaji->created_by = $id_pd.'-'.$getlevel;
			$modelgaji->user_agent = Yii::app()->request->userAgent;
			$modelgaji->date_created = date('Y-m-d H:i:s');

			
			// $modelbugaji->id_gaji 		= $modelgaji->id_gaji;
			// $modelbugaji->id_pd 		= $modelgaji->id_pd;
			// $modelbugaji->tgl 			= $modelgaji->tgl;
			// $modelbugaji->bulan 		= $modelgaji->bulan;
			// $modelbugaji->gj_pokok 		= $modelgaji->gj_pokok;
			// $modelbugaji->tunjangan 	= $modelgaji->tunjangan;
			// $modelbugaji->overtime 		= $modelgaji->overtime;
			// $modelbugaji->neto_setahun 	= $modelgaji->neto_setahun;
			// $modelbugaji->ptkp_setahun 	= $modelgaji->ptkp_setahun;
			// $modelbugaji->pkps 			= $modelgaji->pkps;

			// $modelbugaji->pph_setahun 	= $modelgaji->pph_setahun;
			// $modelbugaji->pph_sebulan 	= $modelgaji->pph_sebulan;
			// $modelbugaji->bpjs_kete 	= $modelgaji->bpjs_kete;
			// $modelbugaji->bpjs_kese		= $modelgaji->bpjs_kese;
			// $modelbugaji->bpjs_pensi 	= $modelgaji->bpjs_pensi;
			// $modelbugaji->tot_deduc 	= $modelgaji->tot_deduc;
			// $modelbugaji->tot_gaji 		= $modelgaji->tot_gaji;
			// $modelbugaji->transport 	= $modelgaji->transport;
			// $modelbugaji->functional 	= $modelgaji->functional;
			// $modelbugaji->breakfast 	= $modelgaji->breakfast;

			// $modelbugaji->lunch 		= $modelgaji->lunch;
			// $modelbugaji->bruto 		= $modelgaji->bruto;
			// $modelbugaji->potongan 		= $modelgaji->potongan;
			// $modelbugaji->deduc_basic 	= $modelgaji->deduc_basic;
			// $modelbugaji->neto_sebulan 	= $modelgaji->neto_sebulan;
			// $modelbugaji->created_by 	= $modelgaji->created_by;
			// $modelbugaji->user_agent 	= $modelgaji->user_agent;
			// $modelbugaji->date_created 	= $modelgaji->date_created;
			
			$modelgaji->save();
			// $modelbugaji->save();
			// $this->refresh();	
			}
		}	
		
		if(isset($_POST['Gaji']['tahun']))
		{	
			if($_POST['Gaji']['tahun']!=''){
			$tahun= $_POST['Gaji']['tahun'];
			}
			else{
			$tahun=date('Y');
			}
		}
		else{
		$tahun=date('Y');
		}
		
		$modelkaryawan=Karyawan::model()->find('id_pd=:id_pd', array(':id_pd'=>$id));
		$model=Gaji::model()->findAll('id_pd=:id_pd AND DATE_FORMAT (tgl,("%Y"))=:thn', array(':id_pd'=>$id , ':thn'=>$tahun));
		$modelbulan=new Gaji;
		$modeltanggal=new Gaji;
		
		$this->render('admin',array(
			'model'=>$model,
			'modelkaryawan'=>$modelkaryawan,
			'modelbulan'=>$modelbulan,
			'modeltanggal'=>$modeltanggal,
			'tahun'=>$tahun,
			
		));
	}
	/** 
	aksi highchart gaji
	*/
	
		public function actionGrafik()
	{
		$sql='SELECT g.nm_jabatan as jabatan, p.tot_gaji as total , count(*) as jumlah 
				FROM `gaji` p, jabatan g, karyawan q
				where p.id_pd=q.id_pd AND q.id_jabatan=g.id_jabatan
				GROUP BY q.id_jabatan';
		$dataProvider=new CSqlDataProvider($sql, array(
								'keyField'=>'jabatan',
		));
		
		
		$this->render('grafik',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	
	public function actionCetak()
	{	
		
		if(isset($_POST['Gaji']['tahun']))
		{	
			$this->redirect('laporan/tahun/'.$_POST['Gaji']['tahun']);
			
		}
		$size="";
		$model=Gaji::model()->findAll();
		$modelbulan=new Gaji;

		$this->render('cetak',array(
			'model'=>$model,
			'modelbulan'=>$modelbulan,
			'size'=>$size,
			
		));
		
			
	}
	
		
	//aksi laporan gaji keseluruhan
	public function actionLaporan($tahun)
	{
		$model=Gaji::model()->findAll('DATE_FORMAT (tgl,("%Y"))=:thn', array(':thn'=>$tahun));
		//extensions fpdf
		$pdf = new fpdf();
		//Deklarasi
		$size="a4";
		$header0="SEKOLAH TINGGI AGAMA ISLAM MASMUR";
		$header="PEKANBARU";
		$judul="LAPORAN DATA PENGGAJIAN";
		
		//Render
		$this->renderPartial('laporan',array(
			'pdf'=>$pdf,
			'judul'=>$judul,
			'header0'=>$header0,
			'header'=>$header,
			'model'=>$model,
			'size'=>$size,
			//'modelbulan'=>$modelbulan,
			
		));
	}
	
	
	//aksi slip gaji pegawai
	public function actionSlip($id_gaji)
	{
		$model=Gaji::model()->find('id_gaji=:id_gaji', array(':id_gaji'=>$id_gaji));
		//extensions fpdf
		$pdf = new fpdf();
		//Deklarasi
		$header0="PT. SPARTAN ERAGON ASIA";
		$header="Bona Bisnis Center No. 8J Lt.2, Jl. Karang Tengah Raya, Jakarta Selatan";
		$judul="PAYSLIP";
		$data1="EMPLOYEE NUMBER";
		$data2="EMPLOYEE NAME";
		$data3="PAYROLL DATE";
		$data4="NO. PAYSLIP";
		
		//Render
		$this->renderPartial('slip',array(
			'pdf'=>$pdf,
			'judul'=>$judul,
			'header0'=>$header0,
			'header'=>$header,
			'data1'=>$data1,
			'data2'=>$data2,
			'data3'=>$data3,
			'data4'=>$data4,
			'model'=>$model,
			
		));
	}
	
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Gaji the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Gaji::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Gaji $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gaji-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	//tanggal lahir : mulai --------------------------------------------------------------------------------------
	public function bulan($i)
	{
		switch($i)
		{
			case 1:
			$bulan="Januari";
			break;
			
			case 2:
			$bulan="Februari";
			break;
			
			case 3:
			$bulan="Maret";
			break;
			
			case 4:
			$bulan="April";
			break;
			
			case 5:
			$bulan="Mei";
			break;
			
			case 6:
			$bulan="Juni";
			break;
			
			case 7:
			$bulan="Juli";
			break;
			
			case 8:
			$bulan="Agustus";
			break;
			
			case 9:
			$bulan="September";
			break;
			
			case 10:
			$bulan="Oktober";
			break;
			
			case 11:
			$bulan="November";
			break;
			
			case 12:
			$bulan="Desember";
			break;
		}
			return $bulan;
	}
	//tanggal lahir : selesai --------------------------------------------------------------------------------------
	
}
?>