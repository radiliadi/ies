<?php

class ModalDetailController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','print','lock','unlock'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{
		$model=new ModalDetail;
		$model->mdld_mdl_id = $id;
		

		$modal=Modal::model()->find('mdl_id=:mdl_id', array(':mdl_id'=>$id));
		$modal->mdl_status = 1;

		// $model->mdld_date = $modal->mdl_date;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ModalDetail']))
		{
			$model->attributes=$_POST['ModalDetail'];
			if($model->save())
				if($modal->save()){
					$this->redirect(array('admin','id'=>$model->mdld_mdl_id));
				}else{
					throw new CHttpException('402', print_r($modal->getErrors()));
				}
				
		}

		$this->render('create',array(
			'model'=>$model,
			'modal'=>$modal,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id, $modalId)
	{
		$model=$this->loadModel($id);
		$modal=Modal::model()->find('mdl_id=:mdl_id', array(':mdl_id'=>$modalId));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ModalDetail']))
		{
			$model->attributes=$_POST['ModalDetail'];
			if($model->save())
				$this->redirect(array('admin','id'=>$model->mdld_mdl_id));
		}

		$this->render('update',array(
			'model'=>$model,
			'modal'=>$modal,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = ModalDetail::model()->findByAttributes(array('mdld_id'=>$id));
		$model->mdld_status = 2;
		if($model->SaveAttributes(array('mdld_status'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR DELETED MODAL DETAIL');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ModalDetail');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id)
	{
		$model=new ModalDetail;
		$model->mdld_mdl_id=$id;

		$modalDetailId = array();
		$modalDetailId=ModalDetail::model()->find('mdld_mdl_id=:mdld_mdl_id AND mdld_status=1', array(':mdld_mdl_id'=>$id));
		$modalDetail=ModalDetail::model()->findAll('mdld_mdl_id=:mdld_mdl_id AND mdld_status=1', array(':mdld_mdl_id'=>$id));
		$modal=Modal::model()->findByAttributes(array('mdl_id'=>$id, 'mdl_status'=>1));
		$model->findByAttributes(array('mdld_mdl_id'=>$id));

		$this->render('admin',array(
			'model'=>$model,
			'modalDetail'=>$modalDetail,
			'modalDetailId'=>$modalDetailId,
			'modal'=>$modal,
		));
	}

	public function actionLock($id)
	{
		$model = Modal::model()->findByAttributes(array('mdl_id'=>$id));
		$model->mdl_is_locked = 1;
		if($model->SaveAttributes(array('mdl_is_locked'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR LOCK MODAL DETAIL');
		}
	}

	public function actionUnlock($id)
	{
		$model = Modal::model()->findByAttributes(array('mdl_id'=>$id));
		$model->mdl_is_locked = 0;
		if($model->SaveAttributes(array('mdl_is_locked'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR LOCK MODAL DETAIL');
		}
	}

	public function actionPrint($id)
	{
		$modalDetailId=ModalDetail::model()->find('mdld_mdl_id=:mdld_mdl_id AND mdld_status=1', array(':mdld_mdl_id'=>$id));
		$modalDetail=ModalDetail::model()->findAll('mdld_mdl_id=:mdld_mdl_id AND mdld_status=1', array(':mdld_mdl_id'=>$id));
		$modal = Modal::model()->findByAttributes(array('mdl_id'=>$id, 'mdl_status'=>1));

		$company = Company::model()->find('cpy_id=:cpy_id', array(':cpy_id'=>1));

		$pdf = new fpdf();

		$size="a4";
		$header0="PT. SPARTAN ERAGON ASIA";
		// $header1="Bona Bisnis Center No. 8J Lt.2,";
		// $header2="Jl. Karang Tengah Raya, Jakarta Selatan";
		$dateModal = ModalDetail::model()->find('mdld_id=:mdld_id', array(':mdld_id'=>$modalDetailId->mdld_id));
		$judul="LAPORAN PERUBAHAN EKUITAS";

		$this->renderPartial('print',array(
			'pdf'=>$pdf,
			'judul'=>$judul,
			'header0'=>$header0,
			'dateModal'=>$dateModal,
			'company'=>$company,
			'modalDetail'=>$modalDetail,
			'modalDetailId'=>$modalDetailId,
			'modal'=>$modal,
			// 'header1'=>$header1,
			// 'header2'=>$header2,
			'size'=>$size,
			
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ModalDetail the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ModalDetail::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ModalDetail $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='modal-detail-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
