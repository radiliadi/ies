<?php

class BankController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','saldoBank'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Bank;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Bank']))
		{
			$model->attributes=$_POST['Bank'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Bank']))
		{
			$model->attributes=$_POST['Bank'];
			$model->bnk_saldo_current 	= $model->bnk_saldo_current + $model->bnk_saldo_first;
			// echo '<pre>';
			// print_r($model->bnk_saldo_current);
			// echo '</pre>';
			// die;
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$id_pd=Yii::app()->session->get('username');
		$model = Bank::model()->findByAttributes(array('bnk_id'=>$id));
		// $model->exp_datetime_delete = date('Y-m-d H:i:s');
		// $model->exp_delete_by = $id_pd;
		$model->bnk_status = 2;
		if($model->SaveAttributes(array('bnk_status'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR DELETED BANK');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Bank');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Bank('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Bank']))
			$model->attributes=$_GET['Bank'];

		$this->render('admin',array(
			'title'=>'Bank',
			'model'=>$model,
		));
	}

	public function actionExportExcel()
	{ 	
		if(isset($_POST['fileType']) && $_POST['Bank']){
			$model = new Bank();
			$model->attributes = $_POST['Bank'];
			$date = date('Y-m-d H:i:s');
			if($_POST['fileType']== "Excel"){
				$this->widget('ext.EExcelView', array(
					'title'=>'Daftar Bank',
					'filename'=>'Bank - '.$date,
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->search(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'Excel2007',
					'columns' => array(
						'bnk_name',
						'bnk_saldo_first',
						'bnk_saldo_current',
						'bnk_saldo_used',
					),
				));
			} 
			$date = date('dMY');
			if($_POST['fileType']== "CSV"){
				$this->widget('ext.tcPDF', array(
					'title'=>'Daftar Bank',
					'filename'=>'Bank'.$date,
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->search(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'CSV',
					'columns' => array(
						'bnk_name',
						'bnk_saldo_first',
						'bnk_saldo_current',
						'bnk_saldo_used',
					),
				));
			}
		}
	}

	public function actionSaldoBank($id, $coadId, $bankId)
	{
		$id_pd=Yii::app()->session->get('username');
		$msg = "Invalid param";
		// echo '<pre>';
		// print_r($id);
		// echo '<br/>';
		// print_r($coadId);
		// echo '<br/>';
		// print_r($bankId);
		// echo '<br/>';
		// echo '</pre>';
		// die;
		$bank = Bank::model()->find('bnk_id=:bnk_id AND bnk_status=1', array(':bnk_id'=>$bankId));
		
		$coad = ChartOfAccountDetail::model()->find('coad_id=:coad_id AND coad_status=1', array(':coad_id'=>$coadId));

		$bank->bnk_saldo_current = (($bank->bnk_saldo_current - $coad->coad_debet) + $coad->coad_credit);
		$bank->bnk_saldo_used    = $bank->bnk_saldo_used - $coad->coad_credit + $coad->coad_debet;
		$coad->coad_bb_saldo 	= $bank->bnk_saldo_current;
		$coad->coad_bb_is_saldo = 1;
		$coad->coad_bnk_id 		= $bankId;
		$coad->coad_bnk_id_pd 	= $id_pd;
		// echo '<pre>';
		// print_r($bank);
		// echo '</pre>';
		// die;

		if($bank->SaveAttributes(array('bnk_saldo_current', 'bnk_saldo_used'))){
			if($coad->SaveAttributes(array('coad_bb_saldo', 'coad_bb_is_saldo', 'coad_bnk_id', 'coad_bnk_id_pd'))){
				return $this->redirect(Yii::app()->request->urlReferrer);
			}else{
				print_r($coad->getErrors());
				throw new CHttpException('CNTRL 0 :' ,'ERROR CALCULATION SALDO BANK, COAD SALDO CURRENT CANT SAVE');
			}
			$msg = "success";
		}else{
			print_r($bank->getErrors());
			throw new CHttpException('CNTRL 1 :' ,'ERROR CALCULATION SALDO BANK, BANK SALDO CURRENT CANT SAVE');
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Bank the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Bank::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Bank $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bank-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
