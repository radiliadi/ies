<?php

class ChatController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','laporan','delete'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','laporan','delete','deletedx','admin','hidden','show','exportExcel'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Chat;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Chat']))
		{
			$model->attributes=$_POST['Chat'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->chat_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Chat']))
		{
			$model->attributes=$_POST['Chat'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->chat_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	// public function actionDelete($id)
	// {
	// 	$this->loadModel($id)->delete();

	// 	// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
	// 	if(!isset($_GET['ajax']))
	// 		$this->redirect(Yii::app()->request->urlReferrer);;
	// }

	public function actionDelete($id)
	{
		$model = Chat::model()->findByAttributes(array('chat_id'=>$id));
		$model->chat_status = 2;
		if($model->SaveAttributes(array('chat_status'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR DELETED CHAT');
		}
	}

	public function actionHidden($id)
	{
		$model = Chat::model()->findByAttributes(array('chat_id'=>$id));
		$model->chat_status = 0;
		if($model->SaveAttributes(array('chat_status'))){
		return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR HIDDEN CHAT');
		}
	}

	public function actionShow($id)
	{
		$model = Chat::model()->findByAttributes(array('chat_id'=>$id));
		$model->chat_status = 1;
		if($model->SaveAttributes(array('chat_status'))){
		return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR SHOW CHAT');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$criteria=new CDbCriteria;
		$criteria->select = "DATE_FORMAT(chat_date,'%Y') as year";
		// $criteria->select = "DATE_FORMAT(chat_date,'%m') as month";
		$criteria->order = "DATE_FORMAT(chat_date,'%Y') DESC";
		$criteria->group = "DATE_FORMAT(chat_date,'%Y')";
		$modelTahun=Chat::model()->findAll($criteria);
		$model=new Chat;
		$sId=Yii::app()->session->get('id');
		if(isset($_POST['Chat']))
		{
			$model->attributes=$_POST['Chat'];
			$model->chat_from=$sId;
			$model->chat_to='all';
			$model->chat_status=1;
			$model->chat_date=date('Y-m-d H:i:s');
			$model->save();
			$this->refresh();
		}
		$this->render('index',array(
			'model'=>$model,
			'modelTahun'=>$modelTahun,
		));
	}
	
	public function actionLaporan()
	{		
		//Hitung Total
		$criteria=new CDbCriteria;
		$criteria->select = "*,(select sum(keu_budget) as total FROM keuangan k WHERE k.keu_kat=:keu_kat AND keu_tipe='debet') as debet,(select sum(keu_budget) as total FROM keuangan k WHERE k.keu_kat=:keu_kat AND keu_tipe='kredit') as kredit";
		$criteria->condition = "keu_kat=:keu_kat";
		$criteria->params = array (	
			':keu_kat'=>"kas",
		);
		$modelKas=Keuangan::model()->find($criteria);
		//Zakat
		$criteria->params = array (	
			':keu_kat'=>"zakat",
		);
		$modelZakat=Keuangan::model()->find($criteria);
		//Tim
		$criteria=new CDbCriteria;
		$criteria->select = "*,sum(user_gaji) as user_gaji";
		$criteria->condition = "user_gaji>:user_gaji";
		$criteria->params = array (	
			':user_gaji'=>"0",
		);
		$modelTim=User::model()->find($criteria);
		//Projek
		$criteria=new CDbCriteria;
		$criteria->select = "count(pro_id) as pro_id, sum(pro_budget) as pro_budget";
		$modelProject=Project::model()->find($criteria);
		//User
		$criteria=new CDbCriteria;
		$criteria->select = "count(user_id) as user_id";
		$modelUser=User::model()->find($criteria);
		//Visitor/Viewer
		$criteria=new CDbCriteria;
		$criteria->select = "count(view_id) as view_id";
		$modelView=Viewer::model()->find($criteria);
		//Kantor
		$ofc_tgl=23;
		$ofc_bln=04;
		$ofc_tgl_full='2015-'.$ofc_tgl.'-'.$ofc_tgl;
		//render
		$this->render('laporan',array(
			'modelKas'=>$modelKas,
			'modelZakat'=>$modelZakat,
			'modelTim'=>$modelTim,
			'modelProject'=>$modelProject,
			'modelUser'=>$modelUser,
			'modelView'=>$modelView,
			'ofc_tgl'=>$ofc_tgl,
			'ofc_bln'=>$ofc_bln,
			'ofc_tgl_full'=>$ofc_tgl_full,
		));
	}
	
	public function bulan($id)
	{
		$bulan="";
		switch($id)
		{
			case 01:$bulan="Januari";break;
			case 02:$bulan="Februari";break;
			case 03:$bulan="Maret";break;
			case 04:$bulan="April";break;
			case 05:$bulan="Mei";break;
			case 06:$bulan="Juni";break;
			case 07:$bulan="Juli";break;
			case 8:$bulan="Agustus";break;
			case 9:$bulan="September";break;
			case 10:$bulan="Oktober";break;
			case 11:$bulan="November";break;
			case 12:$bulan="Desember";break;
		}
		return $bulan;
	}
	
	//Hitung hari pembayaran kantor di laporan
	public function timeDiff($firstTime,$lastTime){
	   // convert to unix timestamps
	   $firstTime=strtotime($firstTime);
	   $lastTime=strtotime($lastTime);

	   // perform subtraction to get the difference (in seconds) between times
	   //hari
	   $timeDiff=($lastTime-$firstTime)/(3600*24);
	   //bulan
	   if($timeDiff>30):
			$total=$timeDiff;
			$bulan=number_format($timeDiff/30);
			$hari=$timeDiff%30;
			$timeDiff=$bulan.' bulan, '.$hari.' hari ('.$total.')';
		else:
			$timeDiff.='';
	   endif;

	   // return the difference
	   return $timeDiff;
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Chat('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Chat']))
			$model->attributes=$_GET['Chat'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionExportExcel()
	{ 	
		if(isset($_POST['fileType']) && $_POST['Chat']){
			$model = new Chat();
			$model->attributes = $_POST['Chat'];
			$date = date('Y-m-d H:i:s');
			if($_POST['fileType']== "Excel"){
				$this->widget('ext.EExcelView', array(
					'title'=>'Chat List',
					'filename'=>'Chat - '.$date,
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->search(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'Excel2007',
					'columns' => array(
						'chat_from',
						// 'chat_to',
						'chat_text',
						'chat_date',
				        // 'user_stat',
					),
				));
			} 
			$date = date('dMY');
			if($_POST['fileType']== "CSV"){
				$this->widget('ext.tcPDF', array(
					'title'=>'Chat List',
					'filename'=>'Chat'.$date,
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->search(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'CSV',
					'columns' => array(
						'username',
						'password',
						'level',
				        // 'user_stat',
					),
				));
			}
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Chat the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Chat::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Chat $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='chat-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
