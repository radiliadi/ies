<?php

class InventoryDetailController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','lock','unlock','print'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{
		$model=new InventoryDetail;

		$inventory = Inventory::model()->find('inv_id=:inv_id', array(':inv_id'=>$id));

		$model->invd_inv_id = $inventory->inv_id;
		// echo '<pre>';
		// print_r($inventory);
		// echo '</pre>';
		// die;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['InventoryDetail']))
		{
			$model->attributes=$_POST['InventoryDetail'];
			if($model->save())
				if($inventory->save()){
					$this->redirect(array('admin','id'=>$model->invd_inv_id));
				}else{
					print_r($inventory->getErrors());
				}
				
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['InventoryDetail']))
		{
			$model->attributes=$_POST['InventoryDetail'];
			if($model->save())
				$this->redirect(array('admin','id'=>$model->invd_inv_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = InventoryDetail::model()->findByAttributes(array('invd_id'=>$id));
		$model->invd_status = 2;
		if($model->SaveAttributes(array('invd_status'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR DELETED INVENTORY DETAIL');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('InventoryDetail');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id)
	{
		$model=new InventoryDetail;
		$model->invd_inv_id=$id;

		$inventoryDetailId=InventoryDetail::model()->find('invd_inv_id=:invd_inv_id AND invd_status=1', array(':invd_inv_id'=>$id));
		$inventoryDetail=InventoryDetail::model()->findAll('invd_inv_id=:invd_inv_id AND invd_status=1', array(':invd_inv_id'=>$id));
		$inventorySusut=InventorySusut::model()->findAll('invs_inv_id=:invs_inv_id AND invs_status=1', array(':invs_inv_id'=>$id));
		$inventoryArr=Inventory::model()->findAll('inv_id=:inv_id AND inv_status=1', array(':inv_id'=>$id));
		$inventory=Inventory::model()->findByAttributes(array('inv_id'=>$id, 'inv_status'=>1));
		$model->findByAttributes(array('invd_inv_id'=>$id));

		// echo '<pre>';
		// // print_r($inventoryDetail);
		// print_r($inventorySusut);
		// echo '</pre>';
		// die;

		$this->render('admin',array(
			'model'=>$model,
			'inventoryDetail'=>$inventoryDetail,
			'inventorySusut'=>$inventorySusut,
			'inventoryDetailId'=>$inventoryDetailId,
			'inventory'=>$inventory,
			'inventoryArr'=>$inventoryArr,
		));
	}

	public function actionLock($id)
	{
		$model = Inventory::model()->findByAttributes(array('inv_id'=>$id));
		$model->inv_is_locked = 1;
		if($model->SaveAttributes(array('inv_is_locked'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR LOCK INVENTORY DETAIL');
		}
	}

	public function actionUnlock($id)
	{
		$model = Inventory::model()->findByAttributes(array('inv_id'=>$id));
		$model->inv_is_locked = 0;
		if($model->SaveAttributes(array('inv_is_locked'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR LOCK INVENTORY DETAIL');
		}
	}

	public function actionPrint($id)
	{
		$inventoryDetailId=InventoryDetail::model()->find('invd_inv_id=:invd_inv_id AND invd_status=1', array(':invd_inv_id'=>$id));
		$inventoryDetail=InventoryDetail::model()->findAll('invd_inv_id=:invd_inv_id AND invd_status=1', array(':invd_inv_id'=>$id));
		$inventory = Inventory::model()->findByAttributes(array('inv_id'=>$id, 'inv_status'=>1));

		$company = Company::model()->find('cpy_id=:cpy_id', array(':cpy_id'=>1));

		$pdf = new fpdf();

		$size="a4";
		$header0="PT. SPARTAN ERAGON ASIA";
		// $header1="Bona Bisnis Center No. 8J Lt.2,";
		// $header2="Jl. Karang Tengah Raya, Jakarta Selatan";
		$dateModal = InventoryDetail::model()->find('invd_id=:invd_id', array(':invd_id'=>$inventoryDetailId->invd_id));
		$judul="LAPORAN INVENTORY";

		$this->renderPartial('print',array(
			'pdf'=>$pdf,
			'judul'=>$judul,
			'header0'=>$header0,
			'dateModal'=>$dateModal,
			'company'=>$company,
			'inventoryDetail'=>$inventoryDetail,
			'inventoryDetailId'=>$inventoryDetailId,
			'inventory'=>$inventory,
			// 'header1'=>$header1,
			// 'header2'=>$header2,
			'size'=>$size,
			
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return InventoryDetail the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=InventoryDetail::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param InventoryDetail $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='inventory-detail-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
