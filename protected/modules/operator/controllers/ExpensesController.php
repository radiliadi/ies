<?php

class ExpensesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array(),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','index','view','notVerified','verified','rejected','approved','submit','picture'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array(),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Expenses;
		$msg = 'Invalid parameter';
		$id_pd=Yii::app()->session->get('username');
		$defaultImage = '';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Expenses']))
		{
			$rnd = rand(0,9999);
			$model->attributes=$_POST['Expenses'];
			$model->exp_datetime_insert=date('Y-m-d H:i:s');
			if(CUploadedFile::getInstance($model,'exp_attachment_img'))
			{ 
				$uploadedFile = CUploadedFile::getInstance($model,'exp_attachment_img');
				$fileName="{$rnd}-{$id_pd}-{$uploadedFile->name}";
			}else{
			// $this->redirect(array('abc'));
				$model->exp_attachment_img=$defaultImage;
			}

			// echo '<pre>';
			// print_r($prevImage);
			// echo '</pre>';
			// die;

			if(!empty($uploadedFile))  // check if uploaded file is set or not
            {
                $model->exp_attachment_img=$fileName;
                $model->save();
            	$uploadedFile->saveAs(Yii::app()->basePath.'/../expenses/'.$fileName);
            	$msg = 'sucees';
            }else{
            	$model->exp_attachment_img=$defaultImage;
            	$model->save();
            	$msg = 'sucees';
            }
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$msg = 'Invalid parameter';
		$model=$this->loadModel($id);
		$prevImage=$model->exp_attachment_img;
		$id_pd=Yii::app()->session->get('username');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Expenses']))
		{
			$rnd = rand(0,9999);
			$model->attributes=$_POST['Expenses'];
			if(CUploadedFile::getInstance($model,'exp_attachment_img'))
			{ 
				$uploadedFile = CUploadedFile::getInstance($model,'exp_attachment_img');
				$fileName="{$rnd}-{$id_pd}-{$uploadedFile->name}";
			}else{
			// $this->redirect(array('abc'));
				$model->exp_attachment_img=$prevImage;
			}

			// echo '<pre>';
			// print_r($prevImage);
			// echo '</pre>';
			// die;

			if(!empty($uploadedFile))  // check if uploaded file is set or not
            {
                $model->exp_attachment_img=$fileName;
                $model->save();
            	$uploadedFile->saveAs(Yii::app()->basePath.'/../expenses/'.$fileName);
            	$msg = 'sucees';
            }else{
            	$model->exp_attachment_img=$prevImage;
            	$model->save();
            	$msg = 'sucees';
            }
				$this->redirect(array('admin'));
		}

		if(isset($_POST['User']))
        {
            $rnd = rand(0,9999);
            $model->attributes=$_POST['User'];
            if(CUploadedFile::getInstance($model,'user_foto'))
			{ 
				$uploadedFile = CUploadedFile::getInstance($model,'user_foto');
				$fileName="{$rnd}-{$id_pd}-{$uploadedFile->name}";
				// $model->user_foto=$id_pd.'-'.$_POST['User']['user_foto'];
			}else{
			// $this->redirect(array('abc'));
				$model->user_foto=$prevImage;
			}

			// echo '<pre>';
			// print_r($prevImage);
			// echo '</pre>';
			// die;

			if(!empty($uploadedFile))  // check if uploaded file is set or not
            {
                $model->user_foto=$fileName;
                $model->save();
            	$uploadedFile->saveAs(Yii::app()->basePath.'/../images/'.$fileName);
            	$msg = 'sucees';
            }else{
            	$model->user_foto=$prevImage;
            	$model->save();
            	$msg = 'sucees';
            }

            // $model->save();
            // $uploadedFile->saveAs(Yii::app()->basePath.'/../images/'.$fileName);

            return $this->redirect(Yii::app()->request->urlReferrer);
 
        }

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionPicture($id)
	{
		$model=$this->loadModel($id);

		$this->render('picture',array(
			'model'=>$model,
		));
	}

	public function actionPrint($id)
	{
		// $model=Gaji::model()->findAll('DATE_FORMAT (tgl,("%Y"))=:thn', array(':thn'=>$tahun));
		$expensesDetailId=ExpensesDetail::model()->find('expd_exp_id=:expd_exp_id AND expd_status=1', array(':expd_exp_id'=>$id));
		$expensesDetail=ExpensesDetail::model()->findAll('expd_exp_id=:expd_exp_id AND expd_status=1', array(':expd_exp_id'=>$id));
		$expenses=Expenses::model()->findByAttributes(array('exp_id'=>$id, 'exp_status'=>1));
		// $approvedName=Expenses::model()->findByAttributes(array('exp_id_pd'=>$expenses->exp_is_approved, 'exp_status'=>1));
		$karyawan=Karyawan::model()->find('id_pd=:id_pd', array(':id_pd'=>$expensesDetailId->expd_id_pd));
		$jabatan=Jabatan::model()->find('id_jabatan=:id_jabatan', array(':id_jabatan'=>$karyawan->id_jabatan));
		$company = Company::model()->find('cpy_id=:cpy_id', array(':cpy_id'=>1));
		// echo '<pre>';
		// print_r($jabatan->nm_jabatan);
		// echo '</pre>';
		// die;
		//extensions fpdf
		$pdf = new fpdf();
		//Deklarasi
		$size="a4";
		$header0="PT. SPARTAN ERAGON ASIA";
		$header="Bona Bisnis Center No. 8J Lt.2, Jl. Karang Tengah Raya, Jakarta Selatan";
		$judul="EXPENSES DETAIL";
		// $approvedName=$expenses->exp_is_approved;
		
		//Render
		$this->renderPartial('print',array(
			'pdf'=>$pdf,
			'judul'=>$judul,
			'header0'=>$header0,
			'header'=>$header,
			// 'model'=>$model,
			'expensesDetailId'=>$expensesDetailId,
			'expensesDetail'=>$expensesDetail,
			'expenses'=>$expenses,
			// 'approvedName'=>$approvedName,
			'karyawan'=>$karyawan,
			'jabatan'=>$jabatan,
			'size'=>$size,
			'company'=>$company
			//'modelbulan'=>$modelbulan,
			
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionSubmit($id)
	{
		$id_pd=Yii::app()->session->get('username');

		$model = Expenses::model()->findByAttributes(array('exp_id'=>$id));
		$model->exp_submit_status = 1;
		$model->exp_date_submit = date('Y-m-d H:i:s');
		if($model->SaveAttributes(array('exp_submit_status','exp_date_submit'))){
		return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			print_r($model->getErrors());
			throw new CHttpException('CNTRL 0 :' ,'ERROR SUBMIT EXPENSES');
		}
	}

	public function actionNotVerified($id)
	{
		$id_pd=Yii::app()->session->get('username');

		$model = Expenses::model()->findByAttributes(array('exp_id'=>$id));
		$model->exp_is_verified = 2;
		$model->exp_not_verified_by = $id_pd;
		$model->exp_not_verified_date = date('Y-m-d H:i:s');
		if($model->SaveAttributes(array('exp_is_verified','exp_not_verified_by','exp_not_verified_date'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			print_r($model->getErrors());
			throw new CHttpException('CNTRL 0 :' ,'ERROR UNVERIFIED EXPENSES');
		}
	}

	public function actionVerified($id)
	{
		$id_pd=Yii::app()->session->get('username');
		$karyawan=Karyawan::model()->findByAttributes(array('id_pd'=>$id_pd));

		$model = Expenses::model()->findByAttributes(array('exp_id'=>$id));
		// echo '<pre>';
		// print_r($karyawan);
		// echo '</pre>';
		// die;
		$model->exp_is_verified = 1;
		$model->exp_verified_by = $id_pd;
		$model->exp_verified_name = $karyawan->nm_pd;
		$model->exp_verified_date = date('Y-m-d H:i:s');
		if($model->SaveAttributes(array('exp_is_verified','exp_verified_by','exp_verified_name','exp_verified_date'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR VERIFIED EXPENSES');
		}
	}

	public function actionRejected($id)
	{
		$id_pd=Yii::app()->session->get('username');

		$model = Expenses::model()->findByAttributes(array('exp_id'=>$id));
		$model->exp_is_approved = 2;
		$model->exp_rejected_by = $id_pd;
		$model->exp_rejected_date = date('Y-m-d H:i:s');
		if($model->SaveAttributes(array('exp_is_approved','exp_rejected_by','exp_rejected_date'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR REJECTED EXPENSES');
		}
	}

	public function actionApproved($id)
	{
		$id_pd=Yii::app()->session->get('username');
		// $namaKaryawan=Yii::app()->session->get('namaKaryawan');
		$coaRule=ChartOfAccountRule::model()->find('coar_status=1');
		$karyawan=Karyawan::model()->findByAttributes(array('id_pd'=>$id_pd));
		$company = Company::model()->findByAttributes(array('cpy_id'=>1));
		$expensesDetail=ExpensesDetail::model()->findAll('expd_exp_id=:expd_exp_id AND expd_status=1', array(':expd_exp_id'=>$id));
		$sumDebet = 0;
		$sumCredit = 0;
		foreach($expensesDetail as $expd) {
			$sumDebet = $sumDebet+$expd->expd_debet;
			$sumCredit = $sumCredit+$expd->expd_debet;
			// echo '<pre>';
			// print_r($expd);
			// echo '</pre>';
		}
		// print_r($sum);
		// die;
		
		$model = Expenses::model()->findByAttributes(array('exp_id'=>$id));
		$model->exp_debet = 1;
		$model->exp_credit = 1;
		$model->exp_is_approved = 1;
		$model->exp_approved_by = $id_pd;
		$model->exp_approved_name = $karyawan->nm_pd;
		$model->exp_notes = isset($company->cpy_notes) ? $company->cpy_notes : '';
		// $model->exp_approved_name = isset($karyawan->nm_pd)?$karyawan->nm_pd:'';
		$model->exp_approved_date = date('Y-m-d H:i:s');

		$newCoad = new ChartOfAccountDetail;
		$newCoad->coad_coa_id 			= 77;
		$newCoad->coad_coar_id 			= $coaRule->coar_id;
		$newCoad->coad_coat_id 			= 7;
		$newCoad->coad_id_pd 			= $id_pd;
		$newCoad->coad_datetime 		= date('Y-m-d');
		$newCoad->coad_datetime_insert 	= date('Y-m-d H:i:s');
		$newCoad->coad_desc 			= $model->exp_desc;
		$newCoad->coad_debet 			= $sumDebet;
		$newCoad->coad_credit 			= $sumCredit;
		$newCoad->coad_is_jurnal 		= 1;
		$newCoad->coad_is_submit 		= 1;

		if($model->SaveAttributes(array('exp_is_approved','exp_approved_by','exp_approved_name','exp_approved_date','exp_notes'))) {
			if($newCoad->save()) {
				return $this->redirect(Yii::app()->request->urlReferrer);
			}else{
				print_r($newCoad->getErrors());
			}
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR APPROVED EXPENSES');
		}
	}

	public function actionDelete($id)
	{
		$id_pd=Yii::app()->session->get('username');
		$model = Expenses::model()->findByAttributes(array('exp_id'=>$id));
		$model->exp_datetime_delete = date('Y-m-d H:i:s');
		$model->exp_delete_by = $id_pd;
		$model->exp_status = 2;
		if($model->SaveAttributes(array('exp_datetime_delete','exp_delete_by','exp_status'))){
		return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR DELETED EXPENSES');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Expenses');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Expenses('search');

		$OnProcess=Expenses::model()->findAll('exp_is_verified=0 AND exp_is_approved=0 AND exp_status=1', array('select'=>'exp_id'));
		$notVerified=Expenses::model()->findAll('exp_is_verified=2 AND exp_is_approved=0 AND exp_status=1', array('select'=>'exp_id'));
		$verified=Expenses::model()->findAll('exp_is_verified=1 AND exp_is_approved=0 AND exp_status=1', array('select'=>'exp_id'));
		$rejected=Expenses::model()->findAll('exp_is_verified=1 AND exp_is_approved=2 AND exp_status=1', array('select'=>'exp_id'));
		$approved=Expenses::model()->findAll('exp_is_verified=1 AND exp_is_approved=1 AND exp_status=1', array('select'=>'exp_id'));
		$deleted=Expenses::model()->findAll('exp_status=2', array('select'=>'exp_id'));
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Expenses']))
			$model->attributes=$_GET['Expenses'];

		$this->render('admin',array(
			'model'=>$model,
			'OnProcess'=>$OnProcess,
			'notVerified'=>$notVerified,
			'verified'=>$verified,
			'rejected'=>$rejected,
			'approved'=>$approved,
			'deleted'=>$deleted,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Expenses the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Expenses::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Expenses $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='expenses-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
