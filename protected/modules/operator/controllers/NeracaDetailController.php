<?php

class NeracaDetailController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','generate','lock','unlock','print'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{
		$model=new NeracaDetail;
		$model->nrcd_nrc_id=$id;

		$neraca=Neraca::model()->find('nrc_id=:nrc_id', array(':nrc_id'=>$id));
		$neraca->nrc_status = 1;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NeracaDetail']))
		{
			$model->attributes=$_POST['NeracaDetail'];
			if($model->save())
				if($neraca->save()){
					$this->redirect(array('admin','id'=>$model->nrcd_nrc_id));
				}else{
					throw new Exception("Error Processing Request", print_r($neraca->getErrors()));
				}
		}

		$this->render('create',array(
			'model'=>$model,
			'neraca'=>$neraca,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$neraca=Neraca::model()->find('nrc_id=:nrc_id', array(':nrc_id'=>$id));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NeracaDetail']))
		{
			$model->attributes=$_POST['NeracaDetail'];
			if($model->save())
				$this->redirect(array('admin','id'=>$model->nrcd_nrc_id));
		}

		$this->render('update',array(
			'model'=>$model,
			'neraca'=>$neraca,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = NeracaDetail::model()->findByAttributes(array('nrcd_id'=>$id));
		$model->nrcd_status = 2;
		if($model->SaveAttributes(array('nrcd_status'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR DELETED NERACA DETAIL');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('NeracaDetail');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id)
	{
		$model=new NeracaDetail;
		$model->nrcd_nrc_id=$id;

		$neracaDetailId=NeracaDetail::model()->find('nrcd_nrc_id=:nrcd_nrc_id AND nrcd_status=1', array(':nrcd_nrc_id'=>$id));
		$neracaDetail=NeracaDetail::model()->findAll('nrcd_nrc_id=:nrcd_nrc_id AND nrcd_status=1', array(':nrcd_nrc_id'=>$id));
		$neraca=Neraca::model()->findByAttributes(array('nrc_id'=>$id, 'nrc_status'=>1));
		$model->findByAttributes(array('nrcd_nrc_id'=>$id));

		//declare var
		$varParent1=1;
		$varParent2=2;
		$varChild1=1;
		$varChild2=2;
		$varChild3=3;
		$varChild4=4;
		//parent
		$neracaParent1=NeracaParent::model()->find('nrcp_sort=:nrcp_sort AND nrcp_status=1',array(':nrcp_sort'=>1));
		$neracaParent2=NeracaParent::model()->find('nrcp_sort=:nrcp_sort AND nrcp_status=1',array(':nrcp_sort'=>2));
		$neracaParent3=NeracaParent::model()->find('nrcp_sort=:nrcp_sort AND nrcp_status=1',array(':nrcp_sort'=>3));
		$neracaParent4=NeracaParent::model()->find('nrcp_sort=:nrcp_sort AND nrcp_status=1',array(':nrcp_sort'=>4));
		//child
		$neracaChild1=NeracaChild::model()->find('nrcc_sort=:nrcc_sort AND nrcc_nrcp_id=:nrcc_nrcp_id AND nrcc_status=1',array(':nrcc_sort'=>1,'nrcc_nrcp_id'=>$varParent1));
		$neracaChild2=NeracaChild::model()->find('nrcc_sort=:nrcc_sort AND nrcc_nrcp_id=:nrcc_nrcp_id AND nrcc_status=1',array(':nrcc_sort'=>1,'nrcc_nrcp_id'=>$varParent2));
		$neracaChild3=NeracaChild::model()->find('nrcc_sort=:nrcc_sort AND nrcc_nrcp_id=:nrcc_nrcp_id AND nrcc_status=1',array(':nrcc_sort'=>2,'nrcc_nrcp_id'=>$varParent1));
		$neracaChild4=NeracaChild::model()->find('nrcc_sort=:nrcc_sort AND nrcc_nrcp_id=:nrcc_nrcp_id AND nrcc_status=1',array(':nrcc_sort'=>2,'nrcc_nrcp_id'=>$varParent2));
		//content
		$content1=NeracaDetail::model()->findAll(array('order'=>'nrcd_sort', 'condition'=>'nrcd_sort<=:nrcd_sort AND nrcd_desc!=:nrcd_desc AND nrcd_nrc_id=:nrcd_nrc_id AND nrcd_nrcp_id=:nrcd_nrcp_id AND nrcd_nrcc_id=:nrcd_nrcc_id AND nrcd_status=1', 'params'=>array(':nrcd_nrc_id'=>$id,'nrcd_nrcp_id'=>$varParent1,'nrcd_nrcc_id'=>$varChild1,'nrcd_desc'=>'','nrcd_sort'=>100))); //parent 1, child 1, desc notnull
		// auto generate empat row untuk keempat content dengan sort 101

		$content2=NeracaDetail::model()->findAll(array('order'=>'nrcd_sort', 'condition'=>'nrcd_sort<=:nrcd_sort AND nrcd_desc!=:nrcd_desc AND nrcd_nrc_id=:nrcd_nrc_id AND nrcd_nrcp_id=:nrcd_nrcp_id AND nrcd_nrcc_id=:nrcd_nrcc_id AND nrcd_status=1', 'params'=>array(':nrcd_nrc_id'=>$id,'nrcd_nrcp_id'=>$varParent2,'nrcd_nrcc_id'=>$varChild1,'nrcd_desc'=>'','nrcd_sort'=>100)));

		$content3=NeracaDetail::model()->findAll(array('order'=>'nrcd_sort', 'condition'=>'nrcd_sort<=:nrcd_sort AND nrcd_desc!=:nrcd_desc AND nrcd_nrc_id=:nrcd_nrc_id AND nrcd_nrcp_id=:nrcd_nrcp_id AND nrcd_nrcc_id=:nrcd_nrcc_id AND nrcd_status=1', 'params'=>array(':nrcd_nrc_id'=>$id,'nrcd_nrcp_id'=>$varParent1,'nrcd_nrcc_id'=>$varChild3,'nrcd_desc'=>'','nrcd_sort'=>100)));

		$content4=NeracaDetail::model()->findAll(array('order'=>'nrcd_sort', 'condition'=>'nrcd_sort<=:nrcd_sort AND nrcd_desc!=:nrcd_desc AND nrcd_nrc_id=:nrcd_nrc_id AND nrcd_nrcp_id=:nrcd_nrcp_id AND nrcd_nrcc_id=:nrcd_nrcc_id AND nrcd_status=1', 'params'=>array(':nrcd_nrc_id'=>$id,'nrcd_nrcp_id'=>$varParent2,'nrcd_nrcc_id'=>$varChild4,'nrcd_desc'=>'','nrcd_sort'=>100)));

		$contentGenerate=NeracaDetail::model()->find(array('condition'=>'nrcd_sort<=:nrcd_sort AND nrcd_desc=:nrcd_desc AND nrcd_nrc_id=:nrcd_nrc_id AND nrcd_nrcp_id=:nrcd_nrcp_id AND nrcd_nrcc_id=:nrcd_nrcc_id AND nrcd_status=1', 'params'=>array(':nrcd_nrc_id'=>$id,'nrcd_nrcp_id'=>$varParent1,'nrcd_nrcc_id'=>$varChild1,'nrcd_desc'=>'','nrcd_sort'=>100)));

		// echo '<pre>';
		// print_r($contentGenerate);
		// echo '</pre>';
		// die;

		$this->render('admin',array(
			'model'=>$model,
			'neracaDetail'=>$neracaDetail,
			'neracaDetailId'=>$neracaDetailId,
			'neraca'=>$neraca,
			'neracaParent1'=>$neracaParent1,
			'neracaParent2'=>$neracaParent2,
			'neracaParent3'=>$neracaParent3,
			'neracaParent4'=>$neracaParent4,
			'neracaChild1'=>$neracaChild1,
			'neracaChild2'=>$neracaChild2,
			'neracaChild3'=>$neracaChild3,
			'neracaChild4'=>$neracaChild4,
			'content1'=>$content1,
			'content2'=>$content2,
			'content3'=>$content3,
			'content4'=>$content4,
			'contentGenerate'=>$contentGenerate,
		));
	}

	public function actionGenerate($id, $date){
		$neracaDetailId = NeracaDetail::model()->find('nrcd_nrc_id=:nrcd_nrc_id AND nrcd_status=1', array(':nrcd_nrc_id'=>$id));
		$neracaDetailId->nrcd_is_generate = $neracaDetailId->nrcd_is_generate+1;
		$neracaDetailId->SaveAttributes(array('nrcd_is_generate'));

		$this->actionGenerateDetail($id, $date);
	}

	public function actionGenerateDetail($id, $date){
		// print('rere');die;
		$time = strtotime($date);
		$year = date('Y', $time);

		$noCoaKas 				= 100;
		$noTypeKas 				= 8;

		$noCoaBNIIDR 			= 101;
		$noTypeBNIIDR 			= 8;
		$noCoaBNIGIRO 			= 102;
		$noTypeBNIGIRO 			= 8;
		$noCoaBNIKSO 			= 103;
		$noTypeBNIKSO 			= 8;
		$noCoaBNIUSD 			= 104;
		$noTypeBNIUSD 			= 8;

		$noCoaPiutangUsaha 		= 106;
		$noTypePiutangUsaha 	= 9;
		$noCoaPiutangKaryawan 	= 107;
		$noTypePiutangKaryawan 	= 9;

		$noCoaPDD 				= 109;
		$noTypePDD 				= 11;

		$noCoaTnB 				= 213;
		$noTypeTnB 				= 5;
		// print($year);die;
		if(!empty($year)) {
			$Kas = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaKas, ':coad_coat_id'=>$noTypeKas, 'coad_datetime'=>$year)));

			$BNIIDR = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBNIIDR, ':coad_coat_id'=>$noTypeBNIIDR, 'coad_datetime'=>$year)));
			$BNIGIRO = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBNIGIRO, ':coad_coat_id'=>$noTypeBNIGIRO, 'coad_datetime'=>$year)));
			$BNIKSO = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBNIKSO, ':coad_coat_id'=>$noTypeBNIKSO, 'coad_datetime'=>$year)));
			$BNIUSD = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaBNIUSD, ':coad_coat_id'=>$noTypeBNIUSD, 'coad_datetime'=>$year)));

			$piutangUsaha = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaPiutangUsaha, ':coad_coat_id'=>$noTypePiutangUsaha, 'coad_datetime'=>$year)));
			$piutangKaryawan = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaPiutangKaryawan, ':coad_coat_id'=>$noTypePiutangKaryawan, 'coad_datetime'=>$year)));

			$pajakDibayarDimuka = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaPDD, ':coad_coat_id'=>$noTypePDD, 'coad_datetime'=>$year)));

			// $tanahBangunan = ChartOfAccountDetail::model()->find(array('order'=>'coad_id DESC', 'condition'=>'coad_coa_id=:coad_coa_id AND coad_coat_id=:coad_coat_id AND coad_status=1 AND DATE_FORMAT (coad_datetime,("%Y"))>=:coad_datetime AND coad_is_saldo>=1', 'params'=>array(':coad_coa_id'=>$noCoaTnB, ':coad_coat_id'=>$noTypeTnB, 'coad_datetime'=>$year)));
			// echo '<pre>';
			// print_r($BNIGIRO);
			// echo '</pre>';
			// die;
			$neracaDetailId=NeracaDetail::model()->find('nrcd_nrc_id=:nrcd_nrc_id AND nrcd_nrcp_id=1 AND nrcd_nrcc_id=1 AND nrcd_status=1', array(':nrcd_nrc_id'=>$id));//row ini yg dipake khusus utk insertan generate
			// echo '<pre>';
			// print_r($neracaDetailId);
			// echo '</pre>';
			// die;
			if(!empty($Kas)) {
				if($neracaDetailId->nrcd_kas !== $Kas->coad_saldo) {
					$neracaDetailId->nrcd_kas = $Kas->coad_saldo;
					if($neracaDetailId->SaveAttributes(array('nrcd_kas'))) {
						$return = true;
					}else{
						$return = false;
						print_r($neracaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate Kas');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($neracaDetailId->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'BNIIDR is empty');
			}

			if(!empty($BNIIDR)) {
				if($neracaDetailId->nrcd_bni_idr !== $BNIIDR->coad_saldo) {
					$neracaDetailId->nrcd_bni_idr = $BNIIDR->coad_saldo;
					if($neracaDetailId->SaveAttributes(array('nrcd_bni_idr'))) {
						$return = true;
					}else{
						$return = false;
						print_r($neracaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate BNIIDR');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($neracaDetailId->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'BNIIDR is empty');
			}

			if(!empty($BNIGIRO)) {
				if($neracaDetailId->nrcd_bni_giro !== $BNIGIRO->coad_saldo) {
					$neracaDetailId->nrcd_bni_giro = $BNIGIRO->coad_saldo;
					if($neracaDetailId->SaveAttributes(array('nrcd_bni_giro'))) {
						$return = true;
					}else{
						$return = false;
						print_r($neracaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate BNIGIRO');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($neracaDetailId->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'BNIGIRO is empty');
			}

			if(!empty($BNIKSO)) {
				if($neracaDetailId->nrcd_bni_kso !== $BNIKSO->coad_saldo) {
					$neracaDetailId->nrcd_bni_kso = $BNIKSO->coad_saldo;
					if($neracaDetailId->SaveAttributes(array('nrcd_bni_kso'))) {
						$return = true;
					}else{
						$return = false;
						print_r($neracaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate BNIKSO');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($neracaDetailId->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'BNIKSO is empty');
			}

			if(!empty($BNIUSD)) {
				if($neracaDetailId->nrcd_bni_usd !== $BNIUSD->coad_saldo) {
					$neracaDetailId->nrcd_bni_usd = $BNIUSD->coad_saldo;
					if($neracaDetailId->SaveAttributes(array('nrcd_bni_usd'))) {
						$return = true;
					}else{
						$return = false;
						print_r($neracaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate BNIUSD');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($neracaDetailId->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'BNIKSO is empty');
			}

			if(!empty($piutangUsaha)) {
				if($neracaDetailId->nrcd_piutang_usaha !== $piutangUsaha->coad_saldo) {
					$neracaDetailId->nrcd_piutang_usaha = $piutangUsaha->coad_saldo;
					if($neracaDetailId->SaveAttributes(array('nrcd_piutang_usaha'))) {
						$return = true;
					}else{
						$return = false;
						print_r($neracaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate piutangUsaha');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($neracaDetailId->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'piutangUsaha is empty');
			}

			if(!empty($piutangKaryawan)) {
				if($neracaDetailId->nrcd_piutang_kyw !== $piutangKaryawan->coad_saldo) {
					$neracaDetailId->nrcd_piutang_kyw = $piutangKaryawan->coad_saldo;
					if($neracaDetailId->SaveAttributes(array('nrcd_piutang_kyw'))) {
						$return = true;
					}else{
						$return = false;
						print_r($neracaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate piutangKaryawan');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($neracaDetailId->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'piutangKaryawan is empty');
			}

			if(!empty($pajakDibayarDimuka)) {
				if($neracaDetailId->nrcd_pdd !== $pajakDibayarDimuka->coad_saldo) {
					$neracaDetailId->nrcd_pdd = $pajakDibayarDimuka->coad_saldo;
					if($neracaDetailId->SaveAttributes(array('nrcd_pdd'))) {
						$return = true;
					}else{
						$return = false;
						print_r($neracaDetailId->getErrors());
						throw new CHttpException('CNTRL 2 :' ,'error generate pajakDibayarDimuka');
					}
				}else{
					$return = true;
				}
			}else{
				$return = false;
				print_r($neracaDetailId->getErrors());
				throw new CHttpException('CNTRL 1 :' ,'pajakDibayarDimuka is empty');
			}

			// if(!empty($tanahBangunan)) {
			// 	if($neracaDetailId->nrcd_tanah_bangunan !== $tanahBangunan->coad_saldo) {
			// 		$neracaDetailId->nrcd_tanah_bangunan = $tanahBangunan->coad_saldo;
			// 		if($neracaDetailId->SaveAttributes(array('nrcd_tanah_bangunan'))) {
			// 			$return = true;
			// 		}else{
			// 			$return = false;
			// 			print_r($neracaDetailId->getErrors());
			// 			throw new CHttpException('CNTRL 2 :' ,'error generate tanahBangunan');
			// 		}
			// 	}else{
			// 		$return = true;
			// 	}
			// }else{
			// 	$return = false;
			// 	print_r($neracaDetailId->getErrors());
			// 	throw new CHttpException('CNTRL 1 :' ,'tanahBangunan is empty');
			// }

		}else{
			throw new CHttpException('CNTRL 0 :' ,'year is empty');
		}

		return $this->redirect(Yii::app()->request->urlReferrer);
		
	}

	public function actionLock($id, $sum1, $sum2, $sum3, $sum4, $sum13, $sum24)
	{
		$model = Neraca::model()->findByAttributes(array('nrc_id'=>$id));
		$model->nrc_is_locked 	= 1;
		$model->nrc_sum_1 		= $sum1;
		$model->nrc_sum_2 		= $sum2;
		$model->nrc_sum_3 		= $sum3;
		$model->nrc_sum_4 		= $sum4;
		$model->nrc_sum_13 		= $sum13;
		$model->nrc_sum_24 		= $sum24;
		if($model->SaveAttributes(array('nrc_is_locked', 'nrc_sum_1', 'nrc_sum_2', 'nrc_sum_3', 'nrc_sum_4', 'nrc_sum_13', 'nrc_sum_24'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR LOCK NERACA DETAIL');
		}
	}

	public function actionUnlock($id)
	{
		$model = Neraca::model()->findByAttributes(array('nrc_id'=>$id));
		$model->nrc_is_locked = 0;
		if($model->SaveAttributes(array('nrc_is_locked'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR UNLOCK NERACA DETAIL');
		}
	}

	public function actionPrint($id)
	{
		$neracaDetailId = NeracaDetail::model()->find('nrcd_nrc_id=:nrcd_nrc_id AND nrcd_status=1', array(':nrcd_nrc_id'=>$id));
		$neracaDetail = NeracaDetail::model()->findAll('nrcd_nrc_id=:nrcd_nrc_id AND nrcd_status=1', array(':nrcd_nrc_id'=>$id));
		$neraca = Neraca::model()->findByAttributes(array('nrc_id'=>$id, 'nrc_status'=>1));
		$company = Company::model()->find('cpy_id=:cpy_id', array(':cpy_id'=>1));
		// echo '<pre>';
		// print_r($labaDetailId);
		// echo '</pre>';
		// die;

		$pdf = new fpdf();

		$size="a4";
		$header0="PT. SPARTAN ERAGON ASIA";
		// $header1="Bona Bisnis Center No. 8J Lt.2,";
		// $header2="Jl. Karang Tengah Raya, Jakarta Selatan";
		$dateNeraca = NeracaDetail::model()->find('nrcd_id=:nrcd_id', array(':nrcd_id'=>$neracaDetailId->nrcd_id));
		$judul="LAPORAN NERACA";

		$this->renderPartial('print',array(
			'pdf'=>$pdf,
			'judul'=>$judul,
			'header0'=>$header0,
			'dateNeraca'=>$dateNeraca,
			'company'=>$company,
			// 'header1'=>$header1,
			// 'header2'=>$header2,
			'size'=>$size,
			'neracaDetailId'=>$neracaDetailId,
			'neracaDetail'=>$neracaDetail,
			'neraca'=>$neraca,
			
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return NeracaDetail the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=NeracaDetail::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param NeracaDetail $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='neraca-detail-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
