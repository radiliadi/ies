<?php

class LabaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','updateStatus'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Laba;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Laba']))
		{
			$model->attributes=$_POST['Laba'];
			if($model->save())
				$newLabaDetail 				= new LabaDetail;
				$newLabaDetail->lbd_lb_id 	= $model->lb_id;
				$newLabaDetail->lbd_date 	= $model->lb_date;
				if($newLabaDetail->save()){
					$this->redirect(array('admin'));
				}else{
					print_r($newLabaDetail->getErrors());
				}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Laba']))
		{
			$model->attributes=$_POST['Laba'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	// public function actionDelete($id)
	// {
	// 	$this->loadModel($id)->delete();

	// 	// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
	// 	if(!isset($_GET['ajax']))
	// 		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	// }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('Laba');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Laba('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Laba']))
			$model->attributes=$_GET['Laba'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionUpdateStatus(){
        $postId	   	= Yii::app()->request->getPost('id');
        $return 	= false;
        $msg    	= 'Success';
        $error  	= (object)[];
        $data   	= (object)[];
        $id_pd 		= Yii::app()->session->get('username');
        
        if(!empty($postId)){
            $checkLaba = Laba::model()->find('lb_id=:lb_id', array(':lb_id' => $postId));
            if(!empty($checkLaba)){
            	if($checkLaba->lb_status == 1){
            		$changeStatus = 0;
            	}else if($checkLaba->lb_status !== 1 ){
            		$changeStatus = 1;
            	}
            	$checkLaba->lb_datetime_delete = date('Y-m-d H:i:s');
            	$checkLaba->lb_delete_by = $id_pd;
                $checkLaba->lb_status = $changeStatus;
                if($checkLaba->save()){
                    $return = true;
                    $msg = "Succes";
                }else{
                	print_r($checkLaba->getErrors());
                	$return = false;
                    $msg = "Failed to save data";
                }
            }else{
                $msg = "Data is Empty";
            }
        }
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return CJSON::encode($result);
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Laba the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Laba::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Laba $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='laba-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
