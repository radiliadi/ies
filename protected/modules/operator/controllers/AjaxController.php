<?php
/**
 * Ajax controller
 */
class AjaxController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            // 'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            // array('allow',  // allow all users to perform 'index' and 'view' actions
            //  'actions'=>array('index','view'),
            //  'users'=>array('*'),
            // ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update','admin','generate'),
                'users'=>array('@'),
            ),
            // array('allow', // allow admin user to perform 'admin' and 'delete' actions
            //  'actions'=>array('admin','delete'),
            //  'users'=>array('admin'),
            // ),
            // array('deny',  // deny all users
            //  'users'=>array('*'),
            // ),
        );
    }
    
    public function actionGetCategoryBySizeTypeId()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $html           = '<option value="">Please Sellect class Courses</option>';
        try
        {
            if(isset($post->szt_id)){
                $categorySizeType = CategorySizeType::find()->where('cst_szt_id =:sizeTypeId AND cst_status=1',[':sizeTypeId'=>$post->szt_id])->all();
                if(!empty($categorySizeType)){
                    $return = true;
                    $msg    = 'Success';
                    foreach ($categorySizeType as $resultData) {
                        $html .= '<option value="'.$resultData->category->ctg_id.'">'.$resultData->category->ctg_name.'</option>';
                    }
                }else{
                    $msg = 'Category not found';
                }
                $data = (object)['html'=>$html];
            }else{
                $msg = 'invalid parameter szt_id';
            }
        }catch (Exception $e)
        {
            $msg    = 'Sintac Error';
        }
        //$result = ['return'=>$return,'data'=>$data,'msg'=>$msg];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    public function actionGetBrandByVendorId()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Error, CNTRL : Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $html           = '<option> Select Brand</option>';
        try
        {
            if(isset($post->vendorId)){
                $brands = Brand::find()->where('brd_vdr_id =:vendorId AND brd_status=1',[':vendorId'=>$post->vendorId])->all();
                if(!empty($brands)){
                    $return = true;
                    $msg    = 'Success';
                    foreach ($brands as $resultData) {
                        $html .= '<option value="'.$resultData->brd_id.'">'.$resultData->brd_name.'</option>';
                    }
                }else{
                    $msg = 'Error, CNTRL2: Brand not found';
                }
                $data = (object)['html'=>$html];
            }else{
                $msg = 'Error, CNTRL1: invalid parameter vendorId';
            }
        }catch (Exception $e)
        {
            $msg    = 'Error, CNTRL0: Sintac Error';
        }
        //$result = ['return'=>$return,'data'=>$data,'msg'=>$msg];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    public function actionGetProductByBrandId()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Error, CNTRL: Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $html           = '';
        try
        {
            if(isset($post->brand_id)){
                $products = Product::find()->where('prd_brd_id =:brandId AND prd_status=1',[':brandId'=>$post->brand_id])->all();
                if(!empty($products)){
                    $return = true;
                    $msg    = 'Success';
                    foreach ($products as $resultData) {
                        $html .= '<option value="'.$resultData->prd_id.'">'.$resultData->prd_name.'</option>';
                    }
                }else{
                    $msg = 'Error, CNTR2: Product not found';
                }
                $data = (object)['html'=>$html];
            }else{
                $msg = 'Error, CNTRL1: invalid parameter brand_id';
            }
        }catch (Exception $e)
        {
            $msg    = 'Error, CNTRL0:Sintac Error';
        }
        //$result = ['return'=>$return,'data'=>$data,'msg'=>$msg];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    public function actionGetSizeByTypeId()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $htmlProduct    = '<option value="">Please Sellect a product</option>';
        $htmlSize       = '<option value="">Please Sellect a size</option>';

        try
        {
            if(isset($post->sizeTypeId)){
                $sizeType = SizeType::findOne($post->sizeTypeId);
                if(!empty($sizeType)){
                    $sizes = Size::find()->where('szs_szt_id =:sizeTypeId AND szs_status=1',[':sizeTypeId'=>$post->sizeTypeId])->all();
                    if(!empty($sizes)){
                        if(isset($post->brandId)){
                            $brand = Brand::findOne($post->brandId);
                            if(!empty($brand)){
                                if(isset($post->isNew)){
                                    if($post->isNew == 1){ //new product
                                        $return     = true;
                                        $msg        = 'Success';
                                    }else if($post->isNew == 2){ //existing product
                                        $products = Product::find()->where('prd_brd_id =:brandId AND prd_szt_id =:productTypeId',[':brandId'=>$brand->brd_id,':productTypeId'=>$sizeType->szt_id])->all();
                                        if(!empty($products)){
                                            $return     = true;
                                            $msg        = 'Success';
                                            
                                            foreach ($products as $productResult) {
                                                $htmlProduct .= '<option value="'.$productResult->prd_id.'">'.$productResult->prd_name.'</option>';
                                            }
                                            
                                        }else{
                                            $msg = 'product not found';
                                        }
                                    }
                                    if($return){
                                        $sizeCount  = count($sizes); 
                                        foreach ($sizes as $resultData) {
                                            $htmlSize .= '<option value="'.$resultData->szs_id.'">'.$resultData->szs_name.'</option>';
                                        }
                                        $data = (object)['htmlSize'=>$htmlSize,'htmlProduct'=>$htmlProduct,'count'=>$sizeCount];
                                    }
                                }else{
                                    $msg = 'please select insert type first';
                                }
                                
                            }else{
                                $msg = 'brand data not found';
                            }
                        }else{
                            $msg = 'invalid brand id';
                        }
                        
                    }else{
                        $msg = 'Size not found';
                    }
                    
                }else{
                    $msg = 'Size type data not found';
                }
                
            }else{
                $msg = 'invalid parameter szt_id';
            }
        }catch (Exception $e)
        {
            $msg    = 'Sintac Error';
        }
        //$result = ['return'=>$return,'data'=>$data,'msg'=>$msg];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
    }

    public function actionGetSizeById()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $html           = '<option value="">Please Sellect a product</option>';
        try
        {
            if(isset($post->size_type_id)){
                $sizes = Size::find()->where('szs_szt_id =:sizeTypeId AND szs_status=1',[':sizeTypeId'=>$post->size_type_id])->all();
                if(!empty($sizes)){
                    $return     = true;
                    $msg        = 'Success';
                    $sizeCount  = count($sizes); 
                    foreach ($sizes as $resultData) {
                        $html .= '<option value="'.$resultData->szs_id.'">'.$resultData->szs_name.'</option>';
                    }
                }else{
                    $msg = 'Size not found';
                }
                $data = (object)['html'=>$html,'count'=>$sizeCount];
            }else{
                $msg = 'invalid parameter szt_id';
            }
        }catch (Exception $e)
        {
            $msg    = 'Sintac Error';
        }
        //$result = ['return'=>$return,'data'=>$data,'msg'=>$msg];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    public function actionGetProductDetailById()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $html           = '<option value="">Please Sellect a product</option>';
        $htmlCurrentSize='';
        try
        {
            if(isset($post->productId)){
                $product = Product::findOne($post->productId);
                foreach ($product->activeItem as $productItem) {
                    if(!empty($productItem->size)){
                        $htmlCurrentSize .='<label class="col-lg-4 control-label">'.$productItem->size->szs_name.'</label><br/>';
                    }
                    
                }
                if(!empty($product)){
                    $return     = true;
                    $msg        = 'Success';
                    $data       = (object)['id'                 => $product->prd_id,
                                           'price'              => $product->prd_price,
                                           'brandId'            => $product->prd_brd_id,
                                           'type'               => $product->prd_szt_id,
                                           'imageUrl'           => $product->getImageDefaultThumbnail(), 
                                           'htmlCurrentSize'    => $htmlCurrentSize,
                                          ];
                }else{
                    $msg = 'product not found';
                }
                
            }else{
                $msg = 'invalid parameter szt_id';
            }
        }catch (Exception $e)
        {
            $msg    = 'Sintac Error';
        }
        //$result = ['return'=>$return,'data'=>$data,'msg'=>$msg];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    public function actionGetBannerCategoryById()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $html           = '<option value="">Please Sellect a product</option>';
        try
        {
            if(isset($post->bncId)){
                $bannerCategory = BannerCategory::findOne($post->bncId);
                if(!empty($bannerCategory)){
                    $return     = true;
                    $msg        = 'Success';
                    $data       = (object)['id'      => $bannerCategory->bnc_id,
                                           'widht'   => $bannerCategory->bnc_widht,
                                           'height'  => $bannerCategory->bnc_height,
                                           'ratio'   => $bannerCategory->bnc_ratio,
                                          ];
                }else{
                    $msg = 'bannerCategory not found';
                }
                
            }else{
                $msg = 'invalid parameter bannerCategoryId';
            }
        }catch (Exception $e)
        {
            $msg    = 'Sintac Error';
        }
        //$result = ['return'=>$return,'data'=>$data,'msg'=>$msg];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    

    public function actionGetCategoryByType()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $htmlCategory   = '<option value="">Please Select Type</option>';
        try
        {
            if(isset($post->bnt_id)){
                $bannerCategory = BannerCategory::find()->where('bnc_bnt_id =:bannerTypeId AND bnc_status=1',[':bannerTypeId'=>$post->bnt_id])->all();
                if(!empty($bannerCategory)){
                    foreach ($bannerCategory as $category) {
                        $htmlCategory   .= '<option value="'.$category->bnc_id.'">'.$category->bnc_name.'</option>';
                    }
                    $return = true;
                    $msg    = 'Success';
                    //$html   = '<option value="'.$bannerCategory->bnc_id.'">'.$bannerCategory->bnc_name.'</option>';
                    $data   = (object)['htmlCategory'  => $htmlCategory,
                                       ];
                }else{
                    $msg = 'Error, CNTLR2:Category not found';
                }
                //$data = (object)['html'=>$html];
            }else{
                $msg = 'Error, CNTLR1:invalid parameter bnt_id';
            }
        }catch (Exception $e)
        {
            $msg    = 'Error, CNTLR0: Sintac Error';
        }
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    public function actionBannerSetStatus()
    {
        $post           = (object)Yii::$app->request->post();
        $return         = false;
        $data           = (object)[];
        $msg            = 'Failed to Set Status';
        $error          = (object)[];
        $transaction    = Yii::$app->db->beginTransaction();
        try
        {
            if(!empty($post)){
                if(isset($post->bannerId)){
                    $banner = Banner::findOne($post->bannerId);
                    if(!empty($banner)){
                        $status = 1;
                        if($post->status == 1){
                            $status = 0;
                        }
                        $banner->bnr_status = $status;
                        if($banner->save()){
                            $return = true;
                            $msg    = 'Success';
                            $data   = $banner->bnr_status;
                            $transaction->commit();
                        }else{
                            echo '<pre>';
                            print_r($banner->getErrors());
                            echo '</pre>';
                            $msg    = 'Error, AJX4: Sintac Error';
                            $transaction->rollBack();
                        }
                    }else{
                        $msg = 'Error, AJX3: Can not found product item';
                        $transaction->rollBack();
                    }
                }else{
                    $msg = 'Error, AJX2: Can not found product item';
                    $transaction->rollBack();
                }
            }else{
                $msg = 'Error, AJX1: Can not found product item';
                $transaction->rollBack();
            }
        }catch(Exception $e){
            $msg = 'Error, AJX0: Can not found product item';
            $transaction->rollBack();
        }
        
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);

        
    }

    public function actionBannerCategorySetStatus()
    {
        $post           = (object)Yii::$app->request->post();
        $return         = false;
        $data           = (object)[];
        $msg            = 'Failed to Set Status';
        $error          = (object)[];
        $transaction    = Yii::$app->db->beginTransaction();
        try
        {
            if(!empty($post)){
                if(isset($post->bancatId)){
                    $category = BannerCategory::findOne($post->bancatId);
                    if(!empty($category)){
                        $status = 1;
                        if($post->status == 1){
                            $status = 0;
                        }
                        $category->bnc_status = $status;
                        if($category->save()){
                            $return = true;
                            $msg    = 'Success';
                            $data   = $category->bnc_status;
                            $transaction->commit();
                        }else{
                            echo '<pre>';
                            print_r($category->getErrors());
                            echo '</pre>';
                            $msg    = 'Error, AJX4: Sintac Error';
                            $transaction->rollBack();
                        }
                    }else{
                        $msg = 'Error, AJX3: Can not found product item';
                        $transaction->rollBack();
                    }
                }else{
                    $msg = 'Error, AJX2: Can not found product item';
                    $transaction->rollBack();
                }
            }else{
                $msg = 'Error, AJX1: Can not found product item';
                $transaction->rollBack();
            }
        }catch(Exception $e){
            $msg = 'Error, AJX0: Can not found product item';
            $transaction->rollBack();
        }
        
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);

        
    }

    public function actionLookbookTypeSetStatus()
    {
        $post           = (object)Yii::$app->request->post();
        $return         = false;
        $data           = (object)[];
        $msg            = 'Failed to Set Status';
        $error          = (object)[];
        $transaction    = Yii::$app->db->beginTransaction();
        try
        {
            if(!empty($post)){
                if(isset($post->typeId)){
                    $type = LookBookType::findOne($post->typeId);
                    if(!empty($type)){
                        $status = 1;
                        if($post->status == 1){
                            $status = 0;
                        }
                        $type->lbt_status = $status;
                        if($type->save()){
                            $return = true;
                            $msg    = 'Success';
                            $data   = $type->lbt_status;
                            $transaction->commit();
                        }else{
                            echo '<pre>';
                            print_r($type->getErrors());
                            echo '</pre>';
                            $msg    = 'Error, AJX4: Sintac Error';
                            $transaction->rollBack();
                        }
                    }else{
                        $msg = 'Error, AJX3: Can not found Lookbook Type';
                        $transaction->rollBack();
                    }
                }else{
                    $msg = 'Error, AJX2: Can not found Lookbook Type';
                    $transaction->rollBack();
                }
            }else{
                $msg = 'Error, AJX1: Can not found Lookbook Type';
                $transaction->rollBack();
            }
        }catch(Exception $e){
            $msg = 'Error, AJX0: Can not found Lookbook Type';
            $transaction->rollBack();
        }
        
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);

        
    }

    public function actionGetLookbookTypeById()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $html           = '<option value="">Please Select Type</option>';
        try
        {
            if(isset($post->lbtId)){
                $lookbookType = LookBookType::findOne($post->lbtId);
                if(!empty($lookbookType)){
                    $return     = true;
                    $msg        = 'Success';
                    $data       = (object)['id'      => $lookbookType->lbt_id,
                                           'width'   => $lookbookType->lbt_width,
                                           'height'  => $lookbookType->lbt_height,
                                           'rasio'   => $lookbookType->lbt_rasio,
                                          ];
                }else{
                    $msg = 'Error, AJX2: Lookbook Type not found';
                }
                
            }else{
                $msg = 'Error, AJX1: invalid parameter LookbookTypeId';
            }
        }catch (Exception $e)
        {
            $msg    = 'Error, AJX0: Sintac Error';
        }
        //$result = ['return'=>$return,'data'=>$data,'msg'=>$msg];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    public function actionGetLookbookByTypeId()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $htmlLookBook   = '<option value="">Please Select Type</option>';
        try
        {
            if(isset($post->lbtId)){
                $lookbookType = LookBookType::findOne($post->lbtId);
                if(!empty($lookbookType)){
                    if(!empty($lookbookType->lookBook)){
                        foreach ($lookbookType->lookBook as $lookBook) {
                            $htmlLookBook   .= '<option value="'.$lookBook->lob_id.'">'.$lookBook->lob_name.'</option>';
                        }
                    }
                    $return     = true;
                    $msg        = 'Success';
                    $data       = (object)['id'             => $lookbookType->lbt_id,
                                           'width'          => $lookbookType->lbt_width,
                                           'height'         => $lookbookType->lbt_height,
                                           'rasio'          => $lookbookType->lbt_rasio,
                                           'htmlLookBook'   => $htmlLookBook,
                                          ];
                }else{
                    $msg = 'Error, AJX2: Lookbook Type not found';
                }
                
            }else{
                $msg = 'Error, AJX1: invalid parameter LookbookTypeId';
            }
        }catch (Exception $e)
        {
            $msg    = 'Error, AJX0: Sintac Error';
        }
        //$result = ['return'=>$return,'data'=>$data,'msg'=>$msg];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    public function actionGetShippingAddress()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Error, AJX: Invalid parameter';
        $param          = 'shipping';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        try
        {
            if(isset($post->ods_id)){
                $ordersShipping = OrdersShipping::findOne($post->ods_id);
                if(!empty($ordersShipping)){
                    $return     = true;
                    $msg        = 'Success';
                    $data       = (object)['id'         => $ordersShipping->ods_id,
                                           'name'       => $ordersShipping->ods_name,
                                           'address'    => $ordersShipping->ods_address,
                                           'postcode'   => $ordersShipping->ods_postcode,
                                           'phone'      => $ordersShipping->ods_phone,
                                          ];
                }else{
                    $msg = 'Error, AJX2: orders shipping not found';
                }
                
            }else{
                $msg = 'Error, AJX1: invalid parameter OrdersShipping';
            }
        }catch (Exception $e)
        {
            $msg    = 'Error, AJX0: Sintac Error';
        }
        //$result = ['return'=>$return,'data'=>$data,'msg'=>$msg];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    public function actionGetRegionByCountryId()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $count          = 0;
        $html           = '<option value="">Please Sellect a region</option>';
        try
        {
            if(isset($post->id)){
                $regions = Region::find()->where('reg_cny_id =:countryId AND reg_status=1',[':countryId'=>$post->id])->all();
                if(!empty($regions)){
                    $return     = true;
                    $msg        = 'Success';
                    $count  = count($regions); 
                    foreach ($regions as $resultData) {
                        $html .= '<option value="'.$resultData->reg_id.'">'.$resultData->reg_name.'</option>';
                    }
                }else{
                    $msg = 'city not found';
                }
                $data = (object)['html'=>$html,'count'=>$count];
            }else{
                $msg = 'invalid parameter region Id';
            }
        }catch (Exception $e)
        {
            $msg    = 'Sintac Error';
        }
        //$result = ['return'=>$return,'data'=>$data,'msg'=>$msg];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    public function actionGetCityByRegionId()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $count          = 0;
        $html           = '<option value="">Please Sellect a city</option>';
        try
        {
            if(isset($post->id)){
                $citys = City::find()->where('cit_reg_id =:regionId AND cit_status=1',[':regionId'=>$post->id])->all();
                if(!empty($citys)){
                    $return     = true;
                    $msg        = 'Success';
                    $sizeCount  = count($citys); 
                    foreach ($citys as $resultData) {
                        $html .= '<option value="'.$resultData->cit_id.'">'.$resultData->cit_name.'</option>';
                    }
                }else{
                    $msg = 'city not found';
                }
                $data = (object)['html'=>$html,'count'=>$sizeCount];
            }else{
                $msg = 'invalid parameter region Id';
            }
        }catch (Exception $e)
        {
            $msg    = 'Sintac Error';
        }
        //$result = ['return'=>$return,'data'=>$data,'msg'=>$msg];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    public function actionGetAreaByCityId()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $count          = 0;
        $html           = '<option value="">Please Sellect a area</option>';
        try
        {
            if(isset($post->id)){
                $areas = Area::find()->where('ara_cit_id =:cityId AND ara_status=1',[':cityId'=>$post->id])->all();
                if(!empty($areas)){
                    $return     = true;
                    $msg        = 'Success';
                    $count  = count($areas); 
                    foreach ($areas as $resultData) {
                        $html .= '<option value="'.$resultData->ara_id.'">'.$resultData->ara_name.'</option>';
                    }
                }else{
                    $msg = 'city not found';
                }
                $data = (object)['html'=>$html,'count'=>$count];
            }else{
                $msg = 'invalid parameter region Id';
            }
        }catch (Exception $e)
        {
            $msg    = 'Sintac Error';
        }
        //$result = ['return'=>$return,'data'=>$data,'msg'=>$msg];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    public function actionTestGenerate(){
        $return = false;
        $data   = [];
        $msg    = 'invalid generate image';
        $error  = [];
        $code   = '12345678ADSE4660';
        if(!empty($code)){
            $newName    = time().'.jpg';
            $pathTemp   = './runtime/tmp/';
            $imagePath  = 'img/barcode-jne-template.jpg';
            $fontUrl    = 'css/font/IDAutomationHC39M.ttf';
            $s3Path     = 'images/jnebarcode/';
            if (!file_exists($pathTemp)) {
                mkdir($pathTemp, 0777, true);
            }
            $im = imagecreatefromjpeg($imagePath);  

            //The numbers are the RGB values of the color you want to use 
            $black = ImageColorAllocate($im, 000, 000, 000); 

            //The canvas's (0,0) position is the upper left corner 
            //So this is how far down and to the right the text should start 
            $start_x = 40; 
            $start_y = 80; 

            //This writes your text on the image in 12 point using verdana.ttf 
            //For the type of effects you quoted, you'll want to use a truetype font 
            //And not one of GD's built in fonts. Just upload the ttf file from your 
            //c: windows fonts directory to your web server to use it. 
            imagettftext($im, 8, 0, $start_x, $start_y, $black, $fontUrl, $code); 

            //Creates the jpeg image and sends it to the browser 
            //100 is the jpeg quality percentage 
            imagejpeg($im, $pathTemp.$newName, 100);
            
            if(S3::Upload($s3Path,$pathTemp,$newName)){
                $return = true;
                $data = (object)['image'=>$newName];
                imagedestroy($im);
            }

             

        }
       
    }

    public function actionGenerateJnePrice()
    {
        $transaction    = Yii::$app->db->beginTransaction();
        try
        {
            $id = 5;
            $cityFrom = City::find()->where('cit_status=1 AND cit_generate_status=0 AND cit_jne_origin_code is not Null')->one();
            //$cityFrom = City::findOne($id);
            if(!empty($cityFrom)){
                $cityTo = City::find()->where('cit_status=1 AND cit_get_price_status =0 AND cit_jne_origin_code is not Null')->one();
                $updatePriceCity = true;
                if(!empty($cityTo)){
                    
                    //foreach ($cityTo as $citydestination) {

                        $username = Yii::$app->params['usernameJne'];
                        $api_key  = Yii::$app->params['apiKeyJne'];
                        $from     = $cityFrom->cit_jne_origin_code;
                        $thru     = $cityTo->cit_jne_origin_code;
                        $weight   = 1;
                        $data="username=".$username."&api_key=".$api_key."&from=".$from."&thru=".$thru."&weight=".$weight."";
                        $ch = curl_init(); //inisialisasi curl
                        curl_setopt($ch, CURLOPT_URL, Yii::$app->params['apiJneGenereateShippingPrice']);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
                        $result = curl_exec($ch);
                        $resultDecode = Json::decode($result);
                        // echo '<pre>';
                        // print_r($resultDecode);
                        // echo '</pre>';
                        // die;
                        if(!empty($resultDecode)){
                            if(isset($resultDecode['price'])){
                                $prices = $resultDecode['price']; 
                                foreach ($prices as $arrPrice) {
                                    $objectPrice = (object)$arrPrice;
                                    //echo 'code = '.trim($objectPrice['service_code']).'<br/>';
                                    // echo '<pre>';
                                    // print_r($objectPrice);
                                    // echo '</pre>';
                                    if(trim($objectPrice->service_code) == 'CTCYES15'){
                                        $service = ShippingType::findOne(4);
                                    }elseif (trim($objectPrice->service_code) == 'CTCOKE15') {
                                        $service = ShippingType::findOne(3);
                                    }elseif (trim($objectPrice->service_code) == 'CTC15') {
                                        $service = ShippingType::findOne(2);
                                    }else{
                                        //echo "rene";
                                        $service = ShippingType::find()->where('sit_code = :codeJne',[':codeJne'=>strtoupper(trim($objectPrice->service_code))])->one();
                                    }
                                    //$service = ShippingType::find()->where('sit_code = :codeJne',[':codeJne'=>strtoupper(trim($objectPrice->service_code))])->one();
                                    if(!empty($service)){
                                        //echo 'ada<br/>';
                                        $temShippingPrice = ShippingPrice::find()->where('sir_cit_id=:cityDestination AND sir_from_cit_id = :cityFrom AND sir_sit_id = :serviceId',[':cityDestination'=>$cityTo->cit_id,':cityFrom'=>$cityFrom->cit_id,':serviceId'=>$service->sit_id])->one();
                                        if(empty($temShippingPrice)){
                                            //echo 'masuk<br/>';
                                            $shippingPrice = New ShippingPrice;
                                            $shippingPrice->sir_sip_id      = 2;//JNE
                                            $shippingPrice->sir_sit_id      = $service->sit_id;//JNE
                                            $shippingPrice->sir_cny_id      = $cityTo->region->reg_cny_id;//JNE
                                            $shippingPrice->sir_reg_id      = $cityTo->region->reg_id;//JNE
                                            $shippingPrice->sir_cit_id      = $cityTo->cit_id;//JNE
                                            $shippingPrice->sir_price       = $objectPrice->price;//JNE
                                            $shippingPrice->sir_from_cit_id = $cityFrom->cit_id;//JNE
                                            
                                            if($shippingPrice->save(false)){
                                                //echo 'ok<br/>';
                                            }else{
                                                $updatePriceCity = false;
                                                // echo '<pre>';
                                                // print_r($shippingPrice->getErrors());
                                                // echo '</pre>';
                                            }
                                        }
                                    }else{
                                        //echo '1';
                                    }
                                }
                                   
                            }else{
                                //echo '2';
                            }
                        }else{
                            //echo 'result = '.$result;
                        }
                    //}

                    $cityTo->cit_get_price_status =1;
                    if(!$cityTo->save(false)){
                        // echo '<pre>';
                        // print_r($cityTo->getErrors());
                        // echo '</pre>';
                    }

                    if($updatePriceCity){
                        $transaction->commit();
                        return true;
                    }
                }else{
                    $cityFrom->cit_generate_status =1;
                    if($cityFrom->save(false)){
                        City::updateAll(['cit_get_price_status' => 0]);
                        $transaction->commit();
                        return true;
                        // echo '<pre>';
                        // print_r($cityFrom->getErrors());
                        // echo '</pre>';
                    }
                    //echo 'empty city and start from 0 egain';
                }

                // $cityFrom->cit_generate_status =1;
                // if($cityFrom->save(false)){
                   
                // }else{
                //     echo '<pre>';
                //     print_r($cityFrom->getErrors());
                //     echo '</pre>';
                // }
                  
            }else{
                return false;
            } 
        }catch(Exception $e){
            $msg = 'Error, PRD0: invalid object product';
            //$transaction->rollBack();
            return false;
        }
    }

    public function actionGetPushNotificationCategoryByTypeId()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $html           = '<option value="">Please Sellect a category</option>';
        try
        {
            if(isset($post->id)){
                $pushNotificationCategory = PushNotificationCategory::find()->where('pnc_pnt_id =:typeId AND pnc_status=1',[':typeId'=>$post->id])->all();
                if(!empty($pushNotificationCategory)){
                    $return = true;
                    $msg    = 'Success';
                    foreach ($pushNotificationCategory as $resultData) {
                        $html .= '<option value="'.$resultData->pnc_id.'">'.$resultData->pnc_name.'</option>';
                    }
                }else{
                    $msg = 'Category not found';
                }
                $data = (object)['html'=>$html];
            }else{
                $msg = 'invalid parameter id';
            }
        }catch (Exception $e)
        {
            $msg    = 'Sintac Error';
        }
        $data = ['html'=>$html];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }


     public function actionGetPushNotificationCategoryByCategoryId()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $html           = '<option value="">Please Sellect a category</option>';
        try
        {
            if(isset($post->id)){
                $pushNotificationCategory = PushNotificationCategory::find()->where('pnc_pnt_id =:typeId AND pnc_status=1',[':typeId'=>$post->id])->all();
                if(!empty($pushNotificationCategory)){
                    $return = true;
                    $msg    = 'Success';
                    foreach ($pushNotificationCategory as $resultData) {
                        $html .= '<option value="'.$resultData->pnc_id.'">'.$resultData->pnc_name.'</option>';
                    }
                }else{
                    $msg = 'Category not found';
                }
                $data = (object)['html'=>$html];
            }else{
                $msg = 'invalid parameter id';
            }
        }catch (Exception $e)
        {
            $msg    = 'Sintac Error';
        }
        $data = ['html'=>$html];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    public function actionGetPushNotificationTemplateByCategoryId()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $html           = '<option value="">Please Sellect a template</option>';
        $htmlFk         = '';
        try
        {
            if(isset($post->id)){
                $pushNotificationCategory = PushNotificationTemplate::find()->where('pntp_pnc_id =:categoryId AND pntp_status=1',[':categoryId'=>$post->id])->all();
                if(!empty($pushNotificationCategory)){
                    $return = true;
                    $msg    = 'Success';
                    foreach ($pushNotificationCategory as $resultData) {
                        $html .= '<option value="'.$resultData->pntp_id.'">'.$resultData->pntp_title.'</option>';
                    }
                    if($post->id == 2){
                        $htmlFk         = '<option value="">Please Sellect a product category </option>';
                        $category   = Category::find()->where('ctg_status=1')->all();
                        foreach ($category as $fkData) {
                            $htmlFk .= '<option value="'.$fkData->ctg_id.'">'.$fkData->ctg_name.'</option>';
                        }

                    }
                    if($post->id == 3){
                        $htmlFk         = '<option value="">Please Sellect a product brand </option>';
                        $category   = Brand::find()->where('brd_status=1')->all();
                        foreach ($category as $fkData) {
                            $htmlFk .= '<option value="'.$fkData->brd_id.'">'.$fkData->brd_name.'</option>';
                        }

                    }
                }else{
                    $msg = 'template not found';
                }
                $data = (object)['html'=>$html];
            }else{
                $msg = 'invalid parameter id';
            }
        }catch (Exception $e)
        {
            $msg    = 'Sintac Error';
        }
        $data = ['html'=>$html,'htmlFk'=>$htmlFk];
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    public function actionGetCompanyDataById()
    {
        $post           = (object)Yii::$app->request->post();
        $msg            = 'Invalid parameter';
        $param          = 'id';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        try
        {
            if(isset($post->id)){
                $company = Company::findOne($post->id);
                if(!empty($company)){
                    $return = true;
                    $msg    = 'Success';
                    $data   = (object)['address'        => $company->com_address,
                                       'countryName'    => $company->country->cny_name,
                                       'regionName'     => $company->region->reg_name,
                                       'cityName'       => $company->city->cit_name,
                                       'areaName'       => $company->area->ara_name,
                                       'posCode'        => $company->com_postal_code,
                                       ];
                }else{
                    $msg = 'template not found';
                }
            }else{
                $msg = 'invalid parameter id';
            }
        }catch (Exception $e)
        {
            $msg    = 'Sintac Error';
        }
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    public function actionGetDeviceOrderCount($date)
    {
        $model = Orders::topOrderDevice($date);
        $data = [
            ['gender', 'gender']
        ];
        foreach ($model as $row) {
            $lbl = $row['label'];
            $data[] = [
//                0 => ($row['label'] == 'M') ? 'Male ' : 'Female ',
                0 => $lbl,
                1 => (int) $row['value']
            ];
        }
        return Json::encode($data);
    }

}
