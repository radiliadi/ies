<?php

class ArusKasController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','updateStatus'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new ArusKas;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ArusKas']))
		{
			$model->attributes=$_POST['ArusKas'];
			if($model->save())
				$newArusKasDetail 				= new ArusKasDetail;
				$newArusKasDetail->akd_ak_id 	= $model->ak_id;
				$newArusKasDetail->akd_date 	= $model->ak_date;
				if($newArusKasDetail->save()){
					$this->redirect(array('admin'));
				}else{
					print_r($newArusKasDetail->getErrors());
				}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ArusKas']))
		{
			$model->attributes=$_POST['ArusKas'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	// public function actionDelete($id)
	// {
	// 	$id_pd=Yii::app()->session->get('username');
	// 	$model = ArusKas::model()->findByAttributes(array('ak_id'=>$id));
	// 	$model->ak_datetime_delete = date('Y-m-d H:i:s');
	// 	$model->ak_delete_by = $id_pd;
	// 	$model->ak_status = 2;
	// 	if($model->SaveAttributes(array('ak_datetime_delete','ak_delete_by','ak_status'))){
	// 		return $this->redirect(Yii::app()->request->urlReferrer);
	// 	}else{
	// 		throw new CHttpException('CNTRL 0 :' ,'ERROR ARUS KAS DELETE');
	// 	}
	// }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ArusKas');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ArusKas('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ArusKas']))
			$model->attributes=$_GET['ArusKas'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionUpdateStatus(){
        $postId	   	= Yii::app()->request->getPost('id');
        $return 	= false;
        $msg    	= 'Success';
        $error  	= (object)[];
        $data   	= (object)[];
        $id_pd 		= Yii::app()->session->get('username');
        
        if(!empty($postId)){
            $checkArusKas = ArusKas::model()->find('ak_id=:ak_id', array(':ak_id' => $postId));
            if(!empty($checkArusKas)){
            	if($checkArusKas->ak_status == 1){
            		$changeStatus = 0;
            	}else if($checkArusKas->ak_status !== 1 ){
            		$changeStatus = 1;
            	}
            	$checkArusKas->ak_datetime_delete = date('Y-m-d H:i:s');
            	$checkArusKas->ak_delete_by = $id_pd;
                $checkArusKas->ak_status = $changeStatus;
                if($checkArusKas->save()){
                    $return = true;
                    $msg = "Succes";
                }else{
                	print_r($checkArusKas->getErrors());
                	$return = false;
                    $msg = "Failed to save data";
                }
            }else{
                $msg = "Data is Empty";
            }
        }
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return CJSON::encode($result);
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ArusKas the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ArusKas::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ArusKas $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='arus-kas-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
