<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
'id'=>'jabatan-form',
// 'enableAjaxValidation'=>true,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-md-1">
    </div>
        <div class="col-md-10">
            <form class="form-horizontal">
                <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Nama Jabatan</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'nm_jabatan',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Username"));?>
                                        <?php echo $form->error($model,'username'); ?>

                                    </div>                                            
                                </div>
                            </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-12 control-label">Gaji Pokok</label>
                                    <div class="col-md-6 col-xs-12">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-dollar"></span></span>
                                            <?php echo $form->textField($model,'pokok_jabatan',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Gaji Pokok"));?>
                                            <?php echo $form->error($model,'password'); ?>
                                        </div>                                            
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-12 control-label">Tunjangan</label>
                                    <div class="col-md-6 col-xs-12">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-dollar"></span></span>
                                            <?php echo $form->textField($model,'tunj_jabatan',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Tunjangan"));?>
                                            <?php echo $form->error($model,'password'); ?>
                                        </div>                                            
                                    </div>
                                </div>
                        </div>
                            <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
    									'buttonType'=>'submit',
    									'context'=>'primary',
    									'label'=>$model->isNewRecord ? 'Create' : 'Save',
    								)); ?>
                                </div>
                            </div>
                </div>
        </form>
    </div>
    <div class="col-md-1">  
    </div>
</div> 
<?php $this->endWidget(); ?>