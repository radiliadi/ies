<?php $this->widget('booster.widgets.TbBreadcrumbs',
array(
    'links' => array('Settings',
    'Finance',
    'Jabatan',
    ),
)
);
?>
<div class="row">

	<div class="page-title">                    
                    <h2><span class="fa fa-users"></span> Pengaturan Jabatan</h2>
                </div>



<div class="col-md-12">
<div class="panel panel-default">
                                <div class="panel-heading">
									<div class="btn-group pull-right">
                                        <?php echo CHtml::link('<i class="fa fa-plus"></i> Tambah', 'create', array('class'=>'btn btn btn-primary pull-right')); ?>
	
                                    </div> 
                                </div>

   
    <div class="panel-body">

<?php echo CHtml::beginForm(array('mahasiswa/exportExcel')); ?>
<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'mahasiswa-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'responsiveTable' => true,
'columns'=>array(
		array(
		'header' => 'No.',
		'htmlOptions' => array(
			'width' => '20px',
			'style' => 'text-align:center'
		),
		'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		),
		// 'id_jabatan',
		'nm_jabatan',
    array(
      'name'=>'pokok_jabatan',
      'value'=>function($data){
        return 'Rp.'.number_format($data->pokok_jabatan,0);
      }
    ),
    array(
      'name'=>'tunj_jabatan',
      'value'=>function($data){
        return 'Rp.'.number_format($data->tunj_jabatan,0);
        // return 'Rp.'.number_format($data->tunj_jabatan,2);
      }
    ),
		// array(
  //                               'class' => 'CButtonColumn',
  //                               'template' => '{viewx}',
  //                               'header' => '',
  //                               'htmlOptions' => array(
  //                                   'style' => 'white-space: nowrap'
  //                               ),
  //                               'buttons' => array(
  //                                   'viewx' => array(
  //                                       'label' => '',
  //                                       'url' => 'Yii::app()->createAbsoluteUrl("operator/jabatan/update", array(
  //                                           "id" => $data->id_jabatan
  //                                       ))',
  //                                       'options' => array(
  //                                           'class' => 'btn btn-success fa fa-pencil',
  //                                           'title' => 'Update Jabatan'
  //                                       ),
  //                                   ),
  //                               )
  //                           ),
		// array(
  //                               'class' => 'CButtonColumn',
  //                               'template' => '{deletex}',
  //                               // 'deleteConfirmation'=>"js:'Record with ID '+$(this).parent().parent().children(':first-child').text()+' will be deleted! Continue?'",
  //                               'header' => '',
  //                               'htmlOptions' => array(
  //                                   'style' => 'white-space: nowrap'
  //                               ),
  //                               'buttons' => array(
  //                                   'deletex' => array(
  //                                       'label' => '',
  //                                       'url' => 'Yii::app()->createAbsoluteUrl("operator/jabatan/delete", array(
  //                                           "id" => $data->id_jabatan
  //                                       ))',
  //                                       'options' => array(
  //                                           'class' => 'btn btn-danger fa fa-	',
  //                                           'title' => 'Hapus Jabatan'
  //                                       ),
  //                                   ),
  //                               )
  //                           ),
		// array(
	   // 'name' => 'stat_pd',
	   // 'filter' => Karyawan::listStatus()
        // ),
		// 'id_jabatan',
		// array(
                   // 'name' => 'id_jabatan',
                   // 'filter' => Karyawan::listJabatan()
        // ),
		// 'divisi',
		// array(
                   // 'name' => 'divisi',
                   // 'filter' => Karyawan::listDivisi()
        // ),
		// 'lokasi_kerja',
		// array(
                   // 'name' => 'lokasi_kerja',
                   // 'filter' => Karyawan::listLokker()
        // ),
		// array(
			// 'name'=>'jk',
			// 'value'=>'$data->jk == "P" ? "Perempuan" :  "Laki-Laki"',
			 // 'filter'=>array(    
					// 'L'=>'laki-laki',
                    // 'P'=>'Perempuan',
                // )
		// ),
array(
'class'=>'booster.widgets.TbButtonColumn',
'template'=>'{update}',
),
array(
'class'=>'booster.widgets.TbButtonColumn',
'afterDelete'=>'function(link,success,data){ if(success) alert("Delete completed successfully"); }',
'template'=>'{delete}',
),
),
)); ?>


			</div>
		</div>
	</div>
  </div>