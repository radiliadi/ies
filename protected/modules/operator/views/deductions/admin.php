<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Employee' ,
        // 'Semua' => array('Semua'=>'admin', 
        // 'Lalai'=>array('asd'
          // )
        ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-minus-square"></span> <?=$title;?> </h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?=$title;?> List</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
            	<div class="btn-group pull-right">
                </div> 
            </div>
            <div class="panel-body">
			<?php echo CHtml::beginForm(array('deductions/exportExcel')); ?>
			<?php $this->widget('booster.widgets.TbGridView',array(
			'id'=>'deductions-grid',
			// 'type' => ' condensed',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'responsiveTable' => true,
			'pager' => array(
			    'header' => '',
			    'footer' => '',
			    'cssFile' => false,
			    'selectedPageCssClass' => 'active',
			    'hiddenPageCssClass' => 'disabled',
			    'firstPageCssClass' => 'previous',
			    'lastPageCssClass' => 'next',
			    'maxButtonCount' => 5,
			    'firstPageLabel'=> '<i class="fa fa-angle-double-left"></i>',
			    'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
			    'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
			    'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
			    'htmlOptions' => array(
			        'class' => 'dataTables_paginate paging_bootstrap pagination pull-right',
			        'align' => 'center',
			    )
			),
			// 'summaryText' => '',
			// 'emptyText'=>'Belum ada thread pada kategori ini',
			'columns'=>array(
				array(
					'header' => 'No.',
					'htmlOptions' => array(
						'width' => '20px',
						'style' => 'text-align:center'
					),
					'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
					// 'class'=>'booster.widgets.TbTotalSumColumn',
				),
				'ded_id_pd',
				array(
			      'name'=>'ded_loan',
			      'value'=>function($data){
			      	if($data->ded_loan == 0){
			        	return ' -';
			        }else{
			        	return 'Rp. '.number_format($data->ded_loan,0);
			        }
			      }
			    ),
			    array(    
	              'header'=>'',
	              'type'=>'raw', 
	              'value' =>function($data) {
	                          return CHtml::link('<i class="fa fa-pencil"></i>',['/operator/deductions/update/id/'.$data->ded_id],['class' => 'btn btn-info', 'title' => 'Update']);
	                      },
	          	),
				// 'ded_loann',
				// 'ded_loannn',
				// 'ded_status',
				// array(
				// 'class'=>'booster.widgets.TbButtonColumn',
				// 'template'=>'{update}',
				// ),
			),
			)); 
            ?>
            <br></br>
            <hr></hr>
            <div class="col-md-6">
                <div class="col-md-3">
                <select name="fileType" class="form-control">
                    <option value="Excel">EXCEL 5 (xls)</option>
                    <!-- <option value="CSV">CSV</option> -->
                </select>
                </div>
                <div class="col-md-3">
                <?php echo CHtml::submitButton('Export',array(
                            'buttonType' => 'submit',
                            'context'=>'primary',
                            'label'=>'Export Excel',
                            'class'=>'btn btn-primary',
                        )); 
                ?>
                <?php echo CHtml::endForm(); ?>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>