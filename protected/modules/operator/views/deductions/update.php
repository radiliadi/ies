<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Employee' ,
        'Finances',
        // 'Semua' => array('Semua'=>'admin', 
        'Deductions'=>array('admin'
          ),
        $model->ded_id_pd,
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update Deductions : #<?=$model->ded_id_pd;?></h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>