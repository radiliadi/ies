<?php
/* @var $this PagesizeController */
/* @var $model Pagesize */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_ps'); ?>
		<?php echo $form->textField($model,'id_ps'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jml_ps'); ?>
		<?php echo $form->textField($model,'jml_ps'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->