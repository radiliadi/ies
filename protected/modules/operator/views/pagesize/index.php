<?php
/* @var $this PagesizeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pagesizes',
);

$this->menu=array(
	array('label'=>'Create Pagesize', 'url'=>array('create')),
	array('label'=>'Manage Pagesize', 'url'=>array('admin')),
);
?>

<h1>Pagesizes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
