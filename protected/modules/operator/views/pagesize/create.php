<?php
/* @var $this PagesizeController */
/* @var $model Pagesize */

$this->breadcrumbs=array(
	'Pagesizes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Pagesize', 'url'=>array('index')),
	array('label'=>'Manage Pagesize', 'url'=>array('admin')),
);
?>

<h1>Create Pagesize</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>