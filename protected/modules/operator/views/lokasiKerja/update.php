<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Settings' ,
        'Employee',
        // 'Semua' => array('Semua'=>'admin', 
        'Work Place'=>array('admin'
          ),
        'Update',
        $model->id_lokasi_kerja,
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update Work Place : #<?=$model->id_lokasi_kerja;?></h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>