<?php
/* @var $this ModalController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Modals',
);

$this->menu=array(
	array('label'=>'Create Modal', 'url'=>array('create')),
	array('label'=>'Manage Modal', 'url'=>array('admin')),
);
?>

<h1>Modals</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
