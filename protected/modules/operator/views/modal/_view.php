<?php
/* @var $this ModalController */
/* @var $data Modal */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->mdl_id), array('view', 'id'=>$data->mdl_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_desc')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_desc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_date')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_is_checked')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_is_checked); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_checked_by')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_checked_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_datetime_checked')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_datetime_checked); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_datetime_insert')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_datetime_insert); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_is_locked')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_is_locked); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_status')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_status); ?>
	<br />

	*/ ?>

</div>