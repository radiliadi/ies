<?php
/* @var $this ModalController */
/* @var $model Modal */

$this->breadcrumbs=array(
	'Modals'=>array('index'),
	$model->mdl_id,
);

$this->menu=array(
	array('label'=>'List Modal', 'url'=>array('index')),
	array('label'=>'Create Modal', 'url'=>array('create')),
	array('label'=>'Update Modal', 'url'=>array('update', 'id'=>$model->mdl_id)),
	array('label'=>'Delete Modal', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->mdl_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Modal', 'url'=>array('admin')),
);
?>

<h1>View Modal #<?php echo $model->mdl_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'mdl_id',
		'mdl_desc',
		'mdl_date',
		'mdl_is_checked',
		'mdl_checked_by',
		'mdl_datetime_checked',
		'mdl_datetime_insert',
		'mdl_is_locked',
		'mdl_status',
	),
)); ?>
