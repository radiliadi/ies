<?php
/* @var $this ModalController */
/* @var $model Modal */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'mdl_id'); ?>
		<?php echo $form->textField($model,'mdl_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_desc'); ?>
		<?php echo $form->textArea($model,'mdl_desc',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_date'); ?>
		<?php echo $form->textField($model,'mdl_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_is_checked'); ?>
		<?php echo $form->textField($model,'mdl_is_checked'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_checked_by'); ?>
		<?php echo $form->textField($model,'mdl_checked_by',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_datetime_checked'); ?>
		<?php echo $form->textField($model,'mdl_datetime_checked'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_datetime_insert'); ?>
		<?php echo $form->textField($model,'mdl_datetime_insert'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_is_locked'); ?>
		<?php echo $form->textField($model,'mdl_is_locked'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_status'); ?>
		<?php echo $form->textField($model,'mdl_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->