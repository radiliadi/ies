<?php
$this->breadcrumbs=array(
	'Sms'=>array('index'),
	$model->id_sms=>array('view','id'=>$model->id_sms),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Sms','url'=>array('index')),
	array('label'=>'Create Sms','url'=>array('create')),
	array('label'=>'View Sms','url'=>array('view','id'=>$model->id_sms)),
	array('label'=>'Manage Sms','url'=>array('admin')),
	);
	?>

	<h1>Update Sms <?php echo $model->id_sms; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>