<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_sms')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_sms),array('view','id'=>$data->id_sms)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nm_lemb')); ?>:</b>
	<?php echo CHtml::encode($data->nm_lemb); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('smt_mulai')); ?>:</b>
	<?php echo CHtml::encode($data->smt_mulai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_prodi')); ?>:</b>
	<?php echo CHtml::encode($data->kode_prodi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_sp')); ?>:</b>
	<?php echo CHtml::encode($data->id_sp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_jenj_didik')); ?>:</b>
	<?php echo CHtml::encode($data->id_jenj_didik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_jns_sms')); ?>:</b>
	<?php echo CHtml::encode($data->id_jns_sms); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pengguna')); ?>:</b>
	<?php echo CHtml::encode($data->id_pengguna); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_fungsi_lab')); ?>:</b>
	<?php echo CHtml::encode($data->id_fungsi_lab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kel_usaha')); ?>:</b>
	<?php echo CHtml::encode($data->id_kel_usaha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_blob')); ?>:</b>
	<?php echo CHtml::encode($data->id_blob); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_wil')); ?>:</b>
	<?php echo CHtml::encode($data->id_wil); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_jur')); ?>:</b>
	<?php echo CHtml::encode($data->id_jur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_induk_sms')); ?>:</b>
	<?php echo CHtml::encode($data->id_induk_sms); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jln')); ?>:</b>
	<?php echo CHtml::encode($data->jln); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rt')); ?>:</b>
	<?php echo CHtml::encode($data->rt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rw')); ?>:</b>
	<?php echo CHtml::encode($data->rw); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nm_dsn')); ?>:</b>
	<?php echo CHtml::encode($data->nm_dsn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ds_kel')); ?>:</b>
	<?php echo CHtml::encode($data->ds_kel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_pos')); ?>:</b>
	<?php echo CHtml::encode($data->kode_pos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lintang')); ?>:</b>
	<?php echo CHtml::encode($data->lintang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bujur')); ?>:</b>
	<?php echo CHtml::encode($data->bujur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_tel')); ?>:</b>
	<?php echo CHtml::encode($data->no_tel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_fax')); ?>:</b>
	<?php echo CHtml::encode($data->no_fax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('website')); ?>:</b>
	<?php echo CHtml::encode($data->website); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('singkatan')); ?>:</b>
	<?php echo CHtml::encode($data->singkatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_berdiri')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_berdiri); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sk_selenggara')); ?>:</b>
	<?php echo CHtml::encode($data->sk_selenggara); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_sk_selenggara')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_sk_selenggara); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tmt_sk_selenggara')); ?>:</b>
	<?php echo CHtml::encode($data->tmt_sk_selenggara); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tst_sk_selenggara')); ?>:</b>
	<?php echo CHtml::encode($data->tst_sk_selenggara); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kpst_pd')); ?>:</b>
	<?php echo CHtml::encode($data->kpst_pd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sks_lulus')); ?>:</b>
	<?php echo CHtml::encode($data->sks_lulus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gelar_lulusan')); ?>:</b>
	<?php echo CHtml::encode($data->gelar_lulusan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stat_prodi')); ?>:</b>
	<?php echo CHtml::encode($data->stat_prodi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('polesei_nilai')); ?>:</b>
	<?php echo CHtml::encode($data->polesei_nilai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('luas_lab')); ?>:</b>
	<?php echo CHtml::encode($data->luas_lab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kapasitas_prak_satu_shift')); ?>:</b>
	<?php echo CHtml::encode($data->kapasitas_prak_satu_shift); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jml_mhs_pengguna')); ?>:</b>
	<?php echo CHtml::encode($data->jml_mhs_pengguna); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jml_jam_penggunaan')); ?>:</b>
	<?php echo CHtml::encode($data->jml_jam_penggunaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jml_prodi_pengguna')); ?>:</b>
	<?php echo CHtml::encode($data->jml_prodi_pengguna); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jml_modul_prak_sendiri')); ?>:</b>
	<?php echo CHtml::encode($data->jml_modul_prak_sendiri); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jml_modul_prak_lain')); ?>:</b>
	<?php echo CHtml::encode($data->jml_modul_prak_lain); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fungsi_selain_prak')); ?>:</b>
	<?php echo CHtml::encode($data->fungsi_selain_prak); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penggunaan_lab')); ?>:</b>
	<?php echo CHtml::encode($data->penggunaan_lab); ?>
	<br />

	*/ ?>

</div>