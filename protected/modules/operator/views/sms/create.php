<?php
$this->breadcrumbs=array(
	'SMS'=>array('admin'),
	'Create',
);


?>

<div class="row">

	<div class="page-title">                    
                    <h2> <span class="fa fa-plus"></span> Create SMS</h2>
    </div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
  </div>