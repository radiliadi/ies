<?php
$this->breadcrumbs=array(
	'SMS'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('sms-grid', {
data: $(this).serialize()
});
return false;
});
");
?>
<div class="row">

	<div class="page-title">                    
                    <h3><span class="fa fa-laptop"></span> Kelola SMS (Satuan Manajemen Sumberdaya)</h3>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
				<div class="search-form" style="display:none">
					<?php $this->renderPartial('_search',array(
					'model'=>$model,
				)); ?>
				</div>
			</div>
		</div>
	</div>



<div class="col-md-12">
<div class="panel panel-default">
                                <div class="panel-heading">
                                    
                                    <div class="btn-group pull-right">
										<?php echo CHtml::link('<i class="fa fa-search"></i> Advanced Search','#',array('class'=>'search-button btn btn-default')); ?>
									 </div>
									<div class="btn-group pull-right">
                                        <?php echo CHtml::link('<i class="fa fa-plus"></i> Tambah', 'create', array('class'=>'btn btn btn-warning pull-right')); ?>
	
                                    </div> 
									
                                    
                                </div>

   
    <div class="panel-body">


<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'sms-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		array(
			'name'=>'id_sms',
			'value'=>'$data->id_sms',
		),
		array(
			'header'=>'Nama Lembaga',
			'name'=>'nm_lemb',
			'value'=>'$data->nm_lemb',
		),
		array(
			'header'=>'Semester Mulai',
			'name'=>'smt_mulai',
			'value'=>'$data->smt_mulai == 1 ? "Ganjil" :  "Genap"',
			 'filter'=>array(    
					'1'=>'Ganjil',
                    '2'=>'Genap',
					
                )
			
		),
		/*
		'kode_prodi',
		'id_sp',
		'id_jenj_didik',
		
		'id_jns_sms',
		'id_pengguna',
		'id_fungsi_lab',
		'id_kel_usaha',
		'id_blob',
		'id_wil',
		'id_jur',
		'id_induk_sms',
		'jln',
		'rt',
		'rw',
		'nm_dsn',
		'ds_kel',
		'kode_pos',
		'lintang',
		'bujur',
		'no_tel',
		'no_fax',
		'email',
		'website',
		'singkatan',
		'tgl_berdiri',
		'sk_selenggara',
		'tgl_sk_selenggara',
		'tmt_sk_selenggara',
		'tst_sk_selenggara',
		'kpst_pd',
		'sks_lulus',
		'gelar_lulusan',
		'stat_prodi',
		'polesei_nilai',
		'luas_lab',
		'kapasitas_prak_satu_shift',
		'jml_mhs_pengguna',
		'jml_jam_penggunaan',
		'jml_prodi_pengguna',
		'jml_modul_prak_sendiri',
		'jml_modul_prak_lain',
		'fungsi_selain_prak',
		'penggunaan_lab',
		*/
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>

          
        </div>			          
    </div>							          
  </div>
 </div>
