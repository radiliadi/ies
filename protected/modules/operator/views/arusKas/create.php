<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting' => Yii::app()->urlManager->createUrl('operator/accounting/index'),
        'Arus Kas' => 'admin',
        'Create',
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-plus"></span> Create Arus Kas </h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>