<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting' ,
        'Arus Kas' => '../../admin',
        'Update',
        $model->ak_id,
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update : #<?=$model->ak_id;?></h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>