<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Settings' ,
        'Employee',
        // 'Semua' => array('Semua'=>'admin', 
        'Transport'=>array('admin'
          ),
        'Update',
        $model->id_alat_transport,
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update Transport : #<?=$model->id_alat_transport;?></h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>