<?php $this->widget('booster.widgets.TbBreadcrumbs',
	array('links' => array('Expenses'=>'../../../expenses/admin',
	    'Update',
	   	$model->exp_id,
	    ),
	)
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update Arus Kas Detail : #<?=$model->exp_id;?> (<?=$model->exp_desc?>)</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>