<?php
$id_pd=Yii::app()->session->get('username');
?>
<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
'id'=>'ptkp-form',
// 'enableAjaxValidation'=>true,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row">
        <div class="col-md-12">
            <form class="form-horizontal">
                <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label"></label>
                                <div class="col-md-12 col-xs-12">                                            
                                    <?php 
                                     if($model->isNewRecord){
                                      echo CHtml::image(Yii::app()->request->baseUrl.'/expenses/expenses-attachment.png',"exp_attachment_img",array("width"=>'100%', "height"=>'100%'));
                                      }elseif(empty($model->exp_attachment_img)){
                                      echo CHtml::image(Yii::app()->request->baseUrl.'/expenses/expenses-attachment.png',"exp_attachment_img",array("width"=>'100%', "height"=>'100%'));
                                      }else{
                                      echo CHtml::image(Yii::app()->request->baseUrl.'/expenses/'.$model->exp_attachment_img,"exp_attachment_img",array("width"=>'100%', "height"=>'100%'));
                                      }
                                      ?>  
                                </div>
                            </div>
                        </div>
                </div>
        </form>
    </div>
    <div class="col-md-1">  
    </div>
</div> 
<?php $this->endWidget(); ?>