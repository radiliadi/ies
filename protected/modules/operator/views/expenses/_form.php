<?php
$id_pd=Yii::app()->session->get('username');
?>
<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
'id'=>'ptkp-form',
// 'enableAjaxValidation'=>true,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-md-1">
    </div>
        <div class="col-md-10">
            <form class="form-horizontal">
                <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">NIP</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'exp_id_pd',array('value'=>$id_pd, 'size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"exp_id_pd", 'readonly'=>'readonly'));?>
                                        <?php echo $form->error($model,'exp_id_pd'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'exp_desc',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"exp_desc"));?>
                                        <?php echo $form->error($model,'exp_desc'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                    <label class="col-md-4 col-xs-12 control-label">Attachment</label>
                                    <div class="col-md-6 col-xs-12">                                            
                                        <?php echo $form->fileFieldGroup($model,'exp_attachment_img',array()); ?>
                                        <?php echo $form->error($model,'exp_attachment_img'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-12 control-label"></label>
                                    <div class="col-md-6 col-xs-12">                                            
                                        <?php 
                                         if($model->isNewRecord){
                                          echo CHtml::image(Yii::app()->request->baseUrl.'/expenses/expenses-attachment.png',"exp_attachment_img",array("width"=>300));
                                          }elseif(empty($model->exp_attachment_img)){
                                          echo CHtml::image(Yii::app()->request->baseUrl.'/expenses/expenses-attachment.png',"exp_attachment_img",array("width"=>300));
                                          }else{
                                          echo CHtml::image(Yii::app()->request->baseUrl.'/expenses/'.$model->exp_attachment_img,"exp_attachment_img",array("width"=>300));
                                          }
                                          ?>  
                                        </div>
                                </div>
                            <?php echo $form->hiddenField($model,'exp_date_submit',array('value'=>date('Y-m-d H:i:s'))); ?>
                        </div>
                            <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
                                        'buttonType'=>'submit',
                                        'context'=>'primary',
                                        'label'=>$model->isNewRecord ? 'Create' : 'Save',
                                    )); ?>
                                </div>
                            </div>
                </div>
        </form>
    </div>
    <div class="col-md-1">  
    </div>
</div> 
<?php $this->endWidget(); ?>