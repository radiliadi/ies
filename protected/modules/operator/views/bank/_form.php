<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
'id'=>'bank-form',
// 'enableAjaxValidation'=>true,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-md-1">
    </div>
        <div class="col-md-10">
            <form class="form-horizontal">
                <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Bank Name</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <?php echo $form->textField($model,'bnk_name',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Bank Name", 'readonly' => 'readonly'));?>
                                        <?php echo $form->error($model,'bnk_name'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Account Number</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <?php echo $form->textField($model,'bnk_desc',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Account Number"));?>
                                        <?php echo $form->error($model,'bnk_desc'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <!--
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Nominal Top Up</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <?php //echo $form->textField($model,'bnk_saldo_first',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"First Saldo"));?>
                                        <?php //echo $form->error($model,'bnk_saldo_first'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            !-->
                        </div>
                            <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
    									'buttonType'=>'submit',
    									'context'=>'primary',
    									'label'=>$model->isNewRecord ? 'Create' : 'Save',
    								)); ?>
                                </div>
                            </div>
                </div>
        </form>
    </div>
    <div class="col-md-1">  
    </div>
</div> 
<?php $this->endWidget(); ?>