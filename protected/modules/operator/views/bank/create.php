<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Settings' ,
        'Finance',
        // 'Semua' => array('Semua'=>'admin', 
        'Bank'=>array('admin'
          ),
        'Create',
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Create Bank</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>