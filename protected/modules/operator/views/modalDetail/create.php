<?php $this->widget('booster.widgets.TbBreadcrumbs',
	array('links' => array('Accounting'=>'../../../accounting/index',
	    'Modal' => '../../../modal/admin',
	    'Detail',
	   	'Create',
	    ),
	)
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Create Modal Detail</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>