<?php $this->widget('booster.widgets.TbBreadcrumbs',
	array('links' => array('Accounting'=>'../../../accounting/index',
	    'Modal' => '../../../modal/admin',
	    'Detail' => '../../../../admin/id/'.$modal->mdl_id,
	   	$model->mdld_id,
	    ),
	)
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-plus"></span> Update Modal Detail : #<?=$model->mdld_id;?> (<?=$model->mdld_date;?>)</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>