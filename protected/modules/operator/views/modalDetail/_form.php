<?php
$id_pd=Yii::app()->session->get('username');
?>
<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
'id'=>'modalDetail-form',
// 'enableAjaxValidation'=>true,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-md-1">
    </div>
        <div class="col-md-10">
            <form class="form-horizontal">
                <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                        	<div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'mdld_desc',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Description"));?>
                                        <?php echo $form->error($model,'mdld_desc'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Modal Saham</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'mdld_saham',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Modal Saham"));?>
                                        <?php echo $form->error($model,'mdld_saham'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Saldo Laba<small>(Rugi)</small></label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'mdld_rugi',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Description"));?>
                                        <?php echo $form->error($model,'mdld_rugi'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Jumlah<small>(Ekuitas)</small></label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'mdld_ekuitas',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Description"));?>
                                        <?php echo $form->error($model,'mdld_ekuitas'); ?>

                                    </div>                                            
                                </div>
                            </div>
                        </div>
                            <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
    									'buttonType'=>'submit',
    									'context'=>'primary',
    									'label'=>$model->isNewRecord ? 'Create' : 'Save',
    								)); ?>
                                </div>
                            </div>
                </div>
        	</form>
    	</div>
    <div class="col-md-1">  
    </div>
</div> 
<?php $this->endWidget(); ?>