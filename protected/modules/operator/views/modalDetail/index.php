<?php
/* @var $this ModalDetailController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Modal Details',
);

$this->menu=array(
	array('label'=>'Create ModalDetail', 'url'=>array('create')),
	array('label'=>'Manage ModalDetail', 'url'=>array('admin')),
);
?>

<h1>Modal Details</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
