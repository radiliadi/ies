<?php
/* @var $this ModalDetailController */
/* @var $data ModalDetail */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdld_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->mdld_id), array('view', 'id'=>$data->mdld_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdld_mdl_id')); ?>:</b>
	<?php echo CHtml::encode($data->mdld_mdl_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_saham')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_saham); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_rugi')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_rugi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_ekuitas')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_ekuitas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_is_generate')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_is_generate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_date')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_date); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_desc')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_desc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_modal')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_modal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_generate_by')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_generate_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_datetime_insert')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_datetime_insert); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_datetime_update')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_datetime_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdl_status')); ?>:</b>
	<?php echo CHtml::encode($data->mdl_status); ?>
	<br />

	*/ ?>

</div>