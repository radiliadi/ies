<?php
/* @var $this ModalDetailController */
/* @var $model ModalDetail */

$this->breadcrumbs=array(
	'Modal Details'=>array('index'),
	$model->mdld_id,
);

$this->menu=array(
	array('label'=>'List ModalDetail', 'url'=>array('index')),
	array('label'=>'Create ModalDetail', 'url'=>array('create')),
	array('label'=>'Update ModalDetail', 'url'=>array('update', 'id'=>$model->mdld_id)),
	array('label'=>'Delete ModalDetail', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->mdld_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ModalDetail', 'url'=>array('admin')),
);
?>

<h1>View ModalDetail #<?php echo $model->mdld_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'mdld_id',
		'mdld_mdl_id',
		'mdl_saham',
		'mdl_rugi',
		'mdl_ekuitas',
		'mdl_is_generate',
		'mdl_date',
		'mdl_desc',
		'mdl_modal',
		'mdl_generate_by',
		'mdl_datetime_insert',
		'mdl_datetime_update',
		'mdl_status',
	),
)); ?>
