<?php
/* @var $this ModalDetailController */
/* @var $model ModalDetail */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'mdld_id'); ?>
		<?php echo $form->textField($model,'mdld_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdld_mdl_id'); ?>
		<?php echo $form->textField($model,'mdld_mdl_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_saham'); ?>
		<?php echo $form->textField($model,'mdl_saham',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_rugi'); ?>
		<?php echo $form->textField($model,'mdl_rugi',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_ekuitas'); ?>
		<?php echo $form->textField($model,'mdl_ekuitas',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_is_generate'); ?>
		<?php echo $form->textField($model,'mdl_is_generate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_date'); ?>
		<?php echo $form->textField($model,'mdl_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_desc'); ?>
		<?php echo $form->textArea($model,'mdl_desc',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_modal'); ?>
		<?php echo $form->textField($model,'mdl_modal',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_generate_by'); ?>
		<?php echo $form->textField($model,'mdl_generate_by',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_datetime_insert'); ?>
		<?php echo $form->textField($model,'mdl_datetime_insert'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_datetime_update'); ?>
		<?php echo $form->textField($model,'mdl_datetime_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mdl_status'); ?>
		<?php echo $form->textField($model,'mdl_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->