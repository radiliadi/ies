<?php
// $id_pd=Yii::app()->session->get('username');
// if($id_pd !== $expenses->exp_id_pd)
// {
// throw new CHttpException('403', 'Access denied!');
// }
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting' => '../../../accounting/index',
    	'Modal' => '../../../modal/admin' ,
        'Detail',
        // $expenses->exp_id,
      ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-laptop"></span> Modal Detail - <small> <?=$modal->mdl_desc ;?> </small> </h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"> Modal Detail </h3>
                <ul class="panel-controls">
                	<li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
            	<div class="btn-group pull-right">
            		<?php 
            		if($modal->mdl_is_locked == 1 ){
            			echo '';
            		}else{
            			echo CHtml::link('<i class="glyphicon glyphicon-plus"></i>', '../../create/id/'.$modal->mdl_id, array('class'=>'btn btn btn-primary pull-right', 'title'=>'Create')); 
            		}
            		

            		?>
                </div>
            </div>
            <div class="panel-body">
				<div class="form"> 
				<?php

				$form=$this->beginWidget('CActiveForm', array(
					'id'=>'labaDetail-form',
					'enableAjaxValidation'=>false,
					'htmlOptions'=>array(
						'enctype'=>'multipart/form-data',
						'role'=>'form',
					),
				));
				?>
	
					<div class="row">
					    <div class="box col-md-12 ">    
							<div class="box-content">
								<h1></h1>
								<table>
									<tbody>
										<tr>
											<td><b>No. Modal</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $modal->mdl_id;?></td>
										</tr>
										<?php
											$time=strtotime($modal->mdl_date);
											$year=date("Y", $time);
											$month=date("d F", $time);
										?>
										<tr>
											<td><b>Tahun</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $year;?></td>
										</tr>
										<tr>
											<td><b>Date Created</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $month;?></td>
										</tr>
									</tbody>
								</table>	
								<br>
								</br>
								
								<div class="panel-body">	  
									<div class="table-responsive">
										<!-- START DEFAULT DATATABLE -->
                                    <table class="table">
                                    	<thead>
	                                    	<tr>
	                                    		<th>No</th>
	                                    		<th>MODAL</th>
	                                    		<th>Modal Saham</th>
	                                    		<th>Saldo Laba <br/>(Rugi)</th>
	                                    		<th>Jumlah <br/>(Ekuitas)</th>
	                                    		<th></th>
	                                    	</tr>
                                    	</thead>
                                    	<tbody>
                                    	<?php
                                    	$no=1;
                                    	$update = '';
                                    	$delete = '';
                                    	if($modal->mdl_is_locked == 0 ){
                                    		$update = CHtml::link('<i class="glyphicon glyphicon-pencil"></i>', '../../update/id/'.$modalDetailId->mdld_id.'/modalId/'.$modal->mdl_id, array('class'=>'btn btn btn-info', 'title'=>'Update'));
                                    		$delete = CHtml::link('<i class="glyphicon glyphicon-trash"></i>', '../../delete/id/'.$modalDetailId->mdld_id, array('class'=>'btn btn btn-danger', 'confirm' => 'Are you sure want delete "'.$modalDetailId->mdld_desc.'" ?', 'title'=>'Delete'));
                                    	}else{
                                    		$update = '';
                                    		$delete = '';
                                    	}
                                    	$arrModalDetail[] = array();
                                    	foreach($modalDetail as $arrModalDetail){
                                    		if(empty($arrModalDetail)){
                                    			echo '
		                                    		<tr>
		                                    			<td></td>
		                                    			<td></td>
		                                    			<td></td>
		                                    			<td></td>
		                                    			<td></td>
		                                    			<td></td>
		                                    		</tr>'
		                                    		;
                                    		}else{
		                                    	echo '
		                                    		<tr>
		                                    			<td>'.$no.'</td>
		                                    			<td>'.$arrModalDetail->mdld_desc.'</td>
		                                    			<td>Rp. '.number_format($arrModalDetail->mdld_saham).'</td>
		                                    			<td>Rp. '.number_format($arrModalDetail->mdld_rugi).'</td>
		                                    			<td>Rp. '.number_format($arrModalDetail->mdld_ekuitas).'</td>
		                                    			<td>'.$update.' '.$delete.'
														</td>
		                                    		</tr>'
		                                    		;
		                                    		$no++;
                                    		}
                                    	}
                                    	?>
                                    	</tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                            	<div class="table-responsive">
                                    <table class="table">
                                    	<thead>
	                                    	<tr>
	                                    		<td></td>
	                                    		<td></td>
	                                    		<td></td>
	                                    	</tr>
	                                    </thead>
	                                    <tbody>
	                                    </tbody>
	                                    <?php

											if($modal->mdl_is_locked == 0){
												echo'
												<tr>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td class="center">&nbsp; &nbsp;</td>
													<td><a class="btn btn btn-success pull-right" href="../../../../operator/modalDetail/lock/id/'.$modal->mdl_id.'" title="Lock"><i class="fa fa-lock"></i>
														</a>
													</td>
												</tr>
												';
											}else if($modal->mdl_is_locked == 1){
												echo'
												<tr>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td class="center">&nbsp; &nbsp;</td>
													<td><a class="btn btn btn-primary pull-left" href="../../../../operator/modalDetail/print/id/'.$modal->mdl_id.'" title="Print"><i class="fa fa-print"></i>
														</a>
														<a class="btn btn btn-info pull-right" href="../../../../operator/modalDetail/unlock/id/'.$modal->mdl_id.'" title="Lock"><i class="fa fa-unlock"></i>
														</a>
													</td>
												</tr>
												';
											}
                                    	?>
                                    </table>
                               </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
						</div>
					</div>
				</div>

			</div>	
		</div>

	</div>
					<?php $this->endWidget(); ?>
					</div>
                <br>
                </br>
            </div>
            </div>
        </div>
    </div>
</div>