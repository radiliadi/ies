<?php
/* @var $this PaymentMethodController */
/* @var $model PaymentMethod */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'payment-method-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'pyt_name'); ?>
		<?php echo $form->textArea($model,'pyt_name',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'pyt_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pyt_datetime'); ?>
		<?php echo $form->textField($model,'pyt_datetime'); ?>
		<?php echo $form->error($model,'pyt_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pyt_status'); ?>
		<?php echo $form->textField($model,'pyt_status'); ?>
		<?php echo $form->error($model,'pyt_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->