<?php
/* @var $this PaymentMethodController */
/* @var $data PaymentMethod */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('pyt_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->pyt_id), array('view', 'id'=>$data->pyt_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pyt_name')); ?>:</b>
	<?php echo CHtml::encode($data->pyt_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pyt_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->pyt_datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pyt_status')); ?>:</b>
	<?php echo CHtml::encode($data->pyt_status); ?>
	<br />


</div>