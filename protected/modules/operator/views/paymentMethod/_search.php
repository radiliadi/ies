<?php
/* @var $this PaymentMethodController */
/* @var $model PaymentMethod */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'pyt_id'); ?>
		<?php echo $form->textField($model,'pyt_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pyt_name'); ?>
		<?php echo $form->textArea($model,'pyt_name',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pyt_datetime'); ?>
		<?php echo $form->textField($model,'pyt_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pyt_status'); ?>
		<?php echo $form->textField($model,'pyt_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->