<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Employee' ,
        // 'Finances',
        // 'Semua' => array('Semua'=>'admin', 
        'Business Trip'=>array('admin'
          ),
        'Create',
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Create Business Trip</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>