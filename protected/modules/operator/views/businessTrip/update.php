<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Employee' ,
        // 'Finances',
        // 'Semua' => array('Semua'=>'admin', 
        'Business Trip'=>array('admin'
          ),
        $model->bt_id_pd,
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update Business Trip : #<?=$model->bt_id_pd;?></h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>