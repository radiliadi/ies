<?php
$id_pd=Yii::app()->session->get('username');
?>
<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
'id'=>'labaDetail-form',
// 'enableAjaxValidation'=>true,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-md-1">
    </div>
        <div class="col-md-10">
            <form class="form-horizontal">
                <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                        	<div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Harga Pokok Penjualan</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'lbd_hrg_pokok_penjualan',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Harga Pokok Penjualan"));?>
                                        <?php echo $form->error($model,'lbd_hrg_pokok_penjualan'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Beban Penyusutan</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'lbd_bbn_susut',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Beban Penyusutan"));?>
                                        <?php echo $form->error($model,'lbd_bbn_susut'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <?php //echo $form->hiddenField($model,'expd_id_pd',array('value'=>$id_pd)); ?>
                            <?php //echo $form->hiddenField($model,'expd_datetime_insert',array('value'=>date('Y-m-d H:i:s'))); ?>
                        </div>
                            <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
    									'buttonType'=>'submit',
    									'context'=>'primary',
    									'label'=>$model->isNewRecord ? 'Create' : 'Save',
    								)); ?>
                                </div>
                            </div>
                            <!-- <div class="panel-body form-group-separated">
	                            <div class="form-group">
	                                <label class="col-md-12 col-xs-12 control-label"><center><u><b>HASIL GENERATE</b></u></center></label>
	                            </div>
	                        	<div class="form-group">
	                                <label class="col-md-4 col-xs-12 control-label">Pendapatan</label>
	                                <div class="col-md-6 col-xs-12">                                            
	                                    <div class="input-group">
	                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
	                                        <?php //echo $form->textField($model,'lbd_pendapatan',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Pendapatan"));?>
	                                        <?php //echo $form->error($model,'lbd_pendapatan'); ?>
	                                    </div>                                            
	                                </div>
	                            </div>
	                        </div> -->
                </div>
        	</form>
    	</div>
    <div class="col-md-1">  
    </div>
</div> 
<?php $this->endWidget(); ?>