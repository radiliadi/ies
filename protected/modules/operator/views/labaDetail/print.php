<?php
$time=strtotime($laba->lb_date);
$now=date('Y-m-d');
$now=strtotime($now);

$yearStart=date("Y", $time);
$yearStart1=date("Y", $time);
$yearEnd=date("Y", $now);
$monthStart=date("d F", $time);
$monthEnd=date("d F", $now);
if($yearStart == $yearEnd){
	$yearStart = '';
}

$pdf->AliasNbPages();
$pdf->AddPage('P',$size);                            
$pdf->SetFont('Times','b',14);     
$pdf->Image('images/png.png',145,9,35,25);      
$pdf->Ln(1);
$pdf->MultiCell(190,15,$header0,0,'C',false);
$pdf->SetFont('Times','b',12);
$pdf->MultiCell(190,5,$judul,0,'C',false);
$pdf->Ln(2);
if($monthStart == $monthEnd){
	$pdf->MultiCell(190,5,'Per '.$monthStart.' '.$yearStart1,0,'C',false);
}else{
	$pdf->MultiCell(190,5,'Per '.$monthStart.' '.$yearStart.' - '.$monthEnd.' '.$yearEnd,0,'C',false);
}

// $pdf->Ln(7); //Line break
//mulai tabel
//line break
//content
$pdf->Ln(10);
$pdf->SetFont('Times','b',10);
$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(104,104,104); //Set background of the cell to be that grey color
$pdf->SetTextColor(0,0,0);
$pdf->setLeftMargin(30);
$pdf->Cell(100,5,'',0,0,'L',false);
$pdf->SetFont('Times','u',10);
$pdf->SetFont('Times','bu',10);
$pdf->Cell(90,5,$monthStart.' '.$yearStart,0,0,'L',false);	
$pdf->Ln(7);

$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'Pendapatan',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Rp. '.number_format($labaDetailId->lbd_pendapatan),0,0,'L',false);	
$pdf->Ln(7);

$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'Harga Pokok Penjualan',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Rp. '.number_format($labaDetailId->lbd_hrg_pokok_penjualan),0,0,'L',false);
$pdf->Ln(9);

$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'Laba Kotor',0,0,'L',false);
$pdf->SetFont('Times','b',10);
$labaKotor = $labaDetailId->lbd_pendapatan + $labaDetailId->lbd_hrg_pokok_penjualan;
$pdf->Cell(90,5,'Rp. '.number_format($labaKotor),0,0,'L',false);
$pdf->Ln(11);

$pdf->SetFont('Times','b',10);
$pdf->Cell(190,5,'Biaya Umum & Administrasi',0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->setLeftMargin(40); //set margin ampe ke bawah
$pdf->Cell(90,5,'Beban Transaksi',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bbn_transaksi),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Biaya Gaji',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_biy_gaji),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Beban Lembur',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bbn_lembur),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Beban Pengobatan Keryawan',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bbn_pengobatan_kyw),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'BPJS Tenaga Kerja & Kesehatan',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bpjs_tkk),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Biaya Sewa Kantor',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_biy_sewa_kantor),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Beban ATK, Fotocopy, Cetakan & Perlengkapan',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bbn_atk),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Biaya Tlp, Fax, Internet',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_biy_tfi),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Beban Parkir, Bensin, Dll',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bbn_prkr),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Beban Listrik & Air',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bbn_lstrk),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Benda Pos & Biaya Kirim',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bp_bk),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Beban Transport',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bbn_trans),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Beban Entertain',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bbn_entertain),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Keperluan Rumah Tangga',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_k_rt),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Biaya Perijinan & Sertifikasi',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_biy_ps),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Biaya Audit & Konsultan',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_biy_ak),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'BPP - Kantor',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bpp_kantor),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'BPP - Kendaraan',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bpp_kendaraan),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'BPP - Peralatan Kantor',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bpp_alat_kantor),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Beban Penyusutan',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bbn_susut),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Biaya Administrasi Tender',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_biy_adm_tndr),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Biaya Administrasi Bank',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_biy_adm_bank),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Beban Asuransi Kesehatan',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bbn_askes),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Beban Perjalanan Dinas',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bbn_dinas),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Beban Lain - lain',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_bbn_ll),0,0,'L',false);
$pdf->Ln(10);

$pdf->SetFont('Times','b',10);
$pdf->Cell(90,5,'Total',0,0,'L',false);
$pdf->SetFont('Times','bu',10);
$total=0;
$total= (
	$labaDetailId->lbd_bbn_transaksi + 
	$labaDetailId->lbd_biy_gaji + 
	$labaDetailId->lbd_bbn_lembur + 
	$labaDetailId->lbd_bbn_pengobatan_kyw + 
	$labaDetailId->lbd_bpjs_tkk + 
	$labaDetailId->lbd_biy_sewa_kantor + 
	$labaDetailId->lbd_bbn_atk + 
	$labaDetailId->lbd_biy_tfi + 
	$labaDetailId->lbd_bbn_prkr + 
	$labaDetailId->lbd_bbn_lstrk + 
	$labaDetailId->lbd_bp_bk + 
	$labaDetailId->lbd_bbn_trans + 
	$labaDetailId->lbd_bbn_entertain + 
	$labaDetailId->lbd_k_rt + 
	$labaDetailId->lbd_biy_ps + 
	$labaDetailId->lbd_biy_ak + 
	$labaDetailId->lbd_bpp_kantor + 
	$labaDetailId->lbd_bpp_kendaraan + 
	$labaDetailId->lbd_bpp_alat_kantor + 
	$labaDetailId->lbd_bbn_susut + 
	$labaDetailId->lbd_biy_adm_tndr + 
	$labaDetailId->lbd_biy_adm_bank + 
	$labaDetailId->lbd_bbn_askes + 
	$labaDetailId->lbd_bbn_dinas + 
	$labaDetailId->lbd_bbn_ll
	);
$pdf->Cell(100,5,'Rp. '.number_format($total),0,0,'L',false);
$pdf->Ln(20);

$pdf->SetFont('Times','b',10);
$pdf->Cell(190,5,'',0,0,'L',false);
$pdf->setLeftMargin(30);
$pdf->Ln(20);

$pdf->SetFont('Times','b',10);
$labaUsaha = $labaKotor - $total;
$labaUsahax = ($labaUsaha * 28) / 100;
$pdf->Cell(90,5,'Laba (Rugi) Usaha',0,0,'L',false);
$pdf->Cell(100,5,'Rp. '.number_format($labaUsaha).' ('.number_format($labaUsahax).')',0,0,'L',false);
$pdf->Ln(15);

$pdf->SetFont('Times','b',10);
$pdf->Cell(190,5,'Pendapatan (Biaya) Lain-lain',0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->setLeftMargin(40); //set margin ampe ke bawah
$pdf->Cell(80,5,'Pendapatan Lain - lain',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_pll),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(80,5,'Jasa Giro',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($labaDetailId->lbd_jasa_giro),0,0,'L',false);
$pdf->Ln(15);

$pdf->SetFont('Times','b',10);
$totPll = $labaDetailId->lbd_pll + $labaDetailId->lbd_jasa_giro;
$pdf->Cell(80,5,'Total Pendapatan (Biaya) Lain-lain',0,0,'L',false);
$pdf->Cell(100,5,'Rp. '.number_format($totPll),0,0,'L',false);
$pdf->setLeftMargin(30); //netral
$pdf->Ln(15);

$pdf->SetFont('Times','b',10);
$sumEnd = $labaUsaha + $totPll;
$pdf->Cell(90,5,'Laba (Rugi) Sebelum Pajak Penghasilan',0,0,'L',false);
$pdf->Cell(100,5,'Rp. '.number_format($sumEnd),0,0,'L',false);
$pdf->Ln(7);

$pdf->Ln(10);	
$pdf->SetFont('Times','',10);
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(145);
$pdf->MultiCell(80,5,$company->cpy_city.', '.date('d M Y'),0,'L',false);
//Baris Baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(145);
$pdf->MultiCell(80,5,$company->cpy_fullname.',',0,'L',false);
$pdf->Ln(12);
//Baris baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(145);
$pdf->MultiCell(80,5,$company->cpy_dirut,0,'L',false);
//Cetak PDF
$pdf->Output('('.date('dMY').')-'.$judul.'.pdf','I');
?>