<?php $this->widget('booster.widgets.TbBreadcrumbs',
	array('links' => array('Accounting' => Yii::app()->urlManager->createUrl('operator/accounting/index'),
	    'Laba' => '../../../laba/admin',
	    'Detail' => '../../admin/id/'.$model->lbd_id,
	   	$model->lbd_id,
	    ),
	)
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-plus"></span> Update Laba Detail : #<?=$model->lbd_id;?> (<?=$model->lbd_date?>)</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>