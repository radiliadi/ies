<?php
// $id_pd=Yii::app()->session->get('username');
// if($id_pd !== $expenses->exp_id_pd)
// {
// throw new CHttpException('403', 'Access denied!');
// }
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting'=>'../../../accounting/index',
    	'Laba' => '../../../laba/admin' ,
        'Detail',
        $labaDetailId->lbd_id,
      ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-laptop"></span> Laba Detail - <small> <?=$laba->lb_desc ;?> </small> </h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"> </h3>
                <ul class="panel-controls">
                </ul>   
            	<div class="btn-group pull-right">
            		<?php 
            		if($laba->lb_is_locked == 1 ){
            			echo '';
            		}else{
            			echo CHtml::link('<i class="glyphicon glyphicon-download-alt"></i>', '../../generate/id/'.$labaDetailId->lbd_id.'/date/'.$labaDetailId->lbd_date, array('class'=>'btn btn btn-info pull-right', 'confirm' => 'Are you sure to generate? Your data will be automatic changed', 'title'=>'Generate')); 
            		}
            		

            		?>
                </div>
                <div class="btn-group pull-left">
            		<?php 
            		if($laba->lb_is_locked == 1 ){
            			echo '';
            		}else{
            			echo CHtml::link('<i class="glyphicon glyphicon-pencil"></i>', '../../update/id/'.$labaDetailId->lbd_id, array('class'=>'btn btn btn-primary pull-left', 'confirm' => 'Are you sure to update?', 'title'=>'Update')); 
            		}
            		

            		?>
                </div>
            </div>
            <div class="panel-body">
				<div class="form"> 
				<?php

				$form=$this->beginWidget('CActiveForm', array(
					'id'=>'labaDetail-form',
					'enableAjaxValidation'=>false,
					'htmlOptions'=>array(
						'enctype'=>'multipart/form-data',
						'role'=>'form',
					),
				));
				?>
	
					<div class="row">
					    <div class="box col-md-12 ">    
							<div class="box-content">
								<h1></h1>
								<table>
									<tbody>
										<tr>
											<td><b>No. Laba</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $labaDetailId->lbd_id;?></td>
										</tr>
										<?php
											$time=strtotime($laba->lb_date);
											$year=date("Y", $time);
											$month=date("d F", $time);
										?>
										<tr>
											<td><b>Tahun</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $year;?></td>
										</tr>
										<tr>
											<td><b>Date Created</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $month;?> (<i><?=$labaDetailId->lbd_datetime_insert;?></i>)</td>
										</tr>
										<tr>
											<td><b>Last Modified</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $labaDetailId->lbd_datetime_update.' ('.$labaDetailId->lbd_update_by.')';?></td>
										</tr>
										<tr>
											<td><b>Notes</b></td>
											<td class="center">:&nbsp; &nbsp;<font color="blue">*</font> (hasil generate), <font color="red">*</font> (diisi oleh admin)</td>
										</tr>
									</tbody>
								</table>	
								<br>
								</br>
								
								<div class="panel-body">	  
									<div class="table-responsive">
										<!-- START DEFAULT DATATABLE -->
                                    <table class="table">
                                        <tr>
											<td><b>Pendapatan <font color="blue">*</font></b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_pendapatan);?></td>
											<td></td>
										</tr>
										<tr>
											<td><b>Harga Pokok Penjualan <font color="red">*</font></b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_hrg_pokok_penjualan);?></td>
											<td></td>
										</tr>
										<?php
											$labaKotor = $labaDetailId->lbd_pendapatan + $labaDetailId->lbd_hrg_pokok_penjualan;
										?>
										<tr>
											<td><b>Laba Kotor</b></td>
											<td class="center"><b>:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaKotor);?></b></td>
											<td></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td class="center">&nbsp; &nbsp;</td>
											<td></td>
										</tr>
										<tr>
											<td><b>Biaya Umum & Administrasi</b></td>
											<td></td>
											<td></td>
										</tr>
											<tr>
												<td style="padding-left:3%;">Beban Transaksi <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bbn_transaksi);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Biaya Gaji <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_biy_gaji);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Beban Lembur <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bbn_lembur);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Beban Pengobatan Keryawan <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bbn_pengobatan_kyw);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">BPJS Tenaga Kerja & Kesehatan <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bpjs_tkk);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Biaya Sewa Kantor <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_biy_sewa_kantor);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Beban ATK, Fotocopy, Cetakan & Perlengkapan <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bbn_atk);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Biaya Tlp, Fax, Internet <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_biy_tfi);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Beban Parkir, Bensin, Dll <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bbn_prkr);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Beban Listrik & Air <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bbn_lstrk);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Benda Pos & Biaya Kirim <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bp_bk);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Beban Transport <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bbn_trans);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Beban Entertain <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bbn_entertain);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Keperluan Rumah Tangga <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_k_rt);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Biaya Perijinan & Sertifikasi <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_biy_ps);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Biaya Audit & Konsultan <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_biy_ak);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">BPP - Kantor <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bpp_kantor);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">BPP - Kendaraan <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bpp_kendaraan);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">BPP - Peralatan Kantor <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bpp_alat_kantor);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Beban Penyusutan <font color="red">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bbn_susut);?></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Biaya Administrasi Tender <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_biy_adm_tndr);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Biaya Administrasi Bank <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_biy_adm_bank);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Beban Asuransi Kesehatan <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bbn_askes);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Beban Perjalanan Dinas <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bbn_dinas);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Beban Lain - lain <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_bbn_ll);?></td>
												<td></td>
											</tr>

											<tr>
												<td>&nbsp;</td>
												<td class="center">&nbsp; &nbsp;</td>
												<td></td>
											</tr>

											<?php
												$total=0;
												$total= (
													$labaDetailId->lbd_bbn_transaksi + 
													$labaDetailId->lbd_biy_gaji + 
													$labaDetailId->lbd_bbn_lembur + 
													$labaDetailId->lbd_bbn_pengobatan_kyw + 
													$labaDetailId->lbd_bpjs_tkk + 
													$labaDetailId->lbd_biy_sewa_kantor + 
													$labaDetailId->lbd_bbn_atk + 
													$labaDetailId->lbd_biy_tfi + 
													$labaDetailId->lbd_bbn_prkr + 
													$labaDetailId->lbd_bbn_lstrk + 
													$labaDetailId->lbd_bp_bk + 
													$labaDetailId->lbd_bbn_trans + 
													$labaDetailId->lbd_bbn_entertain + 
													$labaDetailId->lbd_k_rt + 
													$labaDetailId->lbd_biy_ps + 
													$labaDetailId->lbd_biy_ak + 
													$labaDetailId->lbd_bpp_kantor + 
													$labaDetailId->lbd_bpp_kendaraan + 
													$labaDetailId->lbd_bpp_alat_kantor + 
													$labaDetailId->lbd_bbn_susut + 
													$labaDetailId->lbd_biy_adm_tndr + 
													$labaDetailId->lbd_biy_adm_bank + 
													$labaDetailId->lbd_bbn_askes + 
													$labaDetailId->lbd_bbn_dinas + 
													$labaDetailId->lbd_bbn_ll
													);
											?>
											<tr>
												<td style="padding-left:3%;"><b>TOTAL</b></td>
												<td class="center"><b>:&nbsp; &nbsp;<?='Rp. '.number_format($total);?></b></td>
												<td></td>
											</tr>

											<tr>
												<td>&nbsp;</td>
												<td class="center">&nbsp; &nbsp;</td>
												<td></td>
											</tr>
										<?php
											$labaUsaha = $labaKotor - $total;
											$labaUsahax = ($labaUsaha * 28) / 100;
										?>
										<tr>
											<td><b>Laba (Rugi) Usaha</b></td>
											<td class="center"><b>:&nbsp; &nbsp;<?='Rp. '.number_format($labaUsaha);?></b></td>
											<td class="center">:&nbsp; &nbsp;<?='(Rp. '.number_format($labaUsahax, 3).')';?></td>
										</tr>

										<tr>
											<td>&nbsp;</td>
											<td class="center">&nbsp; &nbsp;</td>
											<td></td>
										</tr>

										<tr>
											<td><b>Pendapatan (Biaya) Lain-lain</b></td>
											<td></td>
											<td></td>
										</tr>
											<tr>
												<td style="padding-left:3%;">Pendapatan Lain - lain <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_pll);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;">Jasa Giro <font color="blue">*</font></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($labaDetailId->lbd_jasa_giro);?></td>
												<td></td>
											</tr>

											<tr>
												<td>&nbsp;</td>
												<td class="center">&nbsp; &nbsp;</td>
												<td></td>
											</tr>
											<?php
												$totPll = $labaDetailId->lbd_pll + $labaDetailId->lbd_jasa_giro;
											?>
											<tr>
												<td style="padding-left:3%;"><b>Total Pendapatan (Biaya) Lain-lain</b></td>
												<td class="center"><b>:&nbsp; &nbsp;<?='Rp. '.number_format($totPll);?></b></td>
												<td></td>
											</tr>

											<tr>
												<td>&nbsp;</td>
												<td class="center">&nbsp; &nbsp;</td>
												<td></td>
											</tr>
											<?php
												$sumEnd = $labaUsaha + $totPll;
												// echo $labaUsaha.'<br/>';
												// echo $totPll;
											?>
											<tr>
												<td style="padding-left:3%;"><b>Laba (Rugi) Sebelum Pajak Penghasilan</b></td>
												<td class="center"><b>:&nbsp; &nbsp;<?='Rp. '.number_format($sumEnd);?></b></td>
												<td></td>
											</tr>

											<tr>
												<td>&nbsp;</td>
												<td class="center">&nbsp; &nbsp;</td>
												<td></td>
											</tr>

											<?php

												if($laba->lb_is_locked == 0){
													echo'
													<tr>
														<td>&nbsp;</td>
														<td class="center">&nbsp; &nbsp;</td>
														<td><a class="btn btn btn-success pull-right" href="../../../../operator/labaDetail/lock/id/'.$laba->lb_id.'" title="Lock"><i class="fa fa-lock"></i>
															</a>
														</td>
													</tr>
													';
												}else if($laba->lb_is_locked == 1){
													echo'
													<tr>
														<td>&nbsp;</td>
														<td class="center">&nbsp; &nbsp;</td>
														<td><a class="btn btn btn-primary pull-left" href="../../../../operator/labaDetail/print/id/'.$laba->lb_id.'" title="Print"><i class="fa fa-print"></i>
															</a>
															<a class="btn btn btn-info pull-right" href="../../../../operator/labaDetail/unlock/id/'.$laba->lb_id.'" title="Lock"><i class="fa fa-unlock"></i>
															</a>
														</td>
													</tr>
													';
												}
                                        	?>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
						</div>
					</div>
				</div>

			</div>	
		</div>

	</div>
					<?php $this->endWidget(); ?>
					</div>
                <br>
                </br>
            </div>
            </div>
        </div>
    </div>
</div>