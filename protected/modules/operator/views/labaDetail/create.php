<?php
/* @var $this LabaDetailController */
/* @var $model LabaDetail */

$this->breadcrumbs=array(
	'Laba Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LabaDetail', 'url'=>array('index')),
	array('label'=>'Manage LabaDetail', 'url'=>array('admin')),
);
?>

<h1>Create LabaDetail</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>