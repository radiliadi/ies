<div class="modal animated fadeIn" id="modal_coad_list" tabindex="-188" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="smallModalHead"><a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/accounting/" class="btn btn-primary btn-lg btn-block" type="button">back to Accounting</a></h4>
            </div>
            <div class="modal-body form-horizontal form-group-separated"><br/>
                <div class="form-group">
                <center>
                <?php foreach($coaMenu as $coaModal) { ?>
                    <label class="col-md-4 control-label"><center><?php echo $coaModal->coa_id ;?> - <?php echo $coaModal->coa_desc ;?></center></label>
                    <div class="col-md-4">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/chartOfAccountDetail/jurnal/id/<?php echo $coaModal->coa_id ;?>/type/<?php echo $coaModal->coa_coat_id; ?>" class="btn btn-info btn-lg btn-block" type="button">Jurnal</a>
                    </div>
                    <div class="col-md-4">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/chartOfAccountDetail/menu/id/<?php echo $coaModal->coa_id ;?>/type/<?php echo $coaModal->coa_coat_id; ?>" class="btn btn-success btn-lg btn-block">Detail</a><br/>
                    </div>
                <?php } ?>
                </center>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <script>
    $('.jekganteng').click(function () {
        // get the url from the data attribute
        var src = $(this).data('url');

        $('#modal_coad_list').modal('show');
        $('#modal_coad_list').attr('src', src);
    });

    $('#modal_coad_list').click(function () {
        $('#modal_coad_list').removeAttr('src');
    });
</script> -->
<script type="text/javascript">
    $(document).ready(function () {
    $('#modal_coad_list').modal('show');
        // $('#modal_coad_list').modal({
        //   backdrop: 'static',
        //   keyboard: false
        // });

});
</script>