<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting' => '../../../accounting/index',
        // 'Active'=>array('active', 
        //     )
        ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-laptop"></span> COA <small> Type : <?=$coaType->coat_desc ;?> </small> </h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <?php foreach($coaMenu as $coaMenus) {
    ?>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?=$coaMenus->coa_id ;?> : <?=$coaMenus->coa_desc ;?></h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
            	<div class="btn-group pull-right">
                </div> 
                <div class="btn-group pull-left">
                    
                </div> 
            </div>
            
            <div class="panel-body">
            <?php echo CHtml::beginForm(array('chartOfAccount/exportExcel')); ?>
            <?php $this->widget('booster.widgets.TbGridView',array(
            'id'=>'chartOfAccount-grid',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'responsiveTable' => true,
            'pager' => array(
                'header' => '',
                'footer' => '',
                'cssFile' => false,
                'selectedPageCssClass' => 'active',
                'hiddenPageCssClass' => 'disabled',
                'firstPageCssClass' => 'previous',
                'lastPageCssClass' => 'next',
                'maxButtonCount' => 5,
                'firstPageLabel'=> '<i class="fa fa-angle-double-left"></i>',
                'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
                'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
                'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
                'htmlOptions' => array(
                    'class' => 'dataTables_paginate paging_bootstrap pagination pull-right',
                    'align' => 'center',
                    'style' => 'width: 100%;',
                ),
                // 'filterHtmlOptions' => array('style' => 'width: 30px;'),
            ),
            'columns'=>array(
                array(
                    'header' => 'No.',
                    'htmlOptions' => array(
                        'width' => '20px',
                        'style' => 'text-align:center'
                    ),
                    'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                    // 'class'=>'booster.widgets.TbTotalSumColumn',
                ),
                array(
                    'name' => 'coa_id',
                    'type' => 'raw',
                ),
        		array(
                    'name' => 'coa_desc',
                    'type' => 'raw',
                ),
                array(    
                    'header'=>'',
                    'type'=>'raw', 
                    'value' =>function($data) {
                                if($data->coa_status == 1){
                                    return CHtml::link('<i class="fa fa-search"></i>',['/operator/expenses/print/id/'.$data->coa_id],['class' => 'btn btn-default', 'title' => 'View']);
                                }else{
                                    return CHtml::link('<i class="fa fa-cog"></i>',['/operator/expenses/print/id/'.$data->coa_id],['class' => 'btn btn-primary', 'title' => 'Setting']).' '.CHtml::link('<i class="fa fa-check-square-o"></i>',['/operator/expenses/verified/id/'.$data->coa_id],['class' => 'btn btn-success', 'title' => 'Active', 'confirm' => 'Are you sure?']);
                                }
                            },
                ),
            ),
            )); 
            ?>
            </div>
        </div>
    </div>
    <?php }
    ?>
</div>