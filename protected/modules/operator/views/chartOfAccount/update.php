<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting' ,
        'ChartOfAccount' => '../../admin',
        'Update',
        $model->coa_id,
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update : #<?=$model->coa_id;?></h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>