<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
'id'=>'chartOfAccount-form',
// 'enableAjaxValidation'=>true,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); 
// echo '<pre>';
// print_r($model->coa_id);
// echo '</pre>';
// die;
?>
<div class="row">
    <div class="col-md-1">
    </div>
        <div class="col-md-10">
            <form class="form-horizontal">
                <div class="panel panel-default">
                		<div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">ID</label>
                                <div class="col-md-6 col-xs-12">
                                <?php if(!$model->isNewRecord){ ?>                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'coa_id',array('size'=>255,'maxlength'=>255,'class'=>"form-control",'placeholder'=>"ID", 'readonly'=>'readonly'));?>
                                        <?php echo $form->error($model,'coa_id'); ?>

                                    </div>
                                <?php }else{ ?>
                                	<div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'coa_id',array('size'=>255,'maxlength'=>255,'class'=>"form-control",'placeholder'=>"ID"));?>
                                        <?php echo $form->error($model,'coa_id'); ?>

                                    </div>
                                <?php } ?>                                        
                                </div>
                            </div>
                        </div>
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textArea($model,'coa_desc',array('size'=>255,'maxlength'=>255,'class'=>"form-control",'placeholder'=>"Description"));?>
                                        <?php echo $form->error($model,'coa_desc'); ?>

                                    </div>                                            
                                </div>
                            </div>
                        </div>
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Rule<a style="color:red;"><blink> *</blink></a></label>
                                <div class="col-md-6">
                                    <?php echo $form->dropDownList($model,'coa_coar_id',ChartOfAccount::listRule(),array('empty'=>'-Choose Rule-','class'=>"form-control select")); ?>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Type<a style="color:red;"><blink> *</blink></a></label>
                                <div class="col-md-6">
                                    <?php echo $form->dropDownList($model,'coa_coat_id',ChartOfAccount::listType(),array('empty'=>'-Choose Type-','class'=>"form-control select")); ?>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Akun<a style="color:red;"><blink> *</blink></a></label>
                                <div class="col-md-6">
                                    <?php echo $form->radioButton($model,'coa_code',array('value'=>'1','uncheckValue'=>null,' '=>" ")); ?>Debet                                       
                                    <?php echo $form->radioButton($model,'coa_code',array('value'=>'2','uncheckValue'=>null,' '=>" ")); ?>Kredit
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                            <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
    									'buttonType'=>'submit',
    									'context'=>'primary',
    									'label'=>$model->isNewRecord ? 'Create' : 'Save',
    								)); ?>
                                </div>
                            </div>
                </div>
        </form>
    </div>
    <div class="col-md-1">  
    </div>
</div> 
<?php $this->endWidget(); ?>