<script type="text/css" src="<?php echo Yii::app()->request->baseUrl; ?>/custom/css/switch-button.css"></script>

<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting' => Yii::app()->urlManager->createUrl('operator/accounting/index'),
            'Chart Of Account',
        // 'Active'=>array('active', 
        //     )
        ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-laptop"></span> Manage Chart of Account</h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">COA List</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
            	<div class="btn-group pull-right">
                    <?php echo CHtml::link('<i class="fa fa-plus"></i>', 'create', array('class'=>'btn btn btn-primary pull-right')); ?>
                </div> 
                <div class="btn-group pull-left">
                    
                </div> 
            </div>
            
            <div class="panel-body">
            <?php echo CHtml::beginForm(array('chartOfAccount/exportExcel')); ?>
            <?php $this->widget('booster.widgets.TbGridView',array(
            'id'=>'user-grid',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'responsiveTable' => true,
            'pager' => array(
                'header' => '',
                'footer' => '',
                'cssFile' => false,
                'selectedPageCssClass' => 'active',
                'hiddenPageCssClass' => 'disabled',
                'firstPageCssClass' => 'previous',
                'lastPageCssClass' => 'next',
                'maxButtonCount' => 5,
                'firstPageLabel'=> '<i class="fa fa-angle-double-left"></i>',
                'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
                'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
                'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
                'htmlOptions' => array(
                    'class' => 'dataTables_paginate paging_bootstrap pagination pull-right',
                    'align' => 'center',
                    'style' => 'width: 100%;',
                ),
                // 'filterHtmlOptions' => array('style' => 'width: 30px;'),
            ),
            'columns'=>array(
                array(
                    'header' => 'No.',
                    'htmlOptions' => array(
                        'width' => '20px',
                        'style' => 'text-align:center'
                    ),
                    'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                    // 'class'=>'booster.widgets.TbTotalSumColumn',
                ),
                array(
                    'name' => 'coa_id',
                    'type' => 'raw',
                ),
        		array(
                    'name' => 'coa_desc',
                    'type' => 'raw',
                ),
                array(
                    'name' => 'coa_coat_id',
                    'type' => 'raw',
                    'filter' => ChartOfAccount::listType(),
                    'value' => '$data->getType->coat_desc',
                ),
                array(
                    'name' => 'coa_coar_id',
                    'type' => 'raw',
                    'filter' => ChartOfAccount::listRule(),
                    'value' => '$data->getRule->coar_desc',
                ),
                array(
                    'name' => 'coa_code',
                    'type' => 'raw',
                    'filter' => array(
                                    0 => Yii::t('app', 'Unknown'),
                                    1 => Yii::t('app', 'Debet'),
                                    2 => Yii::t('app', 'Kredit'),
                                ),
                    'value' => function($data){
                            if($data->coa_code == 1){
                                return 'Debet';
                            }else if($data->coa_code == 2){
                                return 'Kredit';
                            }else{
                                return 'Unknown';
                            }
                    }
                ),
                array(
                    'name' => 'coa_status',
                    'type' => 'raw',
                    'filter' => array(
                                    1 => Yii::t('app', 'Active'),
                                    0 => Yii::t('app', 'Inactive'),
                                ),
                    'value' => function($data){
                        // if($data->coa_status == 1){
                        //     return 'Active';
                        // }else{
                        //     return 'Inactive';
                        // }
                        $isChecked = '';
                        if($data->coa_status == 1){
                            $isChecked = 'checked="checked"';
                        }
                        return '<label class="switch"><input type="checkbox" id="status_'.$data->coa_id.'" name="status_'.$data->coa_id.'" '.$isChecked.'  class="checkSingleCoa" value="'.$data->coa_id.'"  data-id="'.$data->coa_id.'" data-url="'.Yii::app()->urlManager->createUrl('operator/chartOfAccount/updateStatus').'"><span class="slider"></span></label>';
                    }
                ),
                array(    
                    'header'=>'',
                    'type'=>'raw', 
                    'value' =>function($data) {
                                if($data->coa_status == 1){
                                    return CHtml::link('<i class="fa fa-pencil"></i>',['/operator/chartOfAccount/update/id/'.$data->coa_id],['class' => 'btn btn-info', 'title' => 'Update'])
                                    // .' '.CHtml::link('<i class="fa fa-trash-o"></i>',['/operator/chartOfAccount/delete/id/'.$data->coa_id],['class' => 'btn btn-danger', 'title' => 'Delete', 'confirm' => 'Are you sure to delete "'.$data->coa_desc.'" ?'])
                                    ;
                                }else{
                                    return CHtml::link('<i class="fa fa-pencil"></i>',['/operator/chartOfAccount/update/id/'.$data->coa_id],['class' => 'btn btn-info', 'title' => 'Update']);
                                }
                            },
                ),
            ),
            )); 
            ?>
            <br></br>
            <hr></hr>
            <div class="col-md-6">
                <div class="col-md-3">
                <select name="fileType" class="form-control">
                    <option value="Excel">EXCEL 5 (xls)</option>
                    <!-- <option value="CSV">CSV</option> -->
                </select>
                </div>
                <div class="col-md-3">
                <?php echo CHtml::submitButton('Export',array(
                            'buttonType' => 'submit',
                            'context'=>'primary',
                            'label'=>'Export Excel',
                            'class'=>'btn btn-primary',
                        )); 
                ?>
                <?php echo CHtml::endForm(); ?>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/custom/js/coa/coa.js"></script>