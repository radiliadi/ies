<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Employee' ,
        'Finances',
        // 'Semua' => array('Semua'=>'admin', 
        'Earnings'=>array('admin'
          )
        ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-plus-square"></span> <?=$title;?> </h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?=$title;?> List</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
            	<div class="btn-group pull-right">
                </div> 
            </div>
            <div class="panel-body">
			<?php echo CHtml::beginForm(array('earnings/exportExcel')); ?>
			<?php $this->widget('booster.widgets.TbGridView',array(
			'id'=>'earnings-grid',
			// 'type' => ' condensed',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'responsiveTable' => true,
			'pager' => array(
			    'header' => '',
			    'footer' => '',
			    'cssFile' => false,
			    'selectedPageCssClass' => 'active',
			    'hiddenPageCssClass' => 'disabled',
			    'firstPageCssClass' => 'previous',
			    'lastPageCssClass' => 'next',
			    'maxButtonCount' => 5,
			    'firstPageLabel'=> '<i class="fa fa-angle-double-left"></i>',
			    'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
			    'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
			    'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
			    'htmlOptions' => array(
			        'class' => 'dataTables_paginate paging_bootstrap pagination pull-right',
			        'align' => 'center',
			    )
			),
			// 'summaryText' => '',
			// 'emptyText'=>'Belum ada thread pada kategori ini',
			'columns'=>array(
				array(
					'header' => 'No.',
					'htmlOptions' => array(
						'width' => '20px',
						'style' => 'text-align:center'
					),
					'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
					// 'class'=>'booster.widgets.TbTotalSumColumn',
				),
				'earn_id_pd',
				array(
			      'name'=>'earn_overtime',
			      'value'=>function($data){
			      	if($data->earn_overtime == 0){
			        	return ' -';
			        }else{
			        	return 'Rp. '.number_format($data->earn_overtime,0);
			        }
			      }
			    ),
			    array(
			      'name'=>'earn_daily_trans',
			      'value'=>function($data){
			      	if($data->earn_daily_trans == 0){
			        	return ' -';
			        }else{
			        	return 'Rp. '.number_format($data->earn_daily_trans,0);
			        }
			      }
			    ),
			    array(
			      'name'=>'earn_func_allow',
			      'value'=>function($data){
			      	if($data->earn_func_allow == 0){
			        	return ' -';
			        }else{
			        	return 'Rp. '.number_format($data->earn_func_allow,0);
			        }
			      }
			    ),
			    array(
			      'name'=>'earn_breakfast',
			      'value'=>function($data){
			      	if($data->earn_breakfast == 0){
			        	return ' -';
			        }else{
			        	return 'Rp. '.number_format($data->earn_breakfast,0);
			        }
			      }
			    ),
				array(    
	              'header'=>'',
	              'type'=>'raw', 
	              'value' =>function($data) {
	                          return CHtml::link('<i class="fa fa-pencil"></i>',['/operator/earnings/update/id/'.$data->earn_id],['class' => 'btn btn-info', 'title' => 'Update']);
	                      },
	          	),
			),
			)); 
            ?>
            <br></br>
            <hr></hr>
            <div class="col-md-6">
                <div class="col-md-3">
                <select name="fileType" class="form-control">
                    <option value="Excel">EXCEL 5 (xls)</option>
                    <!-- <option value="CSV">CSV</option> -->
                </select>
                </div>
                <div class="col-md-3">
                <?php echo CHtml::submitButton('Export',array(
                            'buttonType' => 'submit',
                            'context'=>'primary',
                            'label'=>'Export Excel',
                            'class'=>'btn btn-primary',
                        )); 
                ?>
                <?php echo CHtml::endForm(); ?>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>