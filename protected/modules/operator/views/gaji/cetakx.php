<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Report' ,
    	'Payroll',
    	'Monthly',
        ),
    )
);
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pegawai-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data',
		'role'=>'form',
	),
)); 

?>


<div class="row">
    <div class="col-md-1">
    </div>
        <div class="col-md-10">
            <form class="form-horizontal">
                <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                        	<div class="form-group">	<br></br>
                            <h3>  &nbsp;  &nbsp; Monthly Report</h3>
			<table>
				<tbody>
					<tr>
						<td><!--?php echo $tahun?--> &nbsp;</td>
						<td class="center">
							<?php
								$i=01;
								$value = $i;
								for($i=1; $i <= 12; $i++){
									$bln[$i]=$i;
									// $bln[$i]=str_pad($i, 2, "0", STR_PAD_LEFT);
									// echo '<pre>';
								// print_r($bulan);
								// echo '</pre>';
								}
								for($i=2016;$i<=date('Y');$i++){
									$thn[$i]=$i;
								}
								// for($i=1;$i<=12;$i++){
								// $bln[$i]=$i;
								// }
							?>
							<div>
								<div style="float:left;margin:5px;">
								<?php 
								echo $form->dropDownList($modelbulan,'bulan',$bln,array('empty'=>'Pilih Bulan','class'=>"form-control", 'style'=>"float:left;")); 
								?>															
								</div>
								<div style="float:left;margin:5px;">
								<?php 
								echo $form->dropDownList($modelbulan,'tahun',$thn,array('empty'=>'Pilih Tahun','class'=>"form-control", 'style'=>"float:left;")); 
								?>															
								</div>
							</div>	
						</td>
						<td>	
							<?php echo CHtml::submitButton('Lihat', array('class'=>'btn btn-default', 'style'=>'width:100px')); ?>					
						</td>
					</tr>
				</tbody>
			</table>
			<br></br>
                        </div>
                        </div>
                </div>
        </form>
    </div>
    <div class="col-md-1">  
    </div>
</div> 

<?php $this->endWidget(); ?>