<?php
$pdf->AliasNbPages();
$pdf->AddPage('P',$size);                            
$pdf->SetFont('Times','b',16);     
$pdf->Image('images/png.png',9,9,25,25);      
$pdf->Ln(1);
$pdf->MultiCell(190,15,$header0,0,'C',false);
$pdf->SetFont('Times','b',12);
$pdf->MultiCell(190,5,$header,0,'C',false);
$pdf->Ln(12); //Line break
$pdf->SetFont('Times','',16);
$pdf->MultiCell(190,5,$judul,0,'C',false);
$pdf->Ln(7); //Line break
//mulai tabel
//line break
$pdf->Ln(2);
        $pdf->SetFont('Times','b',10);
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetFillColor(104,104,104); //Set background of the cell to be that grey color
        $pdf->SetTextColor(0,0,0);
// $pdf->Cell(5,5,'',0,0,'C',false);
$pdf->Cell(10,7,'No',1,0,'C',false);
$pdf->Cell(30,7,'NIP',1,0,'C',false);
$pdf->Cell(50,7,'NAMA LENGKAP',1,0,'C',false);
$pdf->Cell(25,7,'BULAN',1,0,'C',false);
$pdf->Cell(45,7,'JABATAN',1,0,'C',false);
$pdf->Cell(30,7,'NETT',1,0,'C',false);
$pdf->Ln(7); //Line break
//Data dari database
		$no=1;
		$sum=0;
		foreach($model as $db)
		{
			$sum=$sum+$db->tot_gaji;
			$pdf->SetFont('Times','',10);
			// $pdf->Cell(5,5,'',0,0,'C',false);
			$pdf->Cell(10,7,$no,1,0,'C',false);
			$pdf->Cell(30,7,$db->datakaryawan['id_pd'],1,0,'L',false);
			$pdf->Cell(50,7,$db->datakaryawan['nm_pd'],1,0,'L',false);
			$pdf->Cell(25,7,$db->bulan,1,0,'C',false);
			$pdf->Cell(45,7,$db->datajabatan['nm_jabatan'],1,0,'C',false);
			$pdf->Cell(30,7,"Rp. ".number_format($db->tot_gaji),1,0,'C',false);
			$pdf->Ln(7);
			$no++;
		}
//selesai tabel
//tanda tangan
//Baris baru
$pdf->SetFont('Times','b',10);
// $pdf->Cell(5,5,'',0,0,'C',false);
// $pdf->Cell(10,7,'',1,0,'C',false);
// $pdf->Cell(30,7,'',1,0,'L',false);
// $pdf->Cell(50,7,'',1,0,'L',false);
// $pdf->Cell(25,7,'',1,0,'C',false);
$pdf->Cell(160,7,'TOTAL',1,0,'C',false);
$pdf->Cell(30,7,"Rp. ".number_format($sum),1,0,'C',false);
$pdf->Ln(7);

$pdf->Ln(7);	
$pdf->SetFont('Times','',10);
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(155);
$pdf->MultiCell(80,5,'JAKARTA, '.date('d M Y'),0,'L',false);
//Baris Baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(155);
$pdf->MultiCell(80,5,'Direktur Utama,',0,'L',false);
$pdf->Ln(12);
//Baris baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(155);
$pdf->MultiCell(80,5,'Firman Alvianto',0,'L',false);
//Cetak PDF
// $pdf->Output('('.date('dMY').')-'.$model->id_gaji.'-'.$model->datakaryawan['id_pd'].'-'.$model->datakaryawan['nm_pd'].'.pdf','I');
$pdf->Output('('.date('dMY').')-'.$judul.'.pdf','I');
?>