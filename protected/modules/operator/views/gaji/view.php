<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Employee' ,
        'Payroll'=>'../../../karyawan/pay',
        'Detail',
        $model->id_pd,
      ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
    <h2><span class="fa fa-money"></span> Payroll</h2>
  	</div>
  	<div class="col-md-12">
    	<div class="panel panel-default">
	      	<div class="panel-heading">
		        <h3 class="panel-title">Detail - <?=$model->id_pd;?></h3>
		        <ul class="panel-controls">
		            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
		            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
		            <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
		        </ul>   
		        <div class="btn-group pull-right">
		        </div>
	      	</div>
      		<div class="panel-body">
				<div class="form"> 
				<?php
				$form=$this->beginWidget('CActiveForm', array(
					'id'=>'gaji-form',
					'enableAjaxValidation'=>false,
					'htmlOptions'=>array(
						'enctype'=>'multipart/form-data',
						'role'=>'form',
					),
				));
				$level=Yii::app()->session->get('level');
				?>
					<div class="row">
    					<div class="box col-md-12 ">   
    						<div class="btn-group pull-right">
                                <?php echo'<td class="center"><a class="btn btn-danger pull-right" href="../../delete/id_gaji/'.$model->id_gaji.'" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>'; ?>
                            </div> 
                            <div class="btn-group pull-left">
                            <?php echo'
							<td class="center">
								<a class="btn btn-primary" href="../../admin/id/'.$model->id_pd.'">
								<i class="fa fa-arrow-left"></i></a>
							</td>
							';	
							?>
							</div> 
							<div class="panel-body">	  
								<div class="table-responsive">
	                          		<table class="table table-hover">
                                    <tbody>
                                        <tr>
											<td><h3>P A Y &nbsp; I N F O</h3></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>No. Payslip</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $model->id_gaji;?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>NIP</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $model->id_pd;?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Nama Lengkap</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $model->datakaryawan['nm_pd'];?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>NPWP</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $model->datakaryawan['npwp'];?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Jabatan</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $model->datajabatan['nm_jabatan']; ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Tanggal Terima Gaji</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $model->tgl; ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr bgcolor="#33414e">
											<td></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><h3>E A R N I N G S</h3></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Basic Salary</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->gj_pokok); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Field Allowances</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->tunjangan); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Overtime</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->overtime); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>Daily Trans Allowances</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->transport); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>Functional Allowances</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->functional); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Meals</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->breakfast); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>Total Earnings</b></td>
											<td class="center"><b>:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->bruto); ?></b></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr bgcolor="#33414e">
											<td></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><h3>T A X &nbsp; C A L C U L A T I O N</h3></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>Biaya Jabatan</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->potongan); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Netto Sebulan</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->neto_sebulan); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Netto Setahun</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->neto_setahun); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>PTKP Setahun</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->ptkp_setahun); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>Penghasilan Kena Pajak Setahun</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->pkps); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>PPH Setahun</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->pph_setahun); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>PPH Sebulan</b></td>
											<td class="center"><b>:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->pph_sebulan); ?></b></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr bgcolor="#33414e">
											<td></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><h3>D E D U C T I O N S</h3></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Loan</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->deduc_basic); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>BPJS Ketenagakerjaan</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->bpjs_kete); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>BPJS Kesehatan</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->bpjs_kese); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>BPJS Pensiun</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->bpjs_pensi); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>Tunjangan Kesehatan</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->bpjs_pensi); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Total Deductions</b></td>
											<td class="center"><b>:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->tot_deduc); ?></b></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr bgcolor="#33414e">
											<td></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><h3>S U M M A R Y</h3></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Nett Salary</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <b><?php echo "Rp. ".number_format($model->tot_gaji); ?></b></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr bgcolor="#33414e">
											<td></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
									</tbody>
									</table>
								</div>
								</br>
									<table>
									<tbody>			
									<?php 
									echo'
									<td class="center">
										<a class="btn btn-primary" href="../../admin/id/'.$model->id_pd.'">
										<i class="fa fa-arrow-left"></i></a>
									</td>
									';	
									?>  
									</tbody>
									</table>		
							</div>	
						</div>
					</div>
					<?php $this->endWidget(); ?>
				</div>
			</div>
		</div>
	</div>
</div>