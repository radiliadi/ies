<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Employee' ,
        'Payroll'=>'../../../karyawan/pay',
        $modelkaryawan->id_pd,
      ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
    <h2><span class="fa fa-money"></span> Payroll</h2>
  	</div>
  	<div class="col-md-12">
    	<div class="panel panel-default">
	      	<div class="panel-heading">
		        <h3 class="panel-title">Payroll - <?=$modelkaryawan->id_pd;?> - <?=$modelkaryawan->nm_pd;?></h3>
		        <ul class="panel-controls">
		            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
		            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
		            <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
		        </ul>   
		        <div class="btn-group pull-right">
		        </div>
	      	</div>
      		<div class="panel-body">
				<div class="form"> 
				<?php

				$form=$this->beginWidget('CActiveForm', array(
					'id'=>'gaji-form',
					'enableAjaxValidation'=>false,
					'htmlOptions'=>array(
						'enctype'=>'multipart/form-data',
						'role'=>'form',
					),
				));

				$level=Yii::app()->session->get('level');
				?>
	
					<div class="row">
					    <div class="box col-md-12 ">    
							<div class="box-content">
								<h1></h1>
								<table>
									<tbody>
										<tr>
											<td><b>Nama Lengkap</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $modelkaryawan->nm_pd;?></td>
										</tr>
										<tr>
											<td><b>NIK</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $modelkaryawan->id_pd;?></td>
										</tr>
										<tr>
											<td><b>Tahun</b></td>
											<td>:&nbsp; &nbsp; <?php echo $tahun?> &nbsp;</td>
										<tr>
											<td class="center"> 
												<?php
													for($i=2015;$i<=date('Y');$i++){
													$thn[$i]=$i;
													}
												?>
													<div> &nbsp;
														<div style="float:left;margin:5px;">
														<?php 
														echo $form->dropDownList($modelbulan,'tahun',$thn,array('empty'=>'Pilih Tahun','class'=>"form-control", 'style'=>"float:left;")); 
														?>															
														</div>
													</div>	
											</td>
											<td>	
												<?php echo CHtml::submitButton('Lihat', array('class'=>'btn btn-default', 'style'=>'width:100px')); ?>
											
											</td>
										</tr>
									</tbody>
								</table>	
						
								<div class="panel-body">	  
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="" style="background-color:#FFE4E1">
											<thead>
												<tr>
													<th>No</th>
													<th>&nbsp; &nbsp; &nbsp; Bulan</th>
													<th>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Total Earnings</th>
													<!-- <th>&nbsp; &nbsp; Tax Pph 21</th> -->
													<th>&nbsp; &nbsp; Total Deduction</th>
													<th>&nbsp; &nbsp; Nett Salary</th>
													<th>&nbsp; &nbsp; Created By</th>
													<th>Print</th>
													<th>Payslip</th>
												</tr>
											</thead>
								    <tbody>
										<?php 
										$no=1;
										$sumEarn=0;
										$sumDeduc=0;
										$sumNett=0;
										foreach($model as $db)
										{ 
											$modelkaryawan=Karyawan::model()->findByPk($db->id_pd);
											$sumEarn=$sumEarn+$db->bruto;
											$sumDeduc=$sumDeduc+$db->tot_deduc;
											$sumNett=$sumNett+$db->tot_gaji;
											echo'
											<tr>
												<td>'.$no.'</td>
												<td>'.$db->bulan.' </td>
												<td class="center">Rp. '.number_format($db->bruto).'</td>
												<td class="center">Rp. '.number_format($db->tot_deduc).'</td>
												<td class="center">Rp. '.number_format($db->tot_gaji).'</td>
												<td class="center"><a class="btn btn-default btn-rounded" title='.$db->user_agent.'>'.$db->created_by.'|'.$db->date_created.'</a></td>
												<td class="center"><div class="btn-group">
								                    <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><i class="glyphicon glyphicon-print"></i><span class="caret"></span></a>
								                    <ul class="dropdown-menu" role="menu">
								                        <li><a href="../../slip/id_gaji/'.$db->id_gaji.'" title="Payslip"><i class="fa fa-money"></i>Payslip</a></li>                                                    
								                    </ul>
								                    </div>
								                </td>
												<td class="center">
													&nbsp;
													<a class="btn btn-primary" href="../../view/id_gaji/'.$db->id_gaji.'" title="Detail Payslip">
														<i class="glyphicon glyphicon-search"></i>
													</a>
												</td>
											</tr>
											';
										$no++;
										}
											echo'
											<tr>
												<td></td>
												<td><b>TOTAL</b></td>
												<td class="center"><b>Rp. '.number_format($sumEarn).'</b></td>
												<td class="center"><b>Rp. '.number_format($sumDeduc).'</b></td>
												<td class="center"><b>Rp. '.number_format($sumNett).'</b></td>
												<td class="center"></td>
												<td class="center"></td>
												<td class="center"></td>
											</tr>
											';
										?>
										<tr></tr>
										<th class="center" colspan="7">		
											<div style="float:left;margin:10px;">
											Masukkan Tanggal Gajian
											</div>
											<?php
												//ini utk tanggal : start-----------------------------------------------------------
												for($i=1;$i<=31;$i++){
													$tgl1[$i]=$i;
													}
											?>
											<?php
												for($i=1;$i<=12;$i++){
													$bln1[$i]=$this->bulan($i);
													}
											?>
											<?php
												for($i=2010;$i<=date('Y');$i++){
													$thn1[$i]=$i;
													}
											?>
											<div>
												<div style="float:left;margin:5px;">
													<?php 
														echo $form->dropDownList($modeltanggal,'tanggal1',$tgl1,array('empty'=>'Pilih Tanggal','class'=>"form-control", 'style'=>"float:left;margin;")); 
													?>
												</div>
												<div style="float:left;margin:5px;">
													<?php 
														echo $form->dropDownList($modeltanggal,'bulan1',$bln1,array('empty'=>'Pilih Bulan','class'=>"form-control", 'style'=>"float:center;")); 
													?>
												</div>
												<div style="float:left;margin:5px;">
													<?php 
														echo $form->dropDownList($modeltanggal,'tahun1',$thn1,array('empty'=>'Pilih Tahun','class'=>"form-control", 'style'=>"float:left;",)); 
													//ini utk tgl : end----------------------------------------------
													?>															
												</div>
											</div>	
										</th>
										<tr></tr>
										<th class="center" colspan="7">
											<a class="btn btn-default" href="../../../karyawan/pay">
												<i class="icon-arrow-left"></i>
												Kembali
											</a>
										<?php echo CHtml::submitButton('Tambah', array('class'=>'btn btn-default', 'style'=>'width:100px')); ?>
										</th>
								    </tbody>
								</table>	
										</div>
									</div>
								</div>
								</div>	
							</div>
						</div>
					<?php $this->endWidget(); ?>
					</div>
			</div>
		</div>
	</div>
</div>