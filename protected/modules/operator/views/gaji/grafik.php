<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Report' ,
    	'Payroll',
    	'Graph',
        ),
    )
);
?>

<div class="col-md-12">
	<br>
	<h2><b>Statistik Berdasarkan Golongan</b></h2>
	<br>
		<?php
			$this->breadcrumbs=array(
			'Chart'=>array('grafik'),
			'Statistik Gaji Karyawan',
			);
			
				$form=$this->beginWidget('CActiveForm', array(
				'id'=>'tinstrument-form',
				'enableAjaxValidation'=>false,
		)); 

				$label=array();
				$nilai=array();
					foreach($dataProvider->getData() as $i=>$ii)
						{
							$label[$i]=$ii['jabatan'];
							$nilai[$i]=(int)$ii['total'];
						}
						
				$this->widget('application.extensions.highcharts.HighchartsWidget', array(
					'options'=>array(
					'chart'=> array('defaultSeriesType'=>'column',),
					'title' => array('text' => ''),
					'legend'=>array('enabled'=>false),
					'xAxis'=>array('categories'=>$label,
					'title'=>array('text'=>''),),
					'yAxis'=> array(
						'min'=> 0,
					'title'=> array(
						'text'=>'Jumlah'
					),
				),
					'series' => array(
						array('data' => $nilai)
					),
					
					'tooltip' => array('formatter' => 'js:function(){ return "<b>"+this.point.name+"</b>:"+this.y;}'),
					'tooltip' => array(
					'formatter' => 'js:function() {return "<b>"+ this.x +"</b><br/>"+" Jumlah: "+this.y;}'
					),
						'plotOptions'=>array('pie'=>(array(
							'allowPointSelect'=>true,
							'showInLegend'=>true,
							'cursor'=>'pointer',
								)
							)
						),
						'credits'=>array('enabled'=>false),
						)
					));
				// echo 'Nilai'.number_format($ii['total']);
					
		?>
</div>	
<?php $this->endWidget(); ?>
</div>