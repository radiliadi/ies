<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Settings' ,
        'Employee',
        // 'Semua' => array('Semua'=>'admin', 
        'Kind of Registration'=>array('admin'
          ),
        'Create',
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Create Kind of Registration</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>