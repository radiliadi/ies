<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Settings' ,
        'Employee',
        // 'Semua' => array('Semua'=>'admin', 
        'Kind of Registration'=>array('admin'
          ),
        'Update',
        $model->id_jns_daftar,
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update Division : #<?=$model->id_jns_daftar;?></h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>