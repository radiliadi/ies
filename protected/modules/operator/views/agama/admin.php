<script type="text/css" src="<?php echo Yii::app()->request->baseUrl; ?>/custom/css/switch-button.css"></script>

<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Settings' ,
        'Employee',
        // 'Semua' => array('Semua'=>'admin', 
        'Religion'=>array('admin'
          )
        ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-cog"></span> <?=$title;?> </h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?=$title;?> List</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
            	<div class="btn-group pull-right">
            		<?php echo CHtml::link('<i class="fa fa-plus"></i>', 'create', array('class'=>'btn btn btn-primary pull-right')); ?>
                </div> 
            </div>
            <div class="panel-body">
			<?php echo CHtml::beginForm(array('agama/exportExcel')); ?>
			<?php $this->widget('booster.widgets.TbGridView',array(
			'id'=>'divisi-grid',
			// 'type' => ' condensed',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'responsiveTable' => true,
			'pager' => array(
			    'header' => '',
			    'footer' => '',
			    'cssFile' => false,
			    'selectedPageCssClass' => 'active',
			    'hiddenPageCssClass' => 'disabled',
			    'firstPageCssClass' => 'previous',
			    'lastPageCssClass' => 'next',
			    'maxButtonCount' => 5,
			    'firstPageLabel'=> '<i class="fa fa-angle-double-left"></i>',
			    'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
			    'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
			    'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
			    'htmlOptions' => array(
			        'class' => 'dataTables_paginate paging_bootstrap pagination pull-right',
			        'align' => 'center',
			    )
			),
			// 'summaryText' => '',
			// 'emptyText'=>'Belum ada thread pada kategori ini',
			'columns'=>array(
				array(
					'header' => 'No.',
					'htmlOptions' => array(
						'width' => '20px',
						'style' => 'text-align:center'
					),
					'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
					// 'class'=>'booster.widgets.TbTotalSumColumn',
				),
				'agm_desc',
				// array(
				// 	'class'=>'booster.widgets.TbButtonColumn',
				// 	'template'=>'{update}',
				// ),
	          	array(
                    'name' => 'agm_status',
                    'type' => 'raw',
                    'filter' => array(
                                    1 => Yii::t('app', 'Active'),
                                    0 => Yii::t('app', 'Inactive'),
                                ),
                    'value' => function($data){
                        $isChecked = '';
                        if($data->agm_status == 1){
                            $isChecked = 'checked="checked"';
                        }
                        return '<label class="switch"><input type="checkbox" id="status_'.$data->agm_id.'" name="status_'.$data->agm_id.'" '.$isChecked.'  class="checkSingleCoa" value="'.$data->agm_id.'"  data-id="'.$data->agm_id.'" data-url="'.Yii::app()->urlManager->createUrl('operator/agama/updateStatus').'"><span class="slider"></span></label>';
                    }
                ),
			),
			)); 
            ?>
            <br></br>
            <hr></hr>
            <div class="col-md-6">
                <div class="col-md-3">
                <select name="fileType" class="form-control">
                    <option value="Excel">EXCEL 5 (xls)</option>
                    <!-- <option value="CSV">CSV</option> -->
                </select>
                </div>
                <div class="col-md-3">
                <?php echo CHtml::submitButton('Export',array(
                            'buttonType' => 'submit',
                            'context'=>'primary',
                            'label'=>'Export Excel',
                            'class'=>'btn btn-primary',
                        )); 
                ?>
                <?php echo CHtml::endForm(); ?>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/custom/js/agama/agama.js"></script>