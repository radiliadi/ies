<?php
/* @var $this BpjsController */
/* @var $model Bpjs */

$this->breadcrumbs=array(
	'Bpjs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Bpjs', 'url'=>array('index')),
	array('label'=>'Manage Bpjs', 'url'=>array('admin')),
);
?>

<h1>Create Bpjs</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>