<?php
/* @var $this BpjsController */
/* @var $model Bpjs */

$this->breadcrumbs=array(
	'Bpjs'=>array('index'),
	$model->bpjs_id=>array('view','id'=>$model->bpjs_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Bpjs', 'url'=>array('index')),
	array('label'=>'Create Bpjs', 'url'=>array('create')),
	array('label'=>'View Bpjs', 'url'=>array('view', 'id'=>$model->bpjs_id)),
	array('label'=>'Manage Bpjs', 'url'=>array('admin')),
);
?>

<h1>Update Bpjs <?php echo $model->bpjs_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>