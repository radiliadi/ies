<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
'id'=>'bpjs-form',
// 'enableAjaxValidation'=>true,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-md-1">
    </div>
        <div class="col-md-10">
            <form class="form-horizontal">
                <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Nama Kategori</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <?php echo $form->textField($model,'bpjs_nama',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Nama Kategori"));?>
                                        <?php echo $form->error($model,'bpjs_nama'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">BPJS Ketenagakerjaan</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <?php echo $form->textField($model,'bpjs_kete',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Ketenagakerjaan"));?>
                                        <?php echo $form->error($model,'bpjs_kete'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">BPJS Ketenagakerjaan (Kantor)</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <?php echo $form->textField($model,'bpjs_kete_ktr',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Ketenagakerjaan (Kantor)"));?>
                                        <?php echo $form->error($model,'bpjs_kete_ktr'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">BPJS Kesehatan</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <?php echo $form->textField($model,'bpjs_kese',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Kesehatan"));?>
                                        <?php echo $form->error($model,'bpjs_kese'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">BPJS Kesehatan (Kantor)</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <?php echo $form->textField($model,'bpjs_kese_ktr',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Kesehatan (Kantor)"));?>
                                        <?php echo $form->error($model,'bpjs_kese_ktr'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">BPJS Pensiun</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <?php echo $form->textField($model,'bpjs_pensi',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Pensiun"));?>
                                        <?php echo $form->error($model,'bpjs_pensi'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">BPJS Pensiun (Kantor)</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <?php echo $form->textField($model,'bpjs_pensi_ktr',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Pensiun (Kantor)"));?>
                                        <?php echo $form->error($model,'bpjs_pensi_ktr'); ?>

                                    </div>                                            
                                </div>
                            </div>
                        </div>
                            <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
    									'buttonType'=>'submit',
    									'context'=>'primary',
    									'label'=>$model->isNewRecord ? 'Create' : 'Save',
    								)); ?>
                                </div>
                            </div>
                </div>
        </form>
    </div>
    <div class="col-md-1">  
    </div>
</div> 
<?php $this->endWidget(); ?>