<?php
/* @var $this ArusKasDetailController */
/* @var $model ArusKasDetail */

$this->breadcrumbs=array(
	'Arus Kas Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ArusKasDetail', 'url'=>array('index')),
	array('label'=>'Manage ArusKasDetail', 'url'=>array('admin')),
);
?>

<h1>Create ArusKasDetail</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>