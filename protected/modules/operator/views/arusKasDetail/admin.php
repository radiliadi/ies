<?php
// $id_pd=Yii::app()->session->get('username');
// if($id_pd !== $expenses->exp_id_pd)
// {
// throw new CHttpException('403', 'Access denied!');
// }
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting' => Yii::app()->urlManager->createUrl('operator/accounting/index'),
        'Arus Kas' => '../../../arusKas/admin' ,
        'Detail',
        $arusKasDetailId->akd_id,
      ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-laptop"></span> Arus Kas Detail - <small> <?=$arusKas->ak_desc ;?> </small> </h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"> </h3>
                <ul class="panel-controls">
                </ul>   
            	<div class="btn-group pull-right">
            		<?php 
            		if($arusKas->ak_is_locked == 1 ){
            			echo '';
            		}else{
            			echo CHtml::link('<i class="glyphicon glyphicon-download-alt"></i>', '../../generate/id/'.$arusKasDetailId->akd_id.'/date/'.$arusKasDetailId->akd_date, array('class'=>'btn btn btn-info pull-right', 'confirm' => 'Are you sure to generate? Your data will be automatic changed', 'title'=>'Generate')); 
            		}
            		

            		?>
                </div>
                <div class="btn-group pull-left">
            		<?php 
            		if($arusKas->ak_is_locked == 1 ){
            			echo '';
            		}else{
            			echo CHtml::link('<i class="glyphicon glyphicon-pencil"></i>', '../../update/id/'.$arusKasDetailId->akd_id, array('class'=>'btn btn btn-primary pull-left', 'confirm' => 'Are you sure to update?', 'title'=>'Update')); 
            		}
            		

            		?>
                </div>
            </div>
            <div class="panel-body">
				<div class="form"> 
				<?php

				$form=$this->beginWidget('CActiveForm', array(
					'id'=>'labaDetail-form',
					'enableAjaxValidation'=>false,
					'htmlOptions'=>array(
						'enctype'=>'multipart/form-data',
						'role'=>'form',
					),
				));
				?>
	
					<div class="row">
					    <div class="box col-md-12 ">    
							<div class="box-content">
								<h1></h1>
								<table>
									<tbody>
										<tr>
											<td><b>No. Arus Kas</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $arusKas->ak_id;?></td>
										</tr>
										<?php
											$time=strtotime($arusKas->ak_date);
											$year=date("Y", $time);
											$month=date("d F", $time);
										?>
										<tr>
											<td><b>Tahun</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $year;?></td>
										</tr>
										<tr>
											<td><b>Date Created</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $month;?></td>
										</tr>
										<tr>
											<td><b>Notes</b></td>
											<td class="center">:&nbsp; &nbsp;<font color="blue">*</font> (hasil generate), <font color="red">*</font> (diisi oleh admin)</td>
										</tr>
									</tbody>
								</table>	
								<br>
								</br>
								
								<div class="panel-body">	  
									<div class="table-responsive">
										<!-- START DEFAULT DATATABLE -->
                                    <table class="table">
                                        <tr>
											<td><b>Arus Kas dari Aktivitas Operasi</b></td>
											<td class="center"></td>
											<td></td>
										</tr>
											<tr>
												<td style="padding-left:3%;"><b>Laba (rugi) tahun berjalan <font color="blue">*</font></b><small>(Laba <small>(Rugi)</small>)</small></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($arusKasDetailId->akd_laba_jalan);?></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;"><b>Beban Penyusutan <font color="red">*</font></b><small>(Inventory)</small></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($arusKasDetailId->akd_bbn_susut);?></td>
												<td></td>
											</tr>
											<?php
												$total = 0;
												$total = $arusKasDetailId->akd_laba_jalan + $arusKasDetailId->akd_bbn_susut;
											?>
											<tr>
												<td style="padding-left:3%;"><b>Total</b></td>
												<td class="center"><b>:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($total);?></b></td>
												<td></td>
											</tr>
											<tr>
												<td style="padding-left:3%;"><b></b></td>
												<td class="center"><b>&nbsp; &nbsp;</b></td>
												<td></td>
											</tr>
										<tr>
											<td><b>Arus Kas dari Aktivitas Operasi</b></td>
											<td class="center"></td>
											<td></td>
										</tr>
											<tr>
												<td style="padding-left:3%;"><b>Kenaikan (penurunan)</b></td>
												<td class="center"></td>
												<td></td>
											</tr>
												<tr>
													<td style="padding-left:8%;"><b>Piutang Usaha <font color="blue">*</font></b><small>(Neraca)</small></td>
													<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($arusKasDetailId->akd_piutang_usaha);?></td>
													<td></td>
												</tr>
												<tr>
													<td style="padding-left:8%;"><b>Piutang Karyawan <font color="blue">*</font></b><small>(Neraca)</small></td>
													<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($arusKasDetailId->akd_piutang_kyw);?></td>
													<td></td>
												</tr>
												<tr>
													<td style="padding-left:8%;"><b>Pajak Dibayar Dimuka <font color="blue">*</font></b><small>(Neraca)</small></td>
													<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($arusKasDetailId->akd_pdd);?></td>
													<td></td>
												</tr>
										<?php
											$jmlAkOpe = 0;
											$jmlAkOpe = $arusKasDetailId->akd_piutang_usaha + $arusKasDetailId->akd_piutang_kyw + $arusKasDetailId->akd_pdd;
										?>
										<tr>
											<td><b>Jumlah Arus Kas dari Aktivitas Operasi</b></td>
											<td class="center"><b>:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($jmlAkOpe);?></b></td>
											<td></td>
										</tr>
										<tr>
											<td style="padding-left:3%;"><b></b></td>
											<td class="center"><b>&nbsp; &nbsp;</b></td>
											<td></td>
										</tr>
										<tr>
											<td><b>Arus Kas dari Aktivitas Investasi</b></td>
											<td class="center"></td>
											<td></td>
										</tr>
											<tr>
												<td style="padding-left:3%;"><b>Kenaikan (penurunan)</b></td>
												<td class="center"></td>
												<td></td>
											</tr>
												<tr>
													<td style="padding-left:8%;"><b>Perolehan aset tetap <font color="blue">*</font></b><small>(Neraca)</small></td>
													<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($arusKasDetailId->akd_aset_tetap);?></td>
													<td></td>
												</tr>
										<?php
											$jmlAkInv = 0;
											$jmlAkInv = $jmlAkInv + $arusKasDetailId->akd_aset_tetap;
										?>
										<tr>
											<td><b>Jumlah Arus Kas dari Aktivitas Investasi</b></td>
											<td class="center"><b>:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($jmlAkInv);?></b></td>
											<td></td>
										</tr>
										<tr>
											<td style="padding-left:3%;"><b></b></td>
											<td class="center"><b>&nbsp; &nbsp;</b></td>
											<td></td>
										</tr>
										<tr>
											<td><b>Arus Kas dari Aktivitas Pendanaan</b></td>
											<td class="center"></td>
											<td></td>
										</tr>
											<tr>
												<td style="padding-left:3%;"><b>Kenaikan (penurunan)</b></td>
												<td class="center"></td>
												<td></td>
											</tr>
												<tr>
													<td style="padding-left:8%;"><b>Setoran Modal <font color="red">*</font></b></td>
													<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($arusKasDetailId->akd_setor_modal);?></td>
													<td></td>
												</tr>
												<tr>
													<td style="padding-left:8%;"><b>Hutang Pemegang Saham <font color="red">*</font></b></td>
													<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($arusKasDetailId->akd_hutang_pemegang_saham);?></td>
													<td></td>
												</tr>
										<?php
											$jmlAkDana = 0;
											$jmlAkDana = $arusKasDetailId->akd_setor_modal + $arusKasDetailId->akd_hutang_pemegang_saham;
										?>
										<tr>
											<td><b>Jumlah Arus Kas dari Aktivitas Pendanaan</b></td>
											<td class="center"><b>:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($jmlAkDana);?></b></td>
											<td></td>
										</tr>
											<tr>
												<td style="padding-left:3%;"><b>Kenaikan (penurunan) kas dan setara kas <font color="red">*</font></b></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($arusKasDetailId->akd_naik_turun);?></td>
												<td></td>
											</tr>
											<?php
												$saldoAwal = ($jmlAkOpe + $jmlAkInv + $jmlAkDana);
											?>
											<tr>
												<td style="padding-left:3%;"><b>Saldo Awal Kas dan setara kas</b></td>
												<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($saldoAwal);?></td>
												<td></td>
											</tr>
											<?php
												$saldoAkhir = $saldoAwal + $arusKasDetailId->akd_naik_turun;
											?>
											<tr>
												<td style="padding-left:3%;"><b>Saldo Akhir Kas dan setara kas</b></td>
												<td class="center"><b>:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($saldoAkhir);?></b></td>
												<td></td>
											</tr>
										<tr>
											<td>&nbsp;</td>
											<td class="center">&nbsp; &nbsp;</td>
											<td></td>
										</tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                            	<div class="table-responsive">
                                    <table class="table">
                                    	<thead>
	                                    	<tr>
	                                    		<td></td>
	                                    		<td></td>
	                                    		<td></td>
	                                    	</tr>
	                                    </thead>
	                                    <tbody>
	                                    </tbody>
	                                    <?php
											if($arusKas->ak_is_locked == 0){
												echo'
												<tr>
													<td>&nbsp;</td>
													<td class="center">&nbsp; &nbsp;</td>
													<td><a class="btn btn btn-success pull-right" href="../../../../operator/arusKasDetail/lock/id/'.$arusKas->ak_id.'" title="Lock"><i class="fa fa-lock"></i>
														</a>
													</td>
												</tr>
												';
											}else if($arusKas->ak_is_locked == 1){
												echo'
												<tr>
													<td>&nbsp;</td>
													<td class="center">&nbsp; &nbsp;</td>
													<td><a class="btn btn btn-primary pull-left" href="../../../../operator/arusKasDetail/print/id/'.$arusKas->ak_id.'" title="Print"><i class="fa fa-print"></i>
														</a>
														<a class="btn btn btn-info pull-right" href="../../../../operator/arusKasDetail/unlock/id/'.$arusKas->ak_id.'" title="Lock"><i class="fa fa-unlock"></i>
														</a>
													</td>
												</tr>
												';
											}
	                                    ?>
                                    </table>
                               </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
						</div>
					</div>
				</div>

			</div>	
		</div>

	</div>
					<?php $this->endWidget(); ?>
					</div>
                <br>
                </br>
            </div>
            </div>
        </div>
    </div>
</div>