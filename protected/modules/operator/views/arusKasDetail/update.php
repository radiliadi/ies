<?php $this->widget('booster.widgets.TbBreadcrumbs',
	array('links' => array('Accounting'=>'../../../accounting/index',
	    'Arus Kas' => '../../../arusKas/admin',
	    'Detail' => '../../admin/id/'.$model->akd_id,
	   	$model->akd_id,
	    ),
	)
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-plus"></span> Update Arus Kas Detail : #<?=$model->akd_id;?> (<?=$model->akd_date?>)</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>