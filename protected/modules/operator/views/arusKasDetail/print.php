<?php
$time=strtotime($arusKas->ak_date);
$now=date('Y-m-d');
$now=strtotime($now);

$yearStart=date("Y", $time);
$yearStart1=date("Y", $time);
$yearEnd=date("Y", $now);
$monthStart=date("d F", $time);
$monthEnd=date("d F", $now);
if($yearStart == $yearEnd){
	$yearStart = '';
}

$pdf->AliasNbPages();
$pdf->AddPage('P',$size);                            
$pdf->SetFont('Times','b',14);     
$pdf->Image('images/png.png',145,9,35,25);      
$pdf->Ln(1);
$pdf->MultiCell(190,15,$header0,0,'C',false);
$pdf->SetFont('Times','b',12);
$pdf->MultiCell(190,5,$judul,0,'C',false);
$pdf->Ln(2);
if($monthStart == $monthEnd){
	$pdf->MultiCell(190,5,'Per '.$monthStart.' '.$yearStart1,0,'C',false);
}else{
	$pdf->MultiCell(190,5,'Per '.$monthStart.' '.$yearStart.' - '.$monthEnd.' '.$yearEnd,0,'C',false);
}

// $pdf->Ln(7); //Line break
//mulai tabel
//line break
//content
$pdf->Ln(10);
$pdf->SetFont('Times','b',10);
$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(104,104,104); //Set background of the cell to be that grey color
$pdf->SetTextColor(0,0,0);
$pdf->setLeftMargin(30);
$pdf->Cell(100,5,'',0,0,'L',false);
$pdf->SetFont('Times','u',10);
$pdf->SetFont('Times','bu',10);
$pdf->Cell(90,5,$monthStart.' '.$yearStart1,0,0,'L',false);	
$pdf->Ln(7);
$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'Arus Kas dari Aktivitas Operasi',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,' ',0,0,'L',false);	
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->setLeftMargin(40); //set margin ampe ke bawah
$pdf->Cell(90,5,'Laba (rugi) tahun berjalan',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($arusKasDetailId->akd_laba_jalan),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,'Beban Penyusutan',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($arusKasDetailId->akd_bbn_susut),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','b',10);
$pdf->Cell(90,5,'Total',0,0,'L',false);
$pdf->SetFont('Times','b',10);
$total = 0;
$total = $arusKasDetailId->akd_laba_jalan + $arusKasDetailId->akd_bbn_susut;
$pdf->Cell(100,5,'Rp. '.number_format($total),0,0,'L',false);
$pdf->setLeftMargin(30); //netralin margin
$pdf->Ln(10);

$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'Arus Kas dari Aktivitas Operasi',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,' ',0,0,'L',false);	
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->setLeftMargin(40); //set margin ampe ke bawah
$pdf->Cell(90,5,'Kenaikan (penurunan)',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,' ',0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->setLeftMargin(50);
$pdf->Cell(80,5,'Piutang Usaha',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(110,5,'Rp. '.number_format($arusKasDetailId->akd_piutang_usaha),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->setLeftMargin(50);
$pdf->Cell(80,5,'Piutang Karyawan',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(110,5,'Rp. '.number_format($arusKasDetailId->akd_piutang_kyw),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->setLeftMargin(50);
$pdf->Cell(80,5,'Pajak Dibayar Dimuka',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(110,5,'Rp. '.number_format($arusKasDetailId->akd_pdd),0,0,'L',false);
$pdf->setLeftMargin(30); //netralin margin
$pdf->Ln(7);

$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'Jumlah Arus Kas dari Aktivitas Operasi',0,0,'L',false);
$pdf->SetFont('Times','b',10);
$jmlAkOpe = 0;
$jmlAkOpe = $arusKasDetailId->akd_piutang_usaha + $arusKasDetailId->akd_piutang_kyw + $arusKasDetailId->akd_pdd;
$pdf->Cell(90,5,'Rp. '.number_format($jmlAkOpe),0,0,'L',false);	
$pdf->Ln(10);

$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'Arus Kas dari Aktivitas Investasi',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,' ',0,0,'L',false);	
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->setLeftMargin(40); 
$pdf->Cell(90,5,'Kenaikan (penurunan)',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,' ',0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->setLeftMargin(50);
$pdf->Cell(80,5,'Perolehan aset tetap',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(110,5,'Rp. '.number_format($arusKasDetailId->akd_aset_tetap),0,0,'L',false);
$pdf->setLeftMargin(30); //netralin margin
$pdf->Ln(7);

$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'Jumlah Arus Kas dari Aktivitas Operasi',0,0,'L',false);
$pdf->SetFont('Times','b',10);
$jmlAkInv = 0;
$jmlAkInv = $jmlAkInv + $arusKasDetailId->akd_aset_tetap;
$pdf->Cell(90,5,'Rp. '.number_format($jmlAkInv),0,0,'L',false);	
$pdf->Ln(10);

$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'Arus Kas dari Aktivitas Pendanaan',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(90,5,' ',0,0,'L',false);	
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->setLeftMargin(40); 
$pdf->Cell(90,5,'Kenaikan (penurunan)',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,' ',0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->setLeftMargin(50);
$pdf->Cell(80,5,'Setoran Modal',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(110,5,'Rp. '.number_format($arusKasDetailId->akd_setor_modal),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->setLeftMargin(50);
$pdf->Cell(80,5,'Hutang Pemegang Saham',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(110,5,'Rp. '.number_format($arusKasDetailId->akd_hutang_pemegang_saham),0,0,'L',false);
$pdf->setLeftMargin(30); //netralin margin
$pdf->Ln(7);

$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'Jumlah Arus Kas dari Aktivitas Pendanaan',0,0,'L',false);
$pdf->SetFont('Times','b',10);
$jmlAkDana = 0;
$jmlAkDana = $arusKasDetailId->akd_setor_modal + $arusKasDetailId->akd_hutang_pemegang_saham;
$pdf->Cell(90,5,'Rp. '.number_format($jmlAkDana),0,0,'L',false);	
$pdf->Ln(7);

$pdf->SetFont('Times','',10);
$pdf->setLeftMargin(40); 
$pdf->Cell(90,5,'Kenaikan (penurunan) kas dan setara kas',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Rp. '.number_format($arusKasDetailId->akd_naik_turun),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','b',10);
$pdf->setLeftMargin(40); 
$pdf->Cell(90,5,'Saldo Awal Kas dan setara kas',0,0,'L',false);
$pdf->SetFont('Times','b',10);
$saldoAwal = ($jmlAkOpe + $jmlAkInv + $jmlAkDana);
$pdf->Cell(100,5,'Rp. '.number_format($arusKasDetailId->akd_naik_turun),0,0,'L',false);
$pdf->Ln(7);

$pdf->SetFont('Times','b',10);
$pdf->setLeftMargin(40); 
$pdf->Cell(90,5,'Saldo Akhir Kas dan setara kas',0,0,'L',false);
$pdf->SetFont('Times','b',10);
$saldoAkhir = $saldoAwal + $arusKasDetailId->akd_naik_turun;
$pdf->Cell(100,5,'Rp. '.number_format($arusKasDetailId->akd_naik_turun),0,0,'L',false);
$pdf->Ln(7);

$pdf->Ln(10);	
$pdf->SetFont('Times','',10);
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(145);
$pdf->MultiCell(80,5,$company->cpy_city.', '.date('d M Y'),0,'L',false);
//Baris Baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(145);
$pdf->MultiCell(80,5,$company->cpy_fullname.',',0,'L',false);
$pdf->Ln(12);
//Baris baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(145);
$pdf->MultiCell(80,5,$company->cpy_dirut,0,'L',false);
//Cetak PDF
$pdf->Output('('.date('dMY').')-'.$judul.'.pdf','I');
?>