<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Settings' ,
        'Finance',
        // 'Semua' => array('Semua'=>'admin', 
        'PTKP'=>array('admin'
          ),
        'Update',
        $model->id_ptkp,
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update PTKP : #<?=$model->id_ptkp;?></h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>