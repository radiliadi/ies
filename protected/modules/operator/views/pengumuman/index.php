<?php
$id_pd=Yii::app()->session->get('username');
$this->breadcrumbs=array(
	'Pengumumen',
);
?>

<h1>Pengumuman</h1>

<div class="col-md-12">   
	
	<form action="#" class="form-horizontal">
     <div class="panel panel-default">
        <div class="panel-body"> 
			<div class="box-content">
		<a href="pengumuman/create" class="btn btn-success" style="margin-top:-2%" href="#">
			Tambah Pengumuman
		</a>
	</div>
					<div class="row">
						<div class="col-md-12">
							<form class="form-horizontal">
							<div class="panel panel-default tabs">
								<ul class="nav nav-tabs" role="tablist">
									<li class="active"><a href="#tab1" role="tab" data-toggle="tab">Mahasiswa</a></li>
									<li class=""><a href="#tab2" role="tab" data-toggle="tab">Dosen</a></li>
									<li class=""><a href="#tab3" role="tab" data-toggle="tab">Admin</a></li>
									
								</ul>
								<div class="panel-body tab-content">
									<div class="tab-pane active" id="tab1">
										<div class="panel-body">                          
											<div class="row">
												<?php $no=1;$this->widget('booster.widgets.TbGridView', array(
														'id'=>'Arsip',
														'dataProvider'=>$model->mhs(1),
														'columns'=>array(	
															array('header'=>'No','value'=>'$row+1'),
															'judul',
															'isi',
															'datetime',
															array(    
																'header'=>'Detail',
																'type'=>'raw', 
																'value'=>'CHtml::link("Detail", array("pengumuman/view/id/$data->id_pesan/"),array("class"=>"btn btn-success"))',
															),
															array(    
																'header'=>'Edit',
																'type'=>'raw', 
																'value'=>'CHtml::link("Edit", array("pengumuman/update/id/$data->id_pesan/"),array("class"=>"btn btn-success"))',
															),
															array(    
																'header'=>'Hapus',
																'type'=>'raw', 
																'value'=>'CHtml::link("hapus", array("pengumuman/delete/id/$data->id_pesan/"),array("class"=>"btn btn-danger"))',
															),
															
														),
												)); ?>
											</div>
										</div>					 
									</div>
									<?php
									for($i=2;$i<=2;$i++)
									{
									?>										
										<div class="tab-pane" id="tab<?php echo $i;?>">
											<div class="panel-body">                          
												<div class="row">
													<?php $this->widget('booster.widgets.TbGridView', array(
															'id'=>'Arsip',
															'dataProvider'=>$model->dosen($i),
															
															'columns'=>array(	
																array('header'=>'No','value'=>'$row+1'),
																'judul',
															'isi',
															'datetime',
															array(    
																'header'=>'Detail',
																'type'=>'raw', 
																'value'=>'CHtml::link("Detail", array("pengumuman/view/id/$data->id_pesan/"),array("class"=>"btn btn-success"))',
															),
															array(    
																'header'=>'Edit',
																'type'=>'raw', 
																'value'=>'CHtml::link("Edit", array("pengumuman/update/id/$data->id_pesan/"),array("class"=>"btn btn-success"))',
															),
															array(    
																'header'=>'Hapus',
																'type'=>'raw', 
																'value'=>'CHtml::link("hapus", array("pengumuman/delete/id/$data->id_pesan/"),array("class"=>"btn btn-danger"))',
															),
															),
													)); ?>
												</div>
											</div>					 
										</div>
									<?php
									}
									?>
										<div class="tab-pane" id="tab3">
											<div class="panel-body">                          
												<div class="row">
													<?php $this->widget('booster.widgets.TbGridView', array(
															'id'=>'Arsip',
															'dataProvider'=>$model->admin($i),
															
															'columns'=>array(	
																array('header'=>'No','value'=>'$row+1'),
																'judul',
															'isi',
															'datetime',
															array(    
																'header'=>'Detail',
																'type'=>'raw', 
																'value'=>'CHtml::link("Detail", array("pengumuman/view/id/$data->id_pesan/"),array("class"=>"btn btn-success"))',
															),
															array(    
																'header'=>'Edit',
																'type'=>'raw', 
																'value'=>'CHtml::link("Edit", array("pengumuman/update/id/$data->id_pesan/"),array("class"=>"btn btn-success"))',
															),
															array(    
																'header'=>'Hapus',
																'type'=>'raw', 
																'value'=>'CHtml::link("hapus", array("pengumuman/delete/id/$data->id_pesan/"),array("class"=>"btn btn-danger"))',
															),
															),
													)); ?>
												</div>
											</div>					 
										</div>
								</div>		
							</div>                                
							</form>
						</div>
					</div>                    
				
		</div> 
	</div>
    </form>                                                    
</div>