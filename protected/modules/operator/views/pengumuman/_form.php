<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'pengumuman-form',
	// 'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="panel-body">                                                                        
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-3 control-label">Judul</label>
						<div class="col-md-9">                                            
							<div class="input-group">
								<span class="input-group-addon"><span class="fa fa-pencil"></span></span>
								<?php echo $form->textField($model,'judul',array('size'=>25,'maxlength'=>25,'class'=>"form-control",'placeholder'=>"Masukkan judul"));  ?>
							</div>                                            
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Penerima</label>
						<div class="col-md-9">
							<?php echo $form->dropDownList($model,'penerima',array('empty'=>"Pilih Penerima",'admin'=>"Admin",'dosen'=>"Dosen",'mahasiswa'=>"Mahasiswa"),array('class'=>"form-control select")); ?>
						   <span class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Prodi</label>
						<div class="col-md-9">
							<?php echo $form->dropDownList($model,'id_sms',CHtml::listData(Sms::model()->findAll(array('condition'=>'')),'id_sms','nm_lemb'),array('empty'=>'Semua','class'=>'form-control select')); ?>
						   <span class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Isi</label>
						<div class="col-md-9">                                            
							<div class="input-group">
								<span class="input-group-addon"><span class="fa fa-pencil"></span></span>
								<?php echo $form->textArea($model,'isi',array('rows'=>6,'cols'=>50,'class'=>"form-control select",'placeholder'=>"Masukkan isi"));  ?>
							</div>                                            
							<span class="help-block"></span>
						</div>
					</div>

				</div>
			</div>
	
		</div>	
	<div class="row buttons">
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Kirim' : 'Simpan',array('class'=>'btn btn-primary')); ?>
</div>

<?php $this->endWidget(); ?>
