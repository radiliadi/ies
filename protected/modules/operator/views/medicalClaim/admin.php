<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Medical Claim'=>'admin' ,
        ),
    )
);
?>
<script type="text/javascript" src="<?php  echo Yii::app()->request->baseUrl; ?>/themes/admin/js/action.js"></script>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-medkit"></span> <?=$title;?> </h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?=$title;?> List</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
            	<div class="btn-group pull-right">
                </div> 
            </div>
            <div class="panel-body">
            <?php echo CHtml::beginForm(array('medicalClaim/exportExcel')); ?>
            <?php $this->widget('booster.widgets.TbGridView',array(
			'id'=>'medicalClaim-grid',
			// 'type' => ' condensed',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'responsiveTable' => true,
			'pager' => array(
			    'header' => '',
			    'footer' => '',
			    'cssFile' => false,
			    'selectedPageCssClass' => 'active',
			    'hiddenPageCssClass' => 'disabled',
			    'firstPageCssClass' => 'previous',
			    'lastPageCssClass' => 'next',
			    'maxButtonCount' => 5,
			    'firstPageLabel'=> '<i class="fa fa-angle-double-left"></i>',
			    'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
			    'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
			    'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
			    'htmlOptions' => array(
			        'class' => 'dataTables_paginate paging_bootstrap pagination pull-right',
			        'align' => 'center',
			    )
			),
			// 'summaryText' => '',
			// 'emptyText'=>'Belum ada thread pada kategori ini',
			'columns'=>array(
				array(
				'header' => 'No.',
				'htmlOptions' => array(
					'width' => '20px',
					'style' => 'text-align:center'
					),
					'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
					// 'class'=>'booster.widgets.TbTotalSumColumn',
				),
				'mc_id_pd',
				array(
			      'name'=>'mc_bpjs_kete',
			      'value'=>function($data){
			      	if($data->mc_bpjs_kete == 0){
			        	return ' -';
			        }else{
			        	return 'Rp. '.number_format($data->mc_bpjs_kete,0);
			        }
			      }
			    ),
				array(
			      'name'=>'mc_bpjs_kese',
			      'value'=>function($data){
			      	if($data->mc_bpjs_kese == 0){
			        	return ' -';
			        }else{
			        	return 'Rp. '.number_format($data->mc_bpjs_kese,0);
			        }
			      }
			    ),
			    array(
			      'name'=>'mc_bpjs_pensi',
			      'value'=>function($data){
			      	if($data->mc_bpjs_pensi == 0){
			        	return ' -';
			        }else{
			        	return 'Rp. '.number_format($data->mc_bpjs_pensi,0);
			        }
			      }
			    ),
			    array(
			      'name'=>'mc_tunj_kese',
			      'value'=>function($data){
			      	if($data->mc_tunj_kese == 0){
			        	return ' -';
			        }else{
			        	return 'Rp. '.number_format($data->mc_tunj_kese,0);
			        }
			      }
			    ),
				array(
			      'name'=>'mc_tunj_current',
			      'type'=>'raw',
			      'value'=>function($data){
			      	if($data->mc_tunj_current == 0){
			        	return ' -';
			        }else{
			        	if($data->mc_tunj_current < 0){
			        		return '<font color="red"><b>Rp. '.number_format($data->mc_tunj_current,0).'</b></font>';
			        	}else{
			        		return 'Rp. '.number_format($data->mc_tunj_current,0);
			        	}
			        	return 'Rp. '.number_format($data->mc_tunj_current,0);
			        }
			      }
			    ),
			    array(
			      'name'=>'mc_tunj_used',
			      'value'=>function($data){
			      	if($data->mc_tunj_used == 0){
			        	return ' -';
			        }else{
			        	return 'Rp. '.number_format($data->mc_tunj_used,0);
			        }
			      }
			    ),
			    array(    
	              'header'=>'',
	              'type'=>'raw', 
	              'value' =>function($data) {
	                          return CHtml::link('<i class="fa fa-pencil"></i>',['/operator/medicalClaim/update/id/'.$data->mc_id],['class' => 'btn btn-info', 'title' => 'Update']);
	                      },
	          	),
			    // array(    
       //              'header'=>'Top Up',
       //              'type'=>'raw', 
       //              'value' =>function($data) {
       //                          return CHtml::link('<i class="fa fa-search"></i>',['/operator/medicalClaim/updateSaldo/id/'.$data->mc_id],['class' => 'btn btn-default', 'title' => 'Approved']);
       //                      },
       //          ),
    //             array(
    //                 'class' => 'CButtonColumn',
    //                 'template' => '{updateSaldo}',
    //                 'header' => 'TopUp',
    //                 'htmlOptions' => array(
    //                     'style' => 'white-space: 2px'
    //                 ),
    //                 'buttons' => array(
    //                     'updateSaldo' => array(
    //                         'label' => '',
    //                         'url' => 'Yii::app()->createAbsoluteUrl("operator/medicalClaim/updateSaldo/", array(
    //                             "id" => $data->mc_id
    //                         ))',
    //                         'click' => 'function() {if(!confirm("Are you sure to TopUp this user??")) {return false;}}',
    //                         'options' => array(
    //                             'class' => 'btn btn-default glyphicon glyphicon-plus tooltips',
    //                             'title' => 'Inactive'
    //                         ),
    //                     ),
    //                 ),
    //             ),
				// array(
				// 'class'=>'booster.widgets.TbButtonColumn',
				// 'template'=>'{update}',
				// ),
			),
			)); 
            ?>
            <br></br>
            <hr></hr>
            <div class="col-md-6">
                <div class="col-md-3">
                <select name="fileType" class="form-control">
                    <option value="Excel">EXCEL 5 (xls)</option>
                    <!-- <option value="CSV">CSV</option> -->
                </select>
                </div>
                <div class="col-md-3">
                <?php echo CHtml::submitButton('Export',array(
                            'buttonType' => 'submit',
                            'context'=>'primary',
                            'label'=>'Export Excel',
                            'class'=>'btn btn-primary',
                        )); 
                ?>
                <?php echo CHtml::endForm(); ?>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>