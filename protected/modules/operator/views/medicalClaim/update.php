<?php $this->widget('booster.widgets.TbBreadcrumbs',
array(
    'links' => array('Medical Claim'=>'../../admin',
    'Update',
   	$model->mc_id_pd,
    ),
)
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update Medical Claim : #<?=$model->mc_id_pd;?></h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>