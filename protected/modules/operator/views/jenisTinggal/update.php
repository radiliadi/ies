<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Settings' ,
        'Employee',
        // 'Semua' => array('Semua'=>'admin', 
        'Kind of Stay'=>array('admin'
          ),
        'Update',
        $model->id_jns_tinggal,
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update Kind of Stay : #<?=$model->id_jns_tinggal;?></h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>