<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Settings' ,
        'Employee',
        // 'Semua' => array('Semua'=>'admin', 
        'Kind of Stay'=>array('admin'
          ),
        'Create',
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Create Kind of Stay</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>