<?php
/* @var $this ChartOfAccountTypeController */
/* @var $model ChartOfAccountType */

$this->breadcrumbs=array(
	'Chart Of Account Types'=>array('index'),
	$model->coat_id=>array('view','id'=>$model->coat_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ChartOfAccountType', 'url'=>array('index')),
	array('label'=>'Create ChartOfAccountType', 'url'=>array('create')),
	array('label'=>'View ChartOfAccountType', 'url'=>array('view', 'id'=>$model->coat_id)),
	array('label'=>'Manage ChartOfAccountType', 'url'=>array('admin')),
);
?>

<h1>Update ChartOfAccountType <?php echo $model->coat_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>