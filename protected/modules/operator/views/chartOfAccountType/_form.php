<?php
/* @var $this ChartOfAccountTypeController */
/* @var $model ChartOfAccountType */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'chart-of-account-type-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'coat_desc'); ?>
		<?php echo $form->textArea($model,'coat_desc',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'coat_desc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coat_color'); ?>
		<?php echo $form->textField($model,'coat_color',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'coat_color'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coat_span'); ?>
		<?php echo $form->textField($model,'coat_span',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'coat_span'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coat_datetime_insert'); ?>
		<?php echo $form->textField($model,'coat_datetime_insert'); ?>
		<?php echo $form->error($model,'coat_datetime_insert'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coat_status'); ?>
		<?php echo $form->textField($model,'coat_status'); ?>
		<?php echo $form->error($model,'coat_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->