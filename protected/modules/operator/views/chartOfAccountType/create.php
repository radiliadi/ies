<?php
/* @var $this ChartOfAccountTypeController */
/* @var $model ChartOfAccountType */

$this->breadcrumbs=array(
	'Chart Of Account Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ChartOfAccountType', 'url'=>array('index')),
	array('label'=>'Manage ChartOfAccountType', 'url'=>array('admin')),
);
?>

<h1>Create ChartOfAccountType</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>