<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting' => Yii::app()->urlManager->createUrl('operator/accounting/index'),
        'Inventory' => '../../admin',
        'Update',
        $model->inv_id,
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update : #<?=$model->inv_id;?></h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>