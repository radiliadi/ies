<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Employee' ,
        'Payroll' => array('Payroll'=>'pay',
            )
        ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
    <h2><span class="fa fa-money"></span> Payroll</h2>
  </div>
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Payroll List</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
            <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
        </ul>   
        <div class="btn-group pull-right">
        </div> 
      </div>
      <div class="panel-body">
      <?php $this->widget('booster.widgets.TbGridView',array(
      'id'=>'karyawan-grid',
      // 'type' => ' condensed',
      'dataProvider'=>$model->searchpay(),
      'filter'=>$model,
      'responsiveTable' => true,
      'pager' => array(
          'header' => '',
          'footer' => '',
          'cssFile' => false,
          'selectedPageCssClass' => 'active',
          'hiddenPageCssClass' => 'disabled',
          'firstPageCssClass' => 'previous',
          'lastPageCssClass' => 'next',
          'maxButtonCount' => 5,
          'firstPageLabel'=> '<i class="fa fa-angle-double-left"></i>',
          'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
          'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
          'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
          'htmlOptions' => array(
              'class' => 'dataTables_paginate paging_bootstrap pagination pull-right',
              'align' => 'center',
          )
      ),
      // 'summaryText' => '',
      // 'emptyText'=>'Belum ada thread pada kategori ini',
      'columns'=>array(
        array(
      		'header' => 'No.',
      		'htmlOptions' => array(
      			'width' => '20px',
      			'style' => 'text-align:center'
      		),
      		'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
      		// 'class'=>'booster.widgets.TbTotalSumColumn',
    		),
    		'id_pd',
    		'nm_pd',
    		array(
          'name' => 'id_jabatan',
          'filter' => Karyawan::listJabatanx(),
          // 'value'=>'$data->relasijabatan->nm_jabatan',
          'value' => function($data){
                      if(!empty($data->relasijabatan->nm_jabatan)){
                        return $data->relasijabatan->nm_jabatan;
                      }else{
                        '-';
                      }
                    }
        ),
        array(
          'class' => 'CButtonColumn',
          'template' => '{gaji}',
          'header' => '',
          'htmlOptions' => array(
              'style' => 'white-space: nowrap'
          ),
          'buttons' => array(
              'gaji' => array(
                  'label' => '',
                  'url' => 'Yii::app()->createAbsoluteUrl("operator/gaji/admin/", array(
                      "id" => $data->id_pd
                  ))',
                  'options' => array(
                      'class' => 'btn btn-success bola fa fa-dollar tooltips',
                      'title' => 'Gaji'
                  ),
              ),
          ),
        ),
        ),
        )); 
      ?>
      <br></br>
      <hr></hr>
      </div>
    </div>
  </div>
</div>