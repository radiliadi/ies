<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Employee',
    	'Personalia'=>'../../admin',
        $model->id_pd,
        ),
    )
);
?>
<div class="page-title">                    
	<h2><span class="fa fa-edit"></span> <?php echo $model->id_pd; ?> - <?php echo $model->nm_pd; ?></h2>
</div>
<?php $this->renderPartial('_form', array('model'=>$model,'modelObj'=>$modelObj)); ?>