<div class="form">

    <?php $form=$this->beginWidget('CActiveForm',array(
    'id'=>'mahasiswa-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'htmlOptions'=>array(
        'enctype'=>'multipart/form-data',
        'action'=>'javascript:alert("Validated!");',
        'role'=>'form',
        'class'=>'',
        'id'=>"wizard-validation"
    ),
)); ?>
<!-- PAGE CONTENT WRAPPER -->
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-body">
    <!-- START WIZARD WITH VALIDATION -->
            <div class="block">
            <div style="color:red;"><?php echo $form->errorSummary($model); ?></div>   
                <div class="wizard show-submit wizard-validation">
                    <ul>
                        <li>
                            <a href="#step-1">
                                <span class="stepNumber">1</span>
                                <span class="stepDesc"><div style="min-height:20px;">Personal</div><br /><small>step 1</small></span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-2">
                                <span class="stepNumber">2</span>
                                <span class="stepDesc"><div style="min-height:20px;">Address</div><br /><small>step 2</small></span>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#step-3">
                                <span class="stepNumber">3</span>
                                <span class="stepDesc"><div style="min-height:20px;">Parent</div><br /><small>step 3</small></span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-4">
                                <span class="stepNumber">4</span>
                                <span class="stepDesc"><div style="min-height:20px;">Marital</div><br /><small>step 4</small></span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-5">
                                <span class="stepNumber">5</span>
                                <span class="stepDesc"><div style="min-height:20px;">Others</div><br /><small>step 5</small></span>
                            </a>
                        </li> 
                        
                    </ul>
                    <div id="step-1">   
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>1. Personal</strong> </h3>
                                    
                                </div>
                                
                                <div class="panel-body">                                                                        
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">NIK<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                                        <?php 
                                                            if(isset($_SESSION['sessiondrz']))
                                                            {
                                                                echo $form->textField($model,'id_pd',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'value'=>$vari[0], 'readonly'=>'readonly', ' '=>' '));
                                                            }else{
                                                                echo $form->textField($model,'id_pd',array('size'=>50,'maxlength'=>50,'class'=>"form-control", ' '=>' ','placeholder'=>"NIK",' '=>" "));
                                                            }
                                                        ?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">                                        
                                                <label class="col-md-3 control-label">Full Name<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php 
                                                            if(isset($_SESSION['sessiondrz']))
                                                            {
                                                                echo $form->textField($model,'nm_pd',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'value'=>$vari[4], 'readonly'=>'readonly', ' '=>' '));
                                                            }else{
                                                                echo $form->textField($model,'nm_pd',array('size'=>50,'maxlength'=>50,'class'=>"form-control", ' '=>' ','placeholder'=>"Full Name",' '=>" "));
                                                                
                                                            }
                                                        ?>
                                                        
                                                    </div>      
                                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Position<a style="color:red;"><blink>*</blink></a></label>
                                                
                                                <div class="col-md-9">
                                                    <?php echo $form->dropDownList($model,'id_jabatan',Karyawan::listJabatanx(),array('empty'=>'-Choose Position-','class'=>"form-control select", ' '=>' ')); ?>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Division<a style="color:red;"><blink>*</blink></a></label>
                                                
                                                <div class="col-md-9">
                                                    <?php echo $form->dropDownList($model,'divisi',CHtml::listData(Divisi::model()->findAll(),'id_div','nm_div'),array('empty'=>'-Choose Division-','class'=>"form-control select", ' '=>' ')); ?>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Work Place<a style="color:red;"><blink>*</blink></a></label>
                                                
                                                <div class="col-md-9">
                                                    <?php echo $form->dropDownList($model,'lokasi_kerja',CHtml::listData(LokasiKerja::model()->findAll(),'id_lokasi_kerja','nm_lokasi_kerja'),array('empty'=>'-Choose Work Place-','class'=>"form-control select", ' '=>' ')); ?>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            
                                                <label class="col-md-3 control-label">Gender<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9 col-xs-12">
                                                    
                                                    <?php echo $form->radioButton($model,'jk',array('value'=>'L','uncheckValue'=>null,' '=>" ")); ?>Laki-Laki                                       
                                                    <?php echo $form->radioButton($model,'jk',array('value'=>'P','uncheckValue'=>null,' '=>" ")); ?>Perempuan                                           
                                                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <br></br>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Level Account<a style="color:red;"><blink>*</blink></a></label>
                                                
                                                <div class="col-md-9">
                                                    <?php echo $form->dropDownList($model,'akun_level',CHtml::listData(UserLevel::model()->findAll(),'usrlvl_name','usrlvl_name'),array('empty'=>'-Choose Level Account-','class'=>"form-control select", ' '=>' ')); ?>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                        
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Religion<a style="color:red;"><blink>*</blink></a></label>
                                                
                                                <div class="col-md-9">
                                                    <?php echo $form->dropDownList($model,'id_agama',CHtml::listData(Agama::model()->findAll(),'agm_id','agm_desc'),array('empty'=>'-Choose Religion-','class'=>"form-control select")); ?>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Birth Place<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php echo $form->textField($model,'tmpt_lahir',array('size'=>50,'maxlength'=>50,'class'=>"form-control", ' '=>' ','placeholder'=>"Birth Place",' '=>" "));?>
                                                    </div>            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Birth Date<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php   $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                                'name'=>'Karyawan[tgl_lahir]',
                                                                                'value'=> $model->tgl_lahir,
                                                                                'options'=>array(
                                                                                    'showAnim'=>'fold',
                                                                                    'dateFormat'=>'yy-mm-dd',
                                                                                ),
                                                                                'htmlOptions' => array(
                                                                                    'class' => 'form-control',
                                                                                    'placeholder'=>'Birth Date'
                                                                                ),
                                                                            ));
                                                        ?>
                                                    </div>  
                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
    
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Nationality<a style="color:red;"><blink>*</blink></a></label>
                                                
                                                <div class="col-md-9">
                                                    <?php echo $form->dropDownList($model,'kewarganegaraan',CHtml::listData(Negara::model()->findAll(),'id_negara','nm_negara'),array('empty'=>'-Choose Nationality-','class'=>"form-control select", ' '=>' ')); ?>
                                                    
                                                    <span class="help-block"></span>
                                                    </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Status <a style="color:red;"><blink>*</blink></a></label>
                                                
                                                <div class="col-md-9">
                                                    <?php echo $form->dropDownList($model,'stat_pd',CHtml::listData(StatusMahasiswa::model()->findAll(),'nm_stat_mhs','nm_stat_mhs'),array('empty'=>'-Choose Status-','class'=>"form-control select", ' '=>' ')); ?>
                                                    
                                                    <span class="help-block"></span>
                                                    
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">No. Tender </label>
                                                
                                                <div class="col-md-9">
                                                    <?php echo $form->dropDownList($model,'tender_id',CHtml::listData(Tender::model()->findAll(array('condition'=>'tndr_status=1', 'order'=>'id_tender DESC')),'id_tender','tender_no'),array('empty'=>'-Choose Tender-','class'=>"form-control select", ' '=>' ')); ?>
                                                    
                                                    <span class="help-block"></span>
                                                    
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Jenis BPJS<a style="color:red;"><blink>*</blink></a></label>
                                                
                                                <div class="col-md-9">
                                                    <?php echo $form->dropDownList($model,'id_bpjs',Bpjs::listBpjs(),array('empty'=>'-Choose BPJS-','class'=>"form-control select", ' '=>' ')); ?>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a style="color:red;margin-left:3%"><blink>*: Field must not be blank</blink></a>  
                    </div>
                                    
                    <div id="step-2">
                        <div class="panel panel-default">
                                <div class="panel-heading">
                                   <h3 class="panel-title"><strong>2. Address & Contact </strong> </h3>
                                </div>
                                <div class="panel-body">                                                                          
                                    <div class="row">         
                                        <div class="col-md-6">           
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Nomor KTP</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                       <?php echo $form->textField($model,'nik',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"KTP" ));?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">NPWP</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                       <?php echo $form->textField($model,'npwp',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"NPWP" ));?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            
                                           <div class="form-group">
                                                <label class="col-md-3 control-label">Jalan<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                       <?php echo $form->textField($model,'jln',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Jalan"));?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Dusun<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                      <?php echo $form->textField($model,'nm_dsn',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Dusun")); ?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Kelurahan<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php echo $form->textField($model,'ds_kel',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"kelurahan")); ?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Kecamatan<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php echo $form->textField($model,'id_wil',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"kecamatan")); ?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Jenis Tinggal<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-9">                                            
                                                    <?php echo $form->dropDownList($model,'id_jns_tinggal',CHtml::listData(JenisTinggal::model()->findAll(),'id_jns_tinggal','nm_jns_tinggal'),array('empty'=>'-Pilih Jenis Tinggal-','class'=>"form-control select", ' '=>' ', 'style'=>'z-index:1000;')); ?>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Telepon</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                       <?php echo $form->textField($model,'telepon_rumah',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"No telepon")); ?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Email</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Email")); ?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Alat Transportasi<a style="color:red;"><blink>*</blink></a></label>
                                                
                                                <div class="col-md-9">
                                                <?php echo $form->dropDownList($model,'id_alat_transport',CHtml::listData(AlatTransport::model()->findAll(),'id_alat_transport','nm_alat_transport'),array('empty'=>'-Pilih Alat Transportasi-','class'=>"form-control select",' '=>" ")); ?>
                                                <span class="help-block"></span>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <br>
                                            </br>
                                            <br>
                                            </br>
                                            <br>
                                            </br>
                                            
                                            <div class="form-group">                                        
                                                <label class="col-md-2 control-label">RT</label>
                                                <div class="col-md-4 col-xs-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                      <?php echo $form->textField($model,'rt',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"RT")); ?>
                                                    </div>            
                                                    <span class="help-block"></span>
                                                </div>
                                            
                                            
                                                                                 
                                                <label class="col-md-1 control-label">RW</label>
                                                <div class="col-md-4 col-xs-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php echo $form->textField($model,'rw',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"RW")); ?>
                                                    </div>            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            <div class="form-group">                                        
                                                <label class="col-md-2 control-label">Kodepos</label>
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                          <?php echo $form->textField($model,'kode_pos',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Kode pos")); ?>
                                                    </div>            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">                                        
                                                <label class="col-md-1 control-label">No HP</label>
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                          <?php echo $form->textField($model,'telepon_seluler',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"No HP")); ?>
                                                    </div>            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            
                                            
                                           
                                            <br>
                                            </br>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                

                        </div>
                        <a style="color:red;margin-left:3%"><blink>*: Data wajib diisi</blink></a>  
                    </div>
                                
                    <div id="step-3">   
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>3. Orang Tua</strong></h3>
                    </div>
                            <div class="panel-body">                                                                        
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Nama Ayah &nbsp; <a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-8">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php echo $form->textField($model,'nm_ayah',array('size'=>60,'maxlength'=>60,'class'=>"form-control",'placeholder'=>"Nama Ayah")); ?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                        </div>
                                        
                                        <div class="form-group">
                                                <label class="col-md-4 control-label">Tanggal Lahir<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-8 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php   $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                                'name'=>'Karyawan[tgl_lahir_ayah]',
                                                                                'value'=>$model->tgl_lahir_ayah,
                                                                                'options'=>array(
                                                                                    'showAnim'=>'fold',
                                                                                    'dateFormat'=>'yy-mm-dd',
                                                                                ),
                                                                                'htmlOptions' => array(
                                                                                    'class' => 'form-control',
                                                                                    'placeholder'=>'Tanggal Lahir ayah'
                                                                                ),
                                                                            ));
                                                        ?>
                                                    </div>  
                                            
                                                    <span class="help-block"></span>
                                                </div>
                                          </div>
                                        
                                        <div class="form-group">
                                             <label class="col-md-4 control-label">Pendidikan Ayah &nbsp; <a style="color:red;"><blink>*</blink></a></label>
                                                
                                                <div class="col-md-8">
                                                <?php echo $form->dropDownList($model,'id_jenjang_pendidikan_ayah',CHtml::listData(JenjangPendidikan::model()->findAll(),'id_jenj_didik','nm_jenj_didik'),array('empty'=>'-Pilih jenjang pendidikan Ayah-','class'=>"form-control select")); ?>
                                                <span class="help-block"></span>
                                                </div>
                                        </div>
                                            
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Pekerjaan Ayah &nbsp; <a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-8">
                                                <?php echo $form->dropDownList($model,'id_pekerjaan_ayah',CHtml::listData(Pekerjaan::model()->findAll(),'id_pekerjaan','nm_pekerjaan'),array('empty'=>'-Pilih pekerjaan Ayah-','class'=>"form-control select")); ?>
                                               <span class="help-block"></span>
                                                </div>
                                        </div>
                                            
                                            
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Nama Ibu &nbsp; <a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-8">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php echo $form->textField($model,'nm_ibu_kandung',array('size'=>60,'maxlength'=>60,'class'=>"form-control",'placeholder'=>"Nama Ibu")); ?>
                                                    </div>                                            
                                                    <span class="help-block"></span>
                                                </div>
                                        </div>
                                            
                                        <div class="form-group">
                                                <label class="col-md-4 control-label">Tanggal Lahir<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-8 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <?php   $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                                'name'=>'Karyawan[tgl_lahir_ibu]',
                                                                                'value'=>$model->tgl_lahir_ibu,
                                                                                'options'=>array(
                                                                                    'showAnim'=>'fold',
                                                                                    'dateFormat'=>'yy-mm-dd',
                                                                                ),
                                                                                'htmlOptions' => array(
                                                                                    'class' => 'form-control',
                                                                                    'placeholder'=>'Tanggal Lahir Ibu'
                                                                                ),
                                                                            ));
                                                        ?>
                                                    </div>  
                                            
                                                    <span class="help-block"></span>
                                                </div>
                                          </div>
                                        <div class="form-group">
                                             <label class="col-md-4 control-label">Pendidikan Ibu &nbsp; <a style="color:red;"><blink>*</blink></a></label>
                                                
                                                <div class="col-md-8">
                                                <?php echo $form->dropDownList($model,'id_jenjang_pendidikan_ibu',CHtml::listData(JenjangPendidikan::model()->findAll(),'id_jenj_didik','nm_jenj_didik'),array('empty'=>'-Pilih jenjang pendidikan Ibu-','class'=>"form-control select")); ?>
                                                <span class="help-block"></span>
                                                </div>
                                        </div>
                                            
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Pekerjaan Ibu &nbsp; <a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-8">
                                                <?php echo $form->dropDownList($model,'id_pekerjaan_ibu',CHtml::listData(Pekerjaan::model()->findAll(),'id_pekerjaan','nm_pekerjaan'),array('empty'=>'-Pilih pekerjaan Ibu-','class'=>"form-control select")); ?>
                                               <span class="help-block"></span>
                                                </div>
                                        </div>
                                            
                                            
                                            
                                    </div>
                                  
                                </div>

                            </div>
                              
                    </div>
                     <a style="color:red;"><blink>*: Data wajib diisi</blink></a>  
                    </div>

                    <div id="step-4">
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>4. Marital</strong></h3>
                                    
                    </div>
                            <div class="panel-body">                                                                        
                                <div class="row">
                                    <div class="col-md-6">
                                    
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Marital Status</label>
                                               <div class="col-md-8">
                                                <?php echo $form->dropDownList($model,'id_ptkp',CHtml::listData(Ptkp::model()->findAll(),'id_ptkp','ptkp_ket'),array('empty'=>'-Choose Marital Status-','class'=>"form-control select")); ?>
                                                <span class="help-block"></span>
                                                </div>
                                          </div>
                                    </div>
                                </div>  
                            </div>
                            
                    </div>  
                    </div>          
                    <div id="step-5">
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>5. Others</strong></h3>
                                    
                    </div>
                            <div class="panel-body">                                                                        
                                <div class="row">

                                    <div class="col-md-6">
                                            
                                                <div class="form-group">
                                                <label class="col-md-4 control-label">Entry Date<a style="color:red;"><blink>*</blink></a></label>
                                                <div class="col-md-8 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                        <?php   $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                                'name'=>'Karyawan[tgl_masuk]',
                                                                                'value'=> $model->tgl_masuk,
                                                                                'options'=>array(
                                                                                    'showAnim'=>'fold',
                                                                                    'dateFormat'=>'yy-mm-dd',
                                                                                ),
                                                                                'htmlOptions' => array(
                                                                                    'class' => 'form-control',
                                                                                    'placeholder'=>'Entry Date'
                                                                                ),
                                                                            ));
                                                        ?>
                                                    </div>  
                                            
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Jenis Daftar</label>
                                               <div class="col-md-8">
                                                <?php echo $form->dropDownList($model,'regpd_id_jns_daftar',CHtml::listData(JenisPendaftaran::model()->findAll(),'id_jns_daftar','nm_jns_daftar'),array('empty'=>'Pilih Jenis Daftar','class'=>"form-control select")); ?>
                                                <span class="help-block"></span>
                                                </div>
                                        </div>
                                        
                                 </div>
                                <div class="col-md-6">
                                
                                         
                                        
                                    </div>
                                </div>  
                            </div>
                            
                    </div>  
                    </div> 
                
                </div>
            </div>                        
    <!-- END WIZARD WITH VALIDATION -->                 
        </div>
    </div>                    
                    
</div>
<!-- END PAGE CONTENT WRAPPER -->                       
<?php $this->endWidget(); ?>
</div><!-- form -->
