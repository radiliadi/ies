<script type="text/css" src="<?php echo Yii::app()->request->baseUrl; ?>/custom/css/switch-button.css"></script>
<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Employee' ,
        'Personalia' => array('Personalia'=>'admin',
            )
        ),
    )
);
?>
<div class="row">
  <div class="page-title">                    
      <h2><span class="fa fa-users"></span> Manage Employees</h2>
  </div>
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Employee List</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
            <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
        </ul>   
        <div class="btn-group pull-right">
          <?php echo CHtml::link('<i class="fa fa-plus" title="Create"></i>', 'create', array('class'=>'btn btn btn-primary pull-right')); ?>
        </div> 
      </div>
      <div class="panel-body">
      <?php echo CHtml::beginForm(array('karyawan/exportExcel')); ?>
      <?php $this->widget('booster.widgets.TbGridView',array(
      'id'=>'karyawan-grid',
      // 'type' => ' condensed',
      'dataProvider'=>$model->search(),
      'filter'=>$model,
      'responsiveTable' => true,
      'pager' => array(
          'header' => '',
          'footer' => '',
          'cssFile' => false,
          'selectedPageCssClass' => 'active',
          'hiddenPageCssClass' => 'disabled',
          'firstPageCssClass' => 'previous',
          'lastPageCssClass' => 'next',
          'maxButtonCount' => 5,
          'firstPageLabel'=> '<i class="fa fa-angle-double-left"></i>',
          'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
          'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
          'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
          'htmlOptions' => array(
              'class' => 'dataTables_paginate paging_bootstrap pagination pull-right',
              'align' => 'center',
          )
      ),
      // 'summaryText' => '',
      // 'emptyText'=>'Belum ada thread pada kategori ini',
      'columns'=>array(
      		array(
        		'header' => 'No.',
        		'htmlOptions' => array(
        			'width' => '20px',
        			'style' => 'text-align:center'
        		),
        		'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        		// 'class'=>'booster.widgets.TbTotalSumColumn',
      		),
      		'id_pd',
      		'nm_pd',
      		array(
            'name' => 'jabatanku',
            'filter' => Karyawan::listJabatanx(),
             // 'value' => '$data->id_jabatan == "10" ? "Engineer On Site" : 
             // ($data->id_jabatan == 9 ? "Engineer Warehouse" : 
             // ($data->id_jabatan == 8 ? "Field Engineer" : 
             // ($data->id_jabatan == 7 ? "Insurance & Tax" : 
             // ($data->id_jabatan == 6 ? "Manager Finance & Accounting" : 
             // ($data->id_jabatan == 5 ? "Formalities Insurance" : 
             // ($data->id_jabatan == 4 ? "Kontrak, Tender & HSE Manager" : 
             // ($data->id_jabatan == 3 ? "Manager HR" : 
             // ($data->id_jabatan == 2 ? "Direktur" : 
             // ($data->id_jabatan == 1 ? "Direktur Utama" : 
             // ($data->id_jabatan == 13 ? "Helpdesk" : 
             // ($data->id_jabatan == 12 ? "Technical Support" : 
             // ($data->id_jabatan == 11 ? "Asst. Project Manager" : 
             // "Undefined"))))))))))))',
            // 'value'=>'$data->relasijabatan->nm_jabatan',
            'value' => function($data){
                        if(!empty($data->relasijabatan->nm_jabatan)){
                          return $data->relasijabatan->nm_jabatan;
                        }else{
                          return '-';
                        }
                      }
          ),
      		array(
            'name' => 'divisi',
            'filter' => Karyawan::listDivisi(),
            // 'value' => '$data->getDivisi->nm_div',
            'value' => function($data){
                        if(!empty($data->getDivisi->nm_div)){
                          return $data->getDivisi->nm_div;
                        }else{
                          return '-';
                        }
                        
                      }
          ),
      		array(
            'name' => 'lokasi_kerja',
            'filter' => Karyawan::listLokker(),
            // 'value' => '$data->getLokasiKerja->nm_lokasi_kerja',
            'value' => function($data){
                        if(!empty($data->getLokasiKerja->nm_lokasi_kerja)){
                          return $data->getLokasiKerja->nm_lokasi_kerja;
                        }else{
                          return '-';
                        }
                      }
          ),
          array(
            'name'=>'tender_id',
            'type' => 'raw',
            'filter' => Karyawan::listTender(),
            // 'value' => '$data->getTender->client_name',
            'value' => function($data){
                        if(!empty($data->getTender->client_name)){
                          return $data->getTender->client_name;
                        }else{
                          return '-';
                        }
                      }
          ),
          array(
              'name' => 'status_pd',
              'type' => 'raw',
              'filter' => array(
                              1 => Yii::t('app', 'Active'),
                              0 => Yii::t('app', 'Inactive'),
                          ),
              'value' => function($data){
                  $isChecked = '';
                  if($data->status_pd == 1){
                      $isChecked = 'checked="checked"';
                  }
                  return '<label class="switch"><input type="checkbox" id="status_'.$data->id_pd.'" name="status_'.$data->id_pd.'" '.$isChecked.'  class="checkSingleCoa" value="'.$data->id_pd.'"  data-id="'.$data->id_pd.'" data-url="'.Yii::app()->urlManager->createUrl('operator/karyawan/updateStatus').'"><span class="slider"></span></label>';
              }
          ),
          array(    
              'header'=>'',
              'type'=>'raw', 
              'value' =>function($data) {
                          return CHtml::link('<i class="fa fa-eye"></i>',['/operator/karyawan/view/id/'.$data->id_pd],['class' => 'btn btn-default', 'title' => 'View']);
                      },
          ),
          array(    
              'header'=>'',
              'type'=>'raw', 
              'value' =>function($data) {
                          return CHtml::link('<i class="fa fa-pencil"></i>',['/operator/karyawan/update/id/'.$data->id_pd],['class' => 'btn btn-info', 'title' => 'Update']);
                      },
          ),
          // array(
          // 'class'=>'booster.widgets.TbButtonColumn',
          // 'template'=>'{view}',
          // ),
          // array(
          // 'class'=>'booster.widgets.TbButtonColumn',
          // 'template'=>'{update}',
          // ),
          // array(
          // 'class'=>'booster.widgets.TbButtonColumn',
          // 'afterDelete'=>'function(link,success,data){ if(success) alert("Delete completed successfully"); }',
          // 'template'=>'{delete}',
          // ),
        ),
      )); 
      ?>
      <br></br>
      <hr></hr>
      <div class="col-md-6">
        <div class="col-md-3">
        <select name="fileType" class="form-control">
            <option value="Excel">EXCEL 5 (xls)</option>
            <!-- <option value="CSV">CSV</option> -->
        </select>
        </div>
        <div class="col-md-3">
        <?php echo CHtml::submitButton('Export',array(
                    'buttonType' => 'submit',
                    'context'=>'primary',
                    'label'=>'Export Excel',
                    'class'=>'btn btn-primary',
                )); 
        ?>
        <?php echo CHtml::endForm(); ?>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/custom/js/karyawan/karyawan.js"></script>