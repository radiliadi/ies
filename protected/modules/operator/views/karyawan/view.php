<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Employee' => '../../../karyawan/admin' ,
    	'Personalia'=>'../../admin',
        $model->id_pd,
        ),
    )
);
?>

<h1> Detail Karyawan #<?php echo $model->id_pd; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView', array(
	// 'dataProvider'=>$model->search(),
	// 'itemView'=>'_view',
	// 'emptyText'=>'Tidak ada data yang ditemukan',
	'data'=>$model,
	'attributes'=>array(
		'id_pd',
		'nm_pd',
		'id_jabatan',
		'divisi',
		'lokasi_kerja',
		'jk',
		'nik',
		'tmpt_lahir',
		'tgl_lahir',
		'id_agama',
		// 'id_kk',
		'jln',
		'rt',
		'rw',
		'nm_dsn',
		'ds_kel',
		'id_wil',
		'kode_pos',
		'id_jns_tinggal',
		'id_alat_transport',
		'telepon_rumah',
		'telepon_seluler',
		'email',
		'stat_pd',
		'nm_ayah',
		'tgl_lahir_ayah',
		'id_jenjang_pendidikan_ayah',
		'id_pekerjaan_ayah',
		'nm_ibu_kandung',
		'tgl_lahir_ibu',
		'id_jenjang_pendidikan_ibu',
		'id_pekerjaan_ibu',
		'kewarganegaraan',
	),
)); ?>