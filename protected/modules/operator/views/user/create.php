<?php $this->widget('booster.widgets.TbBreadcrumbs',
array(
    'links' => array('User',
    'Create',
    ),
)
);
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); 
?>
<div class="panel-heading">
    <h3><span class="fa fa-plus"></span> Create User</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php echo $form->hiddenField($model,'user_status',array('value'=>1)); ?>
<?php $this->endWidget(); ?>