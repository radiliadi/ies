<?php $this->widget('booster.widgets.TbBreadcrumbs',
array(
    'links' => array('User',
    	'Active' => array('active'),
    	'Inactive' => array('inactive'),
    	'Deleted' => array('deleted'),
    $model->username,
    ),
)
);
?>
<div class="col-md-12">
	<div class="page-title">                    
        <h2><span class="fa fa-laptop"></span> User #<?= $model->user_id;?></h2>
	</div>
	<div class="panel panel-default">
		<div class="panel-body">
		<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'attributes'=>array(
				'username',
				'password',
				'level',
		        'user_status',
		),
		)); ?>
		</div>
	</div>
</div>