<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('User' ,
        'Deleted'=>array('deleted'
            )
        ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-laptop"></span> Manage User - <small> Deleted </small> </h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">User Deleted List</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
                <div class="btn-group pull-right">
                    <?php echo CHtml::link('<i class="fa fa-plus" title="Create"></i>', 'create', array('class'=>'btn btn btn-primary pull-right')); ?>
                </div>
            </div>
            <div class="panel-body">
            <?php echo CHtml::beginForm(array('user/exportExcel')); ?>
            <?php $this->widget('booster.widgets.TbGridView',array(
            'id'=>'user-grid',
            'dataProvider'=>$model->searchdeleted(),
            'filter'=>$model,
            'responsiveTable' => true,
            'pager' => array(
                'header' => '',
                'footer' => '',
                'cssFile' => false,
                'selectedPageCssClass' => 'active',
                'hiddenPageCssClass' => 'disabled',
                'firstPageCssClass' => 'previous',
                'lastPageCssClass' => 'next',
                'maxButtonCount' => 5,
                'firstPageLabel'=> '<i class="fa fa-angle-double-left"></i>',
                'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
                'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
                'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
                'htmlOptions' => array(
                    'class' => 'dataTables_paginate paging_bootstrap pagination pull-right',
                    'align' => 'center',
                )
            ),
            'columns'=>array(
                array(
                    'header' => 'No.',
                    'htmlOptions' => array(
                      'width' => '20px',
                      'style' => 'text-align:center'
                    ),
                    'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                    // 'class'=>'booster.widgets.TbTotalSumColumn',
                ),
                'username',
                'password',
                array(
                   'name' => 'level',
                   'filter' => User::listUser()
                ),
                'user_datetime_insert',
                array(
                    'class' => 'CButtonColumn',
                    'template' => '{deletex}',
                    'header' => 'Status',
                    'htmlOptions' => array(
                        'style' => 'white-space: 2px'
                    ),
                    'buttons' => array(
                        'deletex' => array(
                            'label' => '',
                            'options' => array(
                                'class' => 'btn btn-danger glyphicon glyphicon-trash tooltips disabled',
                                'title' => 'Deleted'
                            ),
                        ),
                    ),
                ),
                array(
                    'class' => 'CButtonColumn',
                    'template' => '{active}',
                    'header' => '',
                    'htmlOptions' => array(
                        'style' => 'white-space: 2px'
                    ),
                    'buttons' => array(
                        'active' => array(
                            'label' => '',
                            'url' => 'Yii::app()->createAbsoluteUrl("operator/user/activex/", array(
                                "id" => $data->user_id
                            ))',
                            'options' => array(
                                'class' => 'btn btn-primary bola fa fa-check tooltips',
                                'title' => 'Active'
                            ),
                        ),
                    ),
                ),
                array(
                    'class' => 'CButtonColumn',
                    'template' => '{inactive}',
                    'header' => '',
                    'htmlOptions' => array(
                        'style' => 'white-space: 2px'
                    ),
                    'buttons' => array(
                        'inactive' => array(
                            'label' => '',
                            'url' => 'Yii::app()->createAbsoluteUrl("operator/user/inactivex/", array(
                                "id" => $data->user_id
                            ))',
                            'click' => 'function() {if(!confirm("Are you sure?")) {return false;}}',
                            'options' => array(
                                'class' => 'btn btn-default glyphicon glyphicon-remove tooltips',
                                'title' => 'Inactive'
                            ),
                        ),
                    ),
                ),
                array(
                    'class' => 'CButtonColumn',
                    'template' => '{updatex}',
                    'header' => '',
                    'htmlOptions' => array(
                        'style' => 'white-space: 2px'
                    ),
                    'buttons' => array(
                        'updatex' => array(
                            'label' => '',
                            'url' => 'Yii::app()->createAbsoluteUrl("operator/user/update/", array(
                                "id" => $data->user_id
                            ))',
                            'options' => array(
                                'class' => 'btn btn-info glyphicon glyphicon-pencil tooltips',
                                'title' => 'Update'
                            ),
                        ),
                    ),
                ),
            ),
            )); 
            ?>
            <br></br>
            <hr></hr>
            <div class="col-md-6">
                <div class="col-md-3">
                <select name="fileType" class="form-control">
                    <option value="Excel">EXCEL 5 (xls)</option>
                    <!-- <option value="CSV">CSV</option> -->
                </select>
                </div>
                <div class="col-md-3">
                <?php echo CHtml::submitButton('Export',array(
                            'buttonType' => 'submit',
                            'context'=>'primary',
                            'label'=>'Export Excel',
                            'class'=>'btn btn-primary',
                        )); 
                ?>
                <?php echo CHtml::endForm(); ?>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>