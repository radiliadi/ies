<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
'id'=>'user-form',
// 'enableAjaxValidation'=>true,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-md-1">
    </div>
        <div class="col-md-10">
            <form class="form-horizontal">
                <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Username</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'username',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Username"));?>
                                        <?php echo $form->error($model,'username'); ?>

                                    </div>                                            
                                </div>
                            </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-12 control-label">Password</label>
                                    <div class="col-md-6 col-xs-12">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <?php echo $form->textField($model,'password',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Password"));?>
                                            <?php echo $form->error($model,'password'); ?>
                                        </div>                                            
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-12 control-label">Level</label>
                                    <div class="col-md-6 col-xs-12">                                            
                                        <?php echo $form->dropDownList($model,'level',CHtml::listData(UserLevel::model()->findAll(),'usrlvl_name','usrlvl_name'),array('empty'=>'-Pilih Level-','class'=>"form-control select")); ?>
                                        <?php echo $form->error($model,'level'); ?>                                       
                                    </div>
                                </div>
                                <?php echo $form->hiddenField($model,'user_status',array('value'=>$model->user_status)); ?>
                        </div>
                            <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
    									'buttonType'=>'submit',
    									'context'=>'primary',
    									'label'=>$model->isNewRecord ? 'Create' : 'Save',
    								)); ?>
                                </div>
                            </div>
                </div>
        </form>
    </div>
    <div class="col-md-1">  
    </div>
</div> 
<?php $this->endWidget(); ?>