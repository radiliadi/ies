<?php $this->widget('booster.widgets.TbBreadcrumbs',
array(
    'links' => array('User',
    $model->username=>array('view','id'=>$model->user_id),
    ),
)
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-plus"></span> Update User : #<?=$model->user_id;?> (<?=$model->username?>)</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>