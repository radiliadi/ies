<link href="http://vjs.zencdn.net/6.2.7/video-js.css" rel="stylesheet">

  <!-- If you'd like to support IE8 -->
  <script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting' ,
        ),
    )
);
?>
<div class="col-md-6">
    <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/chartOfAccount/admin">
    <div class="widget widget-default widget-item-icon">
        <div class="widget-item-left">
            <span class="glyphicon glyphicon-list-alt"></span>
        </div>
        <div class="widget-data">
            <div class="widget-int num-count"><?php echo $countCoa['coa_id'] ;?></div>
            <div class="widget-title">C O A</div>
            <div class="widget-subtitle">Chart of Account (<?php echo $coaRule->coar_desc ;?> : <?php echo $coaRule->coar_datetime ;?>)</div>
        </div>                          
    </div></a>
</div>

<div class="col-md-6">
    <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/chartOfAccountDetail/admin">
    <div class="widget widget-default widget-item-icon">
        <div class="widget-item-right">
        <span class="glyphicon glyphicon-book"></span>
        </div>                             
            <div class="widget-data-left">
            <div class="widget-int num-count"><?php echo $bukuBesar['coad_id'] ;?></div>
            <div class="widget-title">Buku Besar</div>
            <div class="widget-subtitle"></div>
            </div>                                     
    </div></a>
</div>

<?php foreach($coaTypeCustom as $custom){
        if(empty($custom->coat_color)){
            $custom->coat_color = "primary";
        }else{
            // $common->coat_color = "success";
        }

        $list = 'unknown';
        $count = '-';
        if($custom->coat_id == 1){
            $link = 'laba/admin';
            $count = Laba::getCount();
        }else if($custom->coat_id == 2){
            $link = 'arusKas/admin';
            $count = ArusKas::getCount();
        }else if($custom->coat_id == 3){
            $link = 'modal/admin';
            $count = Modal::getCount();
        }else if($custom->coat_id == 4){
            $link = 'neraca/admin';
            $count = Neraca::getCount();
        }else if($custom->coat_id == 5){
            $link = 'inventory/admin';
            $count = Inventory::getCount();
        }else if($custom->coat_id == 6){
            $link = 'chartOfAccountDetail/adminx';
            $count = $bukuBesarInactive['coad_id'];
        }else{
            $list = 'unknown';
            $count = '-';
        }

        ?>

        <div class="col-md-4">
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/<?php echo $link; ?>"><div class="widget widget-<?php echo $custom->coat_color;?> widget-item-icon">
                <div class="widget-item-right">
                <span class="<?php echo $custom->coat_span;?>"></span>
                </div>                             
                    <div class="widget-data-left">
                    <div class="widget-int num-count"><?=$count;?></div>
                    <div class="widget-title"><?php echo strtoupper($custom->coat_desc);?></div>
                    <div class="widget-subtitle"></div>
                    </div>                                     
            </div>
        </div>
<?php }

// $start = $month = strtotime('2009-02-01');
// $end = strtotime('2011-01-01');
// while($month < $end)
// {
//      echo date('F Y', $month), PHP_EOL;
//      $month = strtotime("+1 month", $month);
// }

?>

<?php foreach($coaTypeCommon as $common){
if(empty($common->coat_color)){
    $common->coat_color = "primary";
}else{
    // $common->coat_color = "success";
}
?>

<div class="col-md-3">

    <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/chartOfAccount/list/id/<?php echo $common->coat_id; ?>"><div class="widget widget-<?php echo $common->coat_color;?> widget-item-icon">
        <div class="widget-item-left">
            <span class="<?php echo $common->coat_span;?>"></span>
        </div>
        <div class="widget-data">
            <div class="widget-int num-count"><small> &nbsp; </small></div>
            <!-- <div class="widget-title"> </div> -->
            <div class="widget-subtitle"><?php echo strtoupper($common->coat_desc);?></div>
        </div>                          
    </div></a>

</div>
<?php }
?>
<!-- <video id="my-video" class="video-js" controls preload="auto" width="640" height="264"
  poster="MY_VIDEO_POSTER.jpg" data-setup="{}">
    <source src="MY_VIDEO.mp4" type='video/mp4'>
    <source src="MY_VIDEO.webm" type='video/webm'>
    <p class="vjs-no-js">
      To view this video please enable JavaScript, and consider upgrading to a web browser that
      <a href="https://www.youtube.com/watch?v=ujhJZWr-XTM" target="_blank">supports HTML5 video</a>
    </p>
  </video> -->

<!-- <div class="page-title">
</div>
<div class="row">
    <div class="panel panel-default" >
        <div class="panel-body">


        </div>
    </div>
</div> -->
