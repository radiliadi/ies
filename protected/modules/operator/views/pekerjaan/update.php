<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Settings' ,
        'Employee',
        // 'Semua' => array('Semua'=>'admin', 
        'Profession'=>array('admin'
          ),
        'Update',
        $model->id_pekerjaan,
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update Profession : #<?=$model->id_pekerjaan;?></h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>