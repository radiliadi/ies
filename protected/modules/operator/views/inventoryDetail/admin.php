<?php
// $id_pd=Yii::app()->session->get('username');
// if($id_pd !== $inventory->inv_id_pd)
// {
// throw new CHttpException('403', 'Access denied!');
// }
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting'=>'../../../accounting/index',
	    'Inventory' => '../../../inventory/admin',
	    'Detail',
        $inventory->inv_id,
      ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-laptop"></span> Inventory Detail - <small> <?=$inventory->inv_desc ;?> </small> </h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Inventory Detail</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
            	<div class="btn-group pull-right">
            	<?php 
            	if($inventory->inv_is_locked == 1){
            		echo '';
            	}else{
            		echo CHtml::link('<i class="glyphicon glyphicon-plus"></i>', '../../create/id/'.$inventory->inv_id, array('class'=>'btn btn btn-primary pull-right', 'title'=>'Create'));
            	}
            	?>
                </div> 
            </div>
            <div class="panel-body">
				<div class="form"> 
				<?php

				$form=$this->beginWidget('CActiveForm', array(
					'id'=>'inventoryDetail-form',
					'enableAjaxValidation'=>false,
					'htmlOptions'=>array(
						'enctype'=>'multipart/form-data',
						'role'=>'form',
					),
				));
				?>
	
					<div class="row">
					    <div class="box col-md-12 ">    
							<div class="box-content">
								<h1></h1>
								<table>
									<tbody>
										<tr>
											<td><b>No. Inventory</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $inventory->inv_id;?></td>
										</tr>
										<?php
											$time=strtotime($inventory->inv_date);
											$year=date("Y", $time);
											$month=date("d F", $time);
										?>
										<tr>
											<td><b>Tahun</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $year;?></td>
										</tr>
										<tr>
											<td><b>Date Created</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $month;?> (<i><?=$inventory->inv_datetime_insert;?></i>)</td>
										</tr>
										<tr>
											<td><b>Last Modified</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $inventory->inv_datetime_update.' ('.$inventory->inv_update_by.')';?></td>
										</tr>
									</tbody>
								</table>	
								<br>
								</br>
								
								<div class="panel-body">	  
									<div class="table-responsive">
										<!-- START DEFAULT DATATABLE -->
                                    <table class="table">
                                    <?php
                                    	$currentYear=date('Y');
                                    ?>
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th></th>
                                                <th>Desc</th>
                                                <th>Date</th>
                                                <th>Value</th>
                                                <th>Qty</th>
                                                <th>Tarif</th>
                                                <th colspan="2"><center>Akum. Penyusutan <?=$currentYear-2;?></center></th>
                                                <th colspan="2"><center>Akum. Penyusutan <?=$currentYear-1;?></center></th>
                                                <th colspan="2"><center>Akum. Penyusutan <?=$currentYear;?></center></th>
                                                <th colspan="2"><center>Akum. Penyusutan <?=$currentYear+1;?></center></th>
                                            </tr>
                                        </thead>
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th><center>Ak. Penyusutan</center></th>
                                                <th><center>Nilai Buku</center></th>
                                                <th><center>Ak. Penyusutan</center></th>
                                                <th><center>Nilai Buku</center></th>
                                                <th><center>Ak. Penyusutan</center></th>
                                                <th><center>Nilai Buku</center></th>
                                                <th><center>Ak. Penyusutan</center></th>
                                                <th><center>Nilai Buku</center></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        	$no=1;

                                        	foreach($inventoryDetail as $arrInvd){
                                        		if(!empty($arrInvd->invd_value)){
                                    				$value = 'Rp. '.number_format($arrInvd->invd_value);
                                    			}else{
                                    				$value = '-';
                                    			}
                                        		// $nilaiBuku = 

                                        		if($inventory->inv_is_locked == 0){
                                        			$updateInvd 	= '<a class="btn btn-info btn-xs" href="../../update/id/'.$arrInvd->invd_id.'" title="Update"><i class="fa fa-pencil"></i></a>';
                                        			$deleteInvd 	= '<a class="btn btn-danger btn-xs" href="../../delete/id/'.$arrInvd->invd_id.'" title="Delete"><i class="fa fa-trash-o"></i></a>';
                                        			$addPenyusutan 	= '<a class="btn btn-primary btn-xs" href="../../../inventorySusut/create/id/'.$arrInvd->invd_id.'/invId/'.$arrInvd->invd_inv_id.'" title="Add Penyusutan"><i class="fa fa-plus"></i></a>';
                                        		}else{
                                        			$addPenyusutan 	= '';
                                        			$deleteInvd 	= '';
                                        		}

	                                        echo '
	                                        <tr>
	                                        	<td>'.$no.'</td>
	                                        	<td>'.$updateInvd.' '.$deleteInvd.' '.$addPenyusutan.'</td>
	                                        	<td>'.$arrInvd->invd_desc.'</td>
	                                        	<td>'.$arrInvd->invd_date.'</td>
	                                        	<td>'.$value.'</td>
	                                        	<td>'.$arrInvd->invd_qty.'</td>
	                                        	<td>'.$arrInvd->invd_tarif.'</td>
	                                        	
	                                        	';

	                                        	foreach($inventorySusut as $arrInvs){
	                                        		if(!empty($arrInvs)){
		                                        		if($arrInvs->invs_invd_id == $arrInvd->invd_id){
		                                        			$nilaiBuku = $arrInvs->invs_penyusutan - $arrInvd->invd_value;
		                                        			if($arrInvs->invs_penyusutan !== 0){
		                                        				$penyusutan = $arrInvs->invs_penyusutan;
		                                        			}else{
		                                        				$penyusutan = '';
		                                        			}

		                                        			if($inventory->inv_is_locked == 0){
			                                        			$updateSusut 	= '<a class="btn btn-success btn-xs" href="../../../inventorySusut/update/id/'.$arrInvs->invs_id.'" title="Update"><i class="fa fa-pencil"></i></a>';
			                                        		}else{
			                                        			$updateSusut 	= '';
			                                        		}
				                                        echo'
				                                        	<td><center>Rp '.number_format($arrInvs->invs_penyusutan).' '.$updateSusut.'</center></td>
				                                        	<td><center>Rp '.number_format($nilaiBuku).' </center></td>
				                                        

				                                        ';
			                                        	}
			                                        }else{
			                                        	echo'
			                                        		<td>-</td>
				                                        	<td>-</td>
				                                        	<td>-</td>
				                                        	<td>-</td>
				                                        	<td>-</td>
				                                        	<td>-</td>
				                                        
			                                        	';
			                                        }
	                                        // $no++;
	                                        	}
	                                        	echo '</tr>';
	                                        	$no++;
	                                        }
                                        ?>
                                        </tbody>
                                        
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                            	<div class="table-responsive">
                                    <table class="table">
                                    	<thead>
	                                    	<tr>
	                                    		<td></td>
	                                    		<td></td>
	                                    		<td></td>
	                                    	</tr>
	                                    </thead>
	                                    <tbody>
	                                    </tbody>
	                                    <?php

											if($inventory->inv_is_locked == 0){
												echo'
												<tr>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td class="center">&nbsp; &nbsp;</td>
													<td><a class="btn btn btn-success pull-right" href="../../../../operator/inventoryDetail/lock/id/'.$inventory->inv_id.'" title="Lock"><i class="fa fa-lock"></i>
														</a>
													</td>
												</tr>
												';
											}else if($inventory->inv_is_locked == 1){
												echo'
												<tr>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td class="center">&nbsp; &nbsp;</td>
													<td><a class="btn btn btn-primary pull-left" href="../../../../operator/inventoryDetail/print/id/'.$inventory->inv_id.'" title="Print"><i class="fa fa-print"></i>
														</a>
														<a class="btn btn btn-info pull-right" href="../../../../operator/inventoryDetail/unlock/id/'.$inventory->inv_id.'" title="Lock"><i class="fa fa-unlock"></i>
														</a>
													</td>
												</tr>
												';
											}
                                    	?>
                                    </table>
                               </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
										</div>
									</div>
								</div>
								</div>	
							</div>
						</div>
					<?php $this->endWidget(); ?>
					</div>
                <br>
                </br>
            </div>
            </div>
        </div>
    </div>
</div>