<?php
$time=strtotime($inventory->inv_date);
$now=date('Y-m-d');
$now=strtotime($now);

$yearStart=date("Y", $time);
$yearStart1=date("Y", $time);
$yearEnd=date("Y", $now);
$monthStart=date("d F", $time);
$monthEnd=date("d F", $now);
if($yearStart == $yearEnd){
	$yearStart = '';
}

$pdf->AliasNbPages();
$pdf->AddPage('L',$size);                            
$pdf->SetFont('Times','b',14);     
$pdf->Image('images/png.png',230,9,45,30);      
$pdf->Ln(1);
$pdf->MultiCell(275,15,$header0,0,'C',false);
$pdf->SetFont('Times','b',12);
$pdf->MultiCell(275,5,$judul,0,'C',false);
$pdf->Ln(2);
if($monthStart == $monthEnd){
	$pdf->MultiCell(275,5,'Per '.$monthStart.' '.$yearStart1,0,'C',false);
}else{
	$pdf->MultiCell(275,5,'Per '.$monthStart.' '.$yearStart.' - '.$monthEnd.' '.$yearEnd,0,'C',false);
}


// //line break
// //content
// $pdf->Ln(10);
// $pdf->SetFont('Times','b',12);
// $pdf->SetDrawColor(0,0,0);
// $pdf->SetFillColor(104,104,104); //Set background of the cell to be that grey color
// $pdf->SetTextColor(0,0,0);
// $pdf->setLeftMargin(20);
// $pdf->SetFont('Times','b',12);
// $pdf->Cell(70,15,'MODAL',0,0,'L',false);
// $pdf->SetFont('Times','',12);
// $pdf->Cell(70,15,strtoupper('Modal Saham '),0,0,'C',false);	
// $pdf->Cell(70,15,strtoupper('Saldo Laba (Rugi)'),0,0,'C',false);	
// $pdf->Cell(70,15,strtoupper('Jumlah (Ekuitas)'),0,0,'C',false);	
// $pdf->Ln(17);

// 	$no = 0;
// 	foreach($modalDetail as $db){
// 		$pdf->SetFont('Times','b',12);
// 		$pdf->SetDrawColor(0,0,0);
// 		$pdf->SetFillColor(104,104,104); //Set background of the cell to be that grey color
// 		$pdf->SetTextColor(0,0,0);
// 		$pdf->setLeftMargin(20);
// 		$pdf->SetFont('Times','b',12);
// 		$pdf->Cell(70,15,$db->mdld_desc,0,0,'L',false);
// 		$pdf->SetFont('Times','',12);
// 		$pdf->Cell(70,15,'Rp. '.number_format($db->mdld_saham),0,0,'L',false);	
// 		$pdf->Cell(70,15,'Rp. '.number_format($db->mdld_rugi),0,0,'L',false);	
// 		$pdf->Cell(70,15,'Rp. '.number_format($db->mdld_ekuitas),0,0,'L',false);	
// 		$pdf->Ln(17);
// 		$no++;
// 	}

$pdf->Ln(10);	
$pdf->SetFont('Times','',12);
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(225);
$pdf->MultiCell(80,5,$company->cpy_city.', '.date('d M Y'),0,'L',false);
//Baris Baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(225);
$pdf->MultiCell(80,5,$company->cpy_fullname.',',0,'L',false);
$pdf->Ln(12);
//Baris baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(225);
$pdf->MultiCell(80,5,$company->cpy_dirut,0,'L',false);
//Cetak PDF
$pdf->Output('('.date('dMY').')-'.$judul.'.pdf','I');
?>