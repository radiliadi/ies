<?php $this->widget('booster.widgets.TbBreadcrumbs',
	array('links' => array('Accounting'=>'../../../accounting/index',
	    'Inventory' => '../../../inventory/admin',
	    'Admin'=>'../../admin/id/'.$model->invd_inv_id,
	   	'Create',
	    ),
	)
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Create Inventory Detail</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>