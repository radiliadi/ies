<?php
$judul="Laporan";
$this->breadcrumbs=array(
	$judul,
);
?>
<div class="row">
	<div class="col-md-12">
		<!-- PAGE CONTENT WRAPPER -->
		<div class="page-content-wrap">
		
			<!-- TILES -->                
			<div class="row">              
				<div class="col-md-2">                        
					<a href="#" class="tile tile-info">
						<?php echo $ofc_tgl;?>
						<p>April (Office)</p>                            
						<div class="informer informer-default dir-tr"><span class="fa fa-calendar"></span></div>
					</a>                        
				</div>
				<div class="col-md-3">
					<div class="widget widget-info widget-padding-sm">        
						<?php $persen=$this->timeDiff(date('Y-').(date('m')-1).'-'.$ofc_tgl,date('Y-').(date('m-d'))); 
						$hari=$persen;
						$persen=($persen/30)*100 ;?>
						<div class="widget-item-left">
							<input class="knob" data-width="100" data-height="100" data-min="0" data-max="100" data-displayInput=false data-bgColor="#d6f4ff" data-fgColor="#FFF" value="<?php echo $persen;?>%" data-readOnly="true" data-thickness=".2"/>
						</div>
						<div class="widget-data">
							<div class="widget-subtitle">
								<span class="num-count"></span><?php echo number_format($persen);?>%
								<br><br><?php echo $hari;?> hari
								<br><br><?php echo 30-$hari;?> sisa
							</div>
						</div>                            
					</div>                        
				</div>
				              
				<div class="col-md-2">                        
					<a href="#" class="tile tile-success">
						<?php echo 30-$hari;?>
						<p>Bayar Kantor (Hari)</p>                            
						<div class="informer informer-default dir-tr"><span class="fa fa-calendar"></span></div>
					</a>                        
				</div>
				<div class="col-md-2">                        
					<a href="#" class="tile tile-default">
						<?php echo $modelProject['pro_id'];?>
						<p>Project</p>                            
						<div class="informer informer-danger dir-tr"><span class="fa fa-caret-down"></span></div>
					</a>                        
				</div>
				<div class="col-md-3">                        
					<a href="#" class="tile tile-primary">
						5 Juta
						<p>Min Budget Project</p>                            
						<div class="informer informer-default"><span class="fa fa-shopping-cart"></span></div>
					</a>                        
				</div>
			</div>                       
			<div class="row">
				<div class="col-md-3">                        
					<a href="#" class="tile tile-default">
						<?php $kas=$modelKas['kredit']-$modelKas['debet'];echo 'Rp.'.number_format($kas);?>
						<p>Keuangan FS - Kas</p>
						<div class="informer informer-primary"><?php echo date('d-m-Y');?></div>
						<div class="informer informer-success dir-tr"><span class="fa fa-caret-up"></span></div>
					</a>                        
				</div>
				<div class="col-md-3">                        
					<a href="#" class="tile tile-primary">
						<?php $zakat=$modelZakat['kredit']-$modelZakat['debet']; echo 'Rp.'.number_format($zakat);?>
						<p>Keuangan FS - Zakat</p>
						<div class="informer informer-warning"><span class="fa fa-caret-down"></span></div>
						<div class="informer informer-default dir-tr"><?php echo date('d-m-Y');?></div>
					</a>
				</div>
				<div class="col-md-3">                        
					<a href="#" class="tile tile-info tile-valign">
						<?php $tim=$modelTim['user_gaji']; echo 'Rp.'.number_format($tim);?>
						<div class="informer informer-default">Keuangan FS - TIM</div>
						<div class="informer informer-default dir-br"><span class="fa fa-users"></span></div>
					</a>                            
				</div>
				<div class="col-md-3">                        
					<a href="#" class="tile tile-success tile-valign">
						<?php $project=$modelProject['pro_budget']; echo 'Rp.'.number_format($project);?>
						<div class="informer informer-default dir-tr"><span class="fa fa-cloud"></span></div>
						<div class="informer informer-default dir-bl">Keuangan FS - Project</div>
					</a>                                                    
				</div>
			</div>         
			<!-- END TILES -->


			<!-- WIDGETS -->
			<div class="row">
				<div class="col-md-3">

					<div class="widget widget-primary">
						<div class="widget-title">TOTAL OMSET</div>
						<div class="widget-subtitle"><?php echo date('d-m-Y');?></div>
						<div class="widget-int"><span data-toggle="counter" data-to="1564">
						<?php 
						$total=($modelKas['kredit']-$modelKas['debet'])+($modelZakat['kredit']-$modelZakat['debet'])+($modelTim['user_gaji']);
						echo 'Rp.'.number_format($project);
						?>
						</span></div>
						<div class="widget-controls">
							<a href="#" class="widget-control-left"><span class="fa fa-upload"></span></a>
							<a href="#" class="widget-control-right"><span class="fa fa-times"></span></a>
						</div>
					</div>

				</div>
				<div class="col-md-3">

					<div class="widget widget-success widget-no-subtitle">
						<div class="widget-subtitle">Target Profit</div>
						<div class="widget-big-int">
						<small>
							<span class="num-count">
								<?php $target="10000000"; echo 'Rp.'.number_format($target);?>
							</span>
						</small>
						</div>
						<div class="widget-controls">
							<a href="#" class="widget-control-left"><span class="fa fa-cloud"></span></a>
							<a href="#" class="widget-control-right"><span class="fa fa-times"></span></a>
						</div>                            
					</div>                        

				</div>
				<div class="col-md-3">

					<div class="widget widget-danger widget-padding-sm">
						<div class="widget-big-int plugin-clock">00:00</div>                            
						<div class="widget-subtitle plugin-date">Loading...</div>
						<div class="widget-controls">                                
							<a href="#" class="widget-control-right"><span class="fa fa-times"></span></a>
						</div>                            
						<div class="widget-buttons widget-c3">
							<div class="col">
								<a href="#"><span class="fa fa-clock-o"></span></a>
							</div>
							<div class="col">
								<a href="#"><span class="fa fa-bell"></span></a>
							</div>
							<div class="col">
								<a href="#"><span class="fa fa-calendar"></span></a>
							</div>
						</div>                            
					</div>                        

				</div>
				<div class="col-md-3">

					<div class="widget widget-info widget-padding-sm">                            
						<div class="widget-item-left">
							<?php $persen=($total/$target)*100;?>
							<input class="knob" data-width="100" data-height="100" data-min="0" data-max="100" data-displayInput=false data-bgColor="#d6f4ff" data-fgColor="#FFF" value="<?php echo $persen;?>%" data-readOnly="true" data-thickness=".2"/>
						</div>
						<div class="widget-data">
							<div class="widget-title">Keuangan</div>
							<div class="widget-big-int"><span class="num-count"><?php echo $persen;?></span>%</div>
							<div class="widget-subtitle">Menuju Target</div>                                
						</div>                            
						<div class="widget-controls">                                
							<a href="#" class="widget-control-right"><span class="fa fa-times"></span></a>
						</div>                            
					</div>                        

				</div>

			</div>
			<div class="row">
				<div class="col-md-3">

					<div class="widget widget-warning widget-carousel">
						<div class="owl-carousel" id="owl-example">
							<div>                                    
								<div class="widget-title">Keuangan</div>
								<div class="widget-subtitle">Kas</div>
								<div class="widget-int">Rp.<?php echo number_format($kas);?></div>
							</div>
							<div>                                    
								<div class="widget-title">Keuangan</div>
								<div class="widget-subtitle">Zakat</div>
								<div class="widget-int">Rp.<?php echo number_format($zakat);?></div>
							</div>
							<div>                                    
								<div class="widget-title">Keuangan</div>
								<div class="widget-subtitle">Tim</div>
								<div class="widget-int">Rp.<?php echo number_format($tim);?></div>
							</div>
						</div>                            
						<div class="widget-controls">                                
							<a href="#" class="widget-control-right"><span class="fa fa-times"></span></a>
						</div>                             
					</div>

				</div>

				<div class="col-md-3">

					<div class="widget widget-primary widget-item-icon">
						<div class="widget-item-left">
							<span class="fa fa-user"></span>
						</div>
						<div class="widget-data">
							<div class="widget-int num-count"><?php echo $modelUser['user_id'];?></div>
							<div class="widget-title">Registred users</div>
							<div class="widget-subtitle">On our website and app</div>
						</div>
						<div class="widget-controls">                                
							<a href="#" class="widget-control-right"><span class="fa fa-times"></span></a>
						</div>                            
					</div>

				</div>

				<div class="col-md-3">

					<div class="widget widget-success widget-item-icon">
						<div class="widget-item-left">
							<span class="fa fa-globe"></span>
						</div>
						<div class="widget-data">
							<div class="widget-int num-count"><?php echo $modelView['view_id'];?></div>
							<div class="widget-title">Total visitors</div>
							<div class="widget-subtitle">That visited our site</div>
						</div>
						<div class="widget-controls">                                
							<a href="#" class="widget-control-right"><span class="fa fa-times"></span></a>
						</div>                            
					</div>

				</div>

				<div class="col-md-3">

					<div class="widget widget-warning widget-item-icon">
						<div class="widget-item-right">
							<span class="fa fa-envelope"></span>
						</div>                             
						<div class="widget-data-left">
							<div class="widget-int num-count">FS</div>
							<div class="widget-title">Flash Soft</div>
							<div class="widget-subtitle">Make it easy, simple and faster.</div>
						</div>                                     
					</div>

				</div>

				<!-- START DEFAULT DATATABLE -->
				<div class="panel panel-default">
					<div class="panel-heading">                                
						<h3 class="panel-title">Count Project</h3>
						<ul class="panel-controls">
							<li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
							<li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
							<li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
						</ul>                                
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>No</th>
										<th>Tahun</th>
										<th>Bulan</th>
										<th>Projek</th>
										<th>Jumlah</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$no=1;
								for($i=2014;$i<=date('Y');$i++):
									$criteria=new CDbCriteria;
									for($j=1;$j<=12;$j++):
									if($j<10):$j='0'.$j;endif;
									//Tgl Mulai
									$criteria->select = "*,count(*) as pro_id";
									$criteria->condition = "DATE_FORMAT(pro_tgl_mulai,'%Y-%m')=:tgl";
									$criteria->params = array (	
										':tgl'=>$i.'-'.$j,
									);
									$modelMasuk=Project::model()->find($criteria);
									//project
									$criteria->select = "*";
									$criteria->condition = "DATE_FORMAT(pro_tgl_mulai,'%Y-%m')=:tgl";
									$criteria->params = array (	
										':tgl'=>$i.'-'.$j,
									);
									$modelMasukAll=Project::model()->findAll($criteria);
									//Tgl Selesai
									$criteria->select = "count(*) as pro_id";
									$criteria->condition = "DATE_FORMAT(pro_tgl_selesai,'%Y-%m')=:tgl";
									$criteria->params = array (	
										':tgl'=>$i.'-'.$j,
									);
									$modelSelesai=Project::model()->find($criteria);
									//project
									$criteria->select = "*";
									$criteria->condition = "DATE_FORMAT(pro_tgl_selesai,'%Y-%m')=:tgl";
									$criteria->params = array (	
										':tgl'=>$i.'-'.$j,
									);
									$modelSelesaiAll=Project::model()->findAll($criteria);
									$tot_projek=10;
									$mulai=($modelMasuk['pro_id']/$tot_projek)*100;
									$selesai=($modelSelesai['pro_id']/$tot_projek)*100;
									if((!empty($modelMasukAll))||(!empty($modelSelesaiAll))):
									echo '
										<tr>
											<td>'.$no.'</td>
											<td>'.$i.'</td>
											<td>'.$this->bulan($j).'</td>
											<td>';
											foreach($modelMasukAll as $db)
											{
												echo '<font color=#3fbae4>'.$db->pro_nama.'</font>, ';
											}
											foreach($modelSelesaiAll as $db)
											{
												echo '<font color=green>'.$db->pro_nama.'</font>, ';
											}
											echo'
											</td>
											<td>'.
												'<div class="progress-list">
													<div class="pull-left"><strong class="text-info">Project Mulai : '.$modelMasuk['pro_id'].'</strong></div>
													<div class="pull-right">'.$tot_projek.'</div>                                                
													<div class="progress progress-small progress-striped active">
														<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: '.$mulai.'%;">'.$tot_projek.'</div>
													</div>
												</div>
												<div class="progress-list">
													<div class="pull-left"><strong class="text-success">Project Selesai : '.$modelSelesai['pro_id'].'</strong></div>
													<div class="pull-right">'.$tot_projek.'</div>                                                
													<div class="progress progress-small progress-striped active">
														<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: '.$selesai.'%;">'.$tot_projek.'</div>
													</div>
												</div>
											</td>
										</tr>
									';
									$no++;
									endif;
									endfor;
								endfor;?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END DEFAULT DATATABLE -->
				<!-- START DEFAULT DATATABLE -->
				<div class="panel panel-default">
					<div class="panel-heading">                                
						<h3 class="panel-title">Chart</h3>
						<ul class="panel-controls">
							<li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
							<li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
							<li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
						</ul>                                
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<div class="row">
								<div class="col-md-6">                        

									<!-- START CUMULATIVE CHART -->
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Line Chart with View Finder</h3>
										</div>
										<div class="panel-body">
											<div id="chart-8" style="height: 300px;"><svg></svg></div>
										</div>
									</div>
									<!-- END CUMULATIVE CHART -->

								</div>                    
								<div class="col-md-6">                        

									<!-- START DISCRETE CHART -->
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Discrete Bar Chart</h3>
										</div>
										<div class="panel-body">
											<div id="chart-4" style="height: 300px;"><svg></svg></div>
										</div>
									</div>
									<!-- END DISCRETE CHART -->

								</div>                    
							</div>
							
							<div class="row">
								<div class="col-md-6">

									<!-- START REGULAR PIE CHART -->
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Regular pie chart</h3>

										</div>
										<div class="panel-body">
											<div id="chart-9" style="height: 300px;"><svg></svg></div>
										</div>
									</div>
									<!-- END REGULAR PIE CHART -->

								</div>    
								<div class="col-md-6">                        

									<!-- START DOUNT CHART -->
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Donut chart</h3>
										</div>
										<div class="panel-body">
											<div id="chart-10" style="height: 300px;"><svg></svg></div>
										</div>
									</div>
									<!-- END DOUNT CHART -->

								</div>                    
							</div>
						</div>
					</div>
				</div>
				<!-- END DEFAULT DATATABLE -->
			</div>
			<!-- END WIDGETS -->
		</div>
		<!-- END PAGE CONTENT WRAPPER -->
	</div>
</div>