<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Settings' ,
        'Employee',
        // 'Semua' => array('Semua'=>'admin', 
        'Division'=>array('admin'
          ),
        'Update',
        $model->id_jenj_didik,
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update Division : #<?=$model->id_jenj_didik;?></h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>