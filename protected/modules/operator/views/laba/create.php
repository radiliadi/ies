<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting' => Yii::app()->urlManager->createUrl('operator/accounting/index') ,
        'Laba' => 'admin',
        'Create',
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-plus"></span> Create Laba </h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>