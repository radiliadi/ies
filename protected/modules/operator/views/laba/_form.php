<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
'id'=>'laba-form',
// 'enableAjaxValidation'=>true,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); 
?>
<div class="row">
    <div class="col-md-1">
    </div>
        <div class="col-md-10">
            <form class="form-horizontal">
                <div class="panel panel-default">
                		<div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Date</label>
                                <div class="col-md-6 col-xs-12">
                                <?php if(!$model->isNewRecord){ ?>                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <?php   $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                'name'=>'Laba[lb_date]',
                                                                'value'=> $model->lb_date,
                                                                'options'=>array(
                                                                    'showAnim'=>'fold',
                                                                    'dateFormat'=>'yy-mm-dd',
                                                                ),
                                                                'htmlOptions' => array(
                                                                    'class' => 'form-control',
                                                                    'placeholder'=>'Laba Date',
                                                                    'readonly' => 'readonly',
                                                                ),
                                                            ));
                                        ?>
                                    </div>
                                <?php }else{ ?>
                                	<div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <?php   $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                'name'=>'Laba[lb_date]',
                                                                'value'=> $model->lb_date,
                                                                'options'=>array(
                                                                    'showAnim'=>'fold',
                                                                    'dateFormat'=>'yy-mm-dd',
                                                                ),
                                                                'htmlOptions' => array(
                                                                    'class' => 'form-control',
                                                                    'placeholder'=>'Laba Date',
                                                                ),
                                                            ));
                                        ?>
                                    </div>
                                <?php } ?>                                        
                                </div>
                            </div>
                        </div>
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textArea($model,'lb_desc',array('size'=>255,'maxlength'=>255,'class'=>"form-control",'placeholder'=>"Description"));?>
                                        <?php echo $form->error($model,'lb_desc'); ?>

                                    </div>                                            
                                </div>
                            </div>
                        </div>
                            <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
    									'buttonType'=>'submit',
    									'context'=>'primary',
    									'label'=>$model->isNewRecord ? 'Create' : 'Save',
    								)); ?>
                                </div>
                            </div>
                </div>
        </form>
    </div>
    <div class="col-md-1">  
    </div>
</div> 
<?php $this->endWidget(); ?>