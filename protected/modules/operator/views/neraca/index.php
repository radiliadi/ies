<?php
/* @var $this NeracaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Neracas',
);

$this->menu=array(
	array('label'=>'Create Neraca', 'url'=>array('create')),
	array('label'=>'Manage Neraca', 'url'=>array('admin')),
);
?>

<h1>Neracas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
