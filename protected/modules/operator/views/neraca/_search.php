<?php
/* @var $this NeracaController */
/* @var $model Neraca */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'nrc_id'); ?>
		<?php echo $form->textField($model,'nrc_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrc_desc'); ?>
		<?php echo $form->textArea($model,'nrc_desc',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrc_date'); ?>
		<?php echo $form->textField($model,'nrc_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrc_is_checked'); ?>
		<?php echo $form->textField($model,'nrc_is_checked'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrc_checked_by'); ?>
		<?php echo $form->textField($model,'nrc_checked_by',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrc_datetime_checked'); ?>
		<?php echo $form->textField($model,'nrc_datetime_checked'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrc_datetime_insert'); ?>
		<?php echo $form->textField($model,'nrc_datetime_insert'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrc_is_locked'); ?>
		<?php echo $form->textField($model,'nrc_is_locked'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrc_status'); ?>
		<?php echo $form->textField($model,'nrc_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->