<?php
/* @var $this NeracaController */
/* @var $data Neraca */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrc_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->nrc_id), array('view', 'id'=>$data->nrc_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrc_desc')); ?>:</b>
	<?php echo CHtml::encode($data->nrc_desc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrc_date')); ?>:</b>
	<?php echo CHtml::encode($data->nrc_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrc_is_checked')); ?>:</b>
	<?php echo CHtml::encode($data->nrc_is_checked); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrc_checked_by')); ?>:</b>
	<?php echo CHtml::encode($data->nrc_checked_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrc_datetime_checked')); ?>:</b>
	<?php echo CHtml::encode($data->nrc_datetime_checked); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrc_datetime_insert')); ?>:</b>
	<?php echo CHtml::encode($data->nrc_datetime_insert); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('nrc_is_locked')); ?>:</b>
	<?php echo CHtml::encode($data->nrc_is_locked); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrc_status')); ?>:</b>
	<?php echo CHtml::encode($data->nrc_status); ?>
	<br />

	*/ ?>

</div>