<?php
/* @var $this NeracaController */
/* @var $model Neraca */

$this->breadcrumbs=array(
	'Neracas'=>array('index'),
	$model->nrc_id,
);

$this->menu=array(
	array('label'=>'List Neraca', 'url'=>array('index')),
	array('label'=>'Create Neraca', 'url'=>array('create')),
	array('label'=>'Update Neraca', 'url'=>array('update', 'id'=>$model->nrc_id)),
	array('label'=>'Delete Neraca', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->nrc_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Neraca', 'url'=>array('admin')),
);
?>

<h1>View Neraca #<?php echo $model->nrc_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'nrc_id',
		'nrc_desc',
		'nrc_date',
		'nrc_is_checked',
		'nrc_checked_by',
		'nrc_datetime_checked',
		'nrc_datetime_insert',
		'nrc_is_locked',
		'nrc_status',
	),
)); ?>
