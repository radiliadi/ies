<?php $this->widget('booster.widgets.TbBreadcrumbs',
	array('links' => array('Accounting'=>'../../../accounting/index',
	    'Neraca' => '../../../neraca/admin',
	    'Detail' => '../../admin/id/'.$model->nrcd_nrc_id,
	   	$model->nrcd_id,
	    ),
	)
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-plus"></span> Update Neraca Detail : #<?=$model->nrcd_id;?> (<?=$model->nrcd_date?>)</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>