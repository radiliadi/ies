<?php
/* @var $this NeracaDetailController */
/* @var $model NeracaDetail */

$this->breadcrumbs=array(
	'Neraca Details'=>array('index'),
	$model->nrcd_id,
);

$this->menu=array(
	array('label'=>'List NeracaDetail', 'url'=>array('index')),
	array('label'=>'Create NeracaDetail', 'url'=>array('create')),
	array('label'=>'Update NeracaDetail', 'url'=>array('update', 'id'=>$model->nrcd_id)),
	array('label'=>'Delete NeracaDetail', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->nrcd_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage NeracaDetail', 'url'=>array('admin')),
);
?>

<h1>View NeracaDetail #<?php echo $model->nrcd_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'nrcd_id',
		'nrcd_nrc_id',
		'nrcd_date',
		'nrcd_parent',
		'nrcd_child',
		'nrcd_desc',
		'nrcd_price',
		'nrcd_bni_idr',
		'nrcd_bni_giro',
		'nrcd_bni_kso',
		'nrcd_bni_usd',
		'nrcd_piutang_usaha',
		'nrcd_piutang_kyw',
		'nrcd_pdd',
		'nrcd_tanah_bangunan',
		'nrcd_datetime_insert',
		'nrcd_datetime_update',
		'nrcd_datetime_delete',
		'nrcd_insert_by',
		'nrcd_update_by',
		'nrcd_delete_by',
		'nrcd_status',
	),
)); ?>
