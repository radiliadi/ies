<?php
/* @var $this NeracaDetailController */
/* @var $model NeracaDetail */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'nrcd_id'); ?>
		<?php echo $form->textField($model,'nrcd_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_nrc_id'); ?>
		<?php echo $form->textField($model,'nrcd_nrc_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_date'); ?>
		<?php echo $form->textField($model,'nrcd_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_parent'); ?>
		<?php echo $form->textField($model,'nrcd_parent'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_child'); ?>
		<?php echo $form->textField($model,'nrcd_child'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_desc'); ?>
		<?php echo $form->textArea($model,'nrcd_desc',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_price'); ?>
		<?php echo $form->textField($model,'nrcd_price',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_bni_idr'); ?>
		<?php echo $form->textField($model,'nrcd_bni_idr',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_bni_giro'); ?>
		<?php echo $form->textField($model,'nrcd_bni_giro',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_bni_kso'); ?>
		<?php echo $form->textField($model,'nrcd_bni_kso',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_bni_usd'); ?>
		<?php echo $form->textField($model,'nrcd_bni_usd',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_piutang_usaha'); ?>
		<?php echo $form->textField($model,'nrcd_piutang_usaha',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_piutang_kyw'); ?>
		<?php echo $form->textField($model,'nrcd_piutang_kyw',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_pdd'); ?>
		<?php echo $form->textField($model,'nrcd_pdd',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_tanah_bangunan'); ?>
		<?php echo $form->textField($model,'nrcd_tanah_bangunan',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_datetime_insert'); ?>
		<?php echo $form->textField($model,'nrcd_datetime_insert'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_datetime_update'); ?>
		<?php echo $form->textField($model,'nrcd_datetime_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_datetime_delete'); ?>
		<?php echo $form->textField($model,'nrcd_datetime_delete'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_insert_by'); ?>
		<?php echo $form->textField($model,'nrcd_insert_by',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_update_by'); ?>
		<?php echo $form->textField($model,'nrcd_update_by',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_delete_by'); ?>
		<?php echo $form->textField($model,'nrcd_delete_by',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nrcd_status'); ?>
		<?php echo $form->textField($model,'nrcd_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->