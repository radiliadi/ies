<?php
/* @var $this NeracaDetailController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Neraca Details',
);

$this->menu=array(
	array('label'=>'Create NeracaDetail', 'url'=>array('create')),
	array('label'=>'Manage NeracaDetail', 'url'=>array('admin')),
);
?>

<h1>Neraca Details</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
