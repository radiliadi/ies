<?php
/* @var $this NeracaDetailController */
/* @var $data NeracaDetail */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->nrcd_id), array('view', 'id'=>$data->nrcd_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_nrc_id')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_nrc_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_date')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_parent')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_parent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_child')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_child); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_desc')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_desc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_price')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_price); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_bni_idr')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_bni_idr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_bni_giro')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_bni_giro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_bni_kso')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_bni_kso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_bni_usd')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_bni_usd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_piutang_usaha')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_piutang_usaha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_piutang_kyw')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_piutang_kyw); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_pdd')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_pdd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_tanah_bangunan')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_tanah_bangunan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_datetime_insert')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_datetime_insert); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_datetime_update')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_datetime_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_datetime_delete')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_datetime_delete); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_insert_by')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_insert_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_update_by')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_update_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_delete_by')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_delete_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nrcd_status')); ?>:</b>
	<?php echo CHtml::encode($data->nrcd_status); ?>
	<br />

	*/ ?>

</div>