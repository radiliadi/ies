<?php
$id_pd=Yii::app()->session->get('username');
?>
<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
'id'=>'neracaDetail-form',
// 'enableAjaxValidation'=>true,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-md-1">
    </div>
        <div class="col-md-10">
            <form class="form-horizontal">
                <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Date</label>
                                <div class="col-md-6 col-xs-12">                                          
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <?php   $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                'name'=>'NeracaDetail[nrcd_date]',
                                                                'value'=> $model->nrcd_date,
                                                                'options'=>array(
                                                                    'showAnim'=>'fold',
                                                                    'dateFormat'=>'yy-mm-dd',
                                                                ),
                                                                'htmlOptions' => array(
                                                                    'class' => 'form-control',
                                                                    'placeholder'=>'NeracaDetail Date',
                                                                    // 'readonly' => 'readonly',
                                                                ),
                                                            ));
                                        ?>
                                    </div>                                
                                </div>
                            </div>
	                        <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Parent</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <?php echo $form->dropDownList($model,'nrcd_nrcp_id',CHtml::listData(NeracaParent::model()->findAll(),'nrcp_id','nrcp_desc'),array('empty'=>'-Pilih Parent-','class'=>"form-control select")); ?>
                                    <?php echo $form->error($model,'nrcd_nrcp_id'); ?>                                       
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Child</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <?php echo $form->dropDownList($model,'nrcd_nrcc_id',CHtml::listData(NeracaChild::model()->findAll(),'nrcc_id','nrcc_desc'),array('empty'=>'-Pilih Child-','class'=>"form-control select")); ?>
                                    <?php echo $form->error($model,'nrcd_nrcc_id'); ?>                                       
                                </div>
                            </div>
                        	<div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'nrcd_desc',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Description"));?>
                                        <?php echo $form->error($model,'nrcd_desc'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Price / Value</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'nrcd_price',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Price / Value"));?>
                                        <?php echo $form->error($model,'nrcd_price'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Sort</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'nrcd_sort',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Sort"));?>
                                        <?php echo $form->error($model,'nrcd_sort'); ?>

                                    </div>                                            
                                </div>
                            </div>
                        </div>
                            <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
    									'buttonType'=>'submit',
    									'context'=>'primary',
    									'label'=>$model->isNewRecord ? 'Create' : 'Save',
    								)); ?>
                                </div>
                            </div>
                </div>
        	</form>
    	</div>
    <div class="col-md-1">  
    </div>
</div> 
<?php $this->endWidget(); ?>