<?php
// $id_pd=Yii::app()->session->get('username');
// if($id_pd !== $expenses->exp_id_pd)
// {
// throw new CHttpException('403', 'Access denied!');
// }
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting' => '../../../accounting/index',
    	'Neraca' => '../../../neraca/admin' ,
        'Detail',
        $neracaDetailId->nrcd_id,
      ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-laptop"></span> Neraca Detail - <small> <?=$neraca->nrc_desc ;?> </small> </h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"> </h3>
                <ul class="panel-controls">
                	<li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
            	<div class="btn-group pull-right">
            		<?php 
            		if($neraca->nrc_is_locked == 1 ){
            			echo '';
            		}else{
            			echo CHtml::link('<i class="glyphicon glyphicon-download-alt"></i>', '../../generate/id/'.$neracaDetailId->nrcd_nrc_id.'/date/'.$neracaDetailId->nrcd_date, array('class'=>'btn btn btn-info pull-right', 'confirm' => 'Are you sure to generate? Your data will be automatic changed', 'title'=>'Generate'));
            			// echo ' ';
            			// echo CHtml::link('<i class="glyphicon glyphicon-plus"></i>', '../../create/id/'.$neraca->nrc_id, array('class'=>'btn btn btn-primary pull-right', 'title'=>'Create'));
            		}
            		?>
                </div>
                <div class="btn-group pull-left">
            		<?php 
            		if($neraca->nrc_is_locked == 1 ){
            			echo '';
            		}else{
            			// echo CHtml::link('<i class="glyphicon glyphicon-pencil"></i>', '../../update/id/'.$neraca->nrc_id, array('class'=>'btn btn btn-primary pull-right', 'title'=>'Update')); 
            			echo CHtml::link('<i class="glyphicon glyphicon-plus"></i>', '../../create/id/'.$neraca->nrc_id, array('class'=>'btn btn btn-primary pull-right', 'title'=>'Create'));
            		}
            		?>
                </div>
            </div>
            <div class="panel-body">
				<div class="form"> 
				<?php

				$form=$this->beginWidget('CActiveForm', array(
					'id'=>'labaDetail-form',
					'enableAjaxValidation'=>false,
					'htmlOptions'=>array(
						'enctype'=>'multipart/form-data',
						'role'=>'form',
					),
				));
				?>
	
					<div class="row">
					    <div class="box col-md-12 ">    
							<div class="box-content">
								<h1></h1>
								<table>
									<tbody>
										<tr>
											<td><b>No. Neraca</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $neraca->nrc_id;?></td>
										</tr>
										<?php
											$time=strtotime($neraca->nrc_date);
											$year=date("Y", $time);
											$month=date("d F", $time);
										?>
										<tr>
											<td><b>Tahun</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $year;?></td>
										</tr>
										<tr>
											<td><b>Date Created</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $month;?> (<i><?=$neracaDetailId->nrcd_datetime_insert;?></i>)</td>
										</tr>
										<tr>
											<td><b>Last Modified</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $neracaDetailId->nrcd_datetime_update.' ('.$neracaDetailId->nrcd_update_by.')';?></td>
										</tr>
										<tr>
											<td><b>Notes</b></td>
											<td class="center">:&nbsp; &nbsp;<font color="blue">*</font> (hasil generate), <font color="red">*</font> (diisi oleh admin)</td>
										</tr>
									</tbody>
								</table>	
								<br>
								</br>
								
								<div class="panel-body">
									<div class="col-md-6">  
										<div class="table-responsive">
											<!-- START DEFAULT DATATABLE -->
	                                    <table class="table">
	                                    	<thead>
		                                    	<tr>
		                                    		<td><h4><b><?=strtoupper($neracaParent1->nrcp_desc) ;?></b></h4></td>
		                                    		<td></td>
		                                    		<td></td>
		                                    	</tr>
		                                    	<tr>
		                                    		<td><b><?=strtoupper($neracaChild1->nrcc_desc) ;?></b></td>
		                                    		<td></td>
		                                    		<td></td>
		                                    	</tr>
		                                    	<?php 
		                                    		$sumPrice1=0;
		                                    		foreach($content1 as $cnt1){
														$sumPrice1=$sumPrice1+$cnt1->nrcd_price;
		                                    	?>
			                                    	<?php if($cnt1->nrcd_sort <= 10){ # kecil sama dgn 10
			                                    	?>
			                                    	<tr>
														<td style="padding-left:5%;"><?=$cnt1->nrcd_desc;?> <font color="red">*</font></td>
														<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($cnt1->nrcd_price);?></td>
														<td>
														<?php if($neraca->nrc_is_locked == 0){ ?>
															<a class="btn btn-primary btn-xs" href="../../update/id/<?=$cnt1->nrcd_id;?>" title="Update">
																<i class="glyphicon glyphicon-pencil"></i>
															</a> 
															<a class="btn btn-danger btn-xs" href="../../delete/id/<?=$cnt1->nrcd_id;?>" title="Delete" onclick="return confirm('Are you sure?')">
																<i class="glyphicon glyphicon-remove"></i>
															</a>
														<?php } ?>
														</td>
													</tr>
													<?php } ?>
												<?php }
												?>
												<tr>
													<td style="padding-left:5%;">Kas dan Setara Kas <small>(<=10)</small><font color="blue">*</font></td>
													<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($contentGenerate->nrcd_kas);?></td>
													<td></td>
												</tr>
												<tr>
													<td style="padding-left:5%;">Bank BNI IDR <font color="blue">*</font></td>
													<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($contentGenerate->nrcd_bni_idr);?></td>
													<td></td>
												</tr>
												<tr>
													<td style="padding-left:5%;">Bank BNI GIRO <font color="blue">*</font></td>
													<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($contentGenerate->nrcd_bni_giro);?></td>
													<td></td>
												</tr>
												<tr>
													<td style="padding-left:5%;">Bank BNI KSO <font color="blue">*</font></td>
													<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($contentGenerate->nrcd_bni_kso);?></td>
													<td></td>
												</tr>
												<tr>
													<td style="padding-left:5%;">Bank BNI USD <font color="blue">*</font></td>
													<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($contentGenerate->nrcd_bni_usd);?></td>
													<td></td>
												</tr>
												<!-- <?php //echo 'asu'; die;?> -->
												<?php 
		                                    		$sumPrice1=0;
		                                    		foreach($content1 as $cnt1){
														$sumPrice1=$sumPrice1+$cnt1->nrcd_price;
		                                    	?>
			                                    	<?php if($cnt1->nrcd_sort > 10){ # besar dari 10
			                                    	?>
			                                    	<tr>
														<td style="padding-left:5%;"><?=$cnt1->nrcd_desc;?> <font color="red">*</font></td>
														<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($cnt1->nrcd_price);?></td>
														<td>
														<?php if($neraca->nrc_is_locked == 0){ ?>
															<a class="btn btn-primary btn-xs" href="../../update/id/<?=$cnt1->nrcd_id;?>" title="Update">
																<i class="glyphicon glyphicon-pencil"></i>
															</a> 
															<a class="btn btn-danger btn-xs" href="../../delete/id/<?=$cnt1->nrcd_id;?>" title="Delete" onclick="return confirm('Are you sure?')">
																<i class="glyphicon glyphicon-remove"></i>
															</a>
														<?php } ?>
														</td>
													</tr>
													<?php } ?>
												<?php }
												?>
												<tr>
													<td style="padding-left:5%;">Piutang Usaha <small>(>10)</small><font color="blue">*</font></td>
													<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($contentGenerate->nrcd_piutang_usaha);?></td>
													<td></td>
												</tr>
												<tr>
													<td style="padding-left:5%;">Piutang Karyawan <font color="blue">*</font></td>
													<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($contentGenerate->nrcd_piutang_kyw);?></td>
													<td></td>
												</tr>
												<tr>
													<td style="padding-left:5%;">Pajak Dibayar Dimuka <small>(>20 & <=30)</small><font color="blue">*</font></td>
													<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($contentGenerate->nrcd_pdd);?></td>
													<td></td>
												</tr>
													<?php if($cnt1->nrcd_sort > 20 && $cnt1->nrcd_sort <=30){ # besar dari 20, kecil sama dgn 30
			                                    	?>
			                                    	<tr>
														<td style="padding-left:5%;"><?=$cnt1->nrcd_desc;?> <font color="red">*</font></td>
														<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($cnt1->nrcd_price);?></td>
														<td>
														<?php if($neraca->nrc_is_locked == 0){ ?>
															<a class="btn btn-primary btn-xs" href="../../update/id/<?=$cnt1->nrcd_id;?>" title="Update">
																<i class="glyphicon glyphicon-pencil"></i>
															</a> 
															<a class="btn btn-danger btn-xs" href="../../delete/id/<?=$cnt1->nrcd_id;?>" title="Delete" onclick="return confirm('Are you sure?')">
																<i class="glyphicon glyphicon-remove"></i>
															</a>
														<?php } ?>
														</td>
													</tr>
													<?php } ?>
												<tr>
													<td style="padding-left:5%;"> &nbsp; </td>
													<td class="center"> &nbsp; </td>
													<td> &nbsp; </td>
												</tr>
												<?php
													$sumPriceEnd1=0;
													$sumPriceEnd1=
														$sumPrice1
														+$contentGenerate->nrcd_kas
														+$contentGenerate->nrcd_bni_idr
														+$contentGenerate->nrcd_bni_giro
														+$contentGenerate->nrcd_bni_kso
														+$contentGenerate->nrcd_bni_usd
														+$contentGenerate->nrcd_piutang_usaha
														+$contentGenerate->nrcd_piutang_kyw
														+$contentGenerate->nrcd_pdd
														;
												?>
												<tr>
													<td style="padding-left:5%;"><b>JUMLAH <?=strtoupper($neracaChild1->nrcc_desc) ;?> </b></td>
													<td class="center"><b>:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($sumPriceEnd1);?></b></td>
													<td></td>
												</tr>
	                                    	</thead>
	                                    	<tbody>
	                                    	</tbody>
	                                    </table>
	                                </div>
	                            </div>
	                            <div class="col-md-6">  
										<div class="table-responsive">
											<!-- START DEFAULT DATATABLE -->
	                                    <table class="table">
	                                    	<thead>
		                                    	<tr>
		                                    		<td><h4><b><?=strtoupper($neracaParent2->nrcp_desc) ;?></b></h4></td>
		                                    		<td></td>
		                                    		<td></td>
		                                    	</tr>
		                                    	<tr>
		                                    		<td><b><?=strtoupper($neracaChild2->nrcc_desc) ;?></b> <small>(<=10)</small></td>
		                                    		<td></td>
		                                    		<td></td>
		                                    	</tr>
		                                    	<?php 
		                                    		$sumPrice2=0;
		                                    		foreach($content2 as $cnt2){
														$sumPrice2=$sumPrice2+$cnt2->nrcd_price;
		                                    	?>
			                                    	<?php if($cnt2->nrcd_sort <= 10){ # kecil sm dgn 10
			                                    	?>
			                                    	<tr>
														<td style="padding-left:5%;"><?=$cnt2->nrcd_desc;?> <font color="red">*</font></td>
														<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($cnt2->nrcd_price);?></td>
														<td>
														<?php if($neraca->nrc_is_locked == 0){ ?>
															<a class="btn btn-primary btn-xs" href="../../update/id/<?=$cnt2->nrcd_id;?>" title="Update">
																<i class="glyphicon glyphicon-pencil"></i>
															</a> 
															<a class="btn btn-danger btn-xs" href="../../delete/id/<?=$cnt2->nrcd_id;?>" title="Delete" onclick="return confirm('Are you sure?')">
																<i class="glyphicon glyphicon-remove"></i>
															</a>
														<?php } ?>
														</td>
													</tr>
													<?php } ?>
												<?php }
												?>
												<tr>
													<td style="padding-left:5%;"> &nbsp; </td>
													<td class="center"> &nbsp; </td>
													<td> &nbsp; </td>
												</tr>
												<?php
													$sumPriceEnd2=0;
													$sumPriceEnd2=$sumPrice2;
												?>
												<tr>
													<td style="padding-left:5%;"><b>JUMLAH <?=strtoupper($neracaChild2->nrcc_desc) ;?> </b></td>
													<td class="center"><b>:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($sumPriceEnd2);?></b></td>
													<td></td>
												</tr>
	                                    	</thead>
	                                    	<tbody>
	                                    	</tbody>
	                                    </table>
	                                </div>
	                            </div>
	                            <div class="col-md-12">
	                            	<div class="table-responsive">
	                                    <table class="table">
	                                    	<thead>
		                                    	<tr>
		                                    		<td></td>
		                                    		<td></td>
		                                    		<td></td>
		                                    	</tr>
	                                    </table>
	                                   </div>
	                            </div>
	                            <div class="col-md-6">  
										<div class="table-responsive">
											<!-- START DEFAULT DATATABLE -->
	                                    <table class="table">
	                                    <br></br>
	                                    	<thead>
		                                    	<tr>
		                                    		<td><b><?=strtoupper($neracaChild3->nrcc_desc) ;?></b></td>
		                                    		<td></td>
		                                    		<td></td>
		                                    	</tr>
		                                    	<tr>
													<td style="padding-left:5%;">Aktiva Tetap <small>(<=10)</small></td>
													<td class="center">&nbsp; &nbsp;</td>
													<td></td>
												</tr>
			                                    	<?php 
			                                    		$sumPrice3=0;
			                                    		foreach($content3 as $cnt3){
															$sumPrice3=$sumPrice3+$cnt3->nrcd_price;
			                                    	?>
				                                    	<?php if($cnt3->nrcd_sort <= 10){ # kecil sm dgn 10
				                                    	?>
				                                    	<tr>
															<td style="padding-left:8%;"><?=$cnt3->nrcd_desc;?> <font color="red">*</font></td>
															<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($cnt3->nrcd_price);?></td>
															<td>
															<?php if($neraca->nrc_is_locked == 0){ ?>
																<a class="btn btn-primary btn-xs" href="../../update/id/<?=$cnt3->nrcd_id;?>" title="Update">
																	<i class="glyphicon glyphicon-pencil"></i>
																</a> 
																<a class="btn btn-danger btn-xs" href="../../delete/id/<?=$cnt3->nrcd_id;?>" title="Delete" onclick="return confirm('Are you sure?')">
																	<i class="glyphicon glyphicon-remove"></i>
																</a>
															<?php } ?>
															</td>
														</tr>
														<?php } ?>
													<?php }
													?>
													<!--
													<tr>
														<td style="padding-left:8%;">Tanah & Bangunan <font color="blue">*</font></td>
														<td class="center">:&nbsp; &nbsp;<?php //echo 'Rp. '.number_format($contentGenerate->nrcd_tanah_bangunan);?></td>
														<td></td>
													</tr>
													!-->
												<tr>
													<td style="padding-left:5%;">Akumulasi Peny. Aktiva Tetap <small>(>10 & <=20)</small></td>
													<td class="center">&nbsp; &nbsp;</td>
													<td></td>
												</tr>
			                                    	<?php 
			                                    		$sumPrice3=0;
			                                    		foreach($content3 as $cnt3){
															$sumPrice3=$sumPrice3+$cnt3->nrcd_price;
			                                    	?>
				                                    	<?php if($cnt3->nrcd_sort > 10 && $cnt3->nrcd_sort <= 20){# besar dr 10 dan kecil sm dgn 20
				                                    	?>
				                                    	<tr>
															<td style="padding-left:8%;"><?=$cnt3->nrcd_desc;?> <font color="red">*</font></td>
															<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($cnt3->nrcd_price);?></td>
															<td>
															<?php if($neraca->nrc_is_locked == 0){ ?>
																<a class="btn btn-primary btn-xs" href="../../update/id/<?=$cnt3->nrcd_id;?>" title="Update">
																	<i class="glyphicon glyphicon-pencil"></i>
																</a> 
																<a class="btn btn-danger btn-xs" href="../../delete/id/<?=$cnt3->nrcd_id;?>" title="Delete" onclick="return confirm('Are you sure?')">
																	<i class="glyphicon glyphicon-remove"></i>
																</a>
															<?php } ?>
															</td>
														</tr>
														<?php } ?>
													<?php }
													?>
													<tr>
														<td style="padding-left:5%;"> &nbsp; </td>
														<td class="center"> &nbsp; </td>
														<td> &nbsp; </td>
													</tr>
													<?php
														$sumPriceEnd3=0;
														$sumPriceEnd3=$sumPrice3 + $contentGenerate->nrcd_tanah_bangunan;
													?>
													<tr>
														<td style="padding-left:5%;"><b>JUMLAH <?=strtoupper($neracaChild3->nrcc_desc) ;?> </b></td>
														<td class="center"><b>:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($sumPriceEnd3);?></b></td>
														<td></td>
													</tr>
													<?php
														$sumPriceAset = $sumPriceEnd1 + $sumPriceEnd3;
													?>
													<tr>
														<td style="padding-left:5%;"><h4><b>JUMLAH <?=strtoupper($neracaParent1->nrcp_desc) ;?> </b></h4></td>
														<td class="center"><h4><b>:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($sumPriceAset);?></b></h4></td>
														<td></td>
													</tr>
	                                    	</thead>
	                                    	<tbody>
	                                    	</tbody>
	                                    </table>
	                                </div>
	                            </div>
	                            <div class="col-md-6">  
										<div class="table-responsive">
											<!-- START DEFAULT DATATABLE -->
	                                    <table class="table">
	                                    <br></br>
	                                    	<thead>
		                                    	<tr>
		                                    		<td><b><?=strtoupper($neracaChild4->nrcc_desc) ;?></b> <small>(<= 100)</small></td>
		                                    		<td></td>
		                                    		<td></td>
		                                    	</tr>
		                                    	<?php 
		                                    		$sumPrice4=0;
		                                    		foreach($content4 as $cnt4){
														$sumPrice4=$sumPrice4+$cnt4->nrcd_price;
		                                    	?>
			                                    	<?php if($cnt4->nrcd_sort <= 100){ # kecil sm dgn 100
			                                    	?>
			                                    	<tr>
														<td style="padding-left:5%;"><?=$cnt4->nrcd_desc;?> <font color="red">*</font></td>
														<td class="center">:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($cnt4->nrcd_price);?></td>
														<td>
														<?php if($neraca->nrc_is_locked == 0){ ?>
															<a class="btn btn-primary btn-xs" href="../../update/id/<?=$cnt4->nrcd_id;?>" title="Update">
																<i class="glyphicon glyphicon-pencil"></i>
															</a> 
															<a class="btn btn-danger btn-xs" href="../../delete/id/<?=$cnt4->nrcd_id;?>" title="Delete" onclick="return confirm('Are you sure?')">
																<i class="glyphicon glyphicon-remove"></i>
															</a>
														<?php } ?>
														</td>
													</tr>
													<?php } ?>
												<?php }
												?>
												<tr>
													<td style="padding-left:5%;"> &nbsp; </td>
													<td class="center"> &nbsp; </td>
													<td> &nbsp; </td>
												</tr>
												<?php
													$sumPriceEnd4=0;
													$sumPriceEnd4=$sumPrice4;
												?>
												<tr>
													<td style="padding-left:5%;"><b>JUMLAH <?=strtoupper($neracaChild4->nrcc_desc) ;?> </b></td>
													<td class="center"><b>:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($sumPriceEnd4);?></b></td>
													<td></td>
												</tr>
												<?php
													$sumPriceEkuitas = $sumPriceEnd2 + $sumPriceEnd4;
												?>
												<tr>
													<td style="padding-left:5%;"><h4><b>JUMLAH <?=strtoupper($neracaParent2->nrcp_desc) ;?> </b></h4></td>
													<td class="center"><h4><b>:&nbsp; &nbsp;<?php echo 'Rp. '.number_format($sumPriceEkuitas);?></b></h4></td>
													<td></td>
												</tr>
	                                    	</thead>
	                                    	<tbody>
	                                    	</tbody>
	                                    </table>
	                                </div>
	                            </div>
	                            <div class="col-md-12">
	                            	<div class="table-responsive">
	                                    <table class="table">
	                                    	<thead>
		                                    	<tr>
		                                    		<td></td>
		                                    		<td></td>
		                                    		<td></td>
		                                    	</tr>
		                                    </thead>
		                                    <tbody>
	                                    	</tbody>
		                                    <?php
												if($neraca->nrc_is_locked == 0){
													echo'
													<tr>
														<td>&nbsp;</td>
														<td class="center">&nbsp; &nbsp;</td>
														<td><a class="btn btn btn-success pull-right" href="../../../../operator/neracaDetail/lock/id/'.$neraca->nrc_id.'/sum1/'.$sumPriceEnd1.'/sum2/'.$sumPriceEnd2.'/sum3/'.$sumPriceEnd3.'/sum4/'.$sumPriceEnd4.'/sum13/'.$sumPriceAset.'/sum24/'.$sumPriceEkuitas.'" title="Lock"><i class="fa fa-lock"></i>
															</a>
														</td>
													</tr>
													';
												}else if($neraca->nrc_is_locked == 1){
													echo'
													<tr>
														<td>&nbsp;</td>
														<td class="center">&nbsp; &nbsp;</td>
														<td><a class="btn btn btn-primary pull-left" href="../../../../operator/neracaDetail/print/id/'.$neraca->nrc_id.'" title="Print"><i class="fa fa-print"></i>
															</a>
															<a class="btn btn btn-info pull-right" href="../../../../operator/neracaDetail/unlock/id/'.$neraca->nrc_id.'" title="Lock"><i class="fa fa-unlock"></i>
															</a>
														</td>
													</tr>
													';
												}
	                                    	?>
	                                    </table>
	                                </div>
	                            </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
						</div>
					</div>
				</div>

			</div>	
		</div>

	</div>
					<?php $this->endWidget(); ?>
					</div>
                <br>
                </br>
            </div>
            </div>
        </div>
    </div>
</div>