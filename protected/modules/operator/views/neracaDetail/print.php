<?php
$time=strtotime($neraca->nrc_date);
$now=date('Y-m-d');
$now=strtotime($now);
// $suffixPrice = '.00';
$alignPrice = 'R';

$yearStart=date("Y", $time);
$yearStart1=date("Y", $time);
$yearEnd=date("Y", $now);
$monthStart=date("d F", $time);
$monthEnd=date("d F", $now);
if($yearStart == $yearEnd){
	$yearStart = '';
}

$pdf->AliasNbPages();
$pdf->AddPage('P',$size);                            
$pdf->SetFont('Times','b',14);     
$pdf->Image('images/png.png',145,9,35,25);      
$pdf->Ln(1);
$pdf->MultiCell(190,15,$header0,0,'C',false);
$pdf->SetFont('Times','b',12);
$pdf->MultiCell(190,5,$judul,0,'C',false);
$pdf->Ln(2);
if($monthStart == $monthEnd){
	$pdf->MultiCell(190,5,'Per '.$monthStart.' '.$yearStart1,0,'C',false);
}else{
	$pdf->MultiCell(190,5,'Per '.$monthStart.' '.$yearStart.' - '.$monthEnd.' '.$yearEnd,0,'C',false);
}

// $pdf->Ln(5); //Line break
//mulai tabel
//line break
//content
$pdf->Ln(7);
$pdf->SetFont('Times','b',10);
$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(104,104,104); //Set background of the cell to be that grey color
$pdf->SetTextColor(0,0,0);
$pdf->setLeftMargin(30);
// $pdf->Cell(100,5,'',0,0,'L',false);
// $pdf->SetFont('Times','u',10);
// $pdf->SetFont('Times','bu',10);
// $pdf->Cell(20,5,$monthStart.' '.$yearStart,0,0,'L',false);

$pdf->Ln(5);
$pdf->Cell(100,5,'',0,0,'L',false);
$pdf->SetFont('Times','b',10);
$pdf->Cell(20,5,'Rp. (Rupiah)',0,0,'C',false);

$pdf->Ln(5);
$pdf->SetFont('Times','bu',14);
$pdf->Cell(100,5,'ASET',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(20,5,'',0,0,'L',false);

$pdf->Ln(5);
$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'ASET LANCAR',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(20,5,'',0,0,'C',false);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->setLeftMargin(35); //set margin ampe ke bawah
$pdf->Cell(100,5,'Kas dan Setara Kas',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(15,5,''.number_format($neracaDetailId->nrcd_kas, 2),0,0,$alignPrice,false);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Bank BNI IDR',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(15,5,''.number_format($neracaDetailId->nrcd_bni_idr, 2),0,0,$alignPrice,false);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Bank BNI GIRO',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(15,5,''.number_format($neracaDetailId->nrcd_bni_giro, 2),0,0,$alignPrice,false);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Bank BNI KSO',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(15,5,''.number_format($neracaDetailId->nrcd_bni_kso, 2),0,0,$alignPrice,false);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Bank BNI USD',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(15,5,''.number_format($neracaDetailId->nrcd_bni_usd, 2),0,0,$alignPrice,false);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Piutang Usaha',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(15,5,''.number_format($neracaDetailId->nrcd_piutang_usaha, 2),0,0,$alignPrice,false);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Piutang Karyawan',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(15,5,''.number_format($neracaDetailId->nrcd_piutang_kyw, 2),0,0,$alignPrice,false);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Pajak Dibayar Dimuka',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(15,5,''.number_format($neracaDetailId->nrcd_pdd, 2),0,0,$alignPrice,false);

$pdf->Ln(5);
$pdf->setLeftMargin(30); //netral
$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'JUMLAH ASET LANCAR',0,0,'L',false);
$pdf->SetFont('Times','b',10);
$pdf->Cell(15,5,''.number_format($neraca->nrc_sum_1, 2),0,0,$alignPrice,false);

$pdf->Ln(7);
$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'ASET TIDAK LANCAR',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(20,5,'',0,0,'C',false);

$pdf->Ln(5);
$pdf->setLeftMargin(35);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Aktiva Tetap',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(15,5,''.number_format($neracaDetailId->nrcd_pdd, 2),0,0,$alignPrice,false);

$pdf->Ln(5);
$pdf->setLeftMargin(40);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Aktivax',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(10,5,''.number_format($neracaDetailId->nrcd_pdd, 2),0,0,$alignPrice,false);	
$pdf->setLeftMargin(35);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Akum. Penyusutan Aktiva Tetap',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(15,5,''.number_format($neracaDetailId->nrcd_pdd, 2),0,0,$alignPrice,false);

$pdf->Ln(5);
$pdf->setLeftMargin(40);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Aktivax',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(10,5,''.number_format($neracaDetailId->nrcd_pdd, 2),0,0,$alignPrice,false);	
$pdf->setLeftMargin(35);

$pdf->Ln(5);
$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'JUMLAH ASET TIDAK LANCAR',0,0,'L',false);
$pdf->SetFont('Times','b',10);
$pdf->Cell(15,5,''.number_format($neraca->nrc_sum_3, 2),0,0,$alignPrice,false);
$pdf->setLeftMargin(30);

$pdf->Ln(5);
$pdf->SetFont('Times','bu',12);
$pdf->Cell(100,5,'JUMLAH ASET',0,0,'L',false);
$pdf->SetFont('Times','bu',12);
$pdf->Cell(20,5,''.number_format($neraca->nrc_sum_13, 2),0,0,$alignPrice,false);

$pdf->Ln(10);
$pdf->SetFont('Times','bu',12);
$pdf->Cell(100,5,'LIABILITAS DAN EKUITAS',0,0,'L',false);
$pdf->SetFont('Times','bu',10);
$pdf->Cell(20,5,'',0,0,$alignPrice,false);

$pdf->Ln(5);
$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'LIABILITAS',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(20,5,'',0,0,'C',false);
$pdf->setLeftMargin(35);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Liabilitas Jangka Pendek',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(20,5,'',0,0,'C',false);
$pdf->setLeftMargin(40);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Pihak Berelasi',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(20,5,'',0,0,'C',false);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Pihak Ketiga',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(20,5,'',0,0,'C',false);
$pdf->setLeftMargin(35);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Jumlah Liabilitas Jangka Pendek',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(20,5,'',0,0,'C',false);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Liabilitas Jangka Panjang',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(20,5,'',0,0,'C',false);

$pdf->Ln(5);
$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'JUMLAH LIABILITAS',0,0,'L',false);
$pdf->SetFont('Times','b',10);
$pdf->Cell(15,5,''.number_format($neraca->nrc_sum_2, 2),0,0,$alignPrice,false);
$pdf->setLeftMargin(30);

$pdf->Ln(7);
$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'EKUITAS',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(20,5,'',0,0,'C',false);
$pdf->setLeftMargin(35);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Aktivax',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(20,5,'',0,0,'C',false);

$pdf->Ln(5);
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,'Saldo Laba',0,0,'L',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(20,5,'',0,0,'C',false);

$pdf->Ln(5);
$pdf->SetFont('Times','b',10);
$pdf->Cell(100,5,'JUMLAH EKUITAS',0,0,'L',false);
$pdf->SetFont('Times','b',10);
$pdf->Cell(15,5,''.number_format($neraca->nrc_sum_4, 2),0,0,$alignPrice,false);
$pdf->setLeftMargin(30);

$pdf->Ln(5);
$pdf->SetFont('Times','bu',12);
$pdf->Cell(100,5,'JUMLAH LIABILITAS DAN EKUITAS',0,0,'L',false);
$pdf->SetFont('Times','bu',12);
$pdf->Cell(20,5,''.number_format($neraca->nrc_sum_24, 2),0,0,$alignPrice,false);







$pdf->Ln(5);
$pdf->Ln(10);	
$pdf->SetFont('Times','',10);
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(145);
$pdf->MultiCell(80,5,$company->cpy_city.', '.date('d M Y'),0,'L',false);
//Baris Baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(145);
$pdf->MultiCell(80,5,$company->cpy_fullname.',',0,'L',false);
$pdf->Ln(12);
//Baris baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(145);
$pdf->MultiCell(80,5,$company->cpy_dirut,0,'L',false);
//Cetak PDF
$pdf->Output('('.date('dMY').')-'.$judul.'.pdf','I');
?>