<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Tender'=>'../../admin' ,
        // 'Finances',
        // 'Semua' => array('Semua'=>'admin', 
        'Update',
        $model->id_tender,
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update Tender : #<?=$model->id_tender;?> (<?=$model->client_code;?> : <?=$model->client_name;?>)</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>