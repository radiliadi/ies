<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Tender'=>'admin' ,
        // 'Finances',
        // 'Semua' => array('Semua'=>'admin', 
        // 'Tender'=>array('admin'
        //   ),
        'Create',
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Create Tender </h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>