<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Tender'
        // 'Finances',
        // 'Semua' => array('Semua'=>'admin', 
        // 'Tender'=>array('admin'
        //   )
        ),
    )
);
?>
<div class="row">
  <div class="page-title">                    
        <h2><span class="fa fa-trophy"></span> <?=$title;?> </h2>
    </div>
  <div class="col-md-12">
    <div class="panel panel-default search-form" style="display:none">
      <div class="panel-body">
      </div>
    </div>
  </div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?=$title;?> List</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
              <div class="btn-group pull-right">
                <?php echo CHtml::link('<i class="fa fa-plus" title="Create"></i>', 'create', array('class'=>'btn btn btn-primary pull-right')); ?>
              </div> 
            </div>
            <div class="panel-body">
      <?php echo CHtml::beginForm(array('tender/exportExcel')); ?>
      <?php $this->widget('booster.widgets.TbGridView',array(
      'id'=>'tender-grid',
      // 'type' => ' condensed',
      'dataProvider'=>$model->search(),
      'filter'=>$model,
      'responsiveTable' => true,
      'pager' => array(
          'header' => '',
          'footer' => '',
          'cssFile' => false,
          'selectedPageCssClass' => 'active',
          'hiddenPageCssClass' => 'disabled',
          'firstPageCssClass' => 'previous',
          'lastPageCssClass' => 'next',
          'maxButtonCount' => 5,
          'firstPageLabel'=> '<i class="fa fa-angle-double-left"></i>',
          'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
          'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
          'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
          'htmlOptions' => array(
              'class' => 'dataTables_paginate paging_bootstrap pagination pull-right',
              'align' => 'center',
          )
      ),
      // 'summaryText' => '',
      // 'emptyText'=>'Belum ada thread pada kategori ini',
      'columns'=>array(
        array(
          'header' => 'No.',
          'htmlOptions' => array(
            'width' => '20px',
            'style' => 'text-align:center'
          ),
          'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
          // 'class'=>'booster.widgets.TbTotalSumColumn',
        ),
        array(
          'name' => 'info_source',
          'filter' => Tender::listInfoSource(),
        ),
        array(
          'name' => 'client_code',
          'filter' => Tender::listClientCode(),
        ),
        array(
          'name' => 'client_name',
          'filter' => Tender::listClientName(),
        ),
        array(
          'name' => 'tender_no',
          'filter' => Tender::listTenderNo(),
        ),
        array(
          'name' => 'tender_status',
          'filter' => Tender::listTenderStatus(),
        ),
        array(
          'name' => 'tender_type',
          'filter' => Tender::listTenderType(),
        ),
        'detail_task',
        'ref_address',
        array(
          'name'=>'deadline',
          'type'=>'raw',
          'filter'=>false,
          'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
              'model' => $model, 
              'attribute' => 'deadline', 
              'language' => 'en-GB',
              'htmlOptions' => array(
                  'id' => 'trx_date',
                  'size' => '10',
                  'class' => 'form-control startClass',
                  'style' =>'width:140px;',
                  // 'value' => date('Y-m-d')
              ),
                 'options'=>array(
         // 'showAnim'=>'fade',
         'dateFormat'=>'yy-mm-dd',
        ),
              'defaultOptions' => array(
                  'showOn' => 'focus', 
                  'language' => 'en-GB',
                  'dateFormat' => 'Y-m-d',
                  'showOtherMonths' => true,
                  'selectOtherMonths' => true,
                  'changeMonth' => true,
                  'changeYear' => true,
                  'showButtonPanel' => true,
                  // 'style' =>'width:100px;',
              )
          ), 
                                      true),
        ),
        'bid_bond',
        array(
        'class'=>'booster.widgets.TbButtonColumn',
        'template'=>'{update}',
        ),
        array(
        'class'=>'booster.widgets.TbButtonColumn',
        'afterDelete'=>'function(link,success,data){ if(success) alert("Delete completed successfully"); }',
        'template'=>'{delete}',
        ),
      ),
      )); 
            ?>
            <br></br>
            <hr></hr>
            <div class="col-md-6">
                <div class="col-md-3">
                <select name="fileType" class="form-control">
                    <option value="Excel">EXCEL 5 (xls)</option>
                    <!-- <option value="CSV">CSV</option> -->
                </select>
                </div>
                <div class="col-md-3">
                <?php echo CHtml::submitButton('Export',array(
                            'buttonType' => 'submit',
                            'context'=>'primary',
                            'label'=>'Export Excel',
                            'class'=>'btn btn-primary',
                        )); 
                ?>
                <?php echo CHtml::endForm(); ?>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>