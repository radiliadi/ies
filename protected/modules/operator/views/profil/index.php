<?php

/* @var $this SiteController */

//level
$level=Yii::app()->session->get('level');

//cek level dan redirect sesuai level

if(Yii::app()->user->isGuest)
{
	$this->redirect('index.php?r=site/login');
}else{
	switch ($level){
		case 'admin':	
		$this->redirect('../admin/beranda/');
		break;
		case 'ver':
		$this->redirect('../admin/beranda/');
		break;
		case 'pa':	
		$this->redirect('../admin/beranda/');
		break;
		case 'bendah':	
		$this->redirect('../admin/beranda/');
		break;
		case 'view':	
		$this->redirect('../admin/beranda/');
		break;
		default:
		$this->redirect('../');
	}
}

$this->pageTitle=Yii::app()->name;
?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>Congratulations! You have successfully created your Yii application.</p>

<p>You may change the content of this page by modifying the following two files:</p>
<ul>
	<li>View file: <code><?php echo __FILE__; ?></code></li>
	<li>Layout file: <code><?php echo $this->getLayoutFile('main'); ?></code></li>
</ul>

<p>For more details on how to further develop this application, please read
the <a href="http://www.yiiframework.com/doc/">documentation</a>.
Feel free to ask in the <a href="http://www.yiiframework.com/forum/">forum</a>,
should you have any questions.</p>
