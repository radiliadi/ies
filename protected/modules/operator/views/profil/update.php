<?php 
$sId=Yii::app()->session->get('id');
$id_pd=Yii::app()->session->get('username');
if($id_pd !== $model->username && $sId !== $model->user_id)
{
throw new CHttpException('403', 'Access denied!');
}

$this->widget('booster.widgets.TbBreadcrumbs',
array(
    'links' => array('Account',
    $model->username,
    // 'Lalai'=>array('asd'
      // )
    ),
)
);
?>
<div class="panel-heading">
                                <h3> <span class="fa fa-plus"></span> Update User : #<?=$model->user_id;?> (<?=$model->username?>)</h3>
                    </div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>