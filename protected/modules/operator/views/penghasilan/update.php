<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Settings' ,
        'Employee',
        // 'Semua' => array('Semua'=>'admin', 
        'Income'=>array('admin'
          ),
        'Update',
        $model->id_penghasilan,
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Update Income : #<?=$model->id_penghasilan;?></h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>