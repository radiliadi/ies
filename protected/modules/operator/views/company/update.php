<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Settings' ,
        'Company',
        ),
    )
);
?>
<div class="panel-heading">
    <h3> <span class="fa fa-edit"></span> Update Company </h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>