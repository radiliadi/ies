<?php
/* @var $this InventorySusutController */
/* @var $model InventorySusut */

$this->breadcrumbs=array(
	'Inventory Susuts'=>array('index'),
	$model->invs_id,
);

$this->menu=array(
	array('label'=>'List InventorySusut', 'url'=>array('index')),
	array('label'=>'Create InventorySusut', 'url'=>array('create')),
	array('label'=>'Update InventorySusut', 'url'=>array('update', 'id'=>$model->invs_id)),
	array('label'=>'Delete InventorySusut', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->invs_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InventorySusut', 'url'=>array('admin')),
);
?>

<h1>View InventorySusut #<?php echo $model->invs_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'invs_id',
		'invs_invd_id',
		'invs_inv_id',
		'invs_date',
		'invs_penyusutan',
		'invs_nilai_buku',
		'invs_datetime_insert',
		'invs_datetime_update',
		'invs_datetime_delete',
		'invs_insert_by',
		'invs_update_by',
		'invs_delete_by',
		'invs_status',
	),
)); ?>
