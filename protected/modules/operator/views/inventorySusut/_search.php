<?php
/* @var $this InventorySusutController */
/* @var $model InventorySusut */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'invs_id'); ?>
		<?php echo $form->textField($model,'invs_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'invs_invd_id'); ?>
		<?php echo $form->textField($model,'invs_invd_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'invs_inv_id'); ?>
		<?php echo $form->textField($model,'invs_inv_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'invs_date'); ?>
		<?php echo $form->textField($model,'invs_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'invs_penyusutan'); ?>
		<?php echo $form->textField($model,'invs_penyusutan',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'invs_nilai_buku'); ?>
		<?php echo $form->textField($model,'invs_nilai_buku',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'invs_datetime_insert'); ?>
		<?php echo $form->textField($model,'invs_datetime_insert'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'invs_datetime_update'); ?>
		<?php echo $form->textField($model,'invs_datetime_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'invs_datetime_delete'); ?>
		<?php echo $form->textField($model,'invs_datetime_delete'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'invs_insert_by'); ?>
		<?php echo $form->textField($model,'invs_insert_by',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'invs_update_by'); ?>
		<?php echo $form->textField($model,'invs_update_by',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'invs_delete_by'); ?>
		<?php echo $form->textField($model,'invs_delete_by',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'invs_status'); ?>
		<?php echo $form->textField($model,'invs_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->