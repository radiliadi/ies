<?php
/* @var $this InventorySusutController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Inventory Susuts',
);

$this->menu=array(
	array('label'=>'Create InventorySusut', 'url'=>array('create')),
	array('label'=>'Manage InventorySusut', 'url'=>array('admin')),
);
?>

<h1>Inventory Susuts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
