<?php $this->widget('booster.widgets.TbBreadcrumbs',
	array('links' => array('Accounting'=>'../../../accounting/index',
	    'Inventory' => '../../../inventory/admin',
	   	'Susut',
	   	'Create',
	    ),
	)
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-pencil"></span> Create Inventory Susut</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>