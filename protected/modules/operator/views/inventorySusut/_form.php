<?php
$id_pd=Yii::app()->session->get('username');
?>
<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
'id'=>'inventorySusut-form',
// 'enableAjaxValidation'=>true,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-md-1">
    </div>
        <div class="col-md-10">
            <form class="form-horizontal">
                <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Date</label>
                                <div class="col-md-6 col-xs-12">                                          
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <?php   $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                'name'=>'InventorySusut[invs_date]',
                                                                'value'=> $model->invs_date,
                                                                'options'=>array(
                                                                    'showAnim'=>'fold',
                                                                    'dateFormat'=>'yy-mm-dd',
                                                                ),
                                                                'htmlOptions' => array(
                                                                    'class' => 'form-control',
                                                                    'placeholder'=>'Date',
                                                                    // 'readonly' => 'readonly',
                                                                ),
                                                            ));
                                        ?>
                                    </div>                                
                                </div>
                            </div>
                        	<div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Penyusutan</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'invs_penyusutan',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Penyusutan"));?>
                                        <?php echo $form->error($model,'invs_penyusutan'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Nilai Buku</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'invs_nilai_buku',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Nilai Buku"));?>
                                        <?php echo $form->error($model,'invs_nilai_buku'); ?>

                                    </div>                                            
                                </div>
                            </div>
                        </div>
                            <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
    									'buttonType'=>'submit',
    									'context'=>'primary',
    									'label'=>$model->isNewRecord ? 'Create' : 'Save',
    								)); ?>
                                </div>
                            </div>
                </div>
        	</form>
    	</div>
    <div class="col-md-1">  
    </div>
</div> 
<?php $this->endWidget(); ?>