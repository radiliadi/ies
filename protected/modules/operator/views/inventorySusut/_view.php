<?php
/* @var $this InventorySusutController */
/* @var $data InventorySusut */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('invs_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->invs_id), array('view', 'id'=>$data->invs_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invs_invd_id')); ?>:</b>
	<?php echo CHtml::encode($data->invs_invd_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invs_inv_id')); ?>:</b>
	<?php echo CHtml::encode($data->invs_inv_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invs_date')); ?>:</b>
	<?php echo CHtml::encode($data->invs_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invs_penyusutan')); ?>:</b>
	<?php echo CHtml::encode($data->invs_penyusutan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invs_nilai_buku')); ?>:</b>
	<?php echo CHtml::encode($data->invs_nilai_buku); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invs_datetime_insert')); ?>:</b>
	<?php echo CHtml::encode($data->invs_datetime_insert); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('invs_datetime_update')); ?>:</b>
	<?php echo CHtml::encode($data->invs_datetime_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invs_datetime_delete')); ?>:</b>
	<?php echo CHtml::encode($data->invs_datetime_delete); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invs_insert_by')); ?>:</b>
	<?php echo CHtml::encode($data->invs_insert_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invs_update_by')); ?>:</b>
	<?php echo CHtml::encode($data->invs_update_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invs_delete_by')); ?>:</b>
	<?php echo CHtml::encode($data->invs_delete_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invs_status')); ?>:</b>
	<?php echo CHtml::encode($data->invs_status); ?>
	<br />

	*/ ?>

</div>