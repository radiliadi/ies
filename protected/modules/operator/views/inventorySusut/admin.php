<?php
/* @var $this InventorySusutController */
/* @var $model InventorySusut */

$this->breadcrumbs=array(
	'Inventory Susuts'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List InventorySusut', 'url'=>array('index')),
	array('label'=>'Create InventorySusut', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inventory-susut-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Inventory Susuts</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'inventory-susut-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'invs_id',
		'invs_invd_id',
		'invs_inv_id',
		'invs_date',
		'invs_penyusutan',
		'invs_nilai_buku',
		/*
		'invs_datetime_insert',
		'invs_datetime_update',
		'invs_datetime_delete',
		'invs_insert_by',
		'invs_update_by',
		'invs_delete_by',
		'invs_status',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
