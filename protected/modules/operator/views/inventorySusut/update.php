<?php $this->widget('booster.widgets.TbBreadcrumbs',
	array('links' => array('Accounting' => Yii::app()->urlManager->createUrl('operator/accounting/index'),
	    'Inventory' => '../../../inventory/admin',
	    'Detail' => '../../../inventoryDetail/admin/id/'.$invDetail->invs_invd_id,
	   	'Susut (#'.$model->invs_id.')',
	    ),
	)
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-plus"></span> Update Inventory Susut : #<?=$model->invs_id;?> (<?=$model->invs_date?>)</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>