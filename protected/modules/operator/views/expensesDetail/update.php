<?php $this->widget('booster.widgets.TbBreadcrumbs',
	array('links' => array('Accounting'=>'../../../accounting/index',
	    'Expenses' => '../../../expenses/admin',
	    'Detail' => '../../admin/id/'.$model->expd_id,
	   	$model->expd_id,
	    ),
	)
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-plus"></span> Update Expenses Detail : #<?=$model->expd_id;?> (<?=$model->expd_date?>)</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>