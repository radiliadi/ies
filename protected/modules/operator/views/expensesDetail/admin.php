<?php
// $id_pd=Yii::app()->session->get('username');
// if($id_pd !== $expenses->exp_id_pd)
// {
// throw new CHttpException('403', 'Access denied!');
// }
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Expenses' => '../../../expenses/admin' ,
        'Detail',
        $expenses->exp_id,
      ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-laptop"></span> Expenses Detail - <small> <?=$expenses->exp_desc ;?> </small> </h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Expenses Detail</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
            	<div class="btn-group pull-right">
            		<?php 
            		if($expenses->exp_submit_status == 1 && $expenses->exp_is_verified == 0){
            			echo '';
            		}else{
            			echo CHtml::link('<i class="fa fa-plus"></i>', '../../create/id/'.$expenses->exp_id, array('class'=>'btn btn btn-primary pull-right')); 
            		}
            		

            		?>
                </div> 
            </div>
            <div class="panel-body">
				<div class="form"> 
				<?php

				$form=$this->beginWidget('CActiveForm', array(
					'id'=>'expensesDeatil-form',
					'enableAjaxValidation'=>false,
					'htmlOptions'=>array(
						'enctype'=>'multipart/form-data',
						'role'=>'form',
					),
				));
				?>
	
					<div class="row">
					    <div class="box col-md-12 ">    
							<div class="box-content">
								<h1></h1>
								<table>
									<tbody>
										<tr>
											<td><b>No. Expenses</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $expenses->exp_id;?></td>
										</tr>
										<tr>
											<td><b>NIP</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $expenses->exp_id_pd;?></td>
										</tr>
										<tr>
											<td><b>Date Created</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $expenses->exp_datetime_insert;?></td>
										</tr>
										<tr>
											<td><b>Date Submit</b></td>
											<td class="center">:&nbsp; &nbsp;<?php echo $expenses->exp_date_submit;?></td>
										</tr>
									</tbody>
								</table>	
								<br>
								</br>
								
								<div class="panel-body">	  
									<div class="table-responsive">
										<!-- START DEFAULT DATATABLE -->
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Date</th>
                                                <th>Description</th>
                                                <th>Debet <small>(cash advance)</small></th>
                                                <th>Credit <small>(expenses)</small></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        	$no=1;
                                        	$sumDebet=0;
											$sumCredit=0;
											$expdUpdate = '';
											$expdDelete = '';

                                        	foreach($expensesDetail as $expD){
	                                    		$sumDebet=$sumDebet+$expD->expd_debet;
												$sumCredit=$sumCredit+$expD->expd_credit;

												if($expenses->exp_is_verified == 2){
													$expdUpdate = '<a class="btn btn-info" href="../../update/id/'.$expD->expd_id.'" title="Update"><i class="fa fa-pencil"></i>
														</a>';
													$expdDelete = '<a class="btn btn-danger" href="../../delete/id/'.$expD->expd_id.'" title="Delete"><i class="fa fa-trash-o"></i>
														</a>';
												}else if($expenses->exp_is_approved == 2){
													$expdUpdate = '<a class="btn btn-info" href="../../update/id/'.$expD->expd_id.'" title="Update"><i class="fa fa-pencil"></i>
														</a>';
												}else if($expenses->exp_submit_status == 0 && $expenses->exp_is_verified == 0 && $expenses->exp_is_approved == 0){
													$expdUpdate = '<a class="btn btn-info" href="../../update/id/'.$expD->expd_id.'" title="Update"><i class="fa fa-pencil"></i>
														</a>';
													$expdDelete = '<a class="btn btn-danger" href="../../delete/id/'.$expD->expd_id.'" title="Delete"><i class="fa fa-trash-o"></i>
														</a>';
												}else{
													$expdUpdate = '';
													$expdDelete = '';
												}


	                                        	echo'
	                                            <tr>
	                                                <td>'.$no.'</td>
	                                                <td>'.$expD->expd_date.'</td>
	                                                <td>'.$expD->expd_desc.'</td>
	                                                <td>Rp. '.number_format($expD->expd_debet).'</td>
	                                                <td>Rp. '.number_format($expD->expd_credit).'</td>
	                                                <td>'.$expdUpdate.'
														&nbsp;
														'.$expdDelete.'
													</td>
	                                            </tr>'
	                                            ;
	                                            $no++;
	                                           }
                                          ?>
                                        </tbody>
                                        <?php
                                        	$showTot = 0;
                                        	$dueCompany = 0;
                                        	$dueEmployee = 0;
                                        	if($sumDebet > $sumCredit){
                                        		$dueCompany = $sumDebet - $sumCredit;
                                        		$showTot = $dueCompany;
                                        	}else if($sumDebet < $sumCredit){
                                        		$dueEmployee = $sumCredit - $sumDebet;
                                        		$showTot = $dueEmployee;
                                        	}

                                            echo'
											<tr>
												<td></td>
												<td><b>TOTAL</b></td>
												<td class="center"></td>
												<td class="center"><b>Rp. '.number_format($sumDebet).'</b></td>
												<td class="center"><b>Rp. '.number_format($sumCredit).'</b></td>
												<td></td>
												
											</tr>
											';

											echo'
											<tr>
												<td></td>
												<td><b>Due to Company</b></td>
												<td class="center"></td>
												<td class="center"><b>Rp. '.number_format($dueCompany).'</b></td>
												<td class="center"><b></b></td>
												<td></td>
												
											</tr>
											';

											echo'
											<tr>
												<td></td>
												<td><b>Due to Employee</b></td>
												<td class="center"></td>
												<td class="center"><b></b></td>
												<td class="center"><b>Rp. '.number_format($dueEmployee).'</b></td>
												<td></td>
												
											</tr>
											';
											if($expenses->exp_submit_status == 0 && $expenses->exp_is_verified == 0){
												echo'
												<tr>
													<td></td>
													<td></td>
													<td class="center"><b></b></td>
													<td class="center"></td>
													<td></td>
													<td>
														<a class="btn btn-primary" href="../../../../operator/expenses/submit/id/'.$expenses->exp_id.'" title="Submit">Submit
														</a>
													</td>
												</tr>
												';
											}else if($expenses->exp_submit_status == 1 && $expenses->exp_is_verified == 0){
												echo'
												<tr>
													<td></td>
													<td></td>
													<td class="center"><b></b></td>
													<td class="center"></td>
													<td></td>
													<td>
														<a class="btn btn-success" title="Waiting"><i class="glyphicon glyphicon-time"></i>Waiting...
														</a>
													</td>
												</tr>
												';
											}else if($expenses->exp_submit_status == 1 && $expenses->exp_is_verified !== 0){
												echo'
												<tr>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
												';
											}
                                        ?>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
										</div>
									</div>
								</div>
								</div>	
							</div>
						</div>
					<?php $this->endWidget(); ?>
					</div>
                <br>
                </br>
            </div>
            </div>
        </div>
    </div>
</div>