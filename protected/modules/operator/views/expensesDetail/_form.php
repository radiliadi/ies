<?php
$id_pd=Yii::app()->session->get('username');
?>
<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
'id'=>'expensesDetail-form',
// 'enableAjaxValidation'=>true,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-md-1">
    </div>
        <div class="col-md-10">
            <form class="form-horizontal">
                <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Expenses Detail Date<a style="color:red;"><blink>*</blink></a></label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <?php   $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                'name'=>'ExpensesDetail[expd_date]',
                                                                'value'=> $model->expd_date,
                                                                'options'=>array(
                                                                    'showAnim'=>'fold',
                                                                    'dateFormat'=>'yy-mm-dd',
                                                                ),
                                                                'htmlOptions' => array(
                                                                    'class' => 'form-control',
                                                                    'placeholder'=>'Expenses Detail Date'
                                                                ),
                                                            ));
                                        ?>
                                    </div>  
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'expd_desc',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Description"));?>
                                        <?php echo $form->error($model,'expd_desc'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Debet</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'expd_debet',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Debet"));?>
                                        <?php echo $form->error($model,'expd_debet'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Credit</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'expd_credit',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Credit"));?>
                                        <?php echo $form->error($model,'expd_credit'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <?php echo $form->hiddenField($model,'expd_id_pd',array('value'=>$id_pd)); ?>
                            <?php echo $form->hiddenField($model,'expd_datetime_insert',array('value'=>date('Y-m-d H:i:s'))); ?>
                        </div>
                            <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
    									'buttonType'=>'submit',
    									'context'=>'primary',
    									'label'=>$model->isNewRecord ? 'Create' : 'Save',
    								)); ?>
                                </div>
                            </div>
                </div>
        </form>
    </div>
    <div class="col-md-1">  
    </div>
</div> 
<?php $this->endWidget(); ?>