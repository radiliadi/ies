<?php
/* @var $this ChartOfAccountRuleController */
/* @var $model ChartOfAccountRule */

$this->breadcrumbs=array(
	'Chart Of Account Rules'=>array('index'),
	$model->coar_id=>array('view','id'=>$model->coar_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ChartOfAccountRule', 'url'=>array('index')),
	array('label'=>'Create ChartOfAccountRule', 'url'=>array('create')),
	array('label'=>'View ChartOfAccountRule', 'url'=>array('view', 'id'=>$model->coar_id)),
	array('label'=>'Manage ChartOfAccountRule', 'url'=>array('admin')),
);
?>

<h1>Update ChartOfAccountRule <?php echo $model->coar_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>