<?php
/* @var $this ChartOfAccountRuleController */
/* @var $model ChartOfAccountRule */

$this->breadcrumbs=array(
	'Chart Of Account Rules'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ChartOfAccountRule', 'url'=>array('index')),
	array('label'=>'Manage ChartOfAccountRule', 'url'=>array('admin')),
);
?>

<h1>Create ChartOfAccountRule</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>