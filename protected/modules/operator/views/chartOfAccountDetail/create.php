<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting' => Yii::app()->urlManager->createUrl('operator/accounting/index') ,
        $coaType->coat_desc.' ('.strtoupper($coaMenu->coa_desc).')' => '../../../../menu/id/'.$coaMenu->coa_id.'/type/'.$coaType->coat_id,
        'Create',
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-plus"></span> Create Chart Of Account Detail </h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>