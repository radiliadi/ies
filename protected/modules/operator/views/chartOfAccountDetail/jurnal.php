<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting' => '../../../../../accounting/index',
            'Jurnal',
            $coaType->coat_desc,
            $coaMenu->coa_id.' : '.strtoupper($coaMenu->coa_desc),
        ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2><center><?=$title;?></center></h2>
                <h3><center><?=$coaMenu->coa_id ;?> : <?=strtoupper($coaMenu->coa_desc) ;?></h3></center>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
            	<div class="btn-group pull-right">
                    <?php 
                        echo CHtml::link('<i class="glyphicon glyphicon-list"></i> COA Detail', '../../../../menu/id/'.$coaMenu->coa_id.'/type/'.$coaMenu->coa_coat_id.'', array('class'=>'btn btn btn-primary pull-right')); 
                    ?>
                </div> 
                <div class="btn-group pull-left">
                    
                </div> 
            </div>
            <div class="panel-body">      
                <div class="table-responsive">
                <!-- START DEFAULT DATATABLE -->
                <table class="table datatable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th></th>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Debet</th>
                        <th>Credit</th>
                        <!-- <th>Saldo</th> -->
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $no=1;
                    $totSaldo=0;
                    $totDebet=0;
                    $totCredit=0;
                    $saldoEnd=0;
                    $count = count($coaDetail);
                    foreach($coaDetail as $key => $value){

                        $totSaldo   = $totSaldo+$value->coad_saldo;
                        $totDebet   = $totDebet+$value->coad_debet;
                        $totCredit  = $totCredit+$value->coad_credit;
                        $saldoEnd   = $coaDetail[($count+$key+1) % $count];
                        $previous   = $coaDetail[($count+$key+1) % $count];
                        $check = '';
                        $update = '';
                        if($value->coad_is_submit == 0){
                            if($value->coad_is_saldo == 0){
                                $check = '<a class="btn btn-success btn-xs" href="../../../../checkSaldo/id/'.$value->coad_id.'/coaId/'.$value->coad_coa_id.'/prevId/'.$previous->coad_id.'" title="Check"><i class="fa fa-check"></i>
                                    </a>';
                                $update = '<a class="btn btn-primary btn-xs" href="../../../../update/id/'.$value->coad_id.'/coaId/'.$value->coad_coa_id.'/type/'.$value->coad_coat_id.'" title="Check"><i class="fa fa-pencil"></i>
                                </a>';
                                
                            }else if($value->coad_is_saldo == 1){
                                $check = '<a class="btn btn-default btn-xs" href="../../../../checkSaldo/id/'.$value->coad_id.'/coaId/'.$value->coad_coa_id.'/prevId/'.$previous->coad_id.'" title="Check"><i class="fa fa-check"></i>
                                    </a>';
                                $update = '<a class="btn btn-primary btn-xs" href="../../../../update/id/'.$value->coad_id.'/coaId/'.$value->coad_coa_id.'/type/'.$value->coad_coat_id.'" title="Check"><i class="fa fa-pencil"></i>
                                </a>';
                            }
                        }else if($value->coad_is_submit == 1){
                            $check = '';
                            $update = '';
                        }

                        
                        

                        if($value->coad_is_submit == 0 && $value->coad_is_saldo > 0){
                            // $submit = '<a class="btn btn-success btn-xs" href="../../../../submit/id/'.$value->coad_id.'" title="Submit to BB" confirm="Are you sure to delete this Expenses?"><i class="glyphicon glyphicon-save"></i>
                            //     </a>';
                            $submit = CHtml::link('<i class="fa fa-download"></i>',['/operator/chartOfAccountDetail/unsubmitJurnal/id/'.$value->coad_id],['class' => 'btn btn-warning btn-xs', 'title' => 'Hilangkan dari Jurnal?', 'confirm' => 'Hilangkan "'.$value->coad_desc.'" dari Jurnal?']);
                        }else{
                            $submit = '';
                        }

                        if(empty($value->coad_color)){
                            $warna = " ";
                        }else{
                            $warna = $value->coad_color;
                        }

                        if($value->coad_debet == 0){
                            $debet = "-";
                        }else{
                            $debet = 'Rp. '.number_format($value->coad_debet);
                        }

                        if($value->coad_credit == 0){
                            $credit = "-";
                        }else{
                            $credit = 'Rp. '.number_format($value->coad_credit);
                        }

                        if($value->coad_color == " " || empty($value->coad_color)){
                            $changeColor = "";
                            // $changeColor = '<div class="btn-group">
                            // <a href="#" data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle"><i class="fa fa-circle"></i><span class="caret"></span></a>
                            // <ul class="dropdown-menu" role="menu">
                            //     <li><a class="btn btn-primary btn-xs" href="../../../../changeColor/id/'.$value->coad_id.'" title="White"><i class="fa fa-circle"></i></a></li>                                                    
                            // </ul>
                            // </div>';
                            // $changeColor = '<a class="btn btn-primary btn-xs" href="../../../../changeColor/id/'.$value->coad_id.'" title="White"><i class="fa fa-circle"></i>
                            //     </a>';
                        }else{
                            $changeColor = '<a class="btn btn-primary btn-xs" href="../../../../changeColor/id/'.$value->coad_id.'" title="White"><i class="fa fa-circle"></i>
                                </a>';
                        }
                        

                        echo'
                        <tr style="background-color:#'.$warna.'">
                            <td>'.$no.'</td>
                            <td>'.$changeColor.'</td>
                            <td>'.$value->coad_datetime.'</td>
                            <td>'.$value->coad_desc.'</td>
                            <td>'.$debet.'</td>
                            <td>'.$credit.'</td>
                            <td>'.$submit.'
                            </td>
                        </tr>'
                        ;
                        $no++;
                       }
                  ?>
                </tbody>
                <?php

                    echo'
                    <tr>
                        <td> </td>
                        <td> </td>
                        <td> Debet Akhir</td>
                        <td class="center"> </td>
                        <td class="center"> <b>Rp. '.number_format($totDebet).'</b> </td>
                        <td class="center"> </td>
                        <td class="center"> </td>
                    </tr>
                    ';

                     echo'
                    <tr>
                        <td> </td>
                        <td> </td>
                        <td> Credit Akhir</td>
                        <td class="center"> </td>
                        <td class="center"> </td>
                        <td class="center"> <b>Rp. '.number_format($totCredit).'</b> </td>
                        <td class="center"> </td>
                    </tr>
                    ';

                    // echo'
                    // <tr>
                    //     <td></td>
                    //     <td></td>
                    //     <td><b>Saldo Akhir</b></td>
                    //     <td class="center"></td>
                    //     <td class="center"></td>
                    //     <td class="center"></td>
                    //     <td class="center"><b>Rp. '.number_format($saldoEnd->coad_saldo).'</b></td>
                    //     <td class="center"></td>
                    // </tr>
                    // ';
                    if($totDebet == $totCredit){
                        $msgColor = 'green';
                        $msgLedger = 'You can submit to Ledger!';
                        // $submitLedger = '<a class="btn btn-success" href="'.Yii::app()->urlManager->createUrl('operator/chartOfAccountDetail/submit/rule/'.$coaRule->coar_id.'/type/'.$coaType->coat_id.'/coaId/'.$coaMenu->coa_id).'" title="Submit to Ledger">Okay</a>';
                        $submitLedger = CHtml::link('<i class="glyphicon glyphicon-book"></i> Ledger',[Yii::app()->urlManager->createUrl('operator/chartOfAccountDetail/submit/rule/'.$coaRule->coar_id.'/type/'.$coaType->coat_id.'/coaId/'.$coaMenu->coa_id)],['class' => 'btn btn-success', 'title' => 'Submit to Ledger', 'confirm' => 'Submit to Ledger?']);
                    }else{
                        $msgColor = 'red';
                        $msgLedger = 'Make sure your debet and credit are equal';
                        $submitLedger = '<a2 class="btn btn-danger" title="Cant submit to Ledger">Sorry</a2>';
                    }
                    echo'
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="center"></td>
                        <td><font color="'.$msgColor.'"><h4><b>'.$msgLedger.'</b></font></h4></td>
                        <td class="center"></td>
                        <td class="center">
                            '.$submitLedger.'
                        </td>
                        <td class="center"></td>
                    </tr>
                    ';
                ?>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
            // $('a1').attr("onclick","noty({text: 'Congratulations! Your jurnal has been submitted!', layout: 'topRight', type: 'success'});");
            $('a2').attr("onclick","noty({text: 'Oops! You cant submit this jurnal', layout: 'topRight', type: 'error'});");
        });
</script>
<!-- <script>
    $(document).on('click', '.btn',function() {
        // var _this  = $(this);
        // var url    = <?php //Yii::app()->urlManager->createUrl('operator/chartOfAccountDetail/submit/rule/'.$coaRule->coar_id.'/type/'.$coaType->coat_id.'/coaId/'.$coaMenu->coa_id)?>
        // if($('a1').click()){
            $('a1').attr("onclick","noty({text: 'Congratulations! Your jurnal has been submitted!', layout: 'topRight', type: 'success'});");
            // $('a1').attr("onclick",null);
            // console.log(url);
        // }
        // if($('a2').click()){
            $('a2').attr("onclick","noty({text: 'Oops! You cant submit this jurnal', layout: 'topRight', type: 'error'});");
        // }
    });
</script> -->