<?php
/* @var $this ChartOfAccountDetailController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Chart Of Account Details',
);

$this->menu=array(
	array('label'=>'Create ChartOfAccountDetail', 'url'=>array('create')),
	array('label'=>'Manage ChartOfAccountDetail', 'url'=>array('admin')),
);
?>

<h1>Chart Of Account Details</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
