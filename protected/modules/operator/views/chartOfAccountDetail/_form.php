<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
'id'=>'chart-of-account-detail-form',
// 'enableAjaxValidation'=>true,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-md-1">
    </div>
        <div class="col-md-10">
            <form class="form-horizontal">
                <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                        	<div class="form-group">
                                <label class="col-md-4 control-label">Date<a style="color:red;"><blink>*</blink></a></label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <?php   $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                'name'=>'ChartOfAccountDetail[coad_datetime]',
                                                                'value'=> $model->coad_datetime,
                                                                'options'=>array(
                                                                    'showAnim'=>'fold',
                                                                    'dateFormat'=>'yy-mm-dd',
                                                                ),
                                                                'htmlOptions' => array(
                                                                    'class' => 'form-control',
                                                                    'placeholder'=>'Date'
                                                                ),
                                                            ));
                                        ?>
                                    </div>  
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textArea($model,'coad_desc',array('class'=>"form-control",'placeholder'=>"Description"));?>
                                        <?php echo $form->error($model,'coad_desc'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Debet</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'coad_debet',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Debet"));?>
                                        <?php echo $form->error($model,'coad_debet'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Credit</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'coad_credit',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Credit"));?>
                                        <?php echo $form->error($model,'coad_credit'); ?>

                                    </div>                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Color</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <?php echo $form->dropDownList($model,'coad_color',ChartOfAccountDetail::listColor(),array('empty'=>'-Choose Color-','class'=>"form-control select", ' '=>' ')); ?>
                                    <?php echo $form->error($model,'coad_color'); ?>                                       
                                </div>
                            </div>
                        </div>
                            <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
    									'buttonType'=>'submit',
    									'context'=>'primary',
    									'label'=>$model->isNewRecord ? 'Create' : 'Save',
    								)); ?>
                                </div>
                            </div>
                </div>
        </form>
    </div>
    <div class="col-md-1">  
    </div>
</div> 
<?php $this->endWidget(); ?>