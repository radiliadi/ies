<?php
/* @var $this ChartOfAccountDetailController */
/* @var $model ChartOfAccountDetail */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'chart-of-account-detail-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'coad_coa_id'); ?>
		<?php echo $form->textField($model,'coad_coa_id'); ?>
		<?php echo $form->error($model,'coad_coa_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coad_coar_id'); ?>
		<?php echo $form->textField($model,'coad_coar_id'); ?>
		<?php echo $form->error($model,'coad_coar_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coad_id_pd'); ?>
		<?php echo $form->textField($model,'coad_id_pd',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'coad_id_pd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coad_desc'); ?>
		<?php echo $form->textArea($model,'coad_desc',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'coad_desc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coad_debet'); ?>
		<?php echo $form->textField($model,'coad_debet',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'coad_debet'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coad_credit'); ?>
		<?php echo $form->textField($model,'coad_credit',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'coad_credit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coad_datetime'); ?>
		<?php echo $form->textField($model,'coad_datetime'); ?>
		<?php echo $form->error($model,'coad_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coad_datetime_insert'); ?>
		<?php echo $form->textField($model,'coad_datetime_insert'); ?>
		<?php echo $form->error($model,'coad_datetime_insert'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coad_datetime_update'); ?>
		<?php echo $form->textField($model,'coad_datetime_update'); ?>
		<?php echo $form->error($model,'coad_datetime_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coad_is_submit'); ?>
		<?php echo $form->textField($model,'coad_is_submit'); ?>
		<?php echo $form->error($model,'coad_is_submit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coad_status'); ?>
		<?php echo $form->textField($model,'coad_status'); ?>
		<?php echo $form->error($model,'coad_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->