<?php
/* @var $this ChartOfAccountDetailController */
/* @var $model ChartOfAccountDetail */

$this->breadcrumbs=array(
	'Chart Of Account Details'=>array('index'),
	$model->coad_id=>array('view','id'=>$model->coad_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ChartOfAccountDetail', 'url'=>array('index')),
	array('label'=>'Create ChartOfAccountDetail', 'url'=>array('create')),
	array('label'=>'View ChartOfAccountDetail', 'url'=>array('view', 'id'=>$model->coad_id)),
	array('label'=>'Manage ChartOfAccountDetail', 'url'=>array('admin')),
);
?>

<h1>Update ChartOfAccountDetail <?php echo $model->coad_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>