<?php
/* @var $this ChartOfAccountDetailController */
/* @var $model ChartOfAccountDetail */

$this->breadcrumbs=array(
	'Chart Of Account Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ChartOfAccountDetail', 'url'=>array('index')),
	array('label'=>'Manage ChartOfAccountDetail', 'url'=>array('admin')),
);
?>

<h1>Create ChartOfAccountDetail</h1>

<?php $this->renderPartial('_formadd', array('model'=>$model)); ?>