<?php
/* @var $this ChartOfAccountDetailController */
/* @var $data ChartOfAccountDetail */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('coad_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->coad_id), array('view', 'id'=>$data->coad_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coad_coa_id')); ?>:</b>
	<?php echo CHtml::encode($data->coad_coa_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coad_coar_id')); ?>:</b>
	<?php echo CHtml::encode($data->coad_coar_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coad_id_pd')); ?>:</b>
	<?php echo CHtml::encode($data->coad_id_pd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coad_desc')); ?>:</b>
	<?php echo CHtml::encode($data->coad_desc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coad_debet')); ?>:</b>
	<?php echo CHtml::encode($data->coad_debet); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coad_credit')); ?>:</b>
	<?php echo CHtml::encode($data->coad_credit); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('coad_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->coad_datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coad_datetime_insert')); ?>:</b>
	<?php echo CHtml::encode($data->coad_datetime_insert); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coad_datetime_update')); ?>:</b>
	<?php echo CHtml::encode($data->coad_datetime_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coad_is_submit')); ?>:</b>
	<?php echo CHtml::encode($data->coad_is_submit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coad_status')); ?>:</b>
	<?php echo CHtml::encode($data->coad_status); ?>
	<br />

	*/ ?>

</div>