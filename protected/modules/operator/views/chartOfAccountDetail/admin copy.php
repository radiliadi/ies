<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting' => '../accounting/index',
        'Buku Besar',
        // 'Active'=>array('active', 
        //     )
        ),
    )
);
?>
<div class="form" id="bukuBesar"> 
<?php

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'chartOfAccountDetail-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data',
		'role'=>'form',
	),
));

$level=Yii::app()->session->get('level');
?>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-laptop"></span> Buku Besar </h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Buku Besar</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
            	<div class="btn-group pull-right">
                </div> 
                <div class="btn-group pull-left">
                    
                </div> 
            </div>
            
            <div class="panel-body">
            	<table>
					<tbody>
						<tr>
							<td><b>Tahun</b></td>
							<td>:&nbsp; &nbsp; <?php echo $tahun?> &nbsp;</td>
						</tr>
						<tr>
							<td><b>Bulan</b></td>
							<td>:&nbsp; &nbsp; <?php echo $bulan?> &nbsp;</td>
						</tr>
						<tr>
							<td><b>COA</b></td>
							<td>:&nbsp; &nbsp; <?php echo $coa?> &nbsp;</td>
						</tr>
						<tr>
							<td class="center"> 
								<?php
									for($i=2015;$i<=date('Y');$i++){
									$thn[$i]=$i;
									}

									$i=01;
									$value = $i;
									for($i=1; $i <= 12; $i++){
										$bln[$i]=$i;
									}
								?>
								<div> &nbsp;
									<div style="float:left;margin:5px;">
									<?php 
									echo $form->dropDownList($modeltahun,'coad_datetime',$thn,array('empty'=>'Pilih Tahun','class'=>"form-control", 'style'=>"float:left;")); 
									?>															
									</div>
									<div style="float:left;margin:5px;">
									<?php 
									echo $form->dropDownList($modelbulan,'coad_datetime_insert',$bln,array('empty'=>'Pilih Bulan','class'=>"form-control", 'style'=>"float:left;")); 
									?>															
									</div>
									<div style="float:left;margin:5px;">
									<?php 
									echo $form->dropDownList($modelcoa,'coad_coa_id',ChartOfAccount::listCoa(),array('empty'=>'Nomor COA','class'=>"form-control", 'style'=>"float:left;")); 
									?>															
									</div>
								</div>	
							</td>
							<td>	
								<?php echo CHtml::submitButton('Lihat', array('class'=>'btn btn-default', 'style'=>'width:100px')); ?>
							</td>
						</tr>
					</tbody>
				</table>
				<?php $this->endWidget(); ?>
	            <table class="table" border="1">
	            	<thead>
	                    <tr>
	                        <th>NO</th>
	                        <th><center>COA ID</center></th>
	                        <th><center>COA DESC</center></th>
	                        <th>DATE</th>
	                        <th>DESCRIPTION</th>
	                        <td><b>DEBET</b></td>
	                        <td><b>CREDIT</b></td>
	                        <th colspan="3"><center>PAYMENT</center></th>
	                        <th>CALCULATION <small>(VIA)</small></th>
	                        <th>BY</th>
	                    </tr>
	                </thead>
	                <thead>
	                    <tr>
	                        <th></th>
	                        <th></th>
	                        <th></th>
	                        <th></th>
	                        <th></th>
	                        <td><b></b></td>
	                        <td><b></b></td>
	                        <?php foreach($bankAll as $dataBank){ ?>
	                        <td><b><u><?=strtoupper($dataBank->bnk_name);?></u></b></td>
	                        <?php } ?>
	                        <th></th>
	                    </tr>
	                </thead>
	                <tbody>
	                <?php 
	                    $no=1;
	                    $totSaldo=0;
	                    $totDebet=0;
	                    $totCredit=0;
	                    $saldoEnd=0;
	                    $count = count($coaDetail);
	                    foreach($coaDetail as $key => $value){

	                        $totSaldo   = $totSaldo+$value->coad_saldo;
	                        $totDebet   = $totDebet+$value->coad_debet;
	                        $totCredit  = $totCredit+$value->coad_credit;
	                        $saldoEnd   = $coaDetail[($count+$key+1) % $count];
	                        $previous   = $coaDetail[($count+$key+1) % $count];
	                        // $previousValue=$value->coad_id[$key-1];
	                        // $previousValue=$value[$key-1]->coad_id;
	                        // $prev = $coaDetail[$key-1];
	                        // $prev = $coaDetail[$key-1]['coad_id'];
	                        // $prev = $value[$key-1]['coad_id'];
	                        // $value->coad_id=prev($value->coad_id);
	                        // echo '<pre>';
	                        // // print_r($value[$key-1]);
	                        // print_r($previous);
	                        // echo '</pre>';
	                        // die;


	                        if($value->coad_bb_saldo == 0){
	                            // foreach($getBankId as $idBank){
	                            // $idBanks = $idBank->bnk_id;
	                            // $check1 = '<a class="btn btn-success btn-xs" href="../bank/saldoBank/id/'.$value->coad_coa_id.'/coadId/'.$value->coad_id.'/bankId/1" onclick="return confirm("Are you sure?")" title="Check"><i class="fa fa-check"></i></a>';
	                            $check1 = CHtml::link('<i class="fa fa-check"></i>',['bank/saldoBank/id/'.$value->coad_coa_id.'/coadId/'.$value->coad_id.'/bankId/1'],['class' => 'btn btn-success btn-xs', 'title' => 'Check', 'confirm' => 'Are you sure?']);
	                            $check2 = CHtml::link('<i class="fa fa-check"></i>',['bank/saldoBank/id/'.$value->coad_coa_id.'/coadId/'.$value->coad_id.'/bankId/2'],['class' => 'btn btn-success btn-xs', 'title' => 'Check', 'confirm' => 'Are you sure?']);
	                            $check3 = CHtml::link('<i class="fa fa-check"></i>',['bank/saldoBank/id/'.$value->coad_coa_id.'/coadId/'.$value->coad_id.'/bankId/3'],['class' => 'btn btn-success btn-xs', 'title' => 'Check', 'confirm' => 'Are you sure?']);
	                            // $check3 = '<a class="btn btn-success btn-xs" href="../bank/saldoBank/id/'.$value->coad_coa_id.'/coadId/'.$value->coad_id.'/bankId/3" title="Check"><i class="fa fa-check"></i>
	                            //     </a>';
	                            //     $idBanks++;
	                            // }
	                        }else{
	                            $check1 = '';
	                            $check2 = '';
	                            $check3 = '';
	                        }

	                        if(empty($value->coad_color)){
	                            $warna = " ";
	                        }else{
	                            $warna = $value->coad_color;
	                        }

	                        if($value->coad_debet == 0){
	                            $debet = "-";
	                        }else{
	                            $debet = 'Rp. '.number_format($value->coad_debet);
	                        }

	                        if($value->coad_credit == 0){
	                            $credit = "-";
	                        }else{
	                            $credit = 'Rp. '.number_format($value->coad_credit);
	                        }

	                        if($value->coad_color == " " || empty($value->coad_color)){
	                            $changeColor = "";
	                            // $changeColor = '<div class="btn-group">
	                            // <a href="#" data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle"><i class="fa fa-circle"></i><span class="caret"></span></a>
	                            // <ul class="dropdown-menu" role="menu">
	                            //     <li><a class="btn btn-primary btn-xs" href="../../../../changeColor/id/'.$value->coad_id.'" title="White"><i class="fa fa-circle"></i></a></li>                                                    
	                            // </ul>
	                            // </div>';
	                            // $changeColor = '<a class="btn btn-primary btn-xs" href="../../../../changeColor/id/'.$value->coad_id.'" title="White"><i class="fa fa-circle"></i>
	                            //     </a>';
	                        }else{
	                            $changeColor = '<a class="btn btn-primary btn-xs" href="changeColor/id/'.$value->coad_id.'" title="White"><i class="fa fa-circle"></i>
	                                </a>';
	                        }

	                        if($value->coad_bnk_id == 1){
	                            $bankName = '<b><font color="blue">BNI IDR</font></b>';
	                        }else if($value->coad_bnk_id == 2){
	                            $bankName = '<b><font color="brown">BNI GIRO</font></b>';
	                        }else if($value->coad_bnk_id == 3){
	                            $bankName = '<b><font color="green">BNI KSO</font></b>';
	                        }else{
	                            $bankName = '<small>Unknown</small>';
	                        }

	                        echo'
	                        <tr style="background-color:#'.$warna.'">
	                            <td>'.$no.'</td>
	                            <td><center>'.$value->coad_coa_id.'</center></td>
	                            <td><center>'.ucwords($value->getCoa->coa_desc).'</center></td>
	                            <td>'.$value->coad_datetime.'</td>
	                            <td>'.$value->coad_desc.'</td>
	                            <td>'.$debet.'</td>
	                            <td>'.$credit.'</td>
	                        ';

	                     //    foreach($bankSaldoCurrent as $fieldSaldo){
	                        //     echo'
	                        //         <td>Rp. '.number_format($fieldSaldo->coad_bb_saldo).'</td>
	                        //         '
	                        //     ;
	                        // }

	                        echo'
	                            <td><a target="_blank">'.$check1.'</a></td>
	                            <td>'.$check2.'</td>
	                            <td>'.$check3.'</td>
	                            <td>Rp. '.number_format($value->coad_bb_saldo).' <small>('.$bankName.')</small></td>
	                            <td>'.$value->coad_bnk_id_pd.'</td>
	                        </tr>
	                        ';
	                        $no++;
	                       }
	                  ?>
	                </tbody>
	                <tfoot>
	                    <tr>
	                        <th></th>
	                        <th></th>
	                        <th></th>
	                        <th>Current Saldo</th>
	                        <th></th>
	                        <td><b></b></td>
	                        <td><b></b></td>
	                        <?php foreach($bankAll as $footerSaldo){ ?>
	                        <td><b><?='Rp. '.number_format($footerSaldo->bnk_saldo_current);?></b></td>
	                        <?php } ?>
	                        <th></th>
	                        <th></th>
	                    </tr>
	                    <tr>
	                        <th></th>
	                        <th></th>
	                        <th></th>
	                        <th>Used Saldo</th>
	                        <th></th>
	                        <td><b></b></td>
	                        <td><b></b></td>
	                        <?php foreach($bankAll as $footerSaldo){ ?>
	                        <td><i><?='Rp. '.number_format($footerSaldo->bnk_saldo_used);?></i></td>
	                        <?php } ?>
	                        <th></th>
	                        <th></th>
	                    </tr>
	                    <!-- <tr>
	                        <th></th>
	                        <th></th>
	                        <th>First Saldo</th>
	                        <th></th>
	                        <td><b></b></td>
	                        <td><b></b></td>
	                        <?php foreach($bankAll as $footerSaldo){ ?>
	                        <td><?='Rp. '.number_format($footerSaldo->bnk_saldo_first);?></td>
	                        <?php } ?>
	                        <th></th>
	                        <th></th>
	                    </tr> -->
	                </tfoot>
	            </table>
	        </div>
	    </div>
	</div>
	</div>
</div>