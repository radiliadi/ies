<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Accounting' => '../../../../../accounting/index',
        // 'Active'=>array('active', 
        //     )
        ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-laptop"></span> COA - <small> Type : <?=$coaType->coat_desc ;?> </small> </h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?=$coaMenu->coa_id ;?> : <?=strtoupper($coaMenu->coa_desc) ;?></h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
            	<div class="btn-group pull-right">
                    <?php 

                    echo CHtml::link('<i class="fa fa-plus"></i>', '../../../../add/id/'.$coaMenu->coa_id.'/type/'.$coaMenu->coa_coat_id.'', array('class'=>'btn btn btn-primary pull-right')); ?>
                </div> 
                <div class="btn-group pull-left">
                    
                </div> 
            </div>
            
            <div class="panel-body">      
                                    <div class="table-responsive">
                                        <!-- START DEFAULT DATATABLE -->
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th></th>
                                                <th>Date</th>
                                                <th>Description</th>
                                                <th>Debet</th>
                                                <th>Credit</th>
                                                <th>Saldo</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                            $no=1;
                                            $totSaldo=0;
                                            $totDebet=0;
                                            $totCredit=0;
                                            $saldoEnd=0;
                                            $count = count($coaDetail);
                                            foreach($coaDetail as $key => $value){

                                                $totSaldo   = $totSaldo+$value->coad_saldo;
                                                $totDebet   = $totDebet+$value->coad_debet;
                                                $totCredit  = $totCredit+$value->coad_credit;
                                                $saldoEnd   = $coaDetail[($count+$key+1) % $count];
                                                $previous   = $coaDetail[($count+$key+1) % $count];
                                                // $previousValue=$value->coad_id[$key-1];
                                                // $previousValue=$value[$key-1]->coad_id;
                                                // $prev = $coaDetail[$key-1];
                                                // $prev = $coaDetail[$key-1]['coad_id'];
                                                // $prev = $value[$key-1]['coad_id'];
                                                // $value->coad_id=prev($value->coad_id);
                                                // echo '<pre>';
                                                // // print_r($value[$key-1]);
                                                // print_r($previous);
                                                // echo '</pre>';
                                                // die;


                                                if($value->coad_is_saldo == 0){
                                                    $check = '<a class="btn btn-success btn-xs" href="../../../../checkSaldo/id/'.$value->coad_id.'/coaId/'.$value->coad_coa_id.'/prevId/'.$previous->coad_id.'" title="Check"><i class="fa fa-check"></i>
                                                        </a>';
                                                }else if($value->coad_is_saldo == 1){
                                                    $check = '<a class="btn btn-default btn-xs" href="../../../../checkSaldo/id/'.$value->coad_id.'/coaId/'.$value->coad_coa_id.'/prevId/'.$previous->coad_id.'" title="Check"><i class="fa fa-check"></i>
                                                        </a>';
                                                    // $check = '';
                                                }else{
                                                    $check = '';
                                                }

                                                if(empty($value->coad_color)){
                                                    $warna = " ";
                                                }else{
                                                    $warna = $value->coad_color;
                                                }

                                                if($value->coad_debet == 0){
                                                    $debet = "-";
                                                }else{
                                                    $debet = 'Rp. '.number_format($value->coad_debet);
                                                }

                                                if($value->coad_credit == 0){
                                                    $credit = "-";
                                                }else{
                                                    $credit = 'Rp. '.number_format($value->coad_credit);
                                                }

                                                if($value->coad_color == " " || empty($value->coad_color)){
                                                    $changeColor = "";
                                                    // $changeColor = '<div class="btn-group">
                                                    // <a href="#" data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle"><i class="fa fa-circle"></i><span class="caret"></span></a>
                                                    // <ul class="dropdown-menu" role="menu">
                                                    //     <li><a class="btn btn-primary btn-xs" href="../../../../changeColor/id/'.$value->coad_id.'" title="White"><i class="fa fa-circle"></i></a></li>                                                    
                                                    // </ul>
                                                    // </div>';
                                                    // $changeColor = '<a class="btn btn-primary btn-xs" href="../../../../changeColor/id/'.$value->coad_id.'" title="White"><i class="fa fa-circle"></i>
                                                    //     </a>';
                                                }else{
                                                    $changeColor = '<a class="btn btn-primary btn-xs" href="../../../../changeColor/id/'.$value->coad_id.'" title="White"><i class="fa fa-circle"></i>
                                                        </a>';
                                                }
                                                

                                                echo'
                                                <tr style="background-color:#'.$warna.'">
                                                    <td>'.$no.'</td>
                                                    <td>'.$changeColor.'</td>
                                                    <td>'.$value->coad_datetime.'</td>
                                                    <td>'.$value->coad_desc.'</td>
                                                    <td>'.$debet.'</td>
                                                    <td>'.$credit.'</td>
                                                    <td>Rp. '.number_format($value->coad_saldo).'</td>
                                                    <td>'.$check.'
                                                    </td>
                                                    <td><a class="btn btn-primary btn-xs" href="../../../../update/id/'.$value->coad_id.'/coaId/'.$value->coad_coa_id.'/type/'.$value->coad_coat_id.'" title="Check"><i class="fa fa-pencil"></i>
                                                        </a>
                                                    </td>
                                                </tr>'
                                                ;
                                                $no++;
                                               }
                                          ?>
                                        </tbody>
                                        <?php

                                            echo'
                                            <tr>
                                                <td> </td>
                                                <td> </td>
                                                <td> Debet Akhir</td>
                                                <td class="center"> </td>
                                                <td class="center"> <b>Rp. '.number_format($saldoEnd->coad_debet).'</b> </td>
                                                <td class="center"> </td>
                                                <td class="center"> </td>
                                                <td class="center"> </td>
                                            </tr>
                                            ';

                                             echo'
                                            <tr>
                                                <td> </td>
                                                <td> </td>
                                                <td> Credit Akhir</td>
                                                <td class="center"> </td>
                                                <td class="center"> </td>
                                                <td class="center"> <b>Rp. '.number_format($saldoEnd->coad_credit).'</b> </td>
                                                <td class="center"> </td>
                                                <td class="center"> </td>
                                            </tr>
                                            ';

                                            echo'
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td><b>Saldo Akhir</b></td>
                                                <td class="center"></td>
                                                <td class="center"></td>
                                                <td class="center"></td>
                                                <td class="center"><b>Rp. '.number_format($saldoEnd->coad_saldo).'</b></td>
                                                <td class="center"></td>
                                            </tr>
                                            ';
                                        ?>
                                    </table>
                                </div>
                            </div>
        </div>
    </div>
</div>