<?php
/* @var $this ChartOfAccountDetailController */
/* @var $model ChartOfAccountDetail */

$this->breadcrumbs=array(
	'Chart Of Account Details'=>array('index'),
	$model->coad_id,
);

$this->menu=array(
	array('label'=>'List ChartOfAccountDetail', 'url'=>array('index')),
	array('label'=>'Create ChartOfAccountDetail', 'url'=>array('create')),
	array('label'=>'Update ChartOfAccountDetail', 'url'=>array('update', 'id'=>$model->coad_id)),
	array('label'=>'Delete ChartOfAccountDetail', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->coad_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ChartOfAccountDetail', 'url'=>array('admin')),
);
?>

<h1>View ChartOfAccountDetail #<?php echo $model->coad_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'coad_id',
		'coad_coa_id',
		'coad_coar_id',
		'coad_id_pd',
		'coad_desc',
		'coad_debet',
		'coad_credit',
		'coad_datetime',
		'coad_datetime_insert',
		'coad_datetime_update',
		'coad_is_submit',
		'coad_status',
	),
)); ?>
