<?php
/* @var $this ChartOfAccountDetailController */
/* @var $model ChartOfAccountDetail */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'coad_id'); ?>
		<?php echo $form->textField($model,'coad_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coad_coa_id'); ?>
		<?php echo $form->textField($model,'coad_coa_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coad_coar_id'); ?>
		<?php echo $form->textField($model,'coad_coar_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coad_id_pd'); ?>
		<?php echo $form->textField($model,'coad_id_pd',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coad_desc'); ?>
		<?php echo $form->textArea($model,'coad_desc',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coad_debet'); ?>
		<?php echo $form->textField($model,'coad_debet',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coad_credit'); ?>
		<?php echo $form->textField($model,'coad_credit',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coad_datetime'); ?>
		<?php echo $form->textField($model,'coad_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coad_datetime_insert'); ?>
		<?php echo $form->textField($model,'coad_datetime_insert'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coad_datetime_update'); ?>
		<?php echo $form->textField($model,'coad_datetime_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coad_is_submit'); ?>
		<?php echo $form->textField($model,'coad_is_submit'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coad_status'); ?>
		<?php echo $form->textField($model,'coad_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->