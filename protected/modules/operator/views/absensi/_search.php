<?php
/* @var $this AbsensiController */
/* @var $model Absensi */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'abs_id'); ?>
		<?php echo $form->textField($model,'abs_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'abs_jkk_tetap'); ?>
		<?php echo $form->textField($model,'abs_jkk_tetap'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'abs_jkk_kontrak'); ?>
		<?php echo $form->textField($model,'abs_jkk_kontrak'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'abs_timesheet'); ?>
		<?php echo $form->textField($model,'abs_timesheet'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'abs_lembur'); ?>
		<?php echo $form->textField($model,'abs_lembur'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'latitude'); ?>
		<?php echo $form->textField($model,'latitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'longitude'); ?>
		<?php echo $form->textField($model,'longitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'abs_lat'); ?>
		<?php echo $form->textField($model,'abs_lat'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'abs_lon'); ?>
		<?php echo $form->textField($model,'abs_lon'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->