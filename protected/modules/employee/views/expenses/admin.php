<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Expenses' => 'admin',
        // 'Active'=>array('active', 
        //     )
        ),
    )
);
?>
<div class="row">
	<div class="page-title">                    
        <h2><span class="fa fa-laptop"></span> Manage Expenses - <small> All Expenses </small> </h2>
    </div>
	<div class="col-md-12">
		<div class="panel panel-default search-form" style="display:none">
			<div class="panel-body">
			</div>
		</div>
	</div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Expenses List</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh" onClick="window.location.href=window.location.href"><span class="fa fa-refresh"></span></a></li>
                </ul>   
            	<div class="btn-group pull-right">
                    <?php echo CHtml::link('<i class="fa fa-plus"></i>', 'create', array('class'=>'btn btn btn-primary pull-right')); ?>
                </div> 
                <div class="btn-group pull-left">
                    
                </div> 
            </div>
            
            <div class="panel-body">
            <button class="btn btn-warning"><i class="fa fa-question"></i>On Process (<?= count($OnProcess) ;?>)</button>
            <button class="btn btn-info"><i class="fa fa-ban"></i>Not Verified (<?= count($notVerified) ;?>)</button>
            <button class="btn btn-success"><i class="fa fa-check"></i>Verified (<?= count($verified) ;?>)</button>
            <button class="btn btn-danger"><i class="fa fa-thumbs-o-down"></i>Rejected (<?= count($rejected) ;?>)</button>
            <button class="btn btn-primary"><i class="fa fa-thumbs-o-up"></i>Approved (<?= count($approved) ;?>)</button>
            <button class="btn btn-default"><i class="fa fa-trash-o"></i>Deleted (<?= count($deleted) ;?>)</button>
            <?php //echo CHtml::beginForm(array('expenses/exportExcel')); ?>
            <?php $this->widget('booster.widgets.TbGridView',array(
            'id'=>'user-grid',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'responsiveTable' => true,
            'pager' => array(
                'header' => '',
                'footer' => '',
                'cssFile' => false,
                'selectedPageCssClass' => 'active',
                'hiddenPageCssClass' => 'disabled',
                'firstPageCssClass' => 'previous',
                'lastPageCssClass' => 'next',
                'maxButtonCount' => 5,
                'firstPageLabel'=> '<i class="fa fa-angle-double-left"></i>',
                'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
                'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
                'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
                'htmlOptions' => array(
                    'class' => 'dataTables_paginate paging_bootstrap pagination pull-right',
                    'align' => 'center',
                    'style' => 'width: 100%;',
                ),
            ),
            'columns'=>array(
                array(
                    'header' => 'No.',
                    'htmlOptions' => array(
                        'width' => '20px',
                        'style' => 'text-align:center'
                    ),
                    'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                ),
                array(
                    'name' => 'exp_id',
                    'type' => 'raw',
                ),
                'exp_id_pd',
        		array(
                    'name' => 'exp_desc',
                    'type' => 'raw',
                ),
                array(
                    'name' => 'exp_date_submit',
                    'type' => 'raw',
                ),
                array(    
                    'header'=>'Status',
                    'type'=>'raw', 
                    'value' =>function($data) {
                                if($data->exp_is_verified == 0 && $data->exp_is_approved == 0){
                                    return CHtml::link('<i class="fa fa-question"></i>',['admin#adiganteng'],['class' => 'btn btn-warning', 'title' => 'On Process']);
                                }else if($data->exp_is_verified == 2 && $data->exp_is_approved == 0){
                                    return CHtml::link('<i class="fa fa-ban"></i>',['admin#adiganteng'],['class' => 'btn btn-info', 'title' => 'Not Verified']);
                                }else if($data->exp_is_verified == 1 && $data->exp_is_approved == 0){
                                    return CHtml::link('<i class="fa fa-check"></i>',['admin#adiganteng'],['class' => 'btn btn-success', 'title' => 'Verified']);
                                }else if($data->exp_is_verified == 1 && $data->exp_is_approved == 2){
                                    return CHtml::link('<i class="fa fa-thumbs-o-down"></i>',['admin#adiganteng'],['class' => 'btn btn-danger', 'title' => 'Rejected']);
                                }else if($data->exp_is_verified == 1 && $data->exp_is_approved == 1){
                                    return CHtml::link('<i class="fa fa-thumbs-o-up"></i>',['admin#adiganteng'],['class' => 'btn btn-primary', 'title' => 'Approved']);
                                }
                            },
                ),
                array(
                    'name' => 'exp_not_verified_by',
                    'type' => 'raw',
                    // 'filter' => User::listUser()
                    'value' =>function($data) {
                                if(!empty($data->exp_not_verified_by)){
                                    return $data->exp_not_verified_by.'<br></br><small>('.$data->exp_not_verified_date.')</small>';
                                }else{
                                    return '-';
                                }
                            },
                ),
                array(
                    'name' => 'exp_verified_by',
                    'type' => 'raw',
                    // 'filter' => User::listUser()
                    'value' =>function($data) {
                                if(!empty($data->exp_verified_by)){
                                    return $data->exp_verified_by.'<br></br><small>('.$data->exp_verified_date.')</small>';
                                }else{
                                    return '-';
                                }
                            },
                ),
                array(
                    'name' => 'exp_rejected_by',
                    'type' => 'raw',
                    // 'filter' => User::listUser()
                    'value' =>function($data) {
                                if(!empty($data->exp_rejected_by)){
                                    return $data->exp_rejected_by.'<br></br><small>('.$data->exp_rejected_date.')</small>';
                                }else{
                                    return '-';
                                }
                            },
                ),
                array(
                    'name' => 'exp_approved_by',
                    'type' => 'raw',
                    // 'filter' => User::listUser()
                    'value' =>function($data) {
                                if(!empty($data->exp_approved_by)){
                                    return $data->exp_approved_by.'<br></br><small>('.$data->exp_approved_date.')</small>';
                                }else{
                                    return '-';
                                }
                            },
                ),
                array(
                    'name' => 'exp_datetime_update',
                    'type' => 'raw',
                    'value' => function($data) {
                            if(!empty($data->exp_datetime_update)){
                                return '<small>'.$data->exp_datetime_update.'</small>';
                            }else{
                                return '-';
                            }
                        }
                ),
                // array(    
                //     'header'=>'Feedback',
                //     'type'=>'raw', 
                //     'value' =>function($data) {
                //                 if($data->exp_is_verified == 0 && $data->exp_is_approved == 0 && $data->exp_submit_status == 1){
                //                     return CHtml::link('<i class="fa fa-check"></i>',['/employee/expenses/verified/id/'.$data->exp_id],['class' => 'btn btn-success', 'title' => 'Verified', 'confirm' => 'Verified this expenses?']).'<br></br>'.CHtml::link('<i class="fa fa-ban"></i>',['/employee/expenses/notVerified/id/'.$data->exp_id],['class' => 'btn btn-info', 'title' => 'Not Verified', 'confirm' => 'Are you sure?']);
                //                 }else if($data->exp_is_verified == 2 && $data->exp_is_approved == 0 && $data->exp_submit_status == 1){
                //                     return CHtml::link('<i class="fa fa-check"></i>',['/employee/expenses/verified/id/'.$data->exp_id],['class' => 'btn btn-success', 'title' => 'Verified', 'confirm' => 'Verified this expenses?']);
                //                 }else if($data->exp_is_verified == 1 && $data->exp_is_approved == 0 && $data->exp_submit_status == 1){
                //                     return CHtml::link('<i class="fa fa-thumbs-o-up"></i>',['/employee/expenses/approved/id/'.$data->exp_id],['class' => 'btn btn-primary', 'title' => 'Approved', 'confirm' => 'Approve this expenses?']).'<br></br>'.CHtml::link('<i class="fa fa-thumbs-o-down"></i>',['/employee/expenses/rejected/id/'.$data->exp_id],['class' => 'btn btn-danger', 'title' => 'Rejected', 'confirm' => 'Are you sure?']);
                //                 }else if($data->exp_is_verified == 1 && $data->exp_is_approved == 2 && $data->exp_submit_status == 1){
                //                     return CHtml::link('<i class="fa fa-thumbs-o-up"></i>',['/employee/expenses/approved/id/'.$data->exp_id],['class' => 'btn btn-primary', 'title' => 'Approved', 'confirm' => 'Approve this expenses?']);
                //                 }
                //             },
                // ),
                array(    
                    'header'=>'View',
                    'type'=>'raw', 
                    'value' =>function($data) {
                                if($data->exp_submit_status == 1 && !empty($data->exp_attachment_img)){
                                    return CHtml::link('<i class="fa fa-search"></i>',['/employee/expensesDetail/admin/id/'.$data->exp_id],['class' => 'btn btn-default btn-xs', 'title' => 'View']).'<br></br>'.CHtml::link('<i class="fa fa-picture-o"></i>',['/employee/expenses/picture/id/'.$data->exp_id],['class' => 'btn btn-default btn-xs', 'title' => 'Attachment']);
                                }else if($data->exp_submit_status == 1 && empty($data->exp_attachment_img)){
                                    return CHtml::link('<i class="fa fa-search"></i>',['/employee/expensesDetail/admin/id/'.$data->exp_id],['class' => 'btn btn-default btn-xs', 'title' => 'View']);
                                }else if($data->exp_submit_status == 0){
                                    return '</br>';
                                }
                            },
                ),
                array(    
                    'header'=>'Action',
                    'type'=>'raw', 
                    'value' =>function($data) {
                                if($data->exp_is_verified == 2 || $data->exp_is_approved == 2 && $data->exp_submit_status == 1){
                                    return CHtml::link('<i class="fa fa-pencil"></i>',['/employee/expenses/update/id/'.$data->exp_id],['class' => 'btn btn-default btn-xs', 'title' => 'Update']).'<br></br>'.CHtml::link('<i class="fa fa-trash-o"></i>',['/employee/expenses/delete/id/'.$data->exp_id],['class' => 'btn btn-default btn-xs', 'title' => 'Deleted', 'confirm' => 'Are you sure to delete this Expenses?']);
                                }else if($data->exp_is_verified == 0 && $data->exp_is_approved == 0 && $data->exp_submit_status == 0){
                                    return CHtml::link('<i class="fa fa-plus"></i>',['/employee/expensesDetail/create/id/'.$data->exp_id],['class' => 'btn btn-default  btn-xs', 'title' => 'Create']);
                                }else if($data->exp_is_verified == 1 && $data->exp_is_approved == 1 && $data->exp_submit_status == 1){
                                    return '</br>';
                                }else if($data->exp_submit_status == 0){
                                    return '</br>';
                                }
                            },
                ),
                array(    
                    'header'=>'Print',
                    'type'=>'raw', 
                    'value' =>function($data) {
                                if($data->exp_is_verified == 1 && $data->exp_is_approved == 1){
                                    return CHtml::link('<i class="fa fa-print"></i>',['/employee/expenses/print/id/'.$data->exp_id],['class' => 'btn btn-default btn-xs', 'title' => 'Print']);
                                }else if($data->exp_submit_status == 0){
                                    return '</br>';
                                }
                            },
                ),
            ),
            )); 
            ?>
            <br></br>
            <hr></hr>
            </div>
        </div>
    </div>
</div>