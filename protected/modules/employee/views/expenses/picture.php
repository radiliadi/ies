<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Expenses' => '../../admin',
        // 'Semua' => array('Semua'=>'admin', 
        'Picture',
        $model->exp_id,
        ),
    )
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-picture-o"></span> Attachment Image : No. Expenses #<?=$model->exp_id;?> (<?=$model->exp_id_pd;?>)</h3>
</div>
<?php echo $this->renderPartial('zoomImage', array('model'=>$model)); ?>