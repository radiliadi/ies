<?php
$pdf->AliasNbPages();
$pdf->AddPage('P',$size);                            
$pdf->SetFont('Times','b',16);  

$pdf->Ln(2);
        $pdf->SetFont('Times','b',10);
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetFillColor(104,104,104); //Set background of the cell to be that grey color
        $pdf->SetTextColor(0,0,0);
$pdf->Cell(40,18,'',1,0,'C',false);
$pdf->Image('images/png.png',20,13,20,15);
$pdf->Cell(70,18,$header0,1,0,'C',false);
if(!empty($expenses->exp_date_submit)){
	$pdf->Cell(80,18,'Date Submitted : ' .$expenses->exp_date_submit,1,0,'C',false);
}else{
	$pdf->Cell(80,18,'Date Submitted : ',1,0,'C',false);
}

$pdf->Ln(18);
$pdf->SetFont('Times','b',10);
$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(104,104,104); //Set background of the cell to be that grey color
$pdf->SetTextColor(0,0,0);
$pdf->Cell(40,5,'NAMA',1,0,'C',false);
$pdf->Cell(150,5,' '.strtoupper($karyawan->nm_pd) . ' - (' . ($expenses->exp_id_pd).')',1,0,'L',false);


// $pdf->Cell(40,5,'NAMA',1,0,'C',false);
// $pdf->Cell(70,5,strtoupper($karyawan->nm_pd),1,0,'L',false);
// $pdf->Cell(40,5,'NIP',1,0,'C',false);
// $pdf->Cell(40,5,strtoupper($karyawan->nm_pd),1,0,'L',false);
// $pdf->Cell(40,5,'DEBET',1,0,'C',false);
// $pdf->Cell(40,5,'CREDIT',1,0,'C',false);

$pdf->Ln(5);
$pdf->SetFont('Times','b',10);
$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(104,104,104); //Set background of the cell to be that grey color
$pdf->SetTextColor(0,0,0);
$pdf->Cell(40,5,'JABATAN',1,0,'C',false);
$pdf->Cell(150,5,' '.strtoupper($jabatan->nm_jabatan),1,0,'L',false);
//mulai tabel

$pdf->Ln(5);
$pdf->SetFont('Times','b',14);
$pdf->Cell(190,10,' E X P E N S E S   R E P O R T',1,0,'C',false);

//line break
$pdf->Ln(10);
        $pdf->SetFont('Times','b',10);
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetFillColor(104,104,104); //Set background of the cell to be that grey color
        $pdf->SetTextColor(0,0,0);
// $pdf->Cell(5,5,'',0,0,'C',false);
$pdf->Cell(10,7,'No',1,0,'C',false);
$pdf->Cell(30,7,'DATE',1,0,'C',false);
$pdf->Cell(70,7,'DESCRIPTION',1,0,'C',false);
$pdf->Cell(40,7,'DEBET',1,0,'C',false);
$pdf->Cell(40,7,'CREDIT',1,0,'C',false);
$pdf->Ln(7); //Line break
//Data dari database
		$no=1;
		$sumDebet=0;
		$sumCredit=0;
		foreach($expensesDetail as $db)
		{
			if(!empty($db->expd_debet)){
				$sumDebet=$sumDebet+$db->expd_debet;
			}else{
				$sumDebet=$sumDebet+0;
			}

			if(!empty($db->expd_credit)){
				$sumCredit=$sumCredit+$db->expd_credit;
			}else{
				$sumCredit=$sumCredit+0;
			}
			
			$pdf->SetFont('Times','',10);
			$pdf->Cell(10,7,$no,1,0,'C',false);
			$pdf->Cell(30,7,$db->expd_date,1,0,'C',false);
			$pdf->Cell(70,7,$db->expd_desc,1,0,'L',false);
			if(!empty($db->expd_debet)){
				$pdf->Cell(40,7,"Rp. ".number_format($db->expd_debet),1,0,'C',false);
			}else{
				$pdf->Cell(40,7," - ",1,0,'C',false);
			}
			if(!empty($db->expd_credit)){
				$pdf->Cell(40,7,"Rp. ".number_format($db->expd_credit),1,0,'C',false);
			}else{
				$pdf->Cell(40,7," - ",1,0,'C',false);
			}
			
			$pdf->Ln(7);
			$no++;
		}
//selesai tabel
//tanda tangan
//Baris baru
$pdf->SetFont('Times','b',10);
$pdf->Cell(10,7,'',1,0,'C',false);
$pdf->Cell(100,7,'TOTAL',1,0,'C',false);
$pdf->Cell(40,7,"Rp. ".number_format($sumDebet),1,0,'C',false);
$pdf->Cell(40,7,"Rp. ".number_format($sumCredit),1,0,'C',false);
$pdf->Ln(7);

//Baris baru
$pdf->SetFont('Times','',10);
$pdf->Cell(110,7,'',1,0,'C',false);
$pdf->Cell(40,7,'',1,0,'C',false);
$pdf->Cell(40,7,'',1,0,'C',false);
$pdf->Ln(7);

//Baris baru
//yg diatas di admin
$no=1;
$sumDebet=0;
$sumCredit=0;
$expdUpdate = '';
$expdDelete = '';

foreach($expensesDetail as $expD){
	$sumDebet=$sumDebet+$expD->expd_debet;
	$sumCredit=$sumCredit+$expD->expd_credit;
	$no++;
}
//yg dibawah di admin
$showTot = 0;
$dueCompany = 0;
$dueEmployee = 0;
if($sumDebet > $sumCredit){
	$dueCompany = $sumDebet - $sumCredit;
	$showTot = $dueCompany;
}else if($sumDebet < $sumCredit){
	$dueEmployee = $sumCredit - $sumDebet;
	$showTot = $dueEmployee;
}
$pdf->SetFont('Times','b',10);
$pdf->Cell(110,7,'Due to Company',1,0,'C',false);
if(!empty($dueCompany)){
	$pdf->Cell(40,7,"Rp. ".number_format($dueCompany),1,0,'C',false);
}else{
	$pdf->Cell(40,7," - ",1,0,'C',false);
}
$pdf->Cell(40,7,'',1,0,'C',false);
$pdf->Ln(7);

//Baris baru
$pdf->SetFont('Times','b',10);
$pdf->Cell(110,7,'Due to Employee',1,0,'C',false);
$pdf->Cell(40,7,'',1,0,'C',false);
if(!empty($dueEmployee)){
	$pdf->Cell(40,7,"Rp. ".number_format($dueEmployee),1,0,'C',false);
}else{
	$pdf->Cell(40,7," - ",1,0,'C',false);
}

$pdf->Ln(7);

//line break
$pdf->Ln(7);
$pdf->SetFont('Times','',10);
$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(104,104,104); //Set background of the cell to be that grey color
$pdf->SetTextColor(0,0,0);

//Baris baru
$pdf->SetFont('Times','',10);
$pdf->Cell(40,7,'Approved By',1,0,'C',false);
$pdf->Cell(70,7,'Verified By',1,0,'C',false);
$pdf->Cell(80,7,'Prepared',1,0,'C',false);
$pdf->Ln(7);

//Baris baru
$pdf->SetFont('Times','',8);
if(!empty($expenses->exp_approved_name)){
	$pdf->Cell(40,14,strtoupper($expenses->exp_approved_name),1,0,'C',false);
}else{
	$pdf->Cell(40,14,'',1,0,'C',false);
}
if(!empty($expenses->exp_verified_name)){
	$pdf->Cell(70,14,strtoupper($expenses->exp_verified_name),1,0,'C',false);
}else{
	$pdf->Cell(70,14,'',1,0,'C',false);
}
$pdf->Cell(80,14,'',1,0,'C',false);
$pdf->Ln(14);

//Baris baru
$pdf->SetFont('Times','',8);
$pdf->Cell(40,14,'REMARKS',1,0,'C',false);
$pdf->SetFont('Times','',8);
if(!empty($company->cpy_notes)){
	$pdf->Cell(150,14,'       '.$company->cpy_notes,1,0,'L',false);
}else{
	$pdf->Cell(150,14,'',1,0,'L',false);
}
$pdf->Ln(14);

$pdf->Ln(10);	
$pdf->SetFont('Times','',10);
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(155);
$pdf->MultiCell(80,5,$company->cpy_city.', '.date('d M Y'),0,'L',false);
//Baris Baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(155);
$pdf->MultiCell(80,5,$company->cpy_fullname.',',0,'L',false);
$pdf->Ln(12);
//Baris baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(155);
$pdf->MultiCell(80,5,$company->cpy_dirut,0,'L',false);
//Cetak PDF
// $pdf->Output('('.date('dMY').')-'.$model->id_gaji.'-'.$model->datakaryawan['id_pd'].'-'.$model->datakaryawan['nm_pd'].'.pdf','I');
$pdf->Output('('.date('dMY').')-'.$judul.'.pdf','I');
?>