<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Payslip',
        $modelkaryawan->id_pd,
        ),
    )
);
?>
<div class="row">

	<div class="page-title">                    
                    <h2><span class="fa fa-money"></span> Payroll - <?php echo $modelkaryawan->id_pd; ?></h2>
                </div>
<div class="col-md-12">
<div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
    <div class="panel-body">
<div class="form"> 
<?php
/* @var $this GajiController */
/* @var $model Gaji */

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'pegawai-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data',
		'role'=>'form',
	),
));

$level=Yii::app()->session->get('level');
$id_pd=Yii::app()->session->get('username');
if($id_pd !== $modelkaryawan->id_pd)
{
throw new CHttpException('403', 'Access denied. You can not access someone else');
}
?>

<div class="row">
    <div class="box col-md-12 ">    
		<div class="box-content">
			<h1></h1>
			<table>
				<tbody>
					<tr>
						<td><b>Nama Lengkap</b></td>
						<td>: <?php echo $modelkaryawan->nm_pd;?></td>
					</tr>
					<tr>
						<td><b>NIP</b></td>
						<td>: <?php echo $modelkaryawan->id_pd;?></td>
					</tr>
					<tr><td><b>Tahun</b></td>
						<td>: <?php echo $tahun?></td>
					</tr>
					<tr>
						<td style="width: 100%;"> 
							<?php
								for($i=2016;$i<=date('Y');$i++){
								$thn[$i]=$i;
								}
							?>
									<?php 
									echo $form->dropDownList($modelbulan,'tahun',$thn,array('empty'=>'Pilih Tahun','class'=>"form-control")); 
									?>	
						</td>
						<td>	
							<?php echo CHtml::submitButton('Lihat', array('class'=>'btn btn-default', 'style'=>'width:140px')); ?>
						
						</td>
					</tr>
				</tbody>
			</table>
		
<div class="panel-body">	  
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover" id="" style="background-color:#FFE4E1">
	
			<thead>
				<tr>
					<th>No</th>
					<th>&nbsp; &nbsp; &nbsp; Bulan</th>
					<th>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Total Earnings</th>
					<!-- <th>&nbsp; &nbsp; Tax Pph 21</th> -->
					<th>&nbsp; &nbsp; Total Deduction</th>
					<th>&nbsp; &nbsp; Nett Salary</th>
					<th>&nbsp; &nbsp; Created By</th>
					<th></th>
				</tr>
			</thead>
    <tbody>
		<?php 
		$no=1;
		foreach($model as $db)
		{ 
			$modelkaryawan=Karyawan::model()->findByPk($db->id_pd);
			
			echo'
			<tr>
				<td>'.$no.'</td>
				<td>'.$db->bulan.' </td>
				<td class="center">Rp '.number_format($db->bruto).'</td>
				<td class="center">Rp '.number_format($db->tot_deduc).'</td>
				<td class="center">Rp '.number_format($db->tot_gaji).'</td>
				<td class="center"><a class="btn btn-default btn-rounded" title='.$db->user_agent.'>'.$db->created_by.' | '.$db->date_created.'</a></td>
				<td class="center">
					&nbsp;
					<a class="btn btn-primary" href="../../slip/id_gaji/'.$db->id_gaji.'" title="Print">
						<i class="glyphicon glyphicon-print"></i>
					</a>
					&nbsp;
					<a class="btn btn-default" href="../../view/id_gaji/'.$db->id_gaji.'" title="Detail">
						<i class="glyphicon glyphicon-search"></i>
					</a>
					&nbsp;
					<!--<a class="btn btn-success" href="../../update/id_gaji/'.$db->id_gaji.'" title="Update">
						<i class="glyphicon glyphicon-edit"></i>
					</a>-->
				</td>
			</tr>
			';
		$no++;
		}
		?>
	</tbody>
		</table>
	</div>
</div>
</div>
	
<?php $this->endWidget(); ?>
</div>