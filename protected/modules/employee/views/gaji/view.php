<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Payslip' => '../../../gaji/index/id/'.$model->id_pd ,
        $model->id_pd,
        ),
    )
);
$id_pd=Yii::app()->session->get('username');
if($id_pd !== $model->id_pd)
{
throw new CHttpException('403', 'Access denied. You can not access someone else');
}
?>
<div class="row">

	<div class="page-title">                    
                    <h2><span class="fa fa-money"></span> Detail - <?php echo $model->id_pd; ?></h2>
                </div>
<div class="col-md-12">
<div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
    <div class="panel-body">
<div class="form"> 
<?php
/* @var $this GajiController */
/* @var $model Gaji */

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'pegawai-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data',
		'role'=>'form',
	),
));

$level=Yii::app()->session->get('level');
						

$this->breadcrumbs=array(
	'Gajis'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Gaji', 'url'=>array('index')),
	array('label'=>'Create Gaji', 'url'=>array('create')),
);

?>
	
	
	
<div class="row">
    <div class="box col-md-12 ">   
                                    <div class="btn-group pull-left">
                                        <?php echo'
						<td class="center">
							<a class="btn btn-primary" href="../../index/id/'.$model->id_pd.'">
							<i class="fa fa-arrow-left"></i></a>
						</td>
						';	
						?>
	
                                    </div> 
<div class="panel-body">	  
	<div class="table-responsive">
                                <table class="table table-hover">
                                    <tbody>
                                        <tr>
											<td><h3>P A Y &nbsp; I N F O</h3></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>NIP</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $model->id_pd;?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Nama Lengkap</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $model->datakaryawan['nm_pd'];?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>NPWP</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $model->datakaryawan['npwp'];?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Jabatan</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $model->datajabatan['nm_jabatan']; ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Tanggal Terima Gaji</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $model->tgl; ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr bgcolor="#33414e">
											<td></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><h3>E A R N I N G S</h3></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Basic Salary</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->gj_pokok); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Field Allowances</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->tunjangan); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Overtime</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->overtime); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>Daily Trans Allowances</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->transport); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>Functional Allowances</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->functional); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Breakfast</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->breakfast); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>Lunch</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->lunch); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr bgcolor="#33414e">
											<td></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><h3>T A X &nbsp; C A L C U L A T I O N</h3></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>Biaya Jabatan</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->potongan); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Netto Sebulan</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->neto_sebulan); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Netto Setahun</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->neto_setahun); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>PTKP Setahun</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->ptkp_setahun); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>Penghasilan Kena Pajak Setahun</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->pkps); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>PPH Sebulan</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->pph_sebulan); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>PPH Setahun</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->pph_setahun); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr bgcolor="#33414e">
											<td></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><h3>D E D U C T I O N S</h3></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Loan</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->deduc_basic); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>BPJS Ketenagakerjaan</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->bpjs_kete); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>BPJS Kesehatan</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->bpjs_kese); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><b>BPJS Pensiun</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->bpjs_pensi); ?></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Total Deductions</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <?php echo "Rp. ".number_format($model->tot_deduc); ?></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr bgcolor="#33414e">
											<td></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr>
											<td><h3>S U M M A R Y</h3></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
										<tr>
											<td><b>Nett Salary</b></td>
											<td class="center">:&nbsp;&nbsp;&nbsp; <b><?php echo "Rp. ".number_format($model->tot_gaji); ?></b></td>
											<td></td>
											<td></td>
                                        </tr>
                                        <tr bgcolor="#33414e">
											<td></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
									</tbody>
									</table>
								</div>
				</br>
				<table>
					<tbody>			
						<?php 
						echo'
						<td class="center">
							<a class="btn btn-primary" href="../../index/id/'.$model->id_pd.'">
							<i class="fa fa-arrow-left"></i></a>
						</td>
						';	
						?>  
					</tbody>
				</table>		
		</div>	
	</div>
</div>
<?php $this->endWidget(); ?>
</div>
			</div>
		</div>
	</div>
  </div>