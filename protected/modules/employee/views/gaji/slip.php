<?php
$pdf->AliasNbPages();
$pdf->AddPage('L','Legal');                            
$pdf->SetFont('Times','b',18);     
$pdf->Image('images/png.png',40,10,50,25);      
$pdf->Ln(1);

$pdf->MultiCell(340,15,$header0,0,'C',false);
$pdf->SetFont('Times','b',12);
$pdf->MultiCell(340,5,$header,0,'C',false);
$pdf->Ln(12); //Line break

$pdf->SetFont('Times','b',16);
$pdf->MultiCell(340,5,$judul,0,'C',false);
$pdf->Ln(7); //Line break

$pdf->SetFont('Times','b',12);

$pdf->Cell(30,7,"",0,0,'C',false); //untuk menggeser tulisan kekanan dengan isi data yang kosong
$pdf->Cell(50,7,$data4,0,0,'L',false);
$pdf->Cell(10,7,':',0,0,'L',false);
$pdf->Cell(45,7,$model->id_gaji,0,0,'L',false);
$pdf->Cell(30,7,"",0,0,'C',false); 
$pdf->Cell(50,7,$data1,0,0,'L',false);
$pdf->Cell(10,7,':',0,0,'L',false);
$pdf->Cell(45,7,$model->datakaryawan['id_pd'],0,0,'L',false);
$pdf->Ln(7);

$pdf->Cell(30,7,"",0,0,'C',false); //untuk menggeser tulisan kekanan dengan isi data yang kosong
$pdf->Cell(50,7,$data3,0,0,'L',false);
$pdf->Cell(10,7,':',0,0,'L',false);
$pdf->Cell(45,7,$model->tgl,0,0,'L',false);
$pdf->Cell(30,7,"",0,0,'C',false); 
$pdf->Cell(50,7,$data2,0,0,'L',false);
$pdf->Cell(10,7,':',0,0,'L',false);
$pdf->Cell(45,7,strtoupper($model->datakaryawan['nm_pd']),0,0,'L',false);
$pdf->Ln(10);

//mulai tabel
//line break
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetFillColor(104,104,104); //Set background of the cell to be that grey color
        $pdf->SetTextColor(0,0,0);


$pdf->Cell(30,10,"",0,0,'C',false);
$pdf->Cell(135,10,"E A R N I N G S",1,0,'C',false);
$pdf->Cell(135,10,"D E D U C T I O N S",1,0,'C',false);
$pdf->Ln(10);

$pdf->SetFont('Times','',12);
$pdf->Cell(30,9,"",0,0,'C',false);
$pdf->Cell(67,9,"Basic Salary",1,0,'L',false);
if ($model->gj_pokok!=0) {
	$pdf->Cell(68,9,"Rp. ".number_format($model->gj_pokok),1,0,'L',false);
}else{
	$pdf->Cell(68,9," -",1,0,'L',false);
}
$pdf->Cell(67,9,"Loan",1,0,'L',false);
if ($model->deduc_basic!=0){
	$pdf->Cell(68,9,"Rp. ".number_format($model->deduc_basic),1,0,'L',false);
}else{
	$pdf->Cell(68,9," -",1,0,'L',false);
}
$pdf->Ln(9);

// $pdf->SetFont('Times','',12);
$pdf->Cell(30,9,"",0,0,'C',false);
$pdf->Cell(67,9,"Functional Allowances",1,0,'L',false);
if($model->functional!=0){
	$pdf->Cell(68,9,"Rp. ".number_format($model->functional),1,0,'L',false);
}else{
	$pdf->Cell(68,9," -",1,0,'L',false);
}
$pdf->SetFont('Times','',12);
$pdf->Cell(67,9,"Tax Income",1,0,'L',false);
if($model->pph_sebulan!=0){
	$pdf->Cell(68,9,"Rp. ".number_format($model->pph_sebulan),1,0,'L',false);
}else{
	$pdf->Cell(68,9," -",1,0,'L',false);
}
$pdf->Ln(9);

$pdf->SetFont('Times','',12);
$pdf->Cell(30,9,"",0,0,'C',false);
$pdf->Cell(67,9,"Overtime",1,0,'L',false);
if ($model->overtime!=0){
	$pdf->Cell(68,9,"Rp. ".number_format($model->overtime),1,0,'L',false);
}else {
	$pdf->Cell(68,9," -",1,0,'L',false);
}
$pdf->Cell(67,9,"BPJS Ketenagakerjaan",1,0,'L',false);
if($model->bpjs_kete!=0){
	$pdf->Cell(68,9,"Rp. ".number_format($model->bpjs_kete),1,0,'L',false);
}else{
	$pdf->Cell(68,9," -",1,0,'L',false);
}
$pdf->Ln(9);

$pdf->Cell(30,9,"",0,0,'C',false);
$pdf->Cell(67,9,"Transport Allowances",1,0,'L',false);
if($model->transport!=0){
	$pdf->Cell(68,9,"Rp. ".number_format($model->transport),1,0,'L',false);
}else{
	$pdf->Cell(68,9," -",1,0,'L',false);
}
$pdf->Cell(67,9,"BPJS Kesehatan",1,0,'L',false);
if($model->bpjs_kese!=0){
	$pdf->Cell(68,9,"Rp. ".number_format($model->bpjs_kese),1,0,'L',false);
}else{
	$pdf->Cell(68,9," -",1,0,'L',false);
}
$pdf->Ln(9);

$pdf->Cell(30,9,"",0,0,'C',false);
$pdf->Cell(67,9,"Field Allowances",1,0,'L',false);
if($model->tunjangan!=0){
	$pdf->Cell(68,9,"Rp. ".number_format($model->tunjangan),1,0,'L',false);
}else{
	$pdf->Cell(68,9," -",1,0,'L',false);
}
$pdf->Cell(67,9,"BPJS Pensiun",1,0,'L',false);
if($model->bpjs_pensi!=0){
	$pdf->Cell(68,9,"Rp. ".number_format($model->bpjs_pensi),1,0,'L',false);
}else{
	$pdf->Cell(68,9," -",1,0,'L',false);
}
$pdf->Ln(9);

$pdf->SetFont('Times','',12);
$pdf->Cell(30,9,"",0,0,'C',false);
$pdf->Cell(67,9,"Meals",1,0,'L',false);
if($model->breakfast!=0){
	$pdf->Cell(68,9,"Rp. ".number_format($model->breakfast),1,0,'L',false);
}else{
	$pdf->Cell(68,9," -",1,0,'L',false);
}
$pdf->Cell(67,9,"Tunjangan Kesehatan",1,0,'L',false);
if($model->tunj_kese!=0){
	$pdf->Cell(68,9,"Rp. ".number_format($model->tunj_kese),1,0,'L',false);
}else{
	$pdf->Cell(68,9," -",1,0,'L',false);
}
$pdf->Ln(9);

$pdf->SetFont('Times','b',12);
$pdf->Cell(30,9,"",0,0,'C',false);
$pdf->Cell(67,9,"Total Earnings",1,0,'L',false);
$pdf->Cell(68,9,"Rp. ".number_format($model->bruto),1,0,'L',false);
$pdf->Cell(67,9,"Total Deductions ",1,0,'L',false);
$pdf->Cell(68,9,"Rp. ".number_format($model->tot_deduc),1,0,'L',false);
$pdf->Ln(9);

$pdf->SetFont('Times','b','u',12);
$pdf->Cell(30,10,"",0,0,'C',false);
$pdf->Cell(202,10,"Nett Salary",1,0,'C',false);
// $pdf->SetDrawColor(80,175,5);
// $pdf->SetFillColor(175,183,50);
// $pdf->SetTextColor(0,0,0);
$pdf->Cell(68,10,"Rp. ".number_format($model->tot_gaji),1,0,'C',false);
$pdf->Ln(12);


//tanda tangan

//tanda tangan
//Baris baru
$pdf->SetFont('Times','b',12);
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(50);
$pdf->MultiCell(100,5,'Approved By',0,'L',false);
$pdf->setY($y);
$pdf->setX(250);
$pdf->MultiCell(80,5,'JAKARTA, '.date('d M Y'),0,'L',false);
//Baris baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(50);
$pdf->MultiCell(50,5,'Pegawai Ybs,',0,'L',false);
$pdf->setY($y);
$pdf->setX(256);
$pdf->MultiCell(80,5,'Direktur Utama,',0,'L',false);
$pdf->Ln(12);
//Baris baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(53);
$pdf->MultiCell(100,5,$model->datakaryawan['nm_pd'],0,'L',false);
$pdf->setY($y);
$pdf->setX(256);
$pdf->MultiCell(80,5,'Firman Alvianto',0,'L',false);

//Cetak PDF
$pdf->Output('('.date('dMY').')-'.$model->id_gaji.'-('.$model->datakaryawan['id_pd'].')-'.$model->datakaryawan['nm_pd'].'.pdf','I');
?>
