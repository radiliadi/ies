<meta http-equiv="refresh" content="180" >
<?php
$this->widget('booster.widgets.TbBreadcrumbs',
array(
    'links' => array('Chat',
    ),
)
);
?>
	<div class="col-md-12">		
		<!-- START NEW RECORD -->
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'login-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
		)); ?>
		<div class="panel panel-default">
			<div class="panel-body">
				<h3>What happened?</h3>
				<form class="form-horizontal" role="form">
				<div class="form-group">
					<div class="col-md-12">
						<div class="input-group">
							<span class="input-group-addon"><span class="fa fa-pencil-square-o"></span></span>
							<?php echo $form->textField($model,'chat_text',array('autofocus'=>'autofocus','placeholder'=>'Will reload every 2 minutes','class'=>"form-control")); ?>
						</div>  
							<br>
					</div>                                        
				</div>
				<div class="form-group">
					<div class="col-md-12">
					<div class="pull-left">
					</div>
						<div class="pull-right">
							<?php echo CHtml::submitButton("Kirim",array('class'=>"btn btn-primary")); ?>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
		<?php $this->endWidget(); ?>
		<!-- END NEW RECORD -->
		
	</div>	
	<div class="col-md-12">
		
		<!-- START TIMELINE -->
		<div class="timeline">
			<!-- START TIMELINE ITEM -->
			<?php
				foreach($modelTahun as $thn)
				{
					echo '
					<div class="timeline-item timeline-main">
						<div class="timeline-date">'.$thn->year.'</div>
					</div>
					';
						$no=1;						
						$criteria=new CDbCriteria;
						$criteria->order = "chat_date DESC";
						$criteria->condition = "DATE_FORMAT(chat_date,'%Y')=:date";
						$criteria->params = array (	
						':date' => $thn->year,
						);
						$criteria->addCondition("chat_status = 1");
						$sId=Yii::app()->session->get('id');
						// $sIdx=Yii::app()->session->get('id');
						$profil=User::model()->find('user_id=:user_id',array(':user_id'=>$sId));
						$model=Chat::model()->findAll($criteria);
						foreach($model as $db)
						{
							//Pengaturan posisi kiri atau kanan
							if($sId!==$db->user['user_id'])
							{
								$position="";
							}else{
								$position="timeline-item-right";
							}
							echo'
							<!-- START TIMELINE ITEM -->
							<div class="timeline-item '.$position.'">
								<div class="timeline-item-info" style="background:#656d78;"><font color="white">'.$db->chat_date.'</font></div>
								<div class="timeline-item-icon"><span class="fa fa-globe"></span></div>
								<div class="timeline-item-content">
									<div class="timeline-heading">
										';
											if($db->user['user_foto']!="") :
											echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$db->user['user_foto'],'Image') ; // Image shown here if page is update page
											else:
												echo '<img src="'.Yii::app()->request->baseUrl.'/images/no-image.jpg"/>';
											endif;
										echo '
										<a>'.$db->user['username'].'</a> as<a> '.$db->user['level'].' </a>
										<div class="btn-group pull-right">
                                        <td><a class="btn btn-default btn-sm pull-right" href="delete/id/'.$db->chat_id.'" title="Delete" confirm="a"><i class="glyphicon glyphicon-remove"></i></a></td>
                                   		</div> 
									</div>
									<div class="timeline-body">
										<p>'.$db->chat_text.'</p>
									</div>
								</div>
							</div>       
							<!-- END TIMELINE ITEM -->
							';
						$no++;
						}
				}
			?>
			<!-- END TIMELINE ITEM -->
			<!-- START TIMELINE ITEM -->
			<div class="timeline-item timeline-main">
				<div class="timeline-date"><a href="#"><span class="fa fa-arrow-up"></span></a></div>
			</div>                                
			<!-- END TIMELINE ITEM -->
		</div>
		<!-- END TIMELINE -->
		
	</div>