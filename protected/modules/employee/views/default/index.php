<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Beranda' ,
        ),
    )
);
?>
	<div class="col-md-12">

					<div class="widget widget-primary widget-item-icon">
						<div class="widget-item-right">
							<span class="fa fa-check"></span>
						</div>                             
						<div class="widget-data-left">
							<div class="widget-int num-count">SEA</div>
							<div class="widget-title"></div>
							<div class="widget-subtitle">SPARTAN ERAGON ASIA</div>
						</div>                                     
					</div>

				</div>
	<div class="col-md-12">

					<div class="widget widget-danger widget-padding-sm">
						<div class="widget-big-int plugin-clock">00:00</div>                            
						<div class="widget-subtitle plugin-date">Loading...</div>                       
						<div class="widget-buttons widget-c3">
							<div class="col">
								<a href="#"><span class="fa fa-clock-o"></span></a>
							</div>
							<div class="col">
								<a href="#"><span class="fa fa-bell"></span></a>
							</div>
							<div class="col">
								<a href="#"><span class="fa fa-calendar"></span></a>
							</div>
						</div>                            
					</div>                        

				</div>