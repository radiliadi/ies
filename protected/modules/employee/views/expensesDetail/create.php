<?php $this->widget('booster.widgets.TbBreadcrumbs',
	array('links' => array('Accounting'=>'../../../accounting/index',
		'Expenses'=>'../../../expenses/admin',
		'Detail' => '../../admin/id/'.$expenses->exp_id,
	    'Create',
	   	$expenses->exp_id,
	    ),
	)
);
?>
<div class="panel-heading">
	<h3><span class="fa fa-plus"></span> Create Expenses Detail (<?=$expenses->exp_desc;?>)</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>