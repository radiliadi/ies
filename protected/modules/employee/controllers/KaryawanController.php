<?php

class KaryawanController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
	return array(
	'accessControl', // perform access control for CRUD operations
	);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
	return array(
	array('allow',  // allow all users to perform 'index' and 'view' actions
	'actions'=>array('index','view','admin','create','adminn','pay'),
	'users'=>array('*'),
	),
	array('allow', // allow authenticated user to perform 'create' and 'update' actions
	'actions'=>array('create','update','exportExcel','delete','ajax'),
	'users'=>array('@'),
	),
	array('allow', // allow admin user to perform 'admin' and 'delete' actions
	'actions'=>array('admin','delete'),
	'users'=>array('admin'),
	),
	array('deny',  // deny all users
	'users'=>array('*'),
	),
	);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{		
		$model=new Karyawan;
		$modelObj=new LargeObject;
		$modeluser=new User;
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Karyawan']))
		{
			//cek form or step
			if(isset($_POST['Karyawan'] ['id_pd']))
			{

				$model->attributes=$_POST['Karyawan'];
					
				
					if(empty($_FILES['LargeObject']['tmp_name']['blob_content']))
					{
						$model->id_agama=null;
					}
				
				if($model->save())
				{
					//save user
					$modeluser->username=$model->id_pd;
					$modeluser->password=$model->id_pd;
					$modeluser->level='operator';
					$modeluser->save();
					
					//convert image to blob
					$modelBlob=LargeObject::model()->findAll('id_blob=:id_blob',array(':id_blob'=>$model->id_pd));
					foreach($modelBlob as $bl){
						$modelBlob=LargeObject::model()->findByPk($model->id_pd)->delete();
					}

					if(!empty($_FILES['LargeObject']['tmp_name']['blob_content']))
					{
						$modelObj->attributes=$_POST['LargeObject'];
						$model->attributes=$_POST['Karyawan'];
						$file = CUploadedFile::getInstance($modelObj,'blob_content');
						// $modelObj->fileName = $file->name;
						// $modelObj->fileType = $file->type;
						$fp = fopen($file->tempName, 'r');
						$content = fread($fp, filesize($file->tempName));
						fclose($fp);
						$modelObj->id_blob = $model->id_pd;
						$modelObj->blob_content = file_get_contents($file->tempName);
						if($modelObj->save())
						{

						}
						
					}else{
						$model->id_agama=null;
					}
						$this->render('step5',array(
						'model'=>$model,
						));
				}else{
					$this->render('create',array(
						'model'=>$model,
						'modelObj'=>$modelObj,
						'modeluser'=>$modeluser,
					));				
				}
				
			}
			
		}else{
		$this->render('create',array(
			'model'=>$model,
			'modelObj'=>$modelObj,
		));
		}
	}
	


public function actionUpdate($id)
{
		$model =Karyawan::model()->findByPk($id);
		$modelObj=new LargeObject;
		$modelImage =Largeobject::model()->findByPk($id);
		

		if(isset($_POST['Karyawan']))
		{
			//cek form or step
			if(isset($_POST['Karyawan'] ['id_pd']))
			{
				$model->attributes=$_POST['Karyawan'];
				if(empty($_FILES['LargeObject']['tmp_name']['blob_content']))
				{
					if($model->save())
					{
					}
				}
				else 
				{
					//hapus gambar di blob
					$modelBlob=LargeObject::model()->findAll('id_blob=:id_blob',array(':id_blob'=>$model->id_pd));
					foreach($modelBlob as $bl){
						$modelBlob=LargeObject::model()->findByPk($model->id_pd)->delete();
					}
						$modelObj->attributes=$_POST['LargeObject'];
						$model->attributes=$_POST['Karyawan'];
						$file = CUploadedFile::getInstance($modelObj,'blob_content');
						// $modelObj->fileName = $file->name;
						// $modelObj->fileType = $file->type;
						$fp = fopen($file->tempName, 'r');
						$content = fread($fp, filesize($file->tempName));
						fclose($fp);
						$modelObj->id_blob = $model->id_pd;
						$modelObj->blob_content = file_get_contents($file->tempName);
						if($modelObj->save())
						{

						}
											
				}
				$this->render('step5',array(
						'model'=>$model,
						));
								
			}
			
		}else{
		$this->render('update',array(
			'model'=>$model,
			'modelObj'=>$modelObj,
		));
		}
}



/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Karyawan');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdminn()
{
	
	$model=new Karyawan('search');
	// $model=Yii::app()->db->createCommand('SELECT * FROM Karyawan where stat_pd=TETAP')->queryAll();
	// $model = Karyawan::model()->findByAttritures(array(), 'stat_pd=TETAP');
	// $model=Karyawan::model()->find('stat_pd=:stat_pd', array('stat_pd'=>'TETAP'));
	// $model=Karyawan::model()->findColumn('stat_pd', 'stat_pd=TETAP');
	// $model=Karyawan::model()->findAllByAttributes(array('stat_pd'=>'TETAP'));
	// $model=Karyawan::model()->findAll(array(
 //    'condition'=>'stat_pd=:stat_pd',
 //    'params'=>array(':stat_pd'=>'TETAP')));
    // $model = Karyawan::model()->find('stat_pd="KONTRAK"');
    // $modell=Karyawan::model()->findAll(array('select'=>'*','condition'=>'stat_pd="KONTRAK"'));   
    $model=Karyawan::model()->findAll(array(
	                              'condition'=>'stat_pd="KONTRAK"',
	                             ));    
	$model->unsetAttributes();  // clear any default values
	if(isset($_GET['Karyawan']))
	$model->attributes=$_GET['Karyawan'];

	$this->render('admin',array(
	'model'=>$model,
	'modell'=>$modell,
	// 'data'=>$data,
	));
}

public function actionAdmin()
{
	$model=new Karyawan('search');
	$model->unsetAttributes();  // clear any default values
	if(isset($_GET['Karyawan']))
	$model->attributes=$_GET['Karyawan'];

	$this->render('admin',array(
	'model'=>$model,
	));
}

public function actionPay()
{
	$model=new Karyawan('search');
	$model->unsetAttributes();  // clear any default values
	if(isset($_GET['Karyawan']))
	$model->attributes=$_GET['Karyawan'];

	$this->render('pay',array(
	'model'=>$model,
	));
}



public function actionAjax(){
    $request=trim($_GET['term']);
    if($request!=''){
        $model=Wilayah::model()->findAll(array("condition"=>"nm_wil like '$request%'"));
        $data=array();
        foreach($model as $get){
            $data[]=$get->nm_wil;
        }
        $this->layout='empty';
        echo json_encode($data);
    }
}
	
public function actionLoadImage($id)
    {
        $model=LargeObject::model()->findAll('id_blob=:id_blob',array(':id_blob'=>$id));
        $this->renderPartial('image', array(
            'model'=>$model
        ));
    }
public function actionExportExcel()
    { 	
		if(isset($_POST['fileType']) && $_POST['Karyawan']){
			$model = new Karyawan();
			$model->attributes = $_POST['Karyawan'];
			if($_POST['fileType']== "Excel"){
				$this->widget('ext.EExcelView', array(
					'title'=>'Daftar Karyawan',
					'filename'=>'Karyawan',
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->search(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'Excel2007',
					'columns' => array(
						'id_pd',  
						'nm_pd',
						'divisi',
						'lokasi_kerja',
						'jk',
						'stat_pd',
					),
				));
				 
			} 
			if($_POST['fileType']== "CSV"){
				$this->widget('ext.tcPDF', array(
					'title'=>'Daftar Karyawan',
					'filename'=>'Karyawan',
					'grid_mode'=>'export',
					'stream'=>true,
					'dataProvider' => $model->search(),
					'filter'=>$model,
					'locked'=>array('A1:A10', 'B1:B10'),
					'grid_mode'=>'export',
					'exportType'=>'CSV',
					'columns' => array(
						'id_pd',  
						'nm_pd',
						'divisi',
						'lokasi_kerja',
						'jk',
						'stat_pd',
					),
				));
				 
			}
		}
    }

public function loadModel($id)
{
	$model=Karyawan::model()->findByPk($id);
	if($model===null)
	throw new CHttpException(404,'The requested page does not exist.');
	return $model;
}

public function actionKwnAjax()
	{
		$jurusan=Negara::model()->findAll('id_negara = :id_negara',
			array(':id_negara'=>(int) $_POST['apo'])
		);
		
		
		foreach($jurusan as $db){}
		echo $db->nm_negara;
	}
	
	public function tgl($yr)
	{
		if(isset($yr)){
			if($yr!==''){
				$tgl=$yr[0].$yr[1];
				$bln=$yr[3].$yr[4];
				$thn=$yr[6].$yr[7].$yr[8].$yr[9];
				$hasil=$thn.'-'.$bln.'-'.$tgl;
			}else{
				$hasil='';
			}
		}else{
			$hasil='';
		}
		return $hasil;
	}
	
	public function bulan($i)
	{
		switch($i)
		{
			case 1:
			$bulan="Januari";
			break;
			
			case 2:
			$bulan="Februari";
			break;
			
			case 3:
			$bulan="Maret";
			break;
			
			case 4:
			$bulan="April";
			break;
			
			case 5:
			$bulan="Mei";
			break;
			
			case 6:
			$bulan="Juni";
			break;
			
			case 7:
			$bulan="Juli";
			break;
			
			case 8:
			$bulan="Agustus";
			break;
			
			case 9:
			$bulan="September";
			break;
			
			case 10:
			$bulan="Oktober";
			break;
			
			case 11:
			$bulan="November";
			break;
			
			case 12:
			$bulan="Desember";
			break;
		}
			return $bulan;
	}


protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='mahasiswa-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
