<?php

class DefaultController extends Controller
{
	
	
	public function  actionIndex()
	{
		// $modelDivisi=Karyawan::model()->findAll();

		$criteria=new CDbCriteria;
		$criteria->select = "count(user_id) as user_id";
		$modelUser=User::model()->find($criteria);
		
		$criteria=new CDbCriteria;
		$criteria->select = "count(view_id) as view_id";
		$modelView=Viewer::model()->find($criteria);
		
		$criteria=new CDbCriteria;
		$criteria->select = "count(id_pd) as id_pd";
		$modelKaryawan=Karyawan::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->select = "count(id_pd) as id_pd";
		$criteria->condition = "stat_pd=:stat_pd";
		$criteria->params = array (	
			':stat_pd'=>"TETAP",
		);
		$modelJKaryawan=Karyawan::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->select = "count(id_pd) as id_pd";
		$criteria->condition = "divisi=:divisi";
		$criteria->params = array (	
			':divisi'=>"Management",
		);
		$modelDivisi1=Karyawan::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->select = "count(id_pd) as id_pd";
		$criteria->condition = "divisi=:divisi";
		$criteria->params = array (	
			':divisi'=>"Finance",
		);
		$modelDivisi2=Karyawan::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->select = "count(id_pd) as id_pd";
		$criteria->condition = "divisi=:divisi";
		$criteria->params = array (	
			':divisi'=>"Managed Services",
		);
		$modelDivisi3=Karyawan::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->select = "count(id_pd) as id_pd";
		$criteria->condition = "stat_pd=:stat_pd";
		$criteria->params = array (	
			':stat_pd'=>"KONTRAK",
		);
		$modelJJKaryawan=Karyawan::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->select = "count(id_tender) as id_tender";
		$criteria->condition = "tender_type=:tender_type";
		$criteria->params = array (	
			':tender_type'=>"Tender",
		);
		$modelTender=Tender::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->select = "count(chat_id) as chat_id";
		$modelChat=Chat::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->select = "count(id_tender) as id_tender";
		$criteria->condition = "tender_status=:tender_status AND tender_type=:tender_type";
		$criteria->params = array (	
			':tender_type'=>"Tender",
			':tender_status'=>"Win",
		);
		$win=Tender::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->select = "count(id_tender) as id_tender";
		$criteria->condition = "tender_status=:tender_status";
		$criteria->params = array (	
			':tender_status'=>"Failed",
		);
		$failed=Tender::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->select = "count(id_tender) as id_tender";
		$criteria->condition = "tender_status=:tender_status";
		$criteria->params = array (	
			':tender_status'=>"Decline",
		);
		$decline=Tender::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->select = "count(id_tender) as id_tender";
		$criteria->condition = "tender_status=:tender_status";
		$criteria->params = array (	
			':tender_status'=>"Openbid",
		);
		$openbid=Tender::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->select = "count(id_tender) as id_tender";
		$criteria->condition = "tender_status=:tender_status";
		$criteria->params = array (	
			':tender_status'=>"Losit",
		);
		$losit=Tender::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->select = "count(id_tender) as id_tender";
		$criteria->condition = "tender_status=:tender_status";
		$criteria->params = array (	
			':tender_status'=>"Pending",
		);
		$pending=Tender::model()->find($criteria);

		$location = Absensi::model()->findAll();
		
		$sql='SELECT count(username),level FROM user GROUP BY level';
		
		$dataProvider=new CSqlDataProvider($sql,array(
                            'keyField' => 'username',
		));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'modelUser'=>$modelUser,
			'modelView'=>$modelView,
			'modelTender'=>$modelTender,
			'modelKaryawan'=>$modelKaryawan,
			'modelJKaryawan'=>$modelJKaryawan,
			'modelJJKaryawan'=>$modelJJKaryawan,
			'modelDivisi1'=>$modelDivisi1,
			'modelDivisi2'=>$modelDivisi2,
			'modelDivisi3'=>$modelDivisi3,
			'modelChat'=>$modelChat,
			'win'=>$win,
			'failed'=>$failed,
			'decline'=>$decline,
			'openbid'=>$openbid,
			'losit'=>$losit,
			'pending'=>$pending,
			'location'=>$location,
		));
	}
	
	public function actionLaporan()
	{		
		//Hitung Total
		$criteria=new CDbCriteria;
		$criteria->select = "*,(select sum(keu_budget) as total FROM keuangan k WHERE k.keu_kat=:keu_kat AND keu_tipe='debet') as debet,(select sum(keu_budget) as total FROM keuangan k WHERE k.keu_kat=:keu_kat AND keu_tipe='kredit') as kredit";
		$criteria->condition = "keu_kat=:keu_kat";
		$criteria->params = array (	
			':keu_kat'=>"kas",
		);
		$modelKas=Keuangan::model()->find($criteria);
		//Zakat
		$criteria->params = array (	
			':keu_kat'=>"zakat",
		);
		$modelZakat=Keuangan::model()->find($criteria);
		//Tim
		$criteria=new CDbCriteria;
		$criteria->select = "*,sum(user_gaji) as user_gaji";
		$criteria->condition = "user_gaji>:user_gaji";
		$criteria->params = array (	
			':user_gaji'=>"0",
		);
		$modelTim=User::model()->find($criteria);
		//Projek
		$criteria=new CDbCriteria;
		$criteria->select = "count(pro_id) as pro_id, sum(pro_budget) as pro_budget";
		$modelProject=Project::model()->find($criteria);
		//User
		$criteria=new CDbCriteria;
		$criteria->select = "count(user_id) as user_id";
		$modelUser=User::model()->find($criteria);
		//Visitor/Viewer
		$criteria=new CDbCriteria;
		$criteria->select = "count(view_id) as view_id";
		$modelView=Viewer::model()->find($criteria);
		//Kantor
		$ofc_tgl=23;
		$ofc_bln=04;
		$ofc_tgl_full='2015-'.$ofc_tgl.'-'.$ofc_tgl;
		//render
		$this->render('laporan',array(
			'modelKas'=>$modelKas,
			'modelZakat'=>$modelZakat,
			'modelTim'=>$modelTim,
			'modelProject'=>$modelProject,
			'modelUser'=>$modelUser,
			'modelView'=>$modelView,
			'ofc_tgl'=>$ofc_tgl,
			'ofc_bln'=>$ofc_bln,
			'ofc_tgl_full'=>$ofc_tgl_full,
		));
	}
	
	public function bulan($id)
	{
		$bulan="";
		switch($id)
		{
			case 01:$bulan="Januari";break;
			case 02:$bulan="Februari";break;
			case 03:$bulan="Maret";break;
			case 04:$bulan="April";break;
			case 05:$bulan="Mei";break;
			case 06:$bulan="Juni";break;
			case 07:$bulan="Juli";break;
			case 8:$bulan="Agustus";break;
			case 9:$bulan="September";break;
			case 10:$bulan="Oktober";break;
			case 11:$bulan="November";break;
			case 12:$bulan="Desember";break;
		}
		return $bulan;
	}
	
	//Hitung hari pembayaran kantor di laporan
	public function timeDiff($firstTime,$lastTime){
	   // convert to unix timestamps
	   $firstTime=strtotime($firstTime);
	   $lastTime=strtotime($lastTime);

	   // perform subtraction to get the difference (in seconds) between times
	   //hari
	   $timeDiff=($lastTime-$firstTime)/(3600*24);
	   //bulan
	   if($timeDiff>30):
			$total=$timeDiff;
			$bulan=number_format($timeDiff/30);
			$hari=$timeDiff%30;
			$timeDiff=$bulan.' bulan, '.$hari.' hari ('.$total.')';
		else:
			$timeDiff.='';
	   endif;

	   // return the difference
	   return $timeDiff;
	}
}

