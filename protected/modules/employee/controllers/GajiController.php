<?php

class GajiController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','slip'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(''),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id_gaji)
	{
		
		$model=Gaji::model()->find('id_gaji=:id_gaji', array(':id_gaji'=>$id_gaji));
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($id)
	{
	
		if(isset($_POST['Gaji']['tahun']))
		{	
			if($_POST['Gaji']['tahun']!=''){
			$tahun= $_POST['Gaji']['tahun'];
			}
			else{
			$tahun=date('Y');
			}
		}
		else{
		$tahun=date('Y');
		}
		
		$modelkaryawan=Karyawan::model()->find('id_pd=:id_pd', array(':id_pd'=>$id));
		$model=Gaji::model()->findAll('id_pd=:id_pd AND DATE_FORMAT (tgl,("%Y"))=:thn', array(':id_pd'=>$id , ':thn'=>$tahun));
		
		$modelbulan=new Gaji;
		$this->render('index',array(
			'model'=>$model,
			'modelkaryawan'=>$modelkaryawan,
			'modelbulan'=>$modelbulan,
			'tahun'=>$tahun,
		));
	}
	
	
	//aksi slip gaji pegawai
	public function actionSlip($id_gaji)
	{
		$model=Gaji::model()->find('id_gaji=:id_gaji', array(':id_gaji'=>$id_gaji));
		//extensions fpdf
		$pdf = new fpdf();
		//Deklarasi
		$header0="PT. SPARTAN ERAGON ASIA";
		$header="Bona Bisnis Center No. 8J Lt.2, Jl. Karang Tengah Raya, Jakarta Selatan";
		$judul="PAYSLIP";
		$data1="EMPLOYEE NUMBER";
		$data2="EMPLOYEE NAME";
		$data3="PAYROLL DATE";
		$data4="NO. PAYSLIP";
		
		//Render
		$this->renderPartial('slip',array(
			'pdf'=>$pdf,
			'judul'=>$judul,
			'header0'=>$header0,
			'header'=>$header,
			'data1'=>$data1,
			'data2'=>$data2,
			'data3'=>$data3,
			'data4'=>$data4,
			'model'=>$model,
			
		));
	}
	
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Gaji the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Gaji::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Gaji $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gaji-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	//tanggal lahir : mulai --------------------------------------------------------------------------------------
	public function bulan($i)
	{
		switch($i)
		{
			case 1:
			$bulan="Januari";
			break;
			
			case 2:
			$bulan="Februari";
			break;
			
			case 3:
			$bulan="Maret";
			break;
			
			case 4:
			$bulan="April";
			break;
			
			case 5:
			$bulan="Mei";
			break;
			
			case 6:
			$bulan="Juni";
			break;
			
			case 7:
			$bulan="Juli";
			break;
			
			case 8:
			$bulan="Agustus";
			break;
			
			case 9:
			$bulan="September";
			break;
			
			case 10:
			$bulan="Oktober";
			break;
			
			case 11:
			$bulan="November";
			break;
			
			case 12:
			$bulan="Desember";
			break;
		}
			return $bulan;
	}
	//tanggal lahir : selesai --------------------------------------------------------------------------------------
	
}
?>