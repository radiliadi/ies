<?php

class ExpensesDetailController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','index','view'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{
		$model=new ExpensesDetail;

		$model->expd_exp_id = $id;
		$expenses = Expenses::model()->findByAttributes(array('exp_id'=>$id, 'exp_status'=>1));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ExpensesDetail']))
		{
			$model->attributes=$_POST['ExpensesDetail'];
			if($model->save())
				$this->redirect(array('expensesDetail/admin/id/'.$model->expd_exp_id));
		}

		$this->render('create',array(
			'model'=>$model,
			'expenses'=>$expenses,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$id_pd=Yii::app()->session->get('username');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		// $expenses = Expenses::model()->findByAttributes(array())

		if(isset($_POST['ExpensesDetail']))
		{
			$model->attributes=$_POST['ExpensesDetail'];
			if(!$model->save()){
				echo '<pre>';
				print_r($model->getErrors());
				echo '</pre>';
				die;
			}else{
				$expenses = Expenses::model()->findByAttributes(array('exp_id'=>$model->expd_exp_id, 'exp_status'=>1));
				$expenses->exp_datetime_update 	= date('Y-m-d H:i:s');
				$expenses->exp_update_by 		= $id_pd;
				$expenses->SaveAttributes(array('exp_datetime_update', 'exp_update_by'));
				$this->redirect(array('expensesDetail/admin/id/'.$model->expd_exp_id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = ExpensesDetail::model()->findByAttributes(array('expd_id'=>$id));
		$model->expd_status = 2;
		if($model->SaveAttributes(array('expd_status'))){
			return $this->redirect(Yii::app()->request->urlReferrer);
		}else{
			throw new CHttpException('CNTRL 0 :' ,'ERROR DELETED EXPENSES DETAIL');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ExpensesDetail');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id)
	{
		// $model=new ExpensesDetail('search');

		$model=new ExpensesDetail;
		$model->expd_exp_id=$id;
		// $modelkaryawan=Karyawan::model()->find('id_pd=:id_pd', array(':id_pd'=>'SEA-001-ADI'));
		$expensesDetailId=ExpensesDetail::model()->find('expd_exp_id=:expd_exp_id AND expd_status=1', array(':expd_exp_id'=>$id));
		$expensesDetail=ExpensesDetail::model()->findAll('expd_exp_id=:expd_exp_id AND expd_status=1', array(':expd_exp_id'=>$id));
		$karyawan=Karyawan::model()->find('id_pd=:id_pd', array(':id_pd'=>$expensesDetailId->expd_id_pd));
		$expenses=Expenses::model()->findByAttributes(array('exp_id'=>$id, 'exp_status'=>1));
		$model->findByAttributes(array('expd_exp_id'=>$id));
		// echo '<pre>';
		// // print_r($modelkaryawan->id_pd);
		// print_r($modeled->expd_exp_id).'</br>';
		// echo '<br>';
		// print_r($modeled->expd_id_pd).'</br>';
		// echo '</pre>';
		// die;
		// $model->findAll(array('condition'=>'expd_status = 2'));
		// $model->expd_exp_id=$id;
		// $modeled->unsetAttributes();  // clear any default values
		// if(isset($_GET['ExpensesDetail']))
		// 	$modeled->attributes=$_GET['ExpensesDetail'];


		$this->render('admin',array(
			'model'=>$model,
			'expensesDetail'=>$expensesDetail,
			'expensesDetailId'=>$expensesDetailId,
			'expenses'=>$expenses,
			'karyawan'=>$karyawan,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ExpensesDetail the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ExpensesDetail::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ExpensesDetail $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='expenses-detail-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
