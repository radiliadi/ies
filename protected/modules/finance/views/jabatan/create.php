<?php
/* @var $this JabatanController */
/* @var $model Jabatan */

$this->breadcrumbs=array(
	'Jabatan'=>array('admin'),
	'Create',
);

?>

<h1>Tambah Jabatan</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>