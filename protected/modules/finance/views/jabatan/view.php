<?php
/* @var $this JabatanController */
/* @var $model Jabatan */

$this->breadcrumbs=array(
	'Jabatan'=>array('admin'),
	$model->id_jabatan,
);
?>

<h1>View Jabatan #<?php echo $model->id_jabatan; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_jabatan',
		'nm_jabatan',
	),
)); ?>
