<div class="form"> 
<?php
/* @var $this GajiController */
/* @var $model Gaji */

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'karyawan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data',
		'role'=>'form',
	),
));

$level=Yii::app()->session->get('level');

echo '
<div class="box col-md-10" style="width:1100px; background-color:white">
        <div class="box col-md-10 alert alert-info" style="width:1070px">
                <h2><i class="glyphicon icon-money "></i> Data Gaji Pegawai </h2>
		</div>
			';
						

$this->breadcrumbs=array(
	'Gajis'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Gaji', 'url'=>array('index')),
	array('label'=>'Create Gaji', 'url'=>array('create')),
);

?>
	
	
	
<div class="row">
    <div class="box col-md-12 ">    
		<div class="box-content">
			<h1></h1>
			<table>
				<tbody>
					<tr>
						<td><b>Nama Lengkap</b></td>
						<td class="center">:&nbsp; &nbsp;<?php echo $modelkaryawan->nm_pd;?></td>
					</tr>
					<tr>
						<td><b>NIP</b></td>
						<td class="center">:&nbsp; &nbsp;<?php echo $modelkaryawan->id_pd;?></td>
					</tr>
					<tr>
						<td><b>Tahun</b></td>
						<td>:&nbsp; &nbsp; <?php echo $tahun?> &nbsp;</td>
						<td class="center"> 
							<?php
								for($i=2010;$i<=date('Y');$i++){
								$thn[$i]=$i;
								}
							?>
								<div> &nbsp;
									<div style="float:left;margin:5px;">
									<?php 
									echo $form->dropDownList($modelbulan,'tahun',$thn,array('empty'=>'Pilih Tahun','class'=>"form-control", 'style'=>"float:left;")); 
									?>															
									</div>
								</div>	
						</td>
						<td>	
							<?php echo CHtml::submitButton('Lihat', array('class'=>'btn btn-default', 'style'=>'width:100px')); ?>
						
						</td>
					</tr>
				</tbody>
			</table>	
	
<div class="panel-body">	  
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover" id="" style="background-color:#FFE4E1">
	
			<thead>
				<tr>
					<th>No</th>
					<th>&nbsp; &nbsp; &nbsp; Bulan</th>
					<th>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Gaji Pokok</th>
					<th>&nbsp; &nbsp; Tunjangan</th>
					<th>&nbsp; &nbsp; Potongan</th>
					<th>&nbsp; &nbsp; Total</th>
					<th>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						Aksi</th>
				</tr>
			</thead>
    <tbody>
	
		<?php 
		$no=1;
		foreach($model as $db)
		{ 
			$modelkaryawan=Karyawan::model()->findByPk($db->id_pd);
			
			echo'
			<tr>
				<td>'.$no.'</td>
				<td>'.$db->bulan.' </td>
				<td class="center">'.$db->gj_pokok.'</td>
				<td class="center">'.$db->tunjangan.'</td>
				<td class="center">'.$db->potongan.'</td>
				<td class="center">'.$db->tot_gaji.'</td>
				<td class="center">
					&nbsp;
					<a class="btn btn-success" href="../../view/id_gaji/'.$db->id_gaji.'">
						<i class="icon-list icon-white"></i>
						Detail
					</a>
					&nbsp;	
					<a class="btn btn-info" href="../../delete/id_gaji/'.$db->id_gaji.'">
						<i class="icon-remove icon-white"></i>
						Hapus
					</a>
				</td>
			</tr>
			';
		$no++;
		}
		?>
	
	
		<tr></tr>
			
		<th class="center" colspan="7">		
			<div style="float:left;margin:10px;">
			Masukkan Tanggal Gajian
			</div>
			
			<?php
				//ini utk tanggal : start-----------------------------------------------------------
				for($i=1;$i<=31;$i++){
					$tgl1[$i]=$i;
					}
			?>
			<?php
				for($i=1;$i<=12;$i++){
					$bln1[$i]=$this->bulan($i);
					}
			?>
			<?php
				for($i=2000;$i<=date('Y');$i++){
					$thn1[$i]=$i;
					}
			?>
			<div>
				<div style="float:left;margin:5px;">
					<?php 
						echo $form->dropDownList($modeltanggal,'tanggal1',$tgl1,array('empty'=>'Pilih Tanggal','class'=>"form-control", 'style'=>"float:left;margin;")); 
					?>
				</div>
				<div style="float:left;margin:5px;">
					<?php 
						echo $form->dropDownList($modeltanggal,'bulan1',$bln1,array('empty'=>'Pilih Bulan','class'=>"form-control", 'style'=>"float:center;")); 
					?>
				</div>
				<div style="float:left;margin:5px;">
					<?php 
						echo $form->dropDownList($modeltanggal,'tahun1',$thn1,array('empty'=>'Pilih Tahun','class'=>"form-control", 'style'=>"float:left;",)); 
					//ini utk tgl : end----------------------------------------------
					?>															
				</div>
			</div>	
		</th>
		<tr></tr>
		
		
		<th class="center" colspan="7">
			<a class="btn btn-default" href="../../../pegawai/admin">
				<i class="icon-arrow-left"></i>
				Kembali
			</a>
		<?php echo CHtml::submitButton('Tambah', array('class'=>'btn btn-default', 'style'=>'width:100px')); ?>
		
		</th>
		
		
    </tbody>
</table>			
	
		</div>
	</div>
</div>

			<!-- ?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'gaji-grid',
				'dataProvider'=>$model->search(),
				'filter'=>$model,
				'columns'=>array(
					'id',
					'nip',
					'nama'=>$modelpegawai->attributes=$_GET['Pegawai'];
					/*
					'tgl',
					'gj_pokok',
					'tunjangan',
					'potongan',
					'tot_gaji',
					*/
					
					array(
						'class'=>'CButtonColumn',
					),
				),
			)); ?-->
		</div>	
	</div>
</div>
<?php $this->endWidget(); ?>
</div>