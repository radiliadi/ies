<?php
/* @var $this GajiController */
/* @var $model Gaji */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_gaji'); ?>
		<?php echo $form->textField($model,'id_gaji'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_pd'); ?>
		<?php echo $form->textField($model,'id_pd'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl'); ?>
		<?php echo $form->textField($model,'tgl'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bulan'); ?>
		<?php echo $form->textField($model,'bulan',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tahun'); ?>
		<?php echo $form->textField($model,'tahun'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gj_pokok'); ?>
		<?php echo $form->textField($model,'gj_pokok'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tunjangan'); ?>
		<?php echo $form->textField($model,'tunjangan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'potongan'); ?>
		<?php echo $form->textField($model,'potongan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tot_gaji'); ?>
		<?php echo $form->textField($model,'tot_gaji'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'neto_setahun'); ?>
		<?php echo $form->textField($model,'neto_setahun'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wp_pribadi'); ?>
		<?php echo $form->textField($model,'wp_pribadi'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wp_menikah'); ?>
		<?php echo $form->textField($model,'wp_menikah'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tanggungan'); ?>
		<?php echo $form->textField($model,'tanggungan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pajak_setahun'); ?>
		<?php echo $form->textField($model,'pajak_setahun'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pph_tahun'); ?>
		<?php echo $form->textField($model,'pph_tahun'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pph_bulan'); ?>
		<?php echo $form->textField($model,'pph_bulan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tot_pinjaman'); ?>
		<?php echo $form->textField($model,'tot_pinjaman'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->