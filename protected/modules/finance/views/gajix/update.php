<?php
/* @var $this GajiController */
/* @var $model Gaji */

$this->breadcrumbs=array(
	'Gajis'=>array('index'),
	$model->id_gaji=>array('view','id'=>$model->id_gaji),
	'Update',
);

$this->menu=array(
	array('label'=>'List Gaji', 'url'=>array('index')),
	array('label'=>'Create Gaji', 'url'=>array('create')),
	array('label'=>'View Gaji', 'url'=>array('view', 'id'=>$model->id_gaji)),
	array('label'=>'Manage Gaji', 'url'=>array('admin')),
);
?>

<h1>Update Gaji <?php echo $model->id_gaji; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>