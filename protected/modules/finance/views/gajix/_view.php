<?php
/* @var $this GajiController */
/* @var $data Gaji */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_gaji')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_gaji), array('view', 'id'=>$data->id_gaji)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pd')); ?>:</b>
	<?php echo CHtml::encode($data->id_pd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl')); ?>:</b>
	<?php echo CHtml::encode($data->tgl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bulan')); ?>:</b>
	<?php echo CHtml::encode($data->bulan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tahun')); ?>:</b>
	<?php echo CHtml::encode($data->tahun); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gj_pokok')); ?>:</b>
	<?php echo CHtml::encode($data->gj_pokok); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tunjangan')); ?>:</b>
	<?php echo CHtml::encode($data->tunjangan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('potongan')); ?>:</b>
	<?php echo CHtml::encode($data->potongan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tot_gaji')); ?>:</b>
	<?php echo CHtml::encode($data->tot_gaji); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('neto_setahun')); ?>:</b>
	<?php echo CHtml::encode($data->neto_setahun); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wp_pribadi')); ?>:</b>
	<?php echo CHtml::encode($data->wp_pribadi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wp_menikah')); ?>:</b>
	<?php echo CHtml::encode($data->wp_menikah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggungan')); ?>:</b>
	<?php echo CHtml::encode($data->tanggungan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pajak_setahun')); ?>:</b>
	<?php echo CHtml::encode($data->pajak_setahun); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pph_tahun')); ?>:</b>
	<?php echo CHtml::encode($data->pph_tahun); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pph_bulan')); ?>:</b>
	<?php echo CHtml::encode($data->pph_bulan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tot_pinjaman')); ?>:</b>
	<?php echo CHtml::encode($data->tot_pinjaman); ?>
	<br />

	*/ ?>

</div>