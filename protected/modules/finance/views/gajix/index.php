<?php
/* @var $this GajiController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Gajis',
);

$this->menu=array(
	array('label'=>'Create Gaji', 'url'=>array('create')),
	array('label'=>'Manage Gaji', 'url'=>array('admin')),
);
?>

<h1>Gajis</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
