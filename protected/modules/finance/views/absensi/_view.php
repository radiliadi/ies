<?php
/* @var $this AbsensiController */
/* @var $data Absensi */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('abs_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->abs_id), array('view', 'id'=>$data->abs_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('abs_jkk_tetap')); ?>:</b>
	<?php echo CHtml::encode($data->abs_jkk_tetap); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('abs_jkk_kontrak')); ?>:</b>
	<?php echo CHtml::encode($data->abs_jkk_kontrak); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('abs_timesheet')); ?>:</b>
	<?php echo CHtml::encode($data->abs_timesheet); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('abs_lembur')); ?>:</b>
	<?php echo CHtml::encode($data->abs_lembur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('latitude')); ?>:</b>
	<?php echo CHtml::encode($data->latitude); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('longitude')); ?>:</b>
	<?php echo CHtml::encode($data->longitude); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('abs_lat')); ?>:</b>
	<?php echo CHtml::encode($data->abs_lat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('abs_lon')); ?>:</b>
	<?php echo CHtml::encode($data->abs_lon); ?>
	<br />

	*/ ?>

</div>