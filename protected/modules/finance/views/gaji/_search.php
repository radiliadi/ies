<?php
/* @var $this GajiController */
/* @var $model Gaji */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nip'); ?>
		<?php echo $form->textField($model,'nip'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl'); ?>
		<?php echo $form->textField($model,'tgl',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gj_pokok'); ?>
		<?php echo $form->textField($model,'gj_pokok'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tunjangan'); ?>
		<?php echo $form->textField($model,'tunjangan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'potongan'); ?>
		<?php echo $form->textField($model,'potongan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tot_gaji'); ?>
		<?php echo $form->textField($model,'tot_gaji'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->