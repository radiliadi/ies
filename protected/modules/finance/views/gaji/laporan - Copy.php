<?php
$pdf->AliasNbPages();
$pdf->AddPage('P',$size);                            
$pdf->SetFont('Times','b',18);     
$pdf->Image('images/png.png',40,10,25,25);      
$pdf->Ln(1);
$pdf->MultiCell(340,15,$header0,0,'C',false);
$pdf->MultiCell(340,5,$header,0,'C',false);
$pdf->Ln(16); //Line break
$pdf->SetFont('Times','',18);
$pdf->MultiCell(340,5,$judul,0,'C',false);
$pdf->Ln(5); //Line break

//mulai tabel
//line break
$pdf->Ln(2);
        $pdf->SetFont('Times','b',12);
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetFillColor(104,104,104); //Set background of the cell to be that grey color
        $pdf->SetTextColor(0,0,0);
		
$pdf->Cell(30,5,'',0,0,'C',false);
$pdf->Cell(20,9,'No',1,0,'C',false);
$pdf->Cell(50,9,'NIP',1,0,'C',false);
$pdf->Cell(80,9,'NAMA LENGKAP',1,0,'C',false);
$pdf->Cell(40,9,'BULAN',1,0,'C',false);
$pdf->Cell(40,9,'GOLONGAN',1,0,'C',false);
$pdf->Cell(40,9,'TOTAL GAJI(Rp.)',1,0,'C',false);
$pdf->Ln(9); //Line break

//Data dari database
		$no=1;
		foreach($model as $db)
		{
			$pdf->SetFont('Times','',12);
			$pdf->Cell(30,5,'',0,0,'C',false);
			$pdf->Cell(20,7,$no,1,0,'C',false);
			$pdf->Cell(50,7,$db->datakaryawan['id_pd'],1,0,'L',false);
			$pdf->Cell(80,7,$db->datakaryawan['nm_pd'],1,0,'L',false);
			$pdf->Cell(40,7,$db->bulan,1,0,'C',false);
			$pdf->Cell(40,7,$db->datajabatan['nm_jabatan'],1,0,'C',false);
			$pdf->Cell(40,7,$db->tot_gaji,1,0,'C',false);
			$pdf->Ln(7);
			$no++;
		}
		
//selesai tabel

//tanda tangan
//Baris baru
$pdf->SetFont('Times','',12);
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(258);
$pdf->MultiCell(80,5,'PEKANBARU, '.date('d M Y'),0,'L',false);
//Baris Baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(254);
$pdf->MultiCell(80,5,'Ketua STAI Al-Azhar Pekanbaru,',0,'L',false);
$pdf->Ln(30);
//Baris baru
$y=$pdf->getY();
$pdf->setY($y);
$pdf->setX(258);
$pdf->MultiCell(80,5,'DR.Hj.Maimanah Umar, MA',0,'L',false);




//Cetak PDF
// $pdf->Output('('.date('dMY').')-'.$model->id_gaji.'-'.$model->datakaryawan['id_pd'].'-'.$model->datakaryawan['nm_pd'].'.pdf','I');
$pdf->Output('('.date('dMY').')-'.$judul.'.pdf','I');
?>