<?php
$level=Yii::app()->session->get('level');

?>

<?php
/* @var $this GajiController */
/* @var $dataProvider CActiveDataProvider */

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'pegawai-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data',
		'role'=>'form',
	),
));


echo '<div class="box col-md-10" style="width:1100px; background-color:white">
        <div class="box col-md-10 alert alert-info" style="width:1070px">
                <h2><i class="glyphicon icon-money "></i> Data Gaji Pegawai </h2>
		</div>
			';

$this->breadcrumbs=array(
	'Gajis',
);

$this->menu=array(
	array('label'=>'Create Gaji', 'url'=>array('create')),
	array('label'=>'Manage Gaji', 'url'=>array('admin')),
);
?>

<div class="row">
    <div class="box col-md-12 ">    
		<div class="box-content">
			<h1></h1>
			<table>
				<tbody>
					<tr>
						<td><b>Nama Lengkap</b></td>
						<td class="center">:&nbsp; &nbsp;<?php echo $modelpegawai->nama;?></td>
					</tr>
					<tr>
						<td><b>NIP</b></td>
						<td class="center">:&nbsp; &nbsp;<?php echo $modelpegawai->nip;?></td>
					</tr>
					<tr>
						<td><b>Tahun</b></td>
						<td>:&nbsp; &nbsp; <?php echo $tahun?> &nbsp;</td>
						<td class="center"> 
							<?php
								for($i=2000;$i<=date('Y');$i++){
								$thn[$i]=$i;
								}
							?>
								<div> &nbsp;
									<div style="float:left;margin:5px;">
									<?php 
									echo $form->dropDownList($modelbulan,'tahun',$thn,array('empty'=>'Pilih Tahun','class'=>"form-control", 'style'=>"float:left;")); 
									?>															
									</div>
								</div>	
						</td>
						<td>	
							<?php echo CHtml::submitButton('Lihat', array('class'=>'btn btn-default', 'style'=>'width:100px')); ?>
						
						</td>
					</tr>
				</tbody>
			</table>	
		
<div class="panel-body">	  
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover" id="" style="background-color:#FFE4E1">
	
			<thead>
				<tr>
					<th>No</th>
					<th>&nbsp; &nbsp; &nbsp; Bulan</th>
					<th>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Gaji Pokok</th>
					<th>&nbsp; &nbsp; Tunjangan</th>
					<th>&nbsp; &nbsp; Potongan</th>
					<th>&nbsp; &nbsp; Total</th>
					<th>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						Aksi</th>
				</tr>
			</thead>
    <tbody>
		<?php 
			$no=1;
			foreach($model as $db)
			{ 
				$modelpegawai=Pegawai::model()->findByPk($db->nip);
				echo'
				<tr>
					<td>'.$no.'</td>
					<td>'.$db->bulan.' </td>
					<td class="center">'.$db->gj_pokok.'</td>
					<td class="center">'.$db->tunjangan.'</td>
					<td class="center">'.$db->potongan.'</td>
					<td class="center">'.$db->tot_gaji.'</td>';
		?>
			<td class="center">
				<?php 			
					echo'
					<a class="btn btn-success" href="../../view/id_gaji/'.$db->id_gaji.'">
						<i class="icon-list icon-white"></i>
						Detail
					</a>';
				?>
				<?php 
					if ($level==1){
					echo'
					<a class="btn btn-info" href="../../delete/id_gaji/'.$db->id_gaji.'">
						<i class="icon-remove icon-white"></i>
						Hapus
					</a>';
				}?>
			</td>
		<?php
			echo'
			</tr>
			';
			$no++;
			}
		?>
	</tbody>
		</table>
	</div>
</div>
</div>
	
<?php $this->endWidget(); ?>
</div>