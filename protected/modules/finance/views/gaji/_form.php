<?php
/* @var $this GajiController */
/* @var $model Gaji */
/* @var $form CActiveForm */
?>

<div class="form" style="background-color:white">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gaji-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

<div class="row" >
    <div class="box col-md-12">
        <div class="box-content" style="background-color:white">		
			<?php echo $form->errorSummary($model); ?>		
						<form class="form-horizontal" role="form">
							<div class="form-group col-lg-6 ">
								<label>NIP</label>
								<?php echo $form->textField($model,'nip', array('class'=>"form-control", 'placeholder'=>"Masukkan NIP")); ?>
							</div>
							<div class="form-group col-lg-6 ">
								<label>Nama Lengkap</label>
								<?php echo $form->textField($modelpegawai,'nama',array('class'=>"form-control", 'placeholder'=>"Masukkan Nama Lengkap")); ?>
							</div>
							<div class="form-group col-lg-6 ">
								<label>Jabatan</label>
								<?php echo $form->textField($modelpegawai,'jabatan',array('class'=>"form-control", 'placeholder'=>"Jabatan")); ?>
							</div>
							<div class="form-group col-lg-6 ">
								<label>Jenis Golongan</label>
								<?php echo $form->textField($modelpegawai,'id_gol',array('class'=>"form-control", 'placeholder'=>"Golongan")); ?>
							</div>
							<div class="form-group col-lg-6 ">
								<label>Gaji Pokok</label>
								<?php echo $form->textField($model,'gj_pokok',array('class'=>"form-control", 'placeholder'=>"Gaji Pokok")); ?>
							</div>
							<div class="form-group col-lg-6 ">
								<label>Tunjangan</label>
								<?php echo $form->textField($model,'tunjangan',array('class'=>"form-control", 'placeholder'=>"Tunjangan")); ?>
							</div>
							<div class="form-group col-lg-6 ">
								<label>Potongan</label>
								<?php echo $form->textField($model,'potongan',array('class'=>"form-control", 'placeholder'=>"Potongan")); ?>
							</div>
							
										<div>
										<?php echo CHtml::submitButton($model->isNewRecord ? 'Hitung' : 'Save', array('class'=>'btn btn-warning', 'style'=>'width:100px'));?>
										</div>							
							
							<div class="form-group col-lg-6 ">
								<label>Total Gaji</label>
								<?php echo $form->textField($model,'tot_gaji',array('class'=>"form-control", 'placeholder'=>"Total Gaji")); ?>
							</div>

						<div>
							<?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Save', array('class'=>'btn btn-primary', 'style'=>'width:100px')); ?>
						</div>

<?php $this->endWidget(); ?>

</div><!-- form -->