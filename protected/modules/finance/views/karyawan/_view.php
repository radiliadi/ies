<?php
/* @var $this KaryawanController */
/* @var $data Karyawan */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pd')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_pd), array('view', 'id'=>$data->id_pd)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nm_pd')); ?>:</b>
	<?php echo CHtml::encode($data->nm_pd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('divisi')); ?>:</b>
	<?php echo CHtml::encode($data->divisi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lokasi_kerja')); ?>:</b>
	<?php echo CHtml::encode($data->lokasi_kerja); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jk')); ?>:</b>
	<?php echo CHtml::encode($data->jk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nik')); ?>:</b>
	<?php echo CHtml::encode($data->nik); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tmpt_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tmpt_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_agama')); ?>:</b>
	<?php echo CHtml::encode($data->id_agama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kk')); ?>:</b>
	<?php echo CHtml::encode($data->id_kk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jln')); ?>:</b>
	<?php echo CHtml::encode($data->jln); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rt')); ?>:</b>
	<?php echo CHtml::encode($data->rt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rw')); ?>:</b>
	<?php echo CHtml::encode($data->rw); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nm_dsn')); ?>:</b>
	<?php echo CHtml::encode($data->nm_dsn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ds_kel')); ?>:</b>
	<?php echo CHtml::encode($data->ds_kel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_wil')); ?>:</b>
	<?php echo CHtml::encode($data->id_wil); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_pos')); ?>:</b>
	<?php echo CHtml::encode($data->kode_pos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_jns_tinggal')); ?>:</b>
	<?php echo CHtml::encode($data->id_jns_tinggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_alat_transport')); ?>:</b>
	<?php echo CHtml::encode($data->id_alat_transport); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telepon_rumah')); ?>:</b>
	<?php echo CHtml::encode($data->telepon_rumah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telepon_seluler')); ?>:</b>
	<?php echo CHtml::encode($data->telepon_seluler); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stat_pd')); ?>:</b>
	<?php echo CHtml::encode($data->stat_pd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nm_ayah')); ?>:</b>
	<?php echo CHtml::encode($data->nm_ayah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_lahir_ayah')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_lahir_ayah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_jenjang_pendidikan_ayah')); ?>:</b>
	<?php echo CHtml::encode($data->id_jenjang_pendidikan_ayah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pekerjaan_ayah')); ?>:</b>
	<?php echo CHtml::encode($data->id_pekerjaan_ayah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nm_ibu_kandung')); ?>:</b>
	<?php echo CHtml::encode($data->nm_ibu_kandung); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_lahir_ibu')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_lahir_ibu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_jenjang_pendidikan_ibu')); ?>:</b>
	<?php echo CHtml::encode($data->id_jenjang_pendidikan_ibu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pekerjaan_ibu')); ?>:</b>
	<?php echo CHtml::encode($data->id_pekerjaan_ibu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kewarganegaraan')); ?>:</b>
	<?php echo CHtml::encode($data->kewarganegaraan); ?>
	<br />

	*/ ?>

</div>