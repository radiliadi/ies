<?php
$this->breadcrumbs=array(
	'Sms'=>array('admin'),
	$model->id_sms,
);
?>
<div class="col-md-12">
					<div class="page-title">                    
									<h2><span class="fa fa-eye"></span> View</h2>
					</div>
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="col-md-12">
<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id_sms',
		'nm_lemb',
		'smt_mulai',
		'kode_prodi',
		'id_sp',
		'id_jenj_didik',
		'id_jns_sms',
		'id_pengguna',
		'id_fungsi_lab',
		'id_kel_usaha',
		'id_blob',
		'id_wil',
		'id_jur',
		'id_induk_sms',
		'jln',
		'rt',
		'rw',
		'nm_dsn',
		'ds_kel',
		'kode_pos',
		'lintang',
		'bujur',
		'no_tel',
		'no_fax',
		'email',
		'website',
		'singkatan',
		'tgl_berdiri',
		'sk_selenggara',
		'tgl_sk_selenggara',
		'tmt_sk_selenggara',
		'tst_sk_selenggara',
		'kpst_pd',
		'sks_lulus',
		'gelar_lulusan',
		'stat_prodi',
		'polesei_nilai',
		'luas_lab',
		'kapasitas_prak_satu_shift',
		'jml_mhs_pengguna',
		'jml_jam_penggunaan',
		'jml_prodi_pengguna',
		'jml_modul_prak_sendiri',
		'jml_modul_prak_lain',
		'fungsi_selain_prak',
		'penggunaan_lab',
),
)); ?>
			</div>
		</div>
	</div>
</div>