<?php
/* @var $this TenderController */
/* @var $model Tender */

$this->breadcrumbs=array(
	'Tenders'=>array('index'),
	$model->id_tender,
);

$this->menu=array(
	array('label'=>'List Tender', 'url'=>array('index')),
	array('label'=>'Create Tender', 'url'=>array('create')),
	array('label'=>'Update Tender', 'url'=>array('update', 'id'=>$model->id_tender)),
	array('label'=>'Delete Tender', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tender),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Tender', 'url'=>array('admin')),
);
?>

<h1>View Tender #<?php echo $model->id_tender; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tender',
		'info_source',
		'client_code',
		'client_name',
		'tender_no',
		'tender_status',
		'tender_type',
		'detail_task',
		'ref_address',
		'deadline',
		'bid_bond',
	),
)); ?>
