<?php
/* @var $this TenderController */
/* @var $model Tender */

$this->breadcrumbs=array(
	'Tender'=>array('admin'),
	'Update',
);

?>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>