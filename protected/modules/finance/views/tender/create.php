<?php
/* @var $this TenderController */
/* @var $model Tender */

$this->breadcrumbs=array(
	'Tender'=>array('admin'),
	'Create',
);
?>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>