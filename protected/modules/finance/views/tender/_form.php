<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'tender-form',
	'enableAjaxValidation'=>true,
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

<div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                            
                            <form class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Tender</strong> Detail</h3>
                                </div>
                                <div class="panel-body form-group-separated">
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-12 control-label">Info Source</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <?php echo $form->textField($model,'info_source',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Masukkan Info Source",'required'=>"required"));?>
                                            </div>                                            
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-12 control-label">Client Code</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <?php echo $form->textField($model,'client_code',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Masukkan Client Code",'required'=>"required"));?>
                                            </div>                                            
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-12 control-label">Client Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <?php echo $form->textField($model,'client_name',array('size'=>50,'maxlength'=>50,'class'=>"form-control", 'required'=>'required','placeholder'=>"Masukkan Client Name",'required'=>"required"));?>
                                            </div>                                            
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-12 control-label">Tender No</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <?php echo $form->textField($model,'tender_no',array('size'=>50,'maxlength'=>50,'class'=>"form-control", 'required'=>'required','placeholder'=>"Masukkan Tender No",'required'=>"required"));?>
                                            </div>                                            
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-12 control-label">Tender Status</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <?php echo $form->dropDownList($model,'tender_status',CHtml::listData(TendStat::model()->findAll(),'nm_tend_stat','nm_tend_stat'),array('empty'=>'-Pilih Status-','class'=>"form-control select", 'required'=>'required')); ?>                                       
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-12 control-label">Tender Type</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <?php echo $form->dropDownList($model,'tender_type',CHtml::listData(TendType::model()->findAll(),'nm_tend_type','nm_tend_type'),array('empty'=>'-Pilih Type-','class'=>"form-control select", 'required'=>'required')); ?>                                       
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-12 control-label">Detail Task</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <?php echo $form->textField($model,'detail_task',array('size'=>50,'maxlength'=>50,'class'=>"form-control", 'required'=>'required','placeholder'=>"Masukkan Detail Task",'required'=>"required"));?>
                                            </div>                                            
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-12 control-label">Ref Address</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <?php echo $form->textField($model,'ref_address',array('size'=>50,'maxlength'=>50,'class'=>"form-control", 'required'=>'required','placeholder'=>"Masukkan Ref Address",'required'=>"required"));?>
                                            </div>                                            
                                        </div>
                                    </div>

                                    <div class="form-group">                                        
                                        <label class="col-md-4 col-xs-12 control-label">Deadline</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <?php echo $form->datePickerGroup($model,'deadline',array('widgetOptions'=>array('options'=>array(),'htmlOptions'=>array('placeholder'=>"2016-12-27",'data-date-format'=>'yyyy-mm-dd')), 'prepend'=>'<i class="fa fa-calendar"></i>', 'append'=>'')); ?>                                            
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-12 control-label">Bid Bond No</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <?php echo $form->textField($model,'bid_bond',array('size'=>50,'maxlength'=>50,'class'=>"form-control", 'required'=>'required','placeholder'=>"Masukkan Bid Bond",'required'=>"required"));?>
                                            </div>                                            
                                        </div>
                                    </div>

                                </div>
                                <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
										'buttonType'=>'submit',
										'context'=>'primary',
										'label'=>$model->isNewRecord ? 'Create' : 'Save',
									)); ?>
                                </div>
                                </div>
                            </div>
                            </form>
                            
                        </div><div class="col-md-1"></div>
                    </div> 
                    <?php $this->endWidget(); ?>