<?php
/* @var $this TenderController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tenders',
);

$this->menu=array(
	array('label'=>'Create Tender', 'url'=>array('create')),
	array('label'=>'Manage Tender', 'url'=>array('admin')),
);
?>

<h1>Tenders</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
