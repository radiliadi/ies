<?php
$this->breadcrumbs=array(
	'Tender'=>array('admin'),
	'Manage',
);
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id'=>'page-form',
    'enableAjaxValidation'=>true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => TRUE,
        'errorCssClass' => 'has-error',
        'successCssClass' => 'has-success'
    ),
    'htmlOptions' => array('class' => 'form-horizontal', 'role' =>'form'),
));
?>

<div class="row">

	<div class="page-title">                    
                    <h2><span class="fa fa-users"></span> Kelola Data Tender</h2>
                </div>

<div class="col-md-12">
<div class="panel panel-default">
                                <div class="panel-heading">
									<div class="btn-group pull-left">
                                        <?php echo CHtml::link('<i class="fa fa-refresh"></i> Refresh', 'admin', array('class'=>'btn btn btn-default pull-right')); ?>
	
                                    </div> 
                                    <div class="btn-group pull-right">
                                        <?php echo CHtml::link('<i class="fa fa-plus"></i> Tambah', 'create', array('class'=>'btn btn btn-primary pull-right')); ?>
	
                                    </div> 
                                </div>

   
    <div class="panel-body">


                    <?php $this->endWidget(); ?>

<?php echo CHtml::beginForm(array('tender/exportExcel')); ?>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'tender-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'pager' => array(
                            'header' => '',
                            'footer' => '',
                            'cssFile' => false,
                            'selectedPageCssClass' => 'active',
                            'hiddenPageCssClass' => 'disabled',
                            'firstPageCssClass' => 'previous',
                            'lastPageCssClass' => 'next',
                            'maxButtonCount' => 5,
                            'firstPageLabel'=> '<i class="fa fa-angle-double-left"></i>',
                            'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
                            'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
                            'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
                            'htmlOptions' => array(
                                'class' => 'dataTables_paginate paging_bootstrap pagination pull-right',
                                'align' => 'center',
                            )
                        ),
'columns'=>array(
		array(
		'header' => 'No.',
		'htmlOptions' => array(
			'width' => '20px',
			'style' => 'text-align:center'
		),
		'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		// 'class'=>'booster.widgets.TbTotalSumColumn',
		),
		'info_source',
		'client_code',
		'client_name',
		'tender_no',
		'tender_status',
		'tender_type',
		'detail_task',
		'ref_address',
		array(
			'name'=>'deadline',
			'type'=>'raw',
			'filter'=>false,
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                      'model' => $model, 
                                      'attribute' => 'deadline', 
                                      'language' => 'en-GB',
                                      'htmlOptions' => array(
                                          'id' => 'trx_date',
                                          'size' => '10',
                                          'class' => 'form-control startClass',
                                          'style' =>'width:140px;',
                                          // 'value' => date('Y-m-d')
                                      ),
                                         'options'=>array(
                                 // 'showAnim'=>'fade',
                                 'dateFormat'=>'yy-mm-dd',
                                ),
                                      'defaultOptions' => array(
                                          'showOn' => 'focus', 
                                          'language' => 'en-GB',
                                          'dateFormat' => 'Y-m-d',
                                          'showOtherMonths' => true,
                                          'selectOtherMonths' => true,
                                          'changeMonth' => true,
                                          'changeYear' => true,
                                          'showButtonPanel' => true,
                                          // 'style' =>'width:100px;',
                                      )
                                  ), 
                                  true),
		),
		'bid_bond',
array(
'class'=>'booster.widgets.TbButtonColumn',
'template'=>'{update}',
),
array(
'class'=>'booster.widgets.TbButtonColumn',
'template'=>'{delete}',
),
),
)); ?>

<hr></hr>
  <div class="col-md-6">
    <div class="col-md-3">
    <select name="fileType" class="form-control">
      <option value="Excel">EXCEL 5 (xls)</option>
    </select>
    </div>
    <div class="col-md-3">
    <?php echo CHtml::submitButton('Export',array(
          'buttonType' => 'submit',
          'context'=>'primary',
          'label'=>'Export Excel',
          'class'=>'btn btn-primary',
        )); ?>
    <?php echo CHtml::endForm(); ?>
    </div>
  </div>


			</div>
		</div>
	</div>
  </div>
 