<?php
$this->breadcrumbs=array(
	'Users'=>array('admin'),
	$model->username=>array('view','id'=>$model->user_id),
	'Update',
);

	// $this->menu=array(
	// array('label'=>'List User','url'=>array('index')),
	// array('label'=>'Create User','url'=>array('create')),
	// array('label'=>'View User','url'=>array('view','id'=>$model->username)),
	// array('label'=>'Manage User','url'=>array('admin')),
	// );
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>