<?php
$this->breadcrumbs=array(
	'User'=>array('index'),
	$model->username,
);

?>

<div class="col-md-12">
	<div class="page-title">                    
                    <h2><span class="fa fa-laptop"></span> User</h2>
	</div>
	<div class="panel panel-default">
		<div class="panel-body">
<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'username',
		'password',
		'level',
),
)); ?>
		</div>
	</div>

</div>
