<?php
$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array(
        'links' => array('Beranda' ,
        ),
    )
);
?>
	<div class="col-md-3">

					<div class="widget widget-primary widget-item-icon">
						<div class="widget-item-right">
							<span class="fa fa-check"></span>
						</div>                             
						<div class="widget-data-left">
							<div class="widget-int num-count">SEA</div>
							<div class="widget-title"></div>
							<div class="widget-subtitle">SPARTAN ERAGON ASIA</div>
						</div>                                     
					</div>

				</div>
	<div class="col-md-3">

					<div class="widget widget-danger widget-padding-sm">
						<div class="widget-big-int plugin-clock">00:00</div>                            
						<div class="widget-subtitle plugin-date">Loading...</div>                       
						<div class="widget-buttons widget-c3">
							<div class="col">
								<a href="#"><span class="fa fa-clock-o"></span></a>
							</div>
							<div class="col">
								<a href="#"><span class="fa fa-bell"></span></a>
							</div>
							<div class="col">
								<a href="#"><span class="fa fa-calendar"></span></a>
							</div>
						</div>                            
					</div>                        

				</div>

	<div class="col-md-3">

					<a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/user/admin"><div class="widget widget-default widget-item-icon">
						<div class="widget-item-left">
							<span class="glyphicon glyphicon-user"></span>
						</div>
						<div class="widget-data">
							<div class="widget-int num-count"><?php echo $modelUser['user_id'];?></div>
							<div class="widget-title">User Terdaftar</div>
							<div class="widget-subtitle"></div>
						</div>                   
					</div></a>

	</div>
	<div class="col-md-3">

					<a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/chat/"><div class="widget widget-success widget-item-icon">
						<div class="widget-item-left">
							<span class="fa fa-comments"></span>
						</div>
						<div class="widget-data">
							<div class="widget-int num-count"><?php echo $modelChat['chat_id'];?></div>
							<div class="widget-title">Jumlah Chat</div>
							<div class="widget-subtitle"></div>
						</div>                   
					</div></a>

	</div>
	<div class="col-md-4">

					<a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/karyawan/admin"><div class="widget widget-default widget-item-icon">
						<div class="widget-item-left">
							<span class="fa fa-users"></span>
						</div>
						<div class="widget-data">
							<div class="widget-int num-count"><?php echo $modelKaryawan['id_pd'];?></div>
							<div class="widget-title">Jumlah Karyawan</div>
							<div class="widget-subtitle"></div>
						</div>                      
					</div></a>

				</div>
	<div class="col-md-4">

                            <div class="widget widget-success widget-carousel">
                                <div class="owl-carousel" id="owl-example">
                                    <div>                                    
                                        <div class="widget-title">Karyawan Tetap</div>                                                                        
                                        <div class="widget-subtitle"></div>
                                        <div class="widget-int"><?php echo $modelJKaryawan['id_pd'];?></div>
                                    </div>
                                    <div>                                    
                                        <div class="widget-title">Karyawan Kontrak</div>
                                        <div class="widget-subtitle"></div>
                                        <div class="widget-int"><?php echo $modelJJKaryawan['id_pd'];?></div>
                                    </div>
                                </div>                           
                            </div>

                        </div>
    <div class="col-md-4">

                            <div class="widget widget-danger widget-carousel">
                                <div class="owl-carousel" id="owl-example">
                                    <div>                                    
                                        <div class="widget-title">Management</div>                                                                        
                                        <div class="widget-subtitle"></div>
                                        <div class="widget-int"><?php echo $modelDivisi1['id_pd'];?></div>
                                    </div>
                                    <div>                                    
                                        <div class="widget-title">Finance</div>
                                        <div class="widget-subtitle"></div>
                                        <div class="widget-int"><?php echo $modelDivisi2['id_pd'];?></div>
                                    </div>
                                    <div>                                    
                                        <div class="widget-title">Managed Services</div>
                                        <div class="widget-subtitle"></div>
                                        <div class="widget-int"><?php echo $modelDivisi3['id_pd'];?></div>
                                    </div>
                                </div>                        
                            </div>

                        </div>
	<div class="col-md-6">

					<a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/tender/admin"><div class="widget widget-primary widget-item-icon">
						<div class="widget-item-left">
							<span class="fa fa-trophy"></span>
						</div>
						<div class="widget-data">
							<div class="widget-int num-count"><?php echo $modelTender['id_tender'];?></div>
							<div class="widget-title">Total Tender</div>
							<div class="widget-subtitle"></div>
						</div>                   
					</div></a>

	</div>
	<div class="col-md-6">

                            <div class="widget widget-default widget-carousel">
                                <div class="owl-carousel" id="owl-example">
                                    <div>                                    
                                        <div class="widget-title">Win</div>                                                                        
                                        <div class="widget-subtitle"></div>
                                        <div class="widget-int"><?php echo $win['id_tender'];?></div>
                                    </div>
                                    <div>                                    
                                        <div class="widget-title">Failed</div>
                                        <div class="widget-subtitle"></div>
                                        <div class="widget-int"><?php echo $failed['id_tender'];?></div>
                                    </div>
                                    <div>                                    
                                        <div class="widget-title">Decline</div>
                                        <div class="widget-subtitle"></div>
                                        <div class="widget-int"><?php echo $decline['id_tender'];?></div>
                                    </div>
                                    <div>                                    
                                        <div class="widget-title">Openbid</div>
                                        <div class="widget-subtitle"></div>
                                        <div class="widget-int"><?php echo $openbid['id_tender'];?></div>
                                    </div>
                                    <div>                                    
                                        <div class="widget-title">Losit</div>
                                        <div class="widget-subtitle"></div>
                                        <div class="widget-int"><?php echo $losit['id_tender'];?></div>
                                    </div>
                                    <div>                                    
                                        <div class="widget-title">Pending</div>
                                        <div class="widget-subtitle"></div>
                                        <div class="widget-int"><?php echo $pending['id_tender'];?></div>
                                    </div>
                                </div>                            
                            </div>
                        </div>	
                        <div class="page-title">    </div>
	<div class="row">
		<div class="panel panel-default" >
			<div class="panel-body">


<!-- <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAE73gQgaeN56gcOaKBoWxOCXdTtU6C8kY&callback=initMap">
    </script> -->
<script src="https://maps.google.com/maps/api/js?sensor=false"></script>

<script>
var x = document.getElementById("demo");
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
        //console.log(navigator.geolocation.getCurrentPosition(showPosition, showError));
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    x.innerHTML = 
    "Latitude: " + position.coords.latitude + 
    "<br>Longitude: " + position.coords.longitude;
    lat = position.coords.latitude;
    lon = position.coords.longitude;
    latlon = new google.maps.LatLng(lat, lon)
    mapholder = document.getElementById('mapholder')
    mapholder.style.height = '500px';
    mapholder.style.width = '500px';

    var myOptions = {
      center:latlon,zoom:14,
      mapTypeId:google.maps.MapTypeId.ROADMAP,
      mapTypeControl:false,
      navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
    }
    // console.log(lat+ ' = '+lon);
    
    var map = new google.maps.Map(document.getElementById("mapholder"), myOptions);
    var marker = new google.maps.Marker({position:latlon,map:map,title:"You are here!"});
}


// $location = Absensi::model()->findAll();
// $location->latitude = $_GET['lat'];



function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "User denied the request for Geolocation."
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "Location information is unavailable."
            break;
        case error.TIMEOUT:
            x.innerHTML = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "An unknown error occurred."
            break;
    }
}
</script>
<div class="col-md-6">
<?php
 $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tinstrument-form',
	'enableAjaxValidation'=>false,
)); 
$label=array();
$nilai=array();

foreach($dataProvider->getData() as $i=>$ii)
{
    $label[$i]=$ii['level'];
    $nilai[$i]=(int)$ii['count(username)'];
}

$this->widget('application.extensions.highcharts.HighchartsWidget', array(
   'options'=>array(
     'chart'=> array('defaultSeriesType'=>'column',),
      'title' => array('text' => 'Statistik User'),
      'legend'=>array('enabled'=>false),
      'xAxis'=>array('categories'=>$label,
			'title'=>array('text'=>'User'),),
      'yAxis'=> array(
            'min'=> 0,
            'title'=> array(
            'text'=>'J u m l a h'
            ),
        ),
      'series' => array(
         array('data' => $nilai)
      ),
      'tooltip' => array('formatter' => 'js:function(){ return "<b>"+this.point.name+"</b> :"+this.y; }'),
      'tooltip' => array(
		'formatter' => 'js:function() {return "<b>"+ this.x +"</b><br/>"+"J u m l a h : "+ this.y; }'
      ),
      'plotOptions'=>array('pie'=>(array(
                    'allowPointSelect'=>true,
                    'showInLegend'=>true,
                    'cursor'=>'pointer',
                )
            )                       
        ),
      'credits'=>array('enabled'=>false),
   )
));

?></div>
<div class="col-md-6">
<?php $this->widget('booster.widgets.TbHighCharts', array(
   'options'=>array(
      'title' => array('text' => 'Statistik Gaji'),
      'xAxis' => array(
         'categories' => $label
      ),
      'yAxis' => array(
         'title' => array('text' => 'J u m l a h')
      ),
      'series' => array(
         array('name' => 'Gaji', 'data' => $nilai),
      )
   )
)); ?></div>
<div class="col-md-6">
<?php 
                            
                            $this->renderPartial('_grafik',array(
                                'labelA'=>'pagu RM',
                                'labelB'=>'pagu BLU',
                                'dataa'=>intval(100),
                                'datab'=>intval(170),
                            )); ?>

<?php $this->endWidget(); ?></div>


				</div>
		</div>
	</div>
	
<script>
     // var time = new Date().getTime();
     // $(document.body).bind("mousemove keypress", function(e) {
         // time = new Date().getTime();
     // });

     // function refresh() {
         // if(new Date().getTime() - time >= 600) 
             // window.location.reload(true);
         // else 
             // setTimeout(refresh, 10);
     // }

     // setTimeout(refresh, 10);
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAE73gQgaeN56gcOaKBoWxOCXdTtU6C8kY&callback=initMap">
    </script>
    <style>
       #map {
        height: 400px;
        width: 100%;
       }
    </style>
