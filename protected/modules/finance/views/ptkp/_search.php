<?php
/* @var $this PtkpController */
/* @var $model Ptkp */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_ptkp'); ?>
		<?php echo $form->textField($model,'id_ptkp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wp_pribadi'); ?>
		<?php echo $form->textField($model,'wp_pribadi'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wp_menikah'); ?>
		<?php echo $form->textField($model,'wp_menikah'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wp_tanggungan'); ?>
		<?php echo $form->textField($model,'wp_tanggungan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ptkp_ket'); ?>
		<?php echo $form->textField($model,'ptkp_ket',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->