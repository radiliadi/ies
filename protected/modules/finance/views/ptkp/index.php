<?php
/* @var $this PtkpController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ptkps',
);

$this->menu=array(
	array('label'=>'Create Ptkp', 'url'=>array('create')),
	array('label'=>'Manage Ptkp', 'url'=>array('admin')),
);
?>

<h1>Ptkps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
