<?php
/* @var $this CvController */
/* @var $model Cv */

$this->breadcrumbs=array(
	'Cvs'=>array('index'),
	$model->id_cv,
);

$this->menu=array(
	array('label'=>'List Cv', 'url'=>array('index')),
	array('label'=>'Create Cv', 'url'=>array('create')),
	array('label'=>'Update Cv', 'url'=>array('update', 'id'=>$model->id_cv)),
	array('label'=>'Delete Cv', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_cv),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Cv', 'url'=>array('admin')),
);
?>

<h1>View Cv #<?php echo $model->id_cv; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_cv',
		'nm_cv',
		'link_cv',
	),
)); ?>
