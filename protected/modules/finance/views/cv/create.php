<?php
/* @var $this CvController */
/* @var $model Cv */

$this->breadcrumbs=array(
	'Bank CV'=>array('admin'),
	'Create',
);

?>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>