<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'IES - Integrated Employee System',
	'defaultController' => 'site/login',
	'timeZone' => 'Asia/Jakarta',

	// preloading 'log' component
	
	'preload'=>array('log','booster'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.extensions.fpdf.*',
		'application.extensions.*',
	),
	
	

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'12345',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
			'generatorPaths' => array('booster.gii'),
		),
		'operator',
		'management',
		'finance',
		'hrd',
		'employee',
	),

	// application components
	'components'=>array(
		'ePdf' => array(
			'class' => 'ext.yii-pdf.EYiiPdf',
				'params' => array(
				'HTML2PDF' => array(
				'librarySourcePath' => 'application.vendor.html2pdf.*',
				'classFile' => 'html2pdf.class.php',
				)
			),
		),
		// 'excel' => array(
		// 	'class' => 'application.extensions.PHPExcel',
		// ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		'booster' => array(
			'class' => 'ext.booster.components.Booster',
		),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=ies',
			// 'connectionString' => 'mysql:host=localhost;dbname=ies_20sept',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		// 'db'=>array(
		// 	'connectionString' => 'mysql:host=localhost;dbname=spartane_ies',
		// 	'emulatePrepare' => true,
		// 	'username' => 'spartane_ies',
		// 	'password' => '4y@tkurs1!!',
		// 	'charset' => 'utf8',
		// ),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);