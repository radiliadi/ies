<?php
if(isset($_SESSION['sessiondrz']))
{
	$vari = explode("|",$_SESSION['sessiondrz']);
}else{
	$vari = "";
}
$sId=Yii::app()->session->get('id');
$sName=Yii::app()->session->get('name');
$id_pd=Yii::app()->session->get('username');
$getlevel=Yii::app()->session->get('level');
$getstat=Yii::app()->session->get('status');
$namaKaryawan=Yii::app()->session->get('namaKaryawan');
// echo '<pre>';
//         print_r(Yii::app()->session->get('namaKaryawan'));
//         echo '</pre>';
//         die;

if($getstat == 0 && $getstat == 2)
{
$this->redirect(Yii::app()->request->baseUrl.'/site/logout');
}
// $status=Yii::app()->session->get('status');
// $sSms=Yii::app()->session->get('sms');
// if(empty($getlevel))
// {
// $this->redirect(Yii::app()->request->baseUrl.'/site/logout');
// }
// if(Yii::app()->user->isGuest)
if(empty($id_pd))
{
$this->redirect(Yii::app()->request->baseUrl.'/site/logout');
}
switch($getlevel){
	case 'operator':
	$level=1;
	break;
	case 'management':
	$level=2;
	break;
    case 'finance':
    $level=3;
    break;
    case 'hrd':
    $level=4;
    break;
    case 'employee':
    $level=5;
    break;
}
// $url='';
// $this->render('menuActive');
include('menuActive.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>       
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/logo/spartan.png" type="image/x-icon" />
        <!-- END META SECTION -->
                        
        <!-- CSS INCLUDE -->    		
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/css/cropper/cropper.min.css"/>
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                   
        <!-- LESSCSS INCLUDE -->        
        <!-- <link rel="stylesheet/less" type="text/css" href="<?php //echo Yii::app()->request->baseUrl; ?>/themes/admin/css/styles.less"/> -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/lesscss/less.min.js"></script>                
        <!-- EOF LESSCSS INCLUDE -->                                       
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                    <a href="#">SEA</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/png.png"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
							<?php
                            $profil=User::model()->find('user_id=:user_id',array(':user_id'=>$sId)); 
                            if(empty($profil->user_foto)) {
                                echo CHtml::image(Yii::app()->request->baseUrl.'/images/no-image.jpg','Image',array('width'=>'10%'));
                            }else{
                                echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$profil->user_foto,'Image') ;
                            }
                            ?>
                            </div>
                            <div class="profile-data">
							<?php
								if(isset($_SESSION['sessiondrz']))
								{
									echo '
									<div class="profile-data-name">'.$vari[4].'</div>
									<div class="profile-data-title">'.$vari[0].'</div>
									';
								}else{
									echo '
									<div class="profile-data-name">'.$id_pd.'</div>
									<div class="profile-data-title">'.$getlevel.'</div>
									';
								}
							?>
                            </div>
                            <div class="profile-controls">
                            <?php if ($level=='1'){ ?>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/chat/" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/profil/update/id/<?= $sId; ?>" class="profile-control-right"><span class="fa fa-wrench"></span></a>
                            <?php }?>
                            <?php if ($level=='2') { ?>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/management/chat/" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/management/profil/update/id/<?= $sId; ?>" class="profile-control-right"><span class="fa fa-wrench"></span></a>
                            <?php }?>
                            <?php if ($level=='3') { ?>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/finance/chat/" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/finance/profil/update/id/<?= $sId; ?>" class="profile-control-right"><span class="fa fa-wrench"></span></a>
                            <?php }?>
                            <?php if ($level=='4') { ?>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/hrd/chat/" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/hrd/profil/update/id/<?= $sId; ?>" class="profile-control-right"><span class="fa fa-wrench"></span></a>
                            <?php }?>
                            <?php if ($level=='5') { ?>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/employee/chat/" class="profile-control-left"><span class="fa fa-comments"></span></a>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/employee/profil/update/id/<?= $sId; ?>" class="profile-control-right"><span class="fa fa-wrench"></span></a>
                            <?php }?>
                            </div>
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Main Menu</li>  
                    <!-- OPERATOR -->
					<?php if ($level=='1'){ ?>
                    <li class="<?= $operatorDashboard[1]; ?>">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/"><span class="fa fa-windows"></span> <span class="xn-text">Dashboard</span></a>
                    </li>
                    <li class="xn-openable <?= $operatorUsers[1]; ?> <?= $operatorUsers[2]; ?> <?= $operatorUsers[3]; ?> <?= $operatorUsers[4]; ?> <?= $operatorUsers[5]; ?> <?= $operatorUsers[6]; ?>">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Users</span></a>
                        <ul>
                            <li class="<?= $operatorUsers[2]; ?>">
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/user/active"><span class="fa fa-check"></span>Active</a>
                            </li>
                            <li class="<?= $operatorUsers[3]; ?>">
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/user/inactive"><span class="glyphicon glyphicon-remove"></span>Inactive</a>
                            </li>
                            <li class="<?= $operatorUsers[4]; ?>">
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/user/deleted"><span class="glyphicon glyphicon-trash"></span>Delete</a>
                            </li>
                        </ul>
                    </li>
                    <li class="xn-openable <?= $operatorEmp[1]; ?> <?= $operatorEmpPers[1]; ?> <?= $operatorEmpPers[2]; ?> <?= $operatorEmpPers[3]; ?> <?= $operatorEmpPers[4]; ?> <?= $operatorEmpPay[1]; ?> <?= $operatorEmpPay[2]; ?> <?= $operatorEmpPay[3]; ?> <?= $operatorEmpMc[1]; ?> <?= $operatorEmpMc[2]; ?> <?= $operatorEmpFinEarn[1]; ?> <?= $operatorEmpFinEarn[2]; ?> <?= $operatorEmpFinDeduc[1]; ?> <?= $operatorEmpFinDeduc[2]; ?> <?= $operatorEmpBt[1]; ?> <?= $operatorEmpBt[2]; ?>">
                        <a href="#"><span class="fa fa-user"></span> <span class="xn-text">Employee</span></a>
                        <ul>
                            <li class="<?= $operatorEmpPers[1]; ?> <?= $operatorEmpPers[2]; ?> <?= $operatorEmpPers[3]; ?> <?= $operatorEmpPers[4]; ?>">
		                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/karyawan/admin"><span class="fa fa-file-text-o"></span>Personalia</a>
		                    </li>
							<!--<li>
		                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/absensi/admin"><span class="fa fa-calendar"></span> <span class="xn-text">Attendance</span></a>
		                    </li>-->
							<li class="<?= $operatorEmpPay[1]; ?> <?= $operatorEmpPay[2]; ?> <?= $operatorEmpPay[3]; ?>">
		                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/karyawan/pay"><span class="fa fa-money"></span>Payroll</a>
		                    </li>
							<li class="<?= $operatorEmpMc[1]; ?> <?= $operatorEmpMc[2]; ?>">
		                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/medicalClaim/admin"><span class="fa fa-medkit"></span>Medical Claim</a>
		                    </li>
							<li class="xn-openable <?= $operatorEmpFinEarn[1]; ?> <?= $operatorEmpFinEarn[2]; ?> <?= $operatorEmpFinDeduc[1]; ?> <?= $operatorEmpFinDeduc[2]; ?>">
		                        <a href="#"><span class="fa fa-dollar"></span>Finances</a>
                                <ul>
                                <li class="<?= $operatorEmpFinEarn[1]; ?> <?= $operatorEmpFinEarn[2]; ?>">
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/earnings/admin"><span class="fa fa-plus-square"></span>Earnings</a>
                                </li>
                                <li class="<?= $operatorEmpFinDeduc[1]; ?> <?= $operatorEmpFinDeduc[2]; ?>">
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/deductions/admin"><span class="fa fa-minus-square"></span>Deductions</a>
                                </li>
                                </ul>
		                    </li>
							<li class="<?= $operatorEmpBt[1]; ?> <?= $operatorEmpBt[2]; ?>">
		                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/businessTrip/admin"><span class="fa fa-plane"></span>Business Trip</a>
		                    </li>
                            <!-- <li>
                                <a href="<?php //echo Yii::app()->request->baseUrl; ?>/operator/businessTrip/admin"><span class="fa fa-shopping-cart"></span>Purchasing Order <small>(ongoing)</small></a>
                            </li> -->
		                </ul>
				    </li>
                    <li class="<?= $operatorExp[1]; ?> <?= $operatorExpDetail[1]; ?>">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/expenses/admin"><span class="fa fa-bullhorn"></span> <span class="xn-text">Expenses</span></a>
                    </li>
                    <li class="<?= $operatorActg[1]; ?> <?= $operatorActgMenu[1]; ?>">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/accounting/index"><span class="fa fa-dollar"></span> <span class="xn-text">Accounting</span></a>
                    </li>
				    <li class="<?= $operatorTender[1]; ?> <?= $operatorTender[2]; ?> <?= $operatorTender[3]; ?>">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/tender/admin"><span class="fa fa-trophy"></span> <span class="xn-text">Tender</span></a>
                    </li>
					<li class="xn-openable <?= $operatorBankCV[1]; ?> <?= $operatorBankCV[2]; ?> <?= $operatorBankCV[3]; ?> <?= $operatorBankCV[4]; ?>">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Bank CV</span></a>
                        <ul>
                            <li class="<?= $operatorBankCV[1]; ?>">
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/cv/create"><span class="fa fa-upload"></span> Upload</a>
                            </li>
                            <li class="<?= $operatorBankCV[2]; ?>">
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/cv/admin"><span class="fa fa-file-text-o"></span> Tetap</a>
                            </li>
                            <li class="<?= $operatorBankCV[3]; ?>">
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/cv/adminn"><span class="fa fa-file-text-o"></span> Kontrak</a>
                            </li>
                            <li class="<?= $operatorBankCV[4]; ?>">
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/cv/adminm"><span class="fa fa-file-text-o"></span> Kandidat</a>
                            </li>
                        </ul>
                    </li>
                    <li class="xn-openable <?= $operatorRptGraph[1]; ?> <?= $operatorRptPayAnn[1]; ?> <?= $operatorRptPayMon[1]; ?> <?= $operatorRptPayDai[1]; ?> <?= $operatorRptDepAnn[1]; ?> <?= $operatorRptDepMon[1]; ?> <?= $operatorRptDepDai[1]; ?>">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Report</span></a>
                            <ul>
                                <li class="<?= $operatorRptGraph[1]; ?>">
                                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/gaji/grafik"><span class="fa fa-bar-chart-o"></span>Graph</span></a>
                                </li>
                                <li class="xn-openable <?= $operatorRptPayAnn[1]; ?> <?= $operatorRptPayMon[1]; ?> <?= $operatorRptPayDai[1]; ?>">
                                    <a href="#"><span class="fa fa-money"></span> Payroll</a>
                                        <ul>
                                            <li class="<?= $operatorRptPayAnn[1]; ?>">
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/gaji/cetak"><span class="fa fa-files-o"></span>Annual</a>
                                            </li>
                                            <li class="<?= $operatorRptPayMon[1]; ?>">
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/gaji/cetakx"><span class="fa fa-files-o"></span>Monthly</a>
                                            </li>
                                            <li class="<?= $operatorRptPayDai[1]; ?>">
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/gaji/cetaky"><span class="fa fa-files-o"></span>Daily</a>
                                            </li>
                                        </ul>
                                </li>
                                <li class="xn-openable <?= $operatorRptDepAnn[1]; ?> <?= $operatorRptDepMon[1]; ?> <?= $operatorRptDepDai[1]; ?>">
                                    <a href="#"><span class="fa fa-money"></span> Dependents</a>
                                        <ul>
                                            <li class="<?= $operatorRptDepAnn[1]; ?>">
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/gaji/doAnnual"><span class="fa fa-files-o"></span>Annual</a>
                                            </li>
                                            <li class="<?= $operatorRptDepMon[1]; ?>">
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/gaji/doMonthly"><span class="fa fa-files-o"></span>Monthly</a>
                                            </li>
                                            <li  class="<?= $operatorRptDepDai[1]; ?>">
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/gaji/doDaily"><span class="fa fa-files-o"></span>Daily</a>
                                            </li>
                                        </ul>
                                </li>
                                <!-- <li class="xn-openable <?= $operatorRptExpAnn[1]; ?> <?= $operatorRptExpMon[1]; ?> <?= $operatorRptExpDai[1]; ?>">
                                    <a href="#"><span class="fa fa-money"></span> Expenses <small>(ongoing)</small></a>
                                        <ul>
                                            <li class="<?= $operatorRptExpAnn[1]; ?>">
                                            <a href="<?php //echo Yii::app()->request->baseUrl; ?>/operator/expenses/doAnnual"><span class="fa fa-files-o"></span>Annual</a>
                                            </li>
                                            <li class="<?= $operatorRptExpMon[1]; ?>">
                                            <a href="<?php //echo Yii::app()->request->baseUrl; ?>/operator/expenses/doMonthly"><span class="fa fa-files-o"></span>Monthly</a>
                                            </li>
                                            <li class="<?= $operatorRptExpDai[1]; ?>">
                                            <a href="<?php //echo Yii::app()->request->baseUrl; ?>/operator/expenses/doDaily"><span class="fa fa-files-o"></span>Daily</a>
                                            </li>
                                        </ul>
                                </li> -->
                            </ul>
                    </li>
					<li class="xn-openable 
                    <?= $operatorSetEmpDiv[1]; ?> <?= $operatorSetEmpDiv[2]; ?> <?= $operatorSetEmpDiv[3]; ?>
                    <?= $operatorSetEmpWp[1]; ?>        <?= $operatorSetEmpWp[2]; ?>        <?= $operatorSetEmpWp[3]; ?> 
                    <?= $operatorSetEmpKos[1]; ?>       <?= $operatorSetEmpKos[2]; ?>       <?= $operatorSetEmpKos[3]; ?> 
                    <?= $operatorSetEmpTrans[1]; ?>     <?= $operatorSetEmpTrans[2]; ?>     <?= $operatorSetEmpTrans[3]; ?> 
                    <?= $operatorSetEmpKor[1]; ?>       <?= $operatorSetEmpKor[2]; ?>       <?= $operatorSetEmpKor[3]; ?> 
                    <?= $operatorSetEmpRlg[1]; ?>       <?= $operatorSetEmpRlg[2]; ?>       <?= $operatorSetEmpRlg[3]; ?> 
                    <?= $operatorSetEmpEdul[1]; ?>      <?= $operatorSetEmpEdul[2]; ?>      <?= $operatorSetEmpEdul[3]; ?> 
                    <?= $operatorSetEmpPfs[1]; ?>       <?= $operatorSetEmpPfs[2]; ?>       <?= $operatorSetEmpPfs[3]; ?> 
                    <?= $operatorSetEmpInc[1]; ?>       <?= $operatorSetEmpInc[2]; ?>       <?= $operatorSetEmpInc[3]; ?>
                    <?= $operatorSetFinPtkp[1]; ?>      <?= $operatorSetFinPtkp[2]; ?>      <?= $operatorSetFinPtkp[3]; ?>
                    <?= $operatorSetFinJbtn[1]; ?>      <?= $operatorSetFinJbtn[2]; ?>      <?= $operatorSetFinJbtn[3]; ?>
                    <?= $operatorSetFinBpjs[1]; ?>      <?= $operatorSetFinBpjs[2]; ?>      <?= $operatorSetFinBpjs[3]; ?>
                    <?= $operatorSetRpp[1]; ?>
                    <?= $operatorSetChat[1]; ?> <?= $operatorSetFinBank[1]; ?> <?= $operatorSetFinBank[2]; ?> <?= $operatorSetFinBank[3]; ?> <?= $operatorSetCmp[1];?>
                    ">
                        <a href="#"><span class="fa fa-cogs"></span> <span class="xn-text">Settings</span></a>
                        	<ul>
                                <li class="xn-openable 
                                <?= $operatorSetEmpDiv[1]; ?>       <?= $operatorSetEmpDiv[2]; ?>       <?= $operatorSetEmpDiv[3]; ?> 
                                <?= $operatorSetEmpWp[1]; ?>        <?= $operatorSetEmpWp[2]; ?>        <?= $operatorSetEmpWp[3]; ?> 
                                <?= $operatorSetEmpKos[1]; ?>       <?= $operatorSetEmpKos[2]; ?>       <?= $operatorSetEmpKos[3]; ?> 
                                <?= $operatorSetEmpTrans[1]; ?>     <?= $operatorSetEmpTrans[2]; ?>     <?= $operatorSetEmpTrans[3]; ?> 
                                <?= $operatorSetEmpKor[1]; ?>       <?= $operatorSetEmpKor[2]; ?>       <?= $operatorSetEmpKor[3]; ?> 
                                <?= $operatorSetEmpRlg[1]; ?>       <?= $operatorSetEmpRlg[2]; ?>       <?= $operatorSetEmpRlg[3]; ?> 
                                <?= $operatorSetEmpEdul[1]; ?>      <?= $operatorSetEmpEdul[2]; ?>      <?= $operatorSetEmpEdul[3]; ?> 
                                <?= $operatorSetEmpPfs[1]; ?>       <?= $operatorSetEmpPfs[2]; ?>       <?= $operatorSetEmpPfs[3]; ?> 
                                <?= $operatorSetEmpInc[1]; ?>       <?= $operatorSetEmpInc[2]; ?>       <?= $operatorSetEmpInc[3]; ?>
                                ">
                                	<a href="#"><span class="fa fa-user"></span>Employee</a>
                                		<ul>
											<li class="<?= $operatorSetEmpDiv[1]; ?> <?= $operatorSetEmpDiv[2]; ?> <?= $operatorSetEmpDiv[3]; ?>">
											<a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/divisi/admin"><span class="fa fa-cog"></span> Division</a>
											</li>
											<li class="<?= $operatorSetEmpWp[1]; ?> <?= $operatorSetEmpWp[2]; ?> <?= $operatorSetEmpWp[3]; ?>">
											<a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/lokasiKerja/admin"><span class="fa fa-cog"></span>Work Place</a>
											</li>
											<li class="<?= $operatorSetEmpKos[1]; ?> <?= $operatorSetEmpKos[2]; ?> <?= $operatorSetEmpKos[3]; ?>">
											<a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/jenisTinggal/admin"><span class="fa fa-cog"></span>Kind of Stay</a>
											</li>
											<li class="<?= $operatorSetEmpTrans[1]; ?> <?= $operatorSetEmpTrans[2]; ?> <?= $operatorSetEmpTrans[3]; ?>">
											<a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/alatTransport/admin"><span class="fa fa-cog"></span>Transport</a>
											</li>
											<li class="<?= $operatorSetEmpKor[1]; ?> <?= $operatorSetEmpKor[2]; ?> <?= $operatorSetEmpKor[3]; ?>">
											<a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/jenisPendaftaran/admin"><span class="fa fa-cog"></span>Kind of Registration</a>
											</li>
                                            <li class="<?= $operatorSetEmpRlg[1]; ?> <?= $operatorSetEmpRlg[2]; ?> <?= $operatorSetEmpRlg[3]; ?>">
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/agama/admin"><span class="fa fa-cog"></span>Religion</a>
                                            </li>
                                            <li class="<?= $operatorSetEmpEdul[1]; ?> <?= $operatorSetEmpEdul[2]; ?> <?= $operatorSetEmpEdul[3]; ?>">
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/jenjangPendidikan/admin"><span class="fa fa-cog"></span>Educational Level</a>
                                            </li>
                                            <li class="<?= $operatorSetEmpPfs[1]; ?> <?= $operatorSetEmpPfs[2]; ?> <?= $operatorSetEmpPfs[3]; ?>">
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/pekerjaan/admin"><span class="fa fa-cog"></span>Profession</a>
                                            </li>
                                            <li class="<?= $operatorSetEmpInc[1]; ?> <?= $operatorSetEmpInc[2]; ?> <?= $operatorSetEmpInc[3]; ?>">
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/penghasilan/admin"><span class="fa fa-cog"></span>Income</a>
                                            </li>
										</ul>
								</li> 
                                <li class="xn-openable <?= $operatorSetFinPtkp[1]; ?> <?= $operatorSetFinPtkp[2]; ?> <?= $operatorSetFinPtkp[3]; ?> <?= $operatorSetFinJbtn[1]; ?> <?= $operatorSetFinJbtn[2]; ?> <?= $operatorSetFinJbtn[3]; ?> <?= $operatorSetFinBpjs[1]; ?> <?= $operatorSetFinBpjs[2]; ?> <?= $operatorSetFinBpjs[3]; ?> <?= $operatorSetFinBank[1]; ?> <?= $operatorSetFinBank[2]; ?> <?= $operatorSetFinBank[3]; ?>">
                                    <a href="#"><span class="fa fa-dollar"></span>Finance</a>
                                        <ul>
                                            <li class="<?= $operatorSetFinPtkp[1]; ?> <?= $operatorSetFinPtkp[2]; ?> <?= $operatorSetFinPtkp[3]; ?>">
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/ptkp/admin"><span class="fa fa-cog"></span> PTKP</a>
                                            </li>
                                            <li class="<?= $operatorSetFinJbtn[1]; ?> <?= $operatorSetFinJbtn[2]; ?> <?= $operatorSetFinJbtn[3]; ?>">
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/jabatan/admin"><span class="fa fa-cog"></span>Jabatan</a>
                                            </li>
                                            <li class="<?= $operatorSetFinBpjs[1]; ?> <?= $operatorSetFinBpjs[2]; ?> <?= $operatorSetFinBpjs[3]; ?>">
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/bpjs/admin"><span class="fa fa-cog"></span> BPJS</a>
                                            </li>
                                            <li class="<?= $operatorSetFinBank[1]; ?> <?= $operatorSetFinBank[2]; ?> <?= $operatorSetFinBank[3]; ?>">
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/bank/admin"><span class="fa fa-cog"></span> Bank Account</a>
                                            </li>
                                            <li class="xn-openable">
                                            <a href="#"><span class="fa fa-dollar"></span>Chart Of Account</a>
                                                <ul>
                                                    <li class="<?= $operatorSetChat[1]; ?>">
                                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/chartOfAccountRule/admin"><span class="glyphicon glyphicon-list-alt"></span>Rules</a>
                                                    </li>
                                                    <li class="<?= $operatorSetChat[1]; ?>">
                                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/chartOfAccountType/admin"><span class="glyphicon glyphicon-list-alt"></span>Type</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                </li>
                    			<li class="<?= $operatorSetRpp[1]; ?>">
									<a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/pagesize/update/id/1"><span class="fa fa-list"></span>Rows per Pages</a>
								</li>
                                <li class="<?= $operatorSetChat[1]; ?>">
                                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/chat/admin"><span class="fa fa-comments"></span>Chat</a>
                                </li>
                                <li class="<?= $operatorSetCmp[1]; ?>">
                                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/company/update/id/1"><span class="fa fa-building-o"></span>Company</a>
                                </li>
                                
                            </ul>
                    </li>
					<!-- MANAGEMENT -->
					<?php }?>
                        <?php  if ($level=='2') { ?>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/"><span class="fa fa-windows"></span> <span class="xn-text">Dashboard</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-user"></span> <span class="xn-text">My</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/karyawan/admin"><span class="fa fa-file-text-o"></span>Attendance</a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/karyawan/admin"><span class="fa fa-file-text-o"></span>Payslip</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/user/admin"><span class="fa fa-users"></span> <span class="xn-text">User</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-trophy"></span> <span class="xn-text">Tender</span></a>
                        <ul>
                            <li>
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/tender/admin"><span class="fa fa-file-text-o"></span>Records</a>
                            </li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Bank CV</span></a>
                        <ul>
                            <li>
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/cv/admin"><span class="fa fa-file-text-o"></span> <span class="xn-text">Employee</span></a>
                            </li>
                            <li>
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/cv/adminn"><span class="fa fa-file-text-o"></span> <span class="xn-text">Candidate</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="glyphicon glyphicon-stats"></span> <span class="xn-text">Report</span></a>
                            <ul>
                                <li class="xn-openable">
                                    <a href="#"><span class="fa fa-money"></span>Payroll</a>
                                        <ul>
                                            <li>
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/gaji/grafik"><span class="fa fa-files-o"></span> <span class="xn-text">Graph</span></a>
                                            </li>
                                            <li>
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/gaji/cetak"><span class="fa fa-files-o"></span><span class="xn-text">Print</span></a>
                                            </li>
                                        </ul>
                                </li>
                            </ul>
                    </li>
                    <!-- FINANCE -->
                    <?php } else ?>
                        <?php  if ($level=='3') { ?>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/"><span class="fa fa-windows"></span> <span class="xn-text">Dashboard</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-user"></span> <span class="xn-text">My</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/karyawan/admin"><span class="fa fa-file-text-o"></span>Attendance</a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/karyawan/admin"><span class="fa fa-file-text-o"></span>Payslip</a>
                            </li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-user"></span> <span class="xn-text">Employee</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/karyawan/pay"><span class="fa fa-money"></span>Payroll</a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/absensi/admin"><span class="fa fa-medkit"></span>Medical Claim</a>
                            </li>
                            <li class="xn-openable">
                                <a href="#"><span class="fa fa-dollar"></span>Finance</a>
                                <ul>
                                <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/absensi/admin"><span class="fa fa-money"></span>Earnings</a>
                                </li>
                                <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/karyawan/pay"><span class="fa fa-money"></span>Loan</a>
                                </li>
                                </ul>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/absensi/admin"><span class="fa fa-plane"></span>Business Trip</a>
                            </li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-cogs"></span> <span class="xn-text">Settings</span></a>
                            <ul>
                                <li class="xn-openable">
                                    <a href="#"><span class="fa fa-dollar"></span>Finance</a>
                                        <ul>
                                            <li>
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/ptkp/admin"><span class="fa fa-cog"></span> <span class="xn-text">PTKP</span></a>
                                            </li>
                                            <li>
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/jabatan/admin"><span class="fa fa-cog"></span><span class="xn-text">Jabatan</span></a>
                                            </li>
                                        </ul>
                                </li>
                            </ul>
                    </li>
                    <!-- HRD -->
                    <?php } else ?>
                        <?php  if ($level=='4') { ?>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/"><span class="fa fa-windows"></span> <span class="xn-text">Dashboard</span></a>
                    </li>
                    <li class="xn-openable">
                                <a href="#"><span class="fa fa-user"></span> <span class="xn-text">Employee</span></a>
                                <ul>
                                    <li>
                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/karyawan/admin"><span class="fa fa-file-text-o"></span>Personalia</a>
                                    </li>
                                </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-star"></span> <span class="xn-text">My</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/karyawan/admin"><span class="fa fa-file-text-o"></span>Attendance</a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/karyawan/admin"><span class="fa fa-file-text-o"></span>Payslip</a>
                            </li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-cogs"></span> <span class="xn-text">Settings</span></a>
                        <ul>
                            <li class="xn-openable">
                                <a href="#"><span class="fa fa-user"></span>Employee</a>
                                    <ul>
                                        <li>
                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/jabatan/admin"><span class="fa fa-cog"></span> <span class="xn-text">Division</span></a>
                                        </li>
                                        <li>
                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/jabatan/admin"><span class="fa fa-cog"></span><span class="xn-text">Work Place</span></a>
                                        </li>
                                        <li>
                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/jabatan/admin"><span class="fa fa-cog"></span><span class="xn-text">Status</span></a>
                                        </li>
                                        <li>
                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/jabatan/admin"><span class="fa fa-cog"></span><span class="xn-text">Kind of Stay</span></a>
                                        </li>
                                        <li>
                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/jabatan/admin"><span class="fa fa-cog"></span><span class="xn-text">Transport</span></a>
                                        </li>
                                        <li>
                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/operator/jabatan/admin"><span class="fa fa-cog"></span><span class="xn-text">Kind of Registration</span></a>
                                        </li>
                                    </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- EMPLOYEE -->
                    <?php } else ?>
                        <?php  if ($level=='5') { ?>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/employee/"><span class="fa fa-windows"></span> <span class="xn-text">Dashboard</span></a>
                    </li>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/employee/gaji/index/id/<?= $id_pd; ?>"><span class="fa fa-money"></span> <span class="xn-text">Payslip</span></a>
                    </li>
                    <li class="<?= $employeeExp[1]; ?> <?= $employeeExpDetail[1]; ?>">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/employee/expenses/admin"><span class="fa fa-bullhorn"></span> <span class="xn-text">Expenses</span></a>
                    </li>
                    <?php } else ?>
					<li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/site/logout"><span class="fa fa-unlock"></span> <span class="xn-text">Log Out</span></a>
                    </li>       
                                     
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
           <!-- PAGE CONTENT -->
            <div class="page-content">
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                  <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
					          
                    <!-- POWER OFF -->
                    <li class="xn-icon-button pull-right last">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-power-off"></span></a>
                        <ul class="xn-drop-left animated zoomIn">
                            <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                        </ul>                        
                    </li> 
                    <!-- END POWER OFF -->                  
                </ul>
 
	<?php echo $content; ?>
 </div>            
            <!-- END PAGE CONTENT -->
	  </div>
	  </div>
        <!-- END PAGE CONTAINER -->
        
        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                    <p>Apakah yakin ingin keluar?</p>                    
                    <p>Tekan "Tidak" jika masih ingin lanjut. Tekan "Iya" jika yakin ingin keluar dari sistem.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/site/logout" class="btn btn-primary btn-lg">Iya</a>
                            <button class="btn btn-danger btn-lg mb-control-close">Tidak</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="message-box animated fadeIn" data-sound="alert" id="ajukan-cuti">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Apakah anda yakin ingin mengajukan <strong>Cuti</strong> pada semester berikutnya ?</div>
                    <div class="mb-content">
                        <p>Apakah yakin?</p>                    
                        <p>Tekan "Tidak" jika ingin membatalkan. Tekan "Iya" jika yakin ingin mengajukan.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/mahasiswa/mahasiswa/ajukanCuti" class="btn btn-success btn-lg">Iya</a>
                            <button class="btn btn-default btn-lg mb-control-close">Tidak</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->
		<!-- MODALS -->
        <div class="modal animated fadeIn" id="modal_change_photo" tabindex="-188" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="smallModalHead">Change photo</h4>
                    </div>                    
                    <form id="cp_upload" method="post" enctype="multipart/form-data" action="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/assets/upload_image.php">
                    <div class="modal-body form-horizontal form-group-separated">
                        <div class="form-group">
                            <label class="col-md-4 control-label">New Photo</label>
                            <div class="col-md-4">
								<input type="hidden" name="id" value="<?php echo $id_pd;?>"/>
                                <input type="file" class="fileinput btn-info" name="file" id="cp_photo" data-filename-placement="inside" title="Select file"/>
                            </div>                            
                        </div>                        
                    </div>
                    </form>
                    <div class="modal-footer">
                        <!--button type="button" class="btn btn-success disabled" id="cp_accept">Accept</button-->
						<a href="../id/<?php echo $id_pd;?>" class="btn btn-success">Simpan</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal animated fadeIn" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
					<form id="cp_upload" method="post" enctype="multipart/form-data" action="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/assets/password.php">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<h4 class="modal-title" id="smallModalHead">Change password</h4>
						</div>
						<div class="modal-body">
							<p>Masukkan Password Lama untuk konfirmasi. Kemudian masukkan password baru.</p>
						</div>
						<div class="modal-body form-horizontal form-group-separated">                        
							<div class="form-group">
								<label class="col-md-3 control-label">Password Lama</label>
								<div class="col-md-9">
									<input type="hidden" name="id" value="<?php echo $id_pd;?>"/>
									<input type="password" class="form-control" name="old_password" required/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Password Baru</label>
								<div class="col-md-9">
									<input type="password" class="form-control" name="new_password" required/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Ulangi Password Baru</label>
								<div class="col-md-9">
									<input type="password" class="form-control" name="re_password" required/>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="submit" class="btn btn-danger" value="Ubah"/>
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</form>
                </div>
            </div>
        </div>        
        <!-- EOF MODALS -->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                      
		
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/smartwizard/jquery.smartWizard-2.0.min.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/jquery-validation/jquery.validate.js"></script>
		<script type="text/javascript" src="<?php  echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/bootstrap/bootstrap-select.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
		<script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/bootstrap/bootstrap-timepicker.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/jquery/jquery-migrate.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/bootstrap/bootstrap-file-input.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/form/jquery.form.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/cropper/cropper.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/owl/owl.carousel.min.js"></script>

        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/icheck/icheck.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/bootstrap/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/bootstrap/bootstrap-file-input.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/bootstrap/bootstrap-select.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>


        <script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/tableexport/tableExport.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/tableexport/jquery.base64.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/tableexport/html2canvas.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/tableexport/jspdf/libs/sprintf.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/tableexport/jspdf/jspdf.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/tableexport/jspdf/libs/base64.js"></script>   
        
        <!-- START TEMPLATE JADI DONAT--> 
        <!-- <script type="text/javascript" src="<?php  //echo Yii::app()->request->baseUrl; ?>/themes/admin/js/settings.js"></script>  -->
        
        <script type="text/javascript" src="<?php  echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins.js"></script>
      
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/actions.js"></script>    
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/demo_edit_profile.js"></script>

        <script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/datatables/jquery.dataTables.min.js"></script>
        
        <script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/noty/jquery.noty.js'></script>
        <script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/noty/layouts/topCenter.js'></script>
        <script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/noty/layouts/topLeft.js'></script>
        <script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/noty/layouts/topRight.js'></script>
        <script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/noty/themes/default.js'></script>
		
		 <!-- <script>
            $(function(){
                
                pageLoadingFrame("show","v2");                
                setTimeout(function(){
                    pageLoadingFrame("hide");
                },2000);     
                
                $("#loader_v1").on("click",function(){
                    pageLoadingFrame("show","v1");
                    setTimeout(function(){
                        pageLoadingFrame("hide");
                    },2000);     
                });
                
                $("#loader_v2").on("click",function(){
                    pageLoadingFrame("show");
                    setTimeout(function(){
                        pageLoadingFrame("hide");
                    },2000);     
                });
                
            });
        </script>  -->     
    <!-- END SCRIPTS -->                   
    </body>
</html>