<?php

$url=Yii::app()->controller->module->getId().'/'.Yii::app()->controller->getId().'/'.Yii::app()->controller->action->getId();
if(!empty($url)){
    $operatorDashboard[1]           ='';
    $operatorUsers[1]               ='';
    $operatorUsers[2]               ='';
    $operatorUsers[3]               ='';
    $operatorUsers[4]               ='';
    $operatorUsers[4]               ='';
    $operatorUsers[5]               ='';
    $operatorUsers[6]               ='';
    $operatorEmp[1]             ='';
    $operatorEmpPers[1]             ='';
    $operatorEmpPers[2]             ='';
    $operatorEmpPers[3]             ='';
    $operatorEmpPers[4]             ='';
    $operatorEmpPay[1]             ='';
    $operatorEmpPay[2]             ='';
    $operatorEmpPay[3]             ='';
    $operatorEmpMc[1]             ='';
    $operatorEmpMc[2]             ='';
    $operatorEmpFinEarn[1]             ='';
    $operatorEmpFinEarn[2]             ='';
    $operatorEmpFinDeduc[1]             ='';
    $operatorEmpFinDeduc[2]             ='';
    $operatorEmpBt[1]             ='';
    $operatorEmpBt[2]             ='';
    $operatorExp[1]             ='';
    $operatorExpDetail[1]             ='';
    $operatorActg[1]                ='';
    $operatorActgMenu[1]                ='';
    $operatorTender[1]             ='';
    $operatorTender[2]             ='';
    $operatorTender[3]             ='';
    $operatorBankCV[1]             ='';
    $operatorBankCV[2]             ='';
    $operatorBankCV[3]             ='';
    $operatorBankCV[4]             ='';
    $operatorRptGraph[1]             ='';
    $operatorRptPayAnn[1]             ='';
    $operatorRptPayMon[1]             ='';
    $operatorRptPayDai[1]             ='';
    $operatorRptDepAnn[1]             ='';
    $operatorRptDepMon[1]             ='';
    $operatorRptDepDai[1]             ='';
    $operatorRptExpAnn[1]             ='';
    $operatorRptExpMon[1]             ='';
    $operatorRptExpDai[1]             ='';
    $operatorSetEmpDiv[1]             ='';
    $operatorSetEmpDiv[2]             ='';
    $operatorSetEmpDiv[3]             ='';
    $operatorSetEmpWp[1]             ='';
    $operatorSetEmpWp[2]             ='';
    $operatorSetEmpWp[3]             ='';
    $operatorSetEmpKos[1]             ='';
    $operatorSetEmpKos[2]             ='';
    $operatorSetEmpKos[3]             ='';
    $operatorSetEmpTrans[1]             ='';
    $operatorSetEmpTrans[2]             ='';
    $operatorSetEmpTrans[3]             ='';
    $operatorSetEmpKor[1]             ='';
    $operatorSetEmpKor[2]             ='';
    $operatorSetEmpKor[3]             ='';
    $operatorSetEmpRlg[1]             ='';
    $operatorSetEmpRlg[2]             ='';
    $operatorSetEmpRlg[3]             ='';
    $operatorSetEmpEdul[1]             ='';
    $operatorSetEmpEdul[2]             ='';
    $operatorSetEmpEdul[3]             ='';
    $operatorSetEmpPfs[1]             ='';
    $operatorSetEmpPfs[2]             ='';
    $operatorSetEmpPfs[3]             ='';
    $operatorSetEmpInc[1]             ='';
    $operatorSetEmpInc[2]             ='';
    $operatorSetEmpInc[3]             ='';
    $operatorSetFinPtkp[1]             ='';
    $operatorSetFinPtkp[2]             ='';
    $operatorSetFinPtkp[3]             ='';
    $operatorSetFinJbtn[1]             ='';
    $operatorSetFinJbtn[2]             ='';
    $operatorSetFinJbtn[3]             ='';
    $operatorSetFinBpjs[1]             ='';
    $operatorSetFinBpjs[2]             ='';
    $operatorSetFinBpjs[3]             ='';
    $operatorSetFinBank[1]             ='';
    $operatorSetFinBank[2]             ='';
    $operatorSetFinBank[3]             ='';
    $operatorSetRpp[1]             ='';
    $operatorSetChat[1]             ='';
    $operatorSetCmp[1]             ='';
    // echo '<pre>';
    // print_r($url);
    // echo '</pre>';
    // die;
    $employeeExp[1]             ='';
    $employeeExpDetail[1]             ='';
    switch($url)
    {
        case 'operator/default/index':
            $operatorDashboard[1]='active';
        break;
        case 'operator/user':
            $operatorUsers[1]='active';
        break;
        case 'operator/user/active':
            $operatorUsers[2]='active';
        break;
        case 'operator/user/inactive':
            $operatorUsers[3]='active';
        break;
        case 'operator/user/deleted':
            $operatorUsers[4]='active';
        break;
        case 'operator/user/create':
            $operatorUsers[5]='active';
        break;
        case 'operator/user/update':
            $operatorUsers[6]='active';
        break;
        case 'operator/karyawan':
            $operatorEmp[1]='active';
        break;
        case 'operator/karyawan/admin':
            $operatorEmpPers[1]='active';
        break;
        case 'operator/karyawan/create':
            $operatorEmpPers[2]='active';
        break;
        case 'operator/karyawan/update':
            $operatorEmpPers[3]='active';
        break;
        case 'operator/karyawan/view':
            $operatorEmpPers[4]='active';
        break;
        case 'operator/karyawan/pay':
            $operatorEmpPay[1]='active';
        break;
        case 'operator/gaji/admin':
            $operatorEmpPay[2]='active';
        break;
        case 'operator/gaji/view':
            $operatorEmpPay[3]='active';
        break;
        case 'operator/medicalClaim/admin':
            $operatorEmpMc[1]='active';
        break;
        case 'operator/medicalClaim/update':
            $operatorEmpMc[2]='active';
        break;
        case 'operator/earnings/admin':
            $operatorEmpFinEarn[1]='active';
        break;
        case 'operator/earnings/update':
            $operatorEmpFinEarn[2]='active';
        break;
        case 'operator/deductions/admin':
            $operatorEmpFinDeduc[1]='active';
        break;
        case 'operator/deductions/update':
            $operatorEmpFinDeduc[2]='active';
        break;
        case 'operator/businessTrip/admin':
            $operatorEmpBt[1]='active';
        break;
        case 'operator/businessTrip/update':
            $operatorEmpBt[1]='active';
        break;
        case 'operator/expenses/admin':
            $operatorExp[1]='active';
        break;
        case 'operator/expenses/create':
            $operatorExp[1]='active';
        break;
        case 'operator/expenses/picture':
            $operatorExp[1]='active';
        break;
        case 'operator/expensesDetail/admin':
            $operatorExpDetail[1]='active';
        break;
        case 'operator/accounting/index':
            $operatorActg[1]='active';
        break;
        case 'operator/laba/admin':
            $operatorActgMenu[1]='active';
        break;
        case 'operator/arusKas/create':
            $operatorActgMenu[1]='active';
        break;
        case 'operator/arusKas/update':
            $operatorActgMenu[1]='active';
        break;
        case 'operator/arusKas/admin':
            $operatorActgMenu[1]='active';
        break;
        case 'operator/arusKas/delete':
            $operatorActgMenu[1]='active';
        break;
        case 'operator/chartOfAccount/admin':
            $operatorActgMenu[1]='active';
        break;
        case 'operator/tender/admin':
            $operatorTender[1]='active';
        break;
        case 'operator/tender/create':
            $operatorTender[2]='active';
        break;
        case 'operator/tender/update':
            $operatorTender[3]='active';
        break;
        case 'operator/cv/create':
            $operatorBankCV[1]='active';
        break;
        case 'operator/cv/admin':
            $operatorBankCV[2]='active';
        break;
        case 'operator/cv/adminn':
            $operatorBankCV[3]='active';
        break;
        case 'operator/cv/adminm':
            $operatorBankCV[4]='active';
        break;
        case 'operator/gaji/grafik':
            $operatorRptGraph[1]='active';
        break;
        case 'operator/gaji/cetak':
            $operatorRptPayAnn[1]='active';
        break;
        case 'operator/gaji/cetakx':
            $operatorRptPayMon[1]='active';
        break;
        case 'operator/gaji/cetaky':
            $operatorRptPayDai[1]='active';
        break;
        case 'operator/gaji/doAnnual':
            $operatorRptDepAnn[1]='active';
        break;
        case 'operator/gaji/doMonthly':
            $operatorRptDepMon[1]='active';
        break;
        case 'operator/gaji/doDaily':
            $operatorRptDepDai[1]='active';
        break;
        case 'operator/expenses/doAnnual':
            $operatorRptExpAnn[1]='active';
        break;
        case 'operator/expenses/doMonthly':
            $operatorRptExpMon[1]='active';
        break;
        case 'operator/expenses/doDaily':
            $operatorRptExpDai[1]='active';
        break;


        case 'operator/divisi/admin':
            $operatorSetEmpDiv[1]='active';
        break;
        case 'operator/divisi/create':
            $operatorSetEmpDiv[2]='active';
        break;
        case 'operator/divisi/update':
            $operatorSetEmpDiv[3]='active';
        break;


        case 'operator/lokasiKerja/admin':
            $operatorSetEmpWp[1]='active';
        break;
        case 'operator/lokasiKerja/create':
            $operatorSetEmpWp[2]='active';
        break;
        case 'operator/lokasiKerja/update':
            $operatorSetEmpWp[3]='active';
        break;


        case 'operator/jenisTinggal/admin':
            $operatorSetEmpKos[1]='active';
        break;
        case 'operator/jenisTinggal/create':
            $operatorSetEmpKos[2]='active';
        break;
        case 'operator/jenisTinggal/update':
            $operatorSetEmpKos[3]='active';
        break;


        case 'operator/alatTransport/admin':
            $operatorSetEmpTrans[1]='active';
        break;
        case 'operator/alatTransport/create':
            $operatorSetEmpTrans[2]='active';
        break;
        case 'operator/alatTransport/update':
            $operatorSetEmpTrans[3]='active';
        break;


        case 'operator/jenisPendaftaran/admin':
            $operatorSetEmpKor[1]='active';
        break;
        case 'operator/jenisPendaftaran/create':
            $operatorSetEmpKor[2]='active';
        break;
        case 'operator/jenisPendaftaran/update':
            $operatorSetEmpKor[3]='active';
        break;


        case 'operator/agama/admin':
            $operatorSetEmpRlg[1]='active';
        break;
        case 'operator/agama/create':
            $operatorSetEmpRlg[2]='active';
        break;
        case 'operator/agama/update':
            $operatorSetEmpRlg[3]='active';
        break;


        case 'operator/jenjangPendidikan/admin':
            $operatorSetEmpEdul[1]='active';
        break;
        case 'operator/jenjangPendidikan/create':
            $operatorSetEmpEdul[2]='active';
        break;
        case 'operator/jenjangPendidikan/update':
            $operatorSetEmpEdul[3]='active';
        break;


        case 'operator/pekerjaan/admin':
            $operatorSetEmpPfs[1]='active';
        break;
        case 'operator/pekerjaan/create':
            $operatorSetEmpPfs[2]='active';
        break;
        case 'operator/pekerjaan/update':
            $operatorSetEmpPfs[3]='active';
        break;


        case 'operator/penghasilan/admin':
            $operatorSetEmpInc[1]='active';
        break;
        case 'operator/penghasilan/create':
            $operatorSetEmpInc[2]='active';
        break;
        case 'operator/penghasilan/update':
            $operatorSetEmpInc[3]='active';
        break;


        case 'operator/ptkp/admin':
            $operatorSetFinPtkp[1]='active';
        break;
        case 'operator/ptkp/create':
            $operatorSetFinPtkp[2]='active';
        break;
        case 'operator/ptkp/update':
            $operatorSetFinPtkp[3]='active';
        break;


        case 'operator/jabatan/admin':
            $operatorSetFinJbtn[1]='active';
        break;
        case 'operator/jabatan/create':
            $operatorSetFinJbtn[2]='active';
        break;
        case 'operator/jabatan/update':
            $operatorSetFinJbtn[3]='active';
        break;


        case 'operator/bpjs/admin':
            $operatorSetFinBpjs[1]='active';
        break;
        case 'operator/bpjs/create':
            $operatorSetFinBpjs[2]='active';
        break;
        case 'operator/bpjs/update':
            $operatorSetFinBpjs[3]='active';
        break;

        case 'operator/bank/admin':
            $operatorSetFinBank[1]='active';
        break;
        case 'operator/bank/create':
            $operatorSetFinBank[2]='active';
        break;
        case 'operator/bank/update':
            $operatorSetFinBank[3]='active';
        break;


        case 'operator/pagesize/update':
            $operatorSetRpp[1]='active';
        break;

        case 'operator/chat/admin':
            $operatorSetChat[1]='active';
        break;

        case 'operator/company/update':
            $operatorSetCmp[1]='active';
        break;

        # EMPLOYEE #

        case 'employee/expenses/admin':
            $employeeExp[1]='active';
        break;
        case 'employee/expenses/create':
            $employeeExp[1]='active';
        break;
        case 'employee/expenses/picture':
            $employeeExp[1]='active';
        break;
        case 'employee/expensesDetail/admin':
            $employeeExpDetail[1]='active';
        break;
    }
}else{
    $url='';
}
?>