<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/logo/spartan.png" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo Yii::app()->request->baseUrl; ?>/css/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                                  
		
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/captcha/jquery-1.3.2.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/captcha/ui.core.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/captcha/ui.sortable.js"></script>
		
        <style type="text/css">
            form.registration{
                width:10%;
                margin: 10px auto;
                padding:10px;
                font-family: "Trebuchet MS";   
            }
            form.registration fieldset{
                background-color:#707070;
                border:none;
                padding:10px;
                -moz-box-shadow: 0 1px 3px rgba(0,0,0,0.5);
                -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.5);
                -moz-border-radius: 15px;
                -webkit-border-radius: 15px;				          
                padding:6px;
                margin:0px 30px 0px 0px;
            }
            form.registration legend{
                text-align:left;
                color:#fff;
                font-size:14px;
                padding:0px 4px 15px 4px;
                font-weight:bold;
            }
            form.registration label{
                font-size: 18px;
                width:200px;
                float: left;
                text-align: right;
                clear:left;
                margin:4px 4px 0px 0px;
                padding:0px;
                color: #FFF;
                text-shadow: 0 1px 1px rgba(0,0,0,0.8);
            }
            form.registration input{
                font-family: "Trebuchet MS";
                font-size: 18px;
                float:left;
                width:10%;
                border:1px solid #cccccc;
                margin:2px 0px 4px 2px;
                color:#00abdf;
                height:26px;
                padding:3px;
                -moz-box-shadow: 0 1px 3px rgba(0,0,0,0.5);
                -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.5);
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
            }
            form.registration input:focus, form.registration select:focus{
                background-color:#E0E6FF;
            }
            form.registration select{
                font-family: "Trebuchet MS";
                font-size: 20px;
                float:left;
                border:1px solid #cccccc;
                margin:2px 0px 2px 2px;
                color:#00abdf;
                height:32px;
                -moz-box-shadow: 0 1px 3px rgba(0,0,0,0.5);
                -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.5);
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
            }
            .button, .button:visited{
                float:right;
                background: #777 url(images/overlay.png) repeat-x; 
                font-weight:bold;
                display: inline-block; 
                padding: 5px 10px 6px; 
                color: #fff; 
                text-decoration: none;
                -moz-border-radius: 5px; 
                -webkit-border-radius: 5px;
                -moz-box-shadow: 0 1px 3px rgba(0,0,0,0.5);
                -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.5);
                text-shadow: 0 -1px 1px rgba(0,0,0,0.25);
                border-bottom: 1px solid rgba(0,0,0,0.25);
                cursor: pointer;
                margin-top:95px;
                margin-right:15px;
            }
            .button:hover{
                background-color: #007d9a; 
            }
            #sortable {
                list-style-type: none;
                margin: 10px 19px 10px 19px;
                padding: 0;
            }
            #sortable li {
                margin: 1% 7% 5% 7%;
                padding: 10px;
                float: left;
                width: 55px;
                height: 55px;
                font-size: 30px;
                text-align: center;
                line-height:35px;
                cursor:pointer;
                -moz-border-radius:5px;
                -webkit-border-radius:5px;
                -moz-box-shadow: 0 1px 1px rgba(0,0,0,0.5);
                -webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.5);
                text-shadow: 0 -1px 1px rgba(0,0,0,0.25);
                background:#777 url(images/overlay.png) repeat-x scroll 50% 50%;
                color:#fff;
                font-weight:normal;
            }
            .captcha_wrap{
                border:1px solid #fff;
                -moz-border-radius:10px;
                -webkit-border-radius:10px;
                -moz-box-shadow: 0 1px 3px rgba(0,0,0,0.5);
                -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.5);
                float:left;
                height:100%;
                overflow:auto;
                width:100%;
                overflow:hidden;
                margin:10px 0px 0px 0px;
                background-color:rgba(0, 0, 0, 0.1);
            }
            .captcha{
                -moz-border-radius:10px;
                -webkit-border-radius:10px;
                font-size:12px;
                color:#BBBBBB;
                text-align: center;
                border-bottom:1px solid #CCC;
                background-color:#fff;
            }
        </style>
        <!-- <script type="text/javascript">
            (
            function($){

                $.fn.shuffle = function() {
                    return this.each(function(){
                        var items = $(this).children();

                        return (items.length)
                            ? $(this).html($.shuffle(items,$(this)))
                        : this;
                    });
                }

                $.fn.validate = function() {
                    var res = false;
                    this.each(function(){
                        var arr = $(this).children();
                        res =    ((arr[0].innerHTML=="1")&&
                            (arr[1].innerHTML=="2")&&
                            (arr[2].innerHTML=="3")&&
                            (arr[3].innerHTML=="4")&&
                            (arr[4].innerHTML=="5")&&
                            (arr[5].innerHTML=="6"));
                    });
                    return res;
                }

                $.shuffle = function(arr,obj) {
                    for(
                    var j, x, i = arr.length; i;
                    j = parseInt(Math.random() * i),
                    x = arr[--i], arr[i] = arr[j], arr[j] = x
                );
                    if(arr[0].innerHTML=="1") obj.html($.shuffle(arr,obj))
                    else return arr;
                }

            })(jQuery);

            $(function() {
                $("#sortable").sortable();
                $("#sortable").disableSelection();
                $('ul').shuffle();
				var url = "http://stackoverflow.com";  
                $("#formsubmit").click(function(){
                    ($('ul').validate()) ? "" : confirm("Captcha yang anda masukkan salah. Silahkan coba lagi!") ? window.history.back(-1):window.history.back(-1);
                });
				//alert("Captcha yang anda masukkan salah. Silahkan coba lagi!")
            });
        </script> -->
    </head>
    <body>
		<div class="login-container">
			<?php echo $content; ?>
		</div>
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <!-- END TEMPLATE -->
		 <script>
            $(function(){
                
                pageLoadingFrame("show","v1");
                setTimeout(function(){
                    pageLoadingFrame("hide");
                },2000);     
                
                $("#loader_v1").on("click",function(){
                    pageLoadingFrame("show","v1");
                    setTimeout(function(){
                        pageLoadingFrame("hide");
                    },2000);     
                });
                
                $("#loader_v2").on("click",function(){
                    pageLoadingFrame("show");
                    setTimeout(function(){
                        pageLoadingFrame("hide");
                    },2000);     
                });
                
            });
        </script>      
    <!-- END SCRIPTS --> 
    </body>
</html>