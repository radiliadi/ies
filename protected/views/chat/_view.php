<?php
/* @var $this ChatController */
/* @var $data Chat */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('chat_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->chat_id), array('view', 'id'=>$data->chat_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('chat_from')); ?>:</b>
	<?php echo CHtml::encode($data->chat_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('chat_to')); ?>:</b>
	<?php echo CHtml::encode($data->chat_to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('chat_text')); ?>:</b>
	<?php echo CHtml::encode($data->chat_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('chat_status')); ?>:</b>
	<?php echo CHtml::encode($data->chat_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('chat_date')); ?>:</b>
	<?php echo CHtml::encode($data->chat_date); ?>
	<br />


</div>