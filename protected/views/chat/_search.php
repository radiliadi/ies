<?php
/* @var $this ChatController */
/* @var $model Chat */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'chat_id'); ?>
		<?php echo $form->textField($model,'chat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'chat_from'); ?>
		<?php echo $form->textField($model,'chat_from',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'chat_to'); ?>
		<?php echo $form->textField($model,'chat_to',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'chat_text'); ?>
		<?php echo $form->textArea($model,'chat_text',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'chat_status'); ?>
		<?php echo $form->textField($model,'chat_status',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'chat_date'); ?>
		<?php echo $form->textField($model,'chat_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->