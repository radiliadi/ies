<?php
/* @var $this ChatController */
/* @var $model Chat */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'chat-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'chat_from'); ?>
		<?php echo $form->textField($model,'chat_from',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'chat_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'chat_to'); ?>
		<?php echo $form->textField($model,'chat_to',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'chat_to'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'chat_text'); ?>
		<?php echo $form->textArea($model,'chat_text',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'chat_text'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'chat_status'); ?>
		<?php echo $form->textField($model,'chat_status',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'chat_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'chat_date'); ?>
		<?php echo $form->textField($model,'chat_date'); ?>
		<?php echo $form->error($model,'chat_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->