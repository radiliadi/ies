<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
'id'=>'tender-form',
'enableAjaxValidation'=>true,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-md-1">
    </div>
        <div class="col-md-10">
            <form class="form-horizontal">
                <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-4 col-xs-12 control-label">Username</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <?php echo $form->textField($model,'username',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Username",'readonly'=>"readonly"));?>
                                        <?php echo $form->error($model,'username'); ?>

                                    </div>                                            
                                </div>
                            </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-12 control-label">Password</label>
                                    <div class="col-md-6 col-xs-12">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <?php echo $form->textField($model,'password',array('size'=>50,'maxlength'=>50,'class'=>"form-control",'placeholder'=>"Password"));?>
                                            <?php echo $form->error($model,'password'); ?>
                                        </div>                                            
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-12 control-label">Photo</label>
                                    <div class="col-md-6 col-xs-12">                                            
                                        <?php echo $form->fileFieldGroup($model,'user_foto',array()); ?>
                                        <?php echo $form->error($model,'user_foto'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-12 control-label"></label>
                                    <div class="col-md-6 col-xs-12">                                            
                                        <?php 
										 if($model->isNewRecord){
										  echo CHtml::image(Yii::app()->request->baseUrl.'/images/no-image.jpg',"user_foto",array("width"=>300));
										  }elseif(empty($model->user_foto)){
										  echo CHtml::image(Yii::app()->request->baseUrl.'/images/no-image.jpg',"user_foto",array("width"=>300));
										  }else{
										  echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$model->user_foto,"user_foto",array("width"=>300));
										  }
										  ?>  
										</div>
                                </div>
                        </div>
                            <div class="panel-footer">
                                <label class="col-md-11 col-xs-12 control-label"></label>
                                <div class="col-md-1 col-xs-12">                     
                                    <?php $this->widget('booster.widgets.TbButton', array(
    									'buttonType'=>'submit',
    									'context'=>'primary',
    									'label'=>$model->isNewRecord ? 'Create' : 'Save',
    								)); ?>
                                </div>
                            </div>
                </div>
        </form>
    </div>
    <div class="col-md-1">  
    </div>
</div> 
<?php $this->endWidget(); ?>