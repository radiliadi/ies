<?php
/* @var $this BankLogController */
/* @var $data BankLog */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->bnkl), array('view', 'id'=>$data->bnkl)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl_bnk_id')); ?>:</b>
	<?php echo CHtml::encode($data->bnkl_bnk_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl_coa_id')); ?>:</b>
	<?php echo CHtml::encode($data->bnkl_coa_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl_coat_id')); ?>:</b>
	<?php echo CHtml::encode($data->bnkl_coat_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl_coar_id')); ?>:</b>
	<?php echo CHtml::encode($data->bnkl_coar_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl_id_pd')); ?>:</b>
	<?php echo CHtml::encode($data->bnkl_id_pd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl_log')); ?>:</b>
	<?php echo CHtml::encode($data->bnkl_log); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl_saldo_current')); ?>:</b>
	<?php echo CHtml::encode($data->bnkl_saldo_current); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl_saldo_used')); ?>:</b>
	<?php echo CHtml::encode($data->bnkl_saldo_used); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl_datetime_insert')); ?>:</b>
	<?php echo CHtml::encode($data->bnkl_datetime_insert); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl_datetime_update')); ?>:</b>
	<?php echo CHtml::encode($data->bnkl_datetime_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl_datetime_delete')); ?>:</b>
	<?php echo CHtml::encode($data->bnkl_datetime_delete); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl_insert_by')); ?>:</b>
	<?php echo CHtml::encode($data->bnkl_insert_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl_update_by')); ?>:</b>
	<?php echo CHtml::encode($data->bnkl_update_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl_delete_by')); ?>:</b>
	<?php echo CHtml::encode($data->bnkl_delete_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnkl_status')); ?>:</b>
	<?php echo CHtml::encode($data->bnkl_status); ?>
	<br />

	*/ ?>

</div>