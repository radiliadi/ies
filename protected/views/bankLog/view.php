<?php
/* @var $this BankLogController */
/* @var $model BankLog */

$this->breadcrumbs=array(
	'Bank Logs'=>array('index'),
	$model->bnkl,
);

$this->menu=array(
	array('label'=>'List BankLog', 'url'=>array('index')),
	array('label'=>'Create BankLog', 'url'=>array('create')),
	array('label'=>'Update BankLog', 'url'=>array('update', 'id'=>$model->bnkl)),
	array('label'=>'Delete BankLog', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->bnkl),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BankLog', 'url'=>array('admin')),
);
?>

<h1>View BankLog #<?php echo $model->bnkl; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'bnkl',
		'bnkl_bnk_id',
		'bnkl_coa_id',
		'bnkl_coat_id',
		'bnkl_coar_id',
		'bnkl_id_pd',
		'bnkl_log',
		'bnkl_saldo_current',
		'bnkl_saldo_used',
		'bnkl_datetime_insert',
		'bnkl_datetime_update',
		'bnkl_datetime_delete',
		'bnkl_insert_by',
		'bnkl_update_by',
		'bnkl_delete_by',
		'bnkl_status',
	),
)); ?>
