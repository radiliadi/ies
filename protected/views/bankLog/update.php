<?php
/* @var $this BankLogController */
/* @var $model BankLog */

$this->breadcrumbs=array(
	'Bank Logs'=>array('index'),
	$model->bnkl=>array('view','id'=>$model->bnkl),
	'Update',
);

$this->menu=array(
	array('label'=>'List BankLog', 'url'=>array('index')),
	array('label'=>'Create BankLog', 'url'=>array('create')),
	array('label'=>'View BankLog', 'url'=>array('view', 'id'=>$model->bnkl)),
	array('label'=>'Manage BankLog', 'url'=>array('admin')),
);
?>

<h1>Update BankLog <?php echo $model->bnkl; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>