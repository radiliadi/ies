<?php
/* @var $this BankLogController */
/* @var $model BankLog */

$this->breadcrumbs=array(
	'Bank Logs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List BankLog', 'url'=>array('index')),
	array('label'=>'Create BankLog', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#bank-log-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Bank Logs</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'bank-log-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'bnkl',
		'bnkl_bnk_id',
		'bnkl_coa_id',
		'bnkl_coat_id',
		'bnkl_coar_id',
		'bnkl_id_pd',
		/*
		'bnkl_log',
		'bnkl_saldo_current',
		'bnkl_saldo_used',
		'bnkl_datetime_insert',
		'bnkl_datetime_update',
		'bnkl_datetime_delete',
		'bnkl_insert_by',
		'bnkl_update_by',
		'bnkl_delete_by',
		'bnkl_status',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
