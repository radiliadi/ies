<?php
/* @var $this BankLogController */
/* @var $model BankLog */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bank-log-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'bnkl_bnk_id'); ?>
		<?php echo $form->textField($model,'bnkl_bnk_id'); ?>
		<?php echo $form->error($model,'bnkl_bnk_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnkl_coa_id'); ?>
		<?php echo $form->textField($model,'bnkl_coa_id'); ?>
		<?php echo $form->error($model,'bnkl_coa_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnkl_coat_id'); ?>
		<?php echo $form->textField($model,'bnkl_coat_id'); ?>
		<?php echo $form->error($model,'bnkl_coat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnkl_coar_id'); ?>
		<?php echo $form->textField($model,'bnkl_coar_id'); ?>
		<?php echo $form->error($model,'bnkl_coar_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnkl_id_pd'); ?>
		<?php echo $form->textField($model,'bnkl_id_pd',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'bnkl_id_pd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnkl_log'); ?>
		<?php echo $form->textField($model,'bnkl_log',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'bnkl_log'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnkl_saldo_current'); ?>
		<?php echo $form->textField($model,'bnkl_saldo_current',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'bnkl_saldo_current'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnkl_saldo_used'); ?>
		<?php echo $form->textField($model,'bnkl_saldo_used',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'bnkl_saldo_used'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnkl_datetime_insert'); ?>
		<?php echo $form->textField($model,'bnkl_datetime_insert'); ?>
		<?php echo $form->error($model,'bnkl_datetime_insert'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnkl_datetime_update'); ?>
		<?php echo $form->textField($model,'bnkl_datetime_update'); ?>
		<?php echo $form->error($model,'bnkl_datetime_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnkl_datetime_delete'); ?>
		<?php echo $form->textField($model,'bnkl_datetime_delete'); ?>
		<?php echo $form->error($model,'bnkl_datetime_delete'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnkl_insert_by'); ?>
		<?php echo $form->textField($model,'bnkl_insert_by',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'bnkl_insert_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnkl_update_by'); ?>
		<?php echo $form->textField($model,'bnkl_update_by',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'bnkl_update_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnkl_delete_by'); ?>
		<?php echo $form->textField($model,'bnkl_delete_by',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'bnkl_delete_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnkl_status'); ?>
		<?php echo $form->textField($model,'bnkl_status'); ?>
		<?php echo $form->error($model,'bnkl_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->