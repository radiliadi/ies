<?php
/* @var $this BankLogController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bank Logs',
);

$this->menu=array(
	array('label'=>'Create BankLog', 'url'=>array('create')),
	array('label'=>'Manage BankLog', 'url'=>array('admin')),
);
?>

<h1>Bank Logs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
