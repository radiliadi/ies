<?php
/* @var $this BankLogController */
/* @var $model BankLog */

$this->breadcrumbs=array(
	'Bank Logs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BankLog', 'url'=>array('index')),
	array('label'=>'Manage BankLog', 'url'=>array('admin')),
);
?>

<h1>Create BankLog</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>