<?php
/* @var $this BankLogController */
/* @var $model BankLog */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'bnkl'); ?>
		<?php echo $form->textField($model,'bnkl',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bnkl_bnk_id'); ?>
		<?php echo $form->textField($model,'bnkl_bnk_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bnkl_coa_id'); ?>
		<?php echo $form->textField($model,'bnkl_coa_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bnkl_coat_id'); ?>
		<?php echo $form->textField($model,'bnkl_coat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bnkl_coar_id'); ?>
		<?php echo $form->textField($model,'bnkl_coar_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bnkl_id_pd'); ?>
		<?php echo $form->textField($model,'bnkl_id_pd',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bnkl_log'); ?>
		<?php echo $form->textField($model,'bnkl_log',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bnkl_saldo_current'); ?>
		<?php echo $form->textField($model,'bnkl_saldo_current',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bnkl_saldo_used'); ?>
		<?php echo $form->textField($model,'bnkl_saldo_used',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bnkl_datetime_insert'); ?>
		<?php echo $form->textField($model,'bnkl_datetime_insert'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bnkl_datetime_update'); ?>
		<?php echo $form->textField($model,'bnkl_datetime_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bnkl_datetime_delete'); ?>
		<?php echo $form->textField($model,'bnkl_datetime_delete'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bnkl_insert_by'); ?>
		<?php echo $form->textField($model,'bnkl_insert_by',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bnkl_update_by'); ?>
		<?php echo $form->textField($model,'bnkl_update_by',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bnkl_delete_by'); ?>
		<?php echo $form->textField($model,'bnkl_delete_by',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bnkl_status'); ?>
		<?php echo $form->textField($model,'bnkl_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->