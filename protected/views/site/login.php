<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);


if(!Yii::app()->user->isGuest)
{
	switch (Yii::app()->session->get('level')){
		case 'operator':	
		$this->redirect('operator');
		break;
		case 'management':
		$this->redirect('management');
		break;
        case 'finance':
        $this->redirect('finance');
        break;
        case 'hrd':
        $this->redirect('hrd');
        break;
        case 'employee':
        $this->redirect('employee');
        break;
	}
}
?>
<div class="" style="margin-top:-5%;">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
			<div class="login-box animated fadeInDown">
                <div class="login-body">
				<?php echo CHtml::Image('logo/spartan.png','SEA',array('width'=>'77px','style'=>'float:left;margin:3px;')); ?>
                    <div class="login-title" style="margin:10px;"><strong><?php echo CHtml::encode(Yii::app()->name); ?></strong></div>
                    <div class="login-title" style="margin:10px;"><strong>Welcome</strong>, please login</div>
                    <form action="index.html" class="form-horizontal" method="post">
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="input-group">
                                <div style="color:white;"><?php echo $form->errorSummary($model); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <span class="fa fa-user"></span>
                                </div>
								<?php echo $form->textField($model,'username',array('class'=>'form-control', 'placeholder'=>'Input ID Here', 'autocomplete'=>'off', 'autofocus'=>'on')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <span class="fa fa-lock"></span>
                                </div>                                
                                 <?php echo $form->passwordField($model,'password',array('class'=>'form-control', 'placeholder'=>'Input Password Here')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                        </div>          
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
							<?php echo CHtml::submitButton('Login',array('class'=>'btn btn-primary btn-lg btn-block', 'id'=>'formsubmit')); ?>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="login-footer" style="margin:10px;">
                        &copy; 2016 - <?php echo date('Y'); ?> | PT. Spartan Eragon Asia
                </div>
            </div>
            

<?php $this->endWidget(); ?>
</div><!-- form -->