<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<div class="error-container">
    <div class="error-code"><font color = "white"><b><?php echo $code; ?></b></font></div>
    <div class="error-text"><font color = "white">oops sorry :(</font></div>
    <div class="error-subtext"><?php echo CHtml::encode($message); ?></div>
    <div class="error-actions">                                
        <div class="row">
            <div class="col-md-6">
                <button class="btn btn-default btn-block btn-lg" onClick="history.back();">Previous</button>
            </div>
            <div class="col-md-6">
                <button class="btn btn-danger btn-block btn-lg" onClick="document.location.href = '/ies/site/logout'">Let me out</button>
            </div>
        </div>                                
    </div>
</div>