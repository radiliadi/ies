<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>       
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- LESSCSS INCLUDE -->        
        <link rel="stylesheet/less" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/css/styles.less"/>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/plugins/lesscss/less.min.js"></script>                
        <!-- EOF LESSCSS INCLUDE -->                                       
    </head>
    <body>
        <div class="error-container">
            <div class="error-text"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/logo.png" alt="Youkai"/></div>
            <div class="error-code"><?php echo $modelFak->sms['nm_lemb'];?></div>
            <div class="error-text"><?php echo $pesan?></div>
            <div class="error-actions">                                
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-info btn-block btn-lg" onClick="document.location.href = 'logout';">Back to login</button>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-primary btn-block btn-lg" onClick="history.back();">Previous page</button>
                    </div>
                </div>                                
            </div>
        </div>                 
    </body>
</html>






