<?php

$level=Yii::app()->session->get('level');

if(Yii::app()->user->isGuest)
{
	$this->redirect('index.php?r=site/login');
}else{
	switch ($level){
		case 'operator':	
		$this->redirect('../operator');
		break;
		case 'management':	
		$this->redirect('../management');
		break;
		case 'finance':	
		$this->redirect('../finance');
		break;
		case 'hrd':	
		$this->redirect('../hrd');
		break;
		case 'employee':
		$this->redirect('../employee');
		break;
		default:
		$this->redirect('../');
	}
}

$this->pageTitle=Yii::app()->name;

?>