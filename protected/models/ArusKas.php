<?php

/**
 * This is the model class for table "tbl_arus_kas".
 *
 * The followings are the available columns in table 'tbl_arus_kas':
 * @property integer $ak_id
 * @property string $ak_desc
 * @property string $ak_date
 * @property integer $ak_is_checked
 * @property string $ak_checked_by
 * @property string $ak_datetime_checked
 * @property string $ak_datetime_insert
 * @property integer $ak_is_locked
 * @property integer $ak_status
 */
class ArusKas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_arus_kas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ak_date, ak_desc','required'),
			array('ak_is_checked, ak_is_locked, ak_status', 'numerical', 'integerOnly'=>true),
			array('ak_checked_by', 'length', 'max'=>255),
			array('ak_datetime_insert, ak_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ak_id, ak_desc, ak_date, ak_is_checked, ak_checked_by, ak_datetime_checked, ak_datetime_insert, ak_datetime_update, ak_datetime_delete, ak_insert_by, ak_update_by, ak_delete_by, ak_is_locked, ak_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ak_id' => 'ID',
			'ak_desc' => 'Desc',
			'ak_date' => ' Date',
			'ak_is_checked' => 'Checked',
			'ak_checked_by' => 'Checked By',
			'ak_datetime_checked' => 'Datetime Checked',
			'ak_datetime_insert' => 'Datetime Insert',
			'ak_datetime_update' => 'Datetime Update',
			'ak_datetime_delete' => 'Datetime Delete',
			'ak_insert_by' => 'Insert By',
			'ak_update_by' => 'Update By',
			'ak_delete_by' => 'Delete By',
			'ak_is_locked' => 'Locked',
			'ak_status' => 'Status',
		);
	}

	public function beforeSave()
    {
    	$id_pd=Yii::app()->session->get('username');
        if($this->isNewRecord || $this->ak_datetime_insert==null){
            $this->ak_datetime_insert=new CDbExpression('NOW()');
            $this->ak_insert_by = $id_pd;
        }else{
            $this->ak_datetime_update = new CDbExpression('NOW()');
            $this->ak_update_by = $id_pd;
        }
        return parent::beforeSave();
    }

    public static function getCount(){
		$count = ArusKas::model()->findAll(array('select'=>'ak_id', 'condition'=>'ak_status=:ak_status', 'params'=>array(':ak_status'=>1)));
		return count($count);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('ak_id',$this->ak_id);
		$criteria->compare('ak_desc',$this->ak_desc,true);
		$criteria->compare('ak_date',$this->ak_date,true);
		$criteria->compare('ak_is_checked',$this->ak_is_checked);
		$criteria->compare('ak_checked_by',$this->ak_checked_by,true);
		$criteria->compare('ak_datetime_checked',$this->ak_datetime_checked,true);
		$criteria->compare('ak_datetime_insert',$this->ak_datetime_insert,true);
		$criteria->compare('ak_datetime_update',$this->ak_datetime_update,true);
		$criteria->compare('ak_datetime_delete',$this->ak_datetime_delete,true);
		$criteria->compare('ak_insert_by',$this->ak_insert_by,true);
		$criteria->compare('ak_update_by',$this->ak_update_by,true);
		$criteria->compare('ak_delete_by',$this->ak_delete_by,true);
		$criteria->compare('ak_is_locked',$this->ak_is_locked);
		$criteria->compare('ak_status',$this->ak_status);
		$criteria->order='ak_date DESC';
		// $criteria->addCondition('ak_status = 1');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
					'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ArusKas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
