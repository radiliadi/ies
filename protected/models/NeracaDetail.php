<?php

/**
 * This is the model class for table "tbl_neraca_detail".
 *
 * The followings are the available columns in table 'tbl_neraca_detail':
 * @property integer $nrcd_id
 * @property integer $nrcd_nrc_id
 * @property string $nrcd_date
 * @property integer $nrcd_nrcp_id
 * @property integer $nrcd_nrcc_id
 * @property string $nrcd_desc
 * @property string $nrcd_price
 * @property string $nrcd_bni_idr
 * @property string $nrcd_bni_giro
 * @property string $nrcd_bni_kso
 * @property string $nrcd_bni_usd
 * @property string $nrcd_piutang_usaha
 * @property string $nrcd_piutang_kyw
 * @property string $nrcd_pdd
 * @property string $nrcd_tanah_bangunan
 * @property string $nrcd_datetime_insert
 * @property string $nrcd_datetime_update
 * @property string $nrcd_datetime_delete
 * @property string $nrcd_insert_by
 * @property string $nrcd_update_by
 * @property string $nrcd_delete_by
 * @property integer $nrcd_status
 */
class NeracaDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_neraca_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nrcd_nrc_id', 'required'),
			array('nrcd_nrc_id, nrcd_nrcp_id, nrcd_nrcc_id, nrcd_sort, nrcd_status', 'numerical', 'integerOnly'=>true),
			array('nrcd_price, nrcd_kas, nrcd_bni_idr, nrcd_bni_giro, nrcd_bni_kso, nrcd_bni_usd, nrcd_piutang_usaha, nrcd_piutang_kyw, nrcd_pdd, nrcd_tanah_bangunan, nrcd_sum_1, nrcd_sum_2, nrcd_sum_3, nrcd_sum_4, nrcd_sum_13, nrcd_sum_24', 'length', 'max'=>20),
			array('nrcd_insert_by, nrcd_update_by, nrcd_delete_by', 'length', 'max'=>255),
			array('nrcd_date, nrcd_desc, nrcd_datetime_insert, nrcd_datetime_update, nrcd_datetime_delete', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('nrcd_id, nrcd_nrc_id, nrcd_date, nrcd_is_generate, nrcd_nrcp_id, nrcd_nrcc_id, nrcd_sort, nrcd_desc, nrcd_price, nrcd_bni_idr, nrcd_bni_giro, nrcd_bni_kso, nrcd_bni_usd, nrcd_piutang_usaha, nrcd_piutang_kyw, nrcd_pdd, nrcd_tanah_bangunan, nrcd_sum_1, nrcd_sum_2, nrcd_sum_3, nrcd_sum_4, nrcd_sum_13, nrcd_sum_24, nrcd_datetime_insert, nrcd_datetime_update, nrcd_datetime_delete, nrcd_insert_by, nrcd_update_by, nrcd_delete_by, nrcd_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nrcd_id' => 'Nrcd',
			'nrcd_nrc_id' => 'Nrcd Nrc',
			'nrcd_date' => 'Nrcd Date',
			'nrcd_is_generate' => 'Nrcd Generate',
			'nrcd_nrcp_id' => 'Nrcd Parent',
			'nrcd_nrcc_id' => 'Nrcd Child',
			'nrcd_sort' => 'Nrcd Sort',
			'nrcd_desc' => 'Nrcd Desc',
			'nrcd_price' => 'Nrcd Price',
			'nrcd_kas' => 'Nrcd Kas',
			'nrcd_bni_idr' => 'Nrcd Bni Idr',
			'nrcd_bni_giro' => 'Nrcd Bni Giro',
			'nrcd_bni_kso' => 'Nrcd Bni Kso',
			'nrcd_bni_usd' => 'Nrcd Bni Usd',
			'nrcd_piutang_usaha' => 'Nrcd Piutang Usaha',
			'nrcd_piutang_kyw' => 'Nrcd Piutang Kyw',
			'nrcd_pdd' => 'Nrcd Pdd',
			'nrcd_tanah_bangunan' => 'Nrcd Tanah Bangunan',
			'nrcd_sum_1' => 'Nrcd SUM 1',
			'nrcd_sum_2' => 'Nrcd SUM 2',
			'nrcd_sum_3' => 'Nrcd SUM 3',
			'nrcd_sum_4' => 'Nrcd SUM 4',
			'nrcd_sum_13' => 'Nrcd SUM 13',
			'nrcd_sum_24' => 'Nrcd SUM 24',
			'nrcd_datetime_insert' => 'Nrcd Datetime Insert',
			'nrcd_datetime_update' => 'Nrcd Datetime Update',
			'nrcd_datetime_delete' => 'Nrcd Datetime Delete',
			'nrcd_insert_by' => 'Nrcd Insert By',
			'nrcd_update_by' => 'Nrcd Update By',
			'nrcd_delete_by' => 'Nrcd Delete By',
			'nrcd_status' => 'Nrcd Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nrcd_id',$this->nrcd_id);
		$criteria->compare('nrcd_nrc_id',$this->nrcd_nrc_id);
		$criteria->compare('nrcd_date',$this->nrcd_date,true);
		$criteria->compare('nrcd_is_generate',$this->nrcd_is_generate);
		$criteria->compare('nrcd_nrcp_id',$this->nrcd_nrcp_id);
		$criteria->compare('nrcd_nrcc_id',$this->nrcd_nrcc_id);
		$criteria->compare('nrcd_sort',$this->nrcd_sort);
		$criteria->compare('nrcd_desc',$this->nrcd_desc,true);
		$criteria->compare('nrcd_price',$this->nrcd_price,true);
		$criteria->compare('nrcd_kas',$this->nrcd_kas,true);
		$criteria->compare('nrcd_bni_idr',$this->nrcd_bni_idr,true);
		$criteria->compare('nrcd_bni_giro',$this->nrcd_bni_giro,true);
		$criteria->compare('nrcd_bni_kso',$this->nrcd_bni_kso,true);
		$criteria->compare('nrcd_bni_usd',$this->nrcd_bni_usd,true);
		$criteria->compare('nrcd_piutang_usaha',$this->nrcd_piutang_usaha,true);
		$criteria->compare('nrcd_piutang_kyw',$this->nrcd_piutang_kyw,true);
		$criteria->compare('nrcd_pdd',$this->nrcd_pdd,true);
		$criteria->compare('nrcd_tanah_bangunan',$this->nrcd_tanah_bangunan,true);
		$criteria->compare('nrcd_sum_1',$this->nrcd_sum_1,true);
		$criteria->compare('nrcd_sum_2',$this->nrcd_sum_2,true);
		$criteria->compare('nrcd_sum_3',$this->nrcd_sum_3,true);
		$criteria->compare('nrcd_sum_4',$this->nrcd_sum_4,true);
		$criteria->compare('nrcd_sum_13',$this->nrcd_sum_31,true);
		$criteria->compare('nrcd_sum_24',$this->nrcd_sum_24,true);
		$criteria->compare('nrcd_datetime_insert',$this->nrcd_datetime_insert,true);
		$criteria->compare('nrcd_datetime_update',$this->nrcd_datetime_update,true);
		$criteria->compare('nrcd_datetime_delete',$this->nrcd_datetime_delete,true);
		$criteria->compare('nrcd_insert_by',$this->nrcd_insert_by,true);
		$criteria->compare('nrcd_update_by',$this->nrcd_update_by,true);
		$criteria->compare('nrcd_delete_by',$this->nrcd_delete_by,true);
		$criteria->compare('nrcd_status',$this->nrcd_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NeracaDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
