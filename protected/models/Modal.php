<?php

/**
 * This is the model class for table "tbl_modal".
 *
 * The followings are the available columns in table 'tbl_modal':
 * @property integer $mdl_id
 * @property string $mdl_desc
 * @property string $mdl_date
 * @property integer $mdl_is_checked
 * @property string $mdl_checked_by
 * @property string $mdl_datetime_checked
 * @property string $mdl_datetime_insert
 * @property integer $mdl_is_locked
 * @property integer $mdl_status
 */
class Modal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_modal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mdl_is_checked, mdl_is_locked, mdl_status', 'numerical', 'integerOnly'=>true),
			array('mdl_checked_by', 'length', 'max'=>255),
			array('mdl_desc, mdl_date, mdl_datetime_checked, mdl_datetime_insert, mdl_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('mdl_id, mdl_desc, mdl_date, mdl_is_checked, mdl_checked_by, mdl_datetime_checked, mdl_datetime_insert, mdl_datetime_update, mdl_datetime_delete, mdl_insert_by, mdl_update_by, mdl_delete_by, mdl_is_locked, mdl_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mdl_id' => 'Modal ID',
			'mdl_desc' => 'Desc',
			'mdl_date' => 'Date',
			'mdl_datetime_checked' => 'Checked',
			'mdl_datetime_insert' => 'Insert',
			'mdl_datetime_update' => 'Update',
			'mdl_datetime_delete' => 'Delete',
			'mdl_checked_by' => 'Checked By',
			'mdl_insert_by' => 'Inserted By',
			'mdl_update_by' => 'Updated By',
			'mdl_delete_by' => 'Deleted By',
			'mdl_is_checked' => 'Is Checked',
			'mdl_is_locked' => 'Is Locked',
			'mdl_status' => 'Status',
		);
	}

	public function beforeSave()
    {
    	$id_pd=Yii::app()->session->get('username');
        if($this->isNewRecord || $this->mdl_datetime_insert==null){
            $this->mdl_datetime_insert=new CDbExpression('NOW()');
            $this->mdl_insert_by = $id_pd;
        }else{
            $this->mdl_datetime_update = new CDbExpression('NOW()');
            $this->mdl_update_by = $id_pd;
        }
        return parent::beforeSave();
    }

    public static function getCount(){
		$count = Modal::model()->findAll(array('select'=>'mdl_id', 'condition'=>'mdl_status=:mdl_status', 'params'=>array(':mdl_status'=>1)));
		return count($count);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('mdl_id',$this->mdl_id);
		$criteria->compare('mdl_desc',$this->mdl_desc,true);
		$criteria->compare('mdl_date',$this->mdl_date,true);
		$criteria->compare('mdl_is_checked',$this->mdl_is_checked);
		$criteria->compare('mdl_checked_by',$this->mdl_checked_by,true);
		$criteria->compare('mdl_datetime_checked',$this->mdl_datetime_checked,true);
		$criteria->compare('mdl_datetime_insert',$this->mdl_datetime_insert,true);
		$criteria->compare('mdl_is_locked',$this->mdl_is_locked);
		$criteria->compare('mdl_status',$this->mdl_status);
		$criteria->order='mdl_datetime_insert DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Modal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
