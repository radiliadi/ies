<?php

/**
 * This is the model class for table "tbl_laba_detail".
 *
 * The followings are the available columns in table 'tbl_laba_detail':
 * @property integer $lbd_id
 * @property integer $lbd_lb_id
 * @property string $lbd_date
 * @property integer $lbd_is_generate
 * @property string $lbd_pendapatan
 * @property string $lbd_hrg_pokok_penjualan
 * @property string $lbd_bebas_transaksi
 * @property string $lbd_biy_gaji
 * @property string $lbd_bbn_lembur
 * @property string $lbd_bbn_pengobatan_kyw
 * @property string $lbd_bpjs_tkk
 * @property string $lbd_biy_sewa_kantor
 * @property string $lbd_bbn_atk
 * @property string $lbd_biy_tfi
 * @property string $lbd_bbn_prkr
 * @property string $lbd_bbn_lstrk
 * @property string $lbd_bp_bk
 * @property string $lbd_bbn_trans
 * @property string $lbd_bbn_entertain
 * @property string $lbd_k_rt
 * @property string $lbd_biy_ps
 * @property string $lbd_biy_ak
 * @property string $lbd_bpp_kantor
 * @property string $lbd_bpp_kendaraan
 * @property string $lbd_bpp_alat_kantor
 * @property string $lbd_bbn_susut
 * @property string $lbd_biy_adm_tndr
 * @property string $lbd_biy_adm_bank
 * @property string $lbd_bbn_askes
 * @property string $lbd_bbn_dinas
 * @property string $lbd_bbn_ll
 * @property string $lbd_pll
 * @property string $lbd_jasa_giro
 * @property string $lbd_datetime_insert
 * @property integer $lbd_status
 */
class LabaDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_laba_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lbd_lb_id', 'required'),
			array('lbd_lb_id, lbd_is_generate, lbd_status', 'numerical', 'integerOnly'=>true),
			array('lbd_pendapatan, lbd_hrg_pokok_penjualan, lbd_bbn_transaksi, lbd_biy_gaji, lbd_bbn_lembur, lbd_bbn_pengobatan_kyw, lbd_bpjs_tkk, lbd_biy_sewa_kantor, lbd_bbn_atk, lbd_biy_tfi, lbd_bbn_prkr, lbd_bbn_lstrk, lbd_bp_bk, lbd_bbn_trans, lbd_bbn_entertain, lbd_k_rt, lbd_biy_ps, lbd_biy_ak, lbd_bpp_kantor, lbd_bpp_kendaraan, lbd_bpp_alat_kantor, lbd_bbn_susut, lbd_biy_adm_tndr, lbd_biy_adm_bank, lbd_bbn_askes, lbd_bbn_dinas, lbd_bbn_ll, lbd_pll, lbd_jasa_giro, lbd_akd_laba_jalan', 'length', 'max'=>20),
			array('lbd_date, lbd_datetime_insert, lbd_datetime_update, lbd_insert_by, lbd_update_by', 'safe'), //utk built-in fucntion beforeSave
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('lbd_id, lbd_lb_id, lbd_date, lbd_is_generate, lbd_pendapatan, lbd_hrg_pokok_penjualan, lbd_bbn_transaksi, lbd_biy_gaji, lbd_bbn_lembur, lbd_bbn_pengobatan_kyw, lbd_bpjs_tkk, lbd_biy_sewa_kantor, lbd_bbn_atk, lbd_biy_tfi, lbd_bbn_prkr, lbd_bbn_lstrk, lbd_bp_bk, lbd_bbn_trans, lbd_bbn_entertain, lbd_k_rt, lbd_biy_ps, lbd_biy_ak, lbd_bpp_kantor, lbd_bpp_kendaraan, lbd_bpp_alat_kantor, lbd_bbn_susut, lbd_biy_adm_tndr, lbd_biy_adm_bank, lbd_bbn_askes, lbd_bbn_dinas, lbd_bbn_ll, lbd_pll, lbd_jasa_giro, lbd_datetime_insert, lbd_insert_by, lbd_datetime_update, lbd_update_by, lbd_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lbd_id' => 'Lbd',
			'lbd_lb_id' => 'Lbd Lb',
			'lbd_date' => 'Lbd Date',
			'lbd_is_generate' => 'Lbd Is Generate',
			'lbd_pendapatan' => 'Lbd Pendapatan',
			'lbd_hrg_pokok_penjualan' => 'Lbd Hrg Pokok Penjualan',
			'lbd_bbn_transaksi' => 'Lbd Beban Transaksi',
			'lbd_biy_gaji' => 'Lbd Biy Gaji',
			'lbd_bbn_lembur' => 'Lbd Bbn Lembur',
			'lbd_bbn_pengobatan_kyw' => 'Lbd Bbn Pengobatan Kyw',
			'lbd_bpjs_tkk' => 'Lbd Bpjs Tkk',
			'lbd_biy_sewa_kantor' => 'Lbd Biy Sewa Kantor',
			'lbd_bbn_atk' => 'Lbd Bbn Atk',
			'lbd_biy_tfi' => 'Lbd Biy Tfi',
			'lbd_bbn_prkr' => 'Lbd Bbn Prkr',
			'lbd_bbn_lstrk' => 'Lbd Bbn Lstrk',
			'lbd_bp_bk' => 'Lbd Bp Bk',
			'lbd_bbn_trans' => 'Lbd Bbn Trans',
			'lbd_bbn_entertain' => 'Lbd Bbn Entertain',
			'lbd_k_rt' => 'Lbd K Rt',
			'lbd_biy_ps' => 'Lbd Biy Ps',
			'lbd_biy_ak' => 'Lbd Biy Ak',
			'lbd_bpp_kantor' => 'Lbd Bpp Kantor',
			'lbd_bpp_kendaraan' => 'Lbd Bpp Kendaraan',
			'lbd_bpp_alat_kantor' => 'Lbd Bpp Alat Kantor',
			'lbd_bbn_susut' => 'Lbd Bbn Susut',
			'lbd_biy_adm_tndr' => 'Lbd Biy Adm Tndr',
			'lbd_biy_adm_bank' => 'Lbd Biy Adm Bank',
			'lbd_bbn_askes' => 'Lbd Bbn Askes',
			'lbd_bbn_dinas' => 'Lbd Bbn Dinas',
			'lbd_bbn_ll' => 'Lbd Bbn Ll',
			'lbd_pll' => 'Lbd Pll',
			'lbd_jasa_giro' => 'Lbd Jasa Giro',
			'lbd_akd_laba_jalan' => 'Lbd Akd Laba Jalan',
			'lbd_datetime_insert' => 'Lbd Datetime Insert',
			'lbd_insert_by' => 'Lbd Insert By',
			'lbd_datetime_update' => 'Lbd Datetime Update',
			'lbd_update_by' => 'Lbd Update By',
			'lbd_status' => 'Lbd Status',
		);
	}

	public function beforeSave()
    {
    	$id_pd=Yii::app()->session->get('username');
        if($this->isNewRecord || $this->lbd_datetime_insert==null){
            $this->lbd_datetime_insert=new CDbExpression('NOW()');
            $this->lbd_insert_by = $id_pd;
        }else{
            $this->lbd_datetime_update = new CDbExpression('NOW()');
            $this->lbd_update_by = $id_pd;
        }
        return parent::beforeSave();
    }

    // public function beforeSave()
    // {
    //     if($this->isNewRecord)
    //     {
    //         $this->createddate=new CDbExpression('NOW()');
    //     }
    //     $this->modifieddate = new CDbExpression('NOW()');
    //     return parent::beforeSave();
    // }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lbd_id',$this->lbd_id);
		$criteria->compare('lbd_lb_id',$this->lbd_lb_id);
		$criteria->compare('lbd_date',$this->lbd_date,true);
		$criteria->compare('lbd_is_generate',$this->lbd_is_generate);
		$criteria->compare('lbd_pendapatan',$this->lbd_pendapatan,true);
		$criteria->compare('lbd_hrg_pokok_penjualan',$this->lbd_hrg_pokok_penjualan,true);
		$criteria->compare('lbd_bbn_transaksi',$this->lbd_bbn_transaksi,true);
		$criteria->compare('lbd_biy_gaji',$this->lbd_biy_gaji,true);
		$criteria->compare('lbd_bbn_lembur',$this->lbd_bbn_lembur,true);
		$criteria->compare('lbd_bbn_pengobatan_kyw',$this->lbd_bbn_pengobatan_kyw,true);
		$criteria->compare('lbd_bpjs_tkk',$this->lbd_bpjs_tkk,true);
		$criteria->compare('lbd_biy_sewa_kantor',$this->lbd_biy_sewa_kantor,true);
		$criteria->compare('lbd_bbn_atk',$this->lbd_bbn_atk,true);
		$criteria->compare('lbd_biy_tfi',$this->lbd_biy_tfi,true);
		$criteria->compare('lbd_bbn_prkr',$this->lbd_bbn_prkr,true);
		$criteria->compare('lbd_bbn_lstrk',$this->lbd_bbn_lstrk,true);
		$criteria->compare('lbd_bp_bk',$this->lbd_bp_bk,true);
		$criteria->compare('lbd_bbn_trans',$this->lbd_bbn_trans,true);
		$criteria->compare('lbd_bbn_entertain',$this->lbd_bbn_entertain,true);
		$criteria->compare('lbd_k_rt',$this->lbd_k_rt,true);
		$criteria->compare('lbd_biy_ps',$this->lbd_biy_ps,true);
		$criteria->compare('lbd_biy_ak',$this->lbd_biy_ak,true);
		$criteria->compare('lbd_bpp_kantor',$this->lbd_bpp_kantor,true);
		$criteria->compare('lbd_bpp_kendaraan',$this->lbd_bpp_kendaraan,true);
		$criteria->compare('lbd_bpp_alat_kantor',$this->lbd_bpp_alat_kantor,true);
		$criteria->compare('lbd_bbn_susut',$this->lbd_bbn_susut,true);
		$criteria->compare('lbd_biy_adm_tndr',$this->lbd_biy_adm_tndr,true);
		$criteria->compare('lbd_biy_adm_bank',$this->lbd_biy_adm_bank,true);
		$criteria->compare('lbd_bbn_askes',$this->lbd_bbn_askes,true);
		$criteria->compare('lbd_bbn_dinas',$this->lbd_bbn_dinas,true);
		$criteria->compare('lbd_bbn_ll',$this->lbd_bbn_ll,true);
		$criteria->compare('lbd_pll',$this->lbd_pll,true);
		$criteria->compare('lbd_jasa_giro',$this->lbd_jasa_giro,true);
		$criteria->compare('lbd_akd_laba_jalan',$this->lbd_akd_laba_jalan,true);
		$criteria->compare('lbd_datetime_insert',$this->lbd_datetime_insert,true);
		$criteria->compare('lbd_insert_by',$this->lbd_insert_by,true);
		$criteria->compare('lbd_datetime_update',$this->lbd_datetime_update,true);
		$criteria->compare('lbd_update_by',$this->lbd_update_by,true);
		$criteria->compare('lbd_status',$this->lbd_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LabaDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
