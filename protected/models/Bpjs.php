<?php

/**
 * This is the model class for table "tbl_bpjs".
 *
 * The followings are the available columns in table 'tbl_bpjs':
 * @property integer $bpjs_id
 * @property string $bpjs_kete
 * @property string $bpjs_kete_ktr
 * @property string $bpjs_kese
 * @property string $bpjs_kese_ktr
 * @property string $bpjs_pensi
 * @property string $bpjs_pensi_ktr
 * @property integer $bpjs_status
 */
class Bpjs extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_bpjs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bpjs_nama', 'required'),
			array('bpjs_status', 'numerical', 'integerOnly'=>true),
			array('bpjs_nama, bpjs_kete, bpjs_kete_ktr, bpjs_kese, bpjs_kese_ktr, bpjs_pensi, bpjs_pensi_ktr', 'length', 'max'=>25),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('bpjs_id, bpjs_nama, bpjs_kete, bpjs_kete_ktr, bpjs_kese, bpjs_kese_ktr, bpjs_pensi, bpjs_pensi_ktr, bpjs_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bpjs_id' => 'Bpjs',
			'bpjs_nama' => 'Bpjs Nama',
			'bpjs_kete' => 'Bpjs Kete',
			'bpjs_kete_ktr' => 'Bpjs Kete Ktr',
			'bpjs_kese' => 'Bpjs Kese',
			'bpjs_kese_ktr' => 'Bpjs Kese Ktr',
			'bpjs_pensi' => 'Bpjs Pensi',
			'bpjs_pensi_ktr' => 'Bpjs Pensi Ktr',
			'bpjs_status' => 'Bpjs Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		
		$criteria=new CDbCriteria;

		$criteria->compare('bpjs_id',$this->bpjs_id);
		$criteria->compare('bpjs_nama',$this->bpjs_nama,true);
		$criteria->compare('bpjs_kete',$this->bpjs_kete,true);
		$criteria->compare('bpjs_kete_ktr',$this->bpjs_kete_ktr,true);
		$criteria->compare('bpjs_kese',$this->bpjs_kese,true);
		$criteria->compare('bpjs_kese_ktr',$this->bpjs_kese_ktr,true);
		$criteria->compare('bpjs_pensi',$this->bpjs_pensi,true);
		$criteria->compare('bpjs_pensi_ktr',$this->bpjs_pensi_ktr,true);
		$criteria->compare('bpjs_status',$this->bpjs_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	public static function listBpjs() {
		$list = CHtml::listData(Bpjs::model()->findAll(array('order' => 'bpjs_id ASC')), 'bpjs_id', 'bpjs_nama');
		return $list;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bpjs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
