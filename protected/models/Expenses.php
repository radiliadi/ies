<?php

/**
 * This is the model class for table "tbl_expenses".
 *
 * The followings are the available columns in table 'tbl_expenses':
 * @property integer $exp_id
 * @property string $exp_id_pd
 * @property string $exp_date_submit
 * @property string $exp_desc
 * @property string $exp_debet
 * @property string $exp_credit
 * @property string $exp_datetime_insert
 * @property string $exp_bln
 * @property string $exp_tot_debet
 * @property string $exp_tot_credit
 * @property integer $exp_is_verified
 * @property integer $exp_is_approved
 * @property string $exp_notes
 * @property integer $exp_status
 */
class Expenses extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_expenses';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('exp_desc, exp_id_pd, exp_date_submit', 'required'),
			array('exp_is_verified, exp_is_approved, exp_status', 'numerical', 'integerOnly'=>true),
			array('exp_id_pd, exp_verified_by, exp_approved_by', 'length', 'max'=>255),
			array('exp_debet, exp_credit, exp_bln, exp_tot_debet, exp_tot_credit', 'length', 'max'=>20),
			array('exp_attachment_img', 'file', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,png,bmp','maxSize'=>32*3600*3600,'tooLarge'=>'Ukuran foto terlalu besar', 'wrongType'=>'Jenis file hanya JPG, JPEG, BMP atau PNG', 'except'=>'insert'),
			array('exp_datetime_insert, exp_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('exp_id, exp_id_pd, exp_submit_status, exp_date_submit, exp_desc, exp_debet, exp_credit, exp_attachment_img, exp_attachment_pdf, exp_datetime_insert, exp_bln, exp_tot_debet, exp_tot_credit, exp_is_verified, exp_not_verified_by, exp_verified_by, exp_verified_name, exp_not_verified_date, exp_verified_date, exp_is_approved, exp_rejected_by, exp_approved_by, exp_approved_name, exp_rejected_date, exp_approved_date, exp_due_to_company, exp_due_to_employee, exp_notes, exp_datetime_update, exp_datetime_delete, exp_insert_by, exp_update_by, exp_delete_by, exp_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'datakaryawan'=>array(self::BELONGS_TO, 'Karyawan', 'exp_id_pd'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'exp_id' => 'No. Exp',
			'exp_id_pd' => 'NIP',
			'exp_date_submit' => 'Date Submit',
			'exp_submit_status' => 'Submit Status',
			'exp_desc' => 'Description',
			'exp_debet' => 'Debet',
			'exp_credit' => 'Credit',
			'exp_attachment_img' => 'Image',
			'exp_attachment_pdf' => 'Scan',
			'exp_datetime_insert' => 'Insert By',
			'exp_bln' => 'Bln',
			'exp_tot_debet' => 'Total Debet',
			'exp_tot_credit' => 'Total Credit',
			'exp_is_verified' => 'Is Verified',
			'exp_not_verified_by' => 'Not Verified By',
			'exp_verified_by' => 'Verified By',
			'exp_verified_name' => 'Verified Name',
			'exp_not_verified_date' => 'Date Not Verified',
			'exp_verified_date' => 'Date Verified',
			'exp_is_approved' => 'Is Approved',
			'exp_rejected_by' => 'Rejected By',
			'exp_approved_by' => 'Approved By',
			'exp_approved_name' => 'Approved Name',
			'exp_rejected_date' => 'Date Rejected',
			'exp_approved_date' => 'Date Approved',
			'exp_due_to_company' => 'Due To Company',
			'exp_due_to_employee' => 'Due To Employee',
			'exp_notes' => 'Notes',
			'exp_datetime_update' => 'Last Update',
			'exp_datetime_delete' => 'Delete By',
			'exp_insert_by' => 'Insert By',
			'exp_update_by' => 'Update By',
			'exp_delete_by' => 'Deleted By',
			'exp_status' => 'Exp Status',
		);
	}

	public function beforeSave()
    {
    	$id_pd=Yii::app()->session->get('username');
        if($this->isNewRecord || $this->exp_datetime_insert==null){
            $this->exp_datetime_insert=new CDbExpression('NOW()');
            $this->exp_insert_by = $id_pd;
        }else{
            $this->exp_datetime_update = new CDbExpression('NOW()');
            $this->exp_update_by = $id_pd;
        }
        return parent::beforeSave();
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('exp_id',$this->exp_id);
		$criteria->compare('exp_id_pd',$this->exp_id_pd,true);
		$criteria->compare('exp_date_submit',$this->exp_date_submit,true);
		$criteria->compare('exp_submit_status',$this->exp_submit_status);
		$criteria->compare('exp_desc',$this->exp_desc,true);
		$criteria->compare('exp_debet',$this->exp_debet,true);
		$criteria->compare('exp_credit',$this->exp_credit,true);
		$criteria->compare('exp_attachment_img',$this->exp_attachment_img,true);
		$criteria->compare('exp_attachment_pdf',$this->exp_attachment_pdf,true);
		$criteria->compare('exp_datetime_insert',$this->exp_datetime_insert,true);
		$criteria->compare('exp_bln',$this->exp_bln,true);
		$criteria->compare('exp_tot_debet',$this->exp_tot_debet,true);
		$criteria->compare('exp_tot_credit',$this->exp_tot_credit,true);
		$criteria->compare('exp_is_verified',$this->exp_is_verified);
		$criteria->compare('exp_not_verified_by',$this->exp_not_verified_by,true);
		$criteria->compare('exp_verified_by',$this->exp_verified_by,true);
		$criteria->compare('exp_verified_name',$this->exp_verified_name,true);
		$criteria->compare('exp_not_verified_date',$this->exp_not_verified_date,true);
		$criteria->compare('exp_verified_date',$this->exp_verified_date,true);
		$criteria->compare('exp_is_approved',$this->exp_is_approved);
		$criteria->compare('exp_rejected_by',$this->exp_rejected_by,true);
		$criteria->compare('exp_approved_name',$this->exp_approved_name,true);
		$criteria->compare('exp_approved_by',$this->exp_approved_by,true);
		$criteria->compare('exp_rejected_date',$this->exp_rejected_date,true);
		$criteria->compare('exp_approved_date',$this->exp_approved_date,true);
		$criteria->compare('exp_due_to_company',$this->exp_due_to_company,true);
		$criteria->compare('exp_due_to_employee',$this->exp_due_to_employee,true);
		$criteria->compare('exp_notes',$this->exp_notes,true);
		$criteria->compare('exp_datetime_update',$this->exp_datetime_update,true);
		$criteria->compare('exp_datetime_delete',$this->exp_datetime_delete,true);
		$criteria->compare('exp_insert_by',$this->exp_insert_by,true);
		$criteria->compare('exp_update_by',$this->exp_update_by,true);
		$criteria->compare('exp_delete_by',$this->exp_delete_by,true);
		$criteria->compare('exp_status',$this->exp_status);
		$criteria->addCondition("exp_status = 1");
		$criteria->order='exp_id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Expenses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
