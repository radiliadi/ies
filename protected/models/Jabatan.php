<?php

/**
 * This is the model class for table "jabatan".
 *
 * The followings are the available columns in table 'jabatan':
 * @property integer $id_jabatan
 * @property string $nm_jabatan
 */
class Jabatan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jabatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nm_jabatan, pokok_jabatan, tunj_jabatan', 'required'),
			array('nm_jabatan', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_jabatan, nm_jabatan, pokok_jabatan, tunj_jabatan', 'safe', 'on'=>'search'),
			array('nm_jabatan','unique','message'=>'{attribute}:{value} Data telah tersedia! Mohon diubah data pada kolom yang berwarna merah'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'karyawan'	=>	array(self::HAS_MANY, 'karyawan', 'id_jabatan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_jabatan' => 'Id Jabatan',
			'nm_jabatan' => 'Jabatan',
			'pokok_jabatan' => 'Gaji Pokok',
			'tunj_jabatan' => 'Tunjangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('id_jabatan',$this->id_jabatan);
		$criteria->compare('nm_jabatan',$this->nm_jabatan,true);
		$criteria->compare('pokok_jabatan',$this->nm_jabatan,true);
		$criteria->compare('tunj_jabatan',$this->nm_jabatan,true);

		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				)
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Jabatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
