<?php

/**
 * This is the model class for table "tbl_earnings".
 *
 * The followings are the available columns in table 'tbl_earnings':
 * @property integer $earn_id
 * @property integer $earn_id_pd
 * @property integer $earn_field_allow
 * @property integer $earn_overtime
 * @property integer $earn_daily_trans
 * @property integer $earn_func_allow
 * @property integer $earn_breakfast
 * @property integer $earn_lunch
 * @property integer $earn_status
 */
class Earnings extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_earnings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('earn_id_pd', 'required'),
			array('earn_field_allow, earn_overtime, earn_daily_trans, earn_func_allow, earn_breakfast, earn_lunch, earn_status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('earn_id, earn_id_pd, earn_field_allow, earn_overtime, earn_daily_trans, earn_func_allow, earn_breakfast, earn_lunch, earn_meals, earn_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'earn_id' => 'Earn ID',
			'earn_id_pd' => 'NIP',
			'earn_field_allow' => 'Earn Field Allow',
			'earn_overtime' => 'Overtime',
			'earn_daily_trans' => 'Transport Allowances',
			'earn_func_allow' => 'Functional Allowances',
			'earn_breakfast' => 'Meals',
			'earn_lunch' => 'Earn Lunch',
			'earn_status' => 'Status',
			'earn_meals' => 'Meals',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('earn_id',$this->earn_id);
		$criteria->compare('earn_id_pd',$this->earn_id_pd);
		$criteria->compare('earn_field_allow',$this->earn_field_allow);
		$criteria->compare('earn_overtime',$this->earn_overtime);
		$criteria->compare('earn_daily_trans',$this->earn_daily_trans);
		$criteria->compare('earn_func_allow',$this->earn_func_allow);
		$criteria->compare('earn_breakfast',$this->earn_breakfast);
		$criteria->compare('earn_lunch',$this->earn_lunch);
		$criteria->compare('earn_meals',$this->earn_meals);
		$criteria->compare('earn_status',$this->earn_status);
		$criteria->addCondition("earn_status = 1");

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Earnings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
