<?php

/**
 * This is the model class for table "tbl_neraca_child".
 *
 * The followings are the available columns in table 'tbl_neraca_child':
 * @property string $nrcc_id
 * @property integer $nrcc_nrcp_id
 * @property string $nrcc_desc
 * @property string $nrcc_datetime_insert
 * @property string $nrcc_datetime_update
 * @property string $nrcc_insert_by
 * @property string $nrcc_update_by
 * @property integer $nrcc_sort
 * @property integer $nrcc_status
 */
class NeracaChild extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_neraca_child';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nrcc_nrcp_id, nrcc_sort, nrcc_status', 'numerical', 'integerOnly'=>true),
			array('nrcc_insert_by, nrcc_update_by', 'length', 'max'=>255),
			array('nrcc_desc, nrcc_datetime_insert, nrcc_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('nrcc_id, nrcc_nrcp_id, nrcc_desc, nrcc_datetime_insert, nrcc_datetime_update, nrcc_insert_by, nrcc_update_by, nrcc_sort, nrcc_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nrcc_id' => 'Nrcc',
			'nrcc_nrcp_id' => 'Nrcc Nrcp',
			'nrcc_desc' => 'Nrcc Desc',
			'nrcc_datetime_insert' => 'Nrcc Datetime Insert',
			'nrcc_datetime_update' => 'Nrcc Datetime Update',
			'nrcc_insert_by' => 'Nrcc Insert By',
			'nrcc_update_by' => 'Nrcc Update By',
			'nrcc_sort' => 'Nrcc Sort',
			'nrcc_status' => 'Nrcc Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nrcc_id',$this->nrcc_id,true);
		$criteria->compare('nrcc_nrcp_id',$this->nrcc_nrcp_id);
		$criteria->compare('nrcc_desc',$this->nrcc_desc,true);
		$criteria->compare('nrcc_datetime_insert',$this->nrcc_datetime_insert,true);
		$criteria->compare('nrcc_datetime_update',$this->nrcc_datetime_update,true);
		$criteria->compare('nrcc_insert_by',$this->nrcc_insert_by,true);
		$criteria->compare('nrcc_update_by',$this->nrcc_update_by,true);
		$criteria->compare('nrcc_sort',$this->nrcc_sort);
		$criteria->compare('nrcc_status',$this->nrcc_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NeracaChild the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
