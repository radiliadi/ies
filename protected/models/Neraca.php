<?php

/**
 * This is the model class for table "tbl_neraca".
 *
 * The followings are the available columns in table 'tbl_neraca':
 * @property integer $nrc_id
 * @property string $nrc_desc
 * @property string $nrc_date
 * @property integer $nrc_is_checked
 * @property string $nrc_checked_by
 * @property string $nrc_datetime_checked
 * @property string $nrc_datetime_insert
 * @property integer $nrc_is_locked
 * @property integer $nrc_status
 */
class Neraca extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_neraca';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nrc_is_checked, nrc_is_locked, nrc_status', 'numerical', 'integerOnly'=>true),
			array('nrc_checked_by', 'length', 'max'=>255),
			array('nrc_desc, nrc_date, nrc_sum_1, nrc_sum_2, nrc_sum_3, nrc_sum_4, nrc_sum_13, nrc_sum_24, nrc_datetime_checked, nrc_datetime_insert, nrc_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('nrc_id, nrc_desc, nrc_date, nrc_sum_1, nrc_sum_2, nrc_sum_3, nrc_sum_4, nrc_sum_13, nrc_sum_24, nrc_is_checked, nrc_checked_by, nrc_datetime_checked, nrc_datetime_insert, nrc_datetime_update, nrc_datetime_delete, nrc_insert_by, nrc_update_by, nrc_delete_by, nrc_is_locked, nrc_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nrc_id' => 'Neraca ID',
			'nrc_desc' => 'Desc',
			'nrc_date' => 'Date',
			'nrc_sum_1' => 'SUM 1',
			'nrc_sum_2' => 'SUM 2',
			'nrc_sum_3' => 'SUM 3',
			'nrc_sum_4' => 'SUM 4',
			'nrc_sum_13' => 'SUM 13',
			'nrc_sum_24' => 'SUM 24',
			'nrc_datetime_checked' => 'Checked',
			'nrc_datetime_insert' => 'Insert',
			'nrc_datetime_update' => 'Update',
			'nrc_datetime_delete' => 'Delete',
			'nrc_checked_by' => 'Checked By',
			'nrc_insert_by' => 'Inserted By',
			'nrc_update_by' => 'Updated By',
			'nrc_delete_by' => 'Deleted By',
			'nrc_is_checked' => 'Is Checked',
			'nrc_is_locked' => 'Is Locked',
			'nrc_status' => 'Status',
		);
	}

	public function beforeSave()
    {
    	$id_pd=Yii::app()->session->get('username');
        if($this->isNewRecord || $this->nrc_datetime_insert==null){
            $this->nrc_datetime_insert=new CDbExpression('NOW()');
            $this->nrc_insert_by = $id_pd;
        }else{
            $this->nrc_datetime_update = new CDbExpression('NOW()');
            $this->nrc_update_by = $id_pd;
        }
        return parent::beforeSave();
    }

    public static function getCount(){
		$count = Neraca::model()->findAll(array('select'=>'nrc_id', 'condition'=>'nrc_status=:nrc_status', 'params'=>array(':nrc_status'=>1)));
		return count($count);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nrc_id',$this->nrc_id);
		$criteria->compare('nrc_desc',$this->nrc_desc,true);
		$criteria->compare('nrc_date',$this->nrc_date,true);
		$criteria->compare('nrc_sum_1',$this->nrc_sum_1,true);
		$criteria->compare('nrc_sum_2',$this->nrc_sum_2,true);
		$criteria->compare('nrc_sum_3',$this->nrc_sum_3,true);
		$criteria->compare('nrc_sum_4',$this->nrc_sum_4,true);
		$criteria->compare('nrc_sum_13',$this->nrc_sum_13,true);
		$criteria->compare('nrc_sum_24',$this->nrc_sum_24,true);
		$criteria->compare('nrc_is_checked',$this->nrc_is_checked);
		$criteria->compare('nrc_checked_by',$this->nrc_checked_by,true);
		$criteria->compare('nrc_datetime_checked',$this->nrc_datetime_checked,true);
		$criteria->compare('nrc_datetime_insert',$this->nrc_datetime_insert,true);
		$criteria->compare('nrc_is_locked',$this->nrc_is_locked);
		$criteria->compare('nrc_status',$this->nrc_status);
		$criteria->order='nrc_date DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Neraca the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
