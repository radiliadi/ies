<?php

/**
 * This is the model class for table "penghasilan".
 *
 * The followings are the available columns in table 'penghasilan':
 * @property integer $id_penghasilan
 * @property string $nm_penghasilan
 * @property integer $batas_bawah
 * @property integer $batas_atas
 */
class Penghasilan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penghasilan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nm_penghasilan, batas_bawah, batas_atas', 'required'),
			array('id_penghasilan, batas_bawah, batas_atas', 'numerical', 'integerOnly'=>true),
			array('nm_penghasilan', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_penghasilan, nm_penghasilan, batas_bawah, batas_atas', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_penghasilan' => 'Id Penghasilan',
			'nm_penghasilan' => 'Nm Penghasilan',
			'batas_bawah' => 'Batas Bawah',
			'batas_atas' => 'Batas Atas',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('id_penghasilan',$this->id_penghasilan);
		$criteria->compare('nm_penghasilan',$this->nm_penghasilan,true);
		$criteria->compare('batas_bawah',$this->batas_bawah);
		$criteria->compare('batas_atas',$this->batas_atas);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Penghasilan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
