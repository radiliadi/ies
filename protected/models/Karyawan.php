<?php

/**
 * This is the model class for table "karyawan".
 *
 * The followings are the available columns in table 'karyawan':
 * @property string $id_pd
 * @property string $nm_pd
 * @property string $id_jabatan
 * @property string $divisi
 * @property string $lokasi_kerja
 * @property string $jk
 * @property string $nik
 * @property string $tmpt_lahir
 * @property string $tgl_lahir
 * @property integer $id_agama
 * @property integer $id_kk
 * @property string $jln
 * @property string $rt
 * @property string $rw
 * @property string $nm_dsn
 * @property string $ds_kel
 * @property string $id_wil
 * @property string $kode_pos
 * @property string $id_jns_tinggal
 * @property string $id_alat_transport
 * @property string $telepon_rumah
 * @property string $telepon_seluler
 * @property string $email
 * @property string $stat_pd
 * @property string $nm_ayah
 * @property string $tgl_lahir_ayah
 * @property string $id_jenjang_pendidikan_ayah
 * @property integer $id_pekerjaan_ayah
 * @property string $nm_ibu_kandung
 * @property string $tgl_lahir_ibu
 * @property string $id_jenjang_pendidikan_ibu
 * @property integer $id_pekerjaan_ibu
 * @property string $kewarganegaraan
 */
class Karyawan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'karyawan';
		// return 'pagesize';
	}

	//tanggal lahir : mulai --------------------------------------------------------------------------------------
	public $tgl1,$bln1,$thn1;
	public $tgl2,$bln2,$thn2;
	public $jabatanku;
	public $divisiku;
	//tanggal lahir : selesai --------------------------------------------------------------------------------------

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pd, nm_pd, npwp, akun_level, tgl_lahir, id_ptkp, tgl_masuk, id_jabatan, lokasi_kerja, divisi, stat_pd, jk, tgl_lahir_ayah, tgl_lahir_ibu, tender_id, id_bpjs', 'required'),
			array('id_agama, tender_id, id_kk, id_pekerjaan_ayah, id_pekerjaan_ibu', 'numerical', 'integerOnly'=>true),
			array('id_pd', 'length', 'max'=>25),
			array('nm_pd, nm_ayah, nm_ibu_kandung', 'length', 'max'=>60),
			array('divisi, lokasi_kerja', 'length', 'max'=>255),
			array('nik', 'length', 'max'=>16),
			array('tmpt_lahir, tgl_lahir', 'length', 'max'=>32),
			array('jln', 'length', 'max'=>80),
			array('rt, rw, kode_pos, id_jns_tinggal, id_alat_transport, id_jenjang_pendidikan_ayah, id_jenjang_pendidikan_ibu', 'length', 'max'=>5),
			array('nm_dsn, ds_kel, email, regpd_id_jns_daftar', 'length', 'max'=>50),
			array('id_wil', 'length', 'max'=>255),
			array('telepon_rumah, telepon_seluler', 'length', 'max'=>20),
			array('kewarganegaraan', 'length', 'max'=>2),
			array('kyw_datetime_insert, kyw_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pd, npwp, tgl_masuk, stat_kawin, tender_id, akun_level, id_jabatan, regpd_id_jns_daftar, divisi, lokasi_kerja, nm_pd, divisi, lokasi_kerja, jk, nik, tmpt_lahir, tgl_lahir, id_agama, id_kk, jln, rt, rw, nm_dsn, ds_kel, id_wil, kode_pos, id_jns_tinggal, id_alat_transport, telepon_rumah, telepon_seluler, email, stat_pd, nm_ayah, tgl_lahir_ayah, id_jenjang_pendidikan_ayah, id_pekerjaan_ayah, nm_ibu_kandung, tgl_lahir_ibu, id_jenjang_pendidikan_ibu, id_pekerjaan_ibu, kewarganegaraan, id_ptkp, jabatanku, divisiku, id_bpjs, kyw_datetime_insert, kyw_datetime_update, kyw_datetime_delete, kyw_insert_by, kyw_update_by, kyw_delete_by, status_pd', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		
			// 'karyawan'=>array(self::HAS_MANY, 'karyawan','gaji'),
			// 'datakaryawan'=>array(self::BELONGS_TO, 'Karyawan', 'id_pd'),
			// 'datajabatan'=>array(self::BELONGS_TO, 'Jabatan', 'id_jabatan',
			// 				'through'=>'datakaryawan'),
			'relasijabatan'		=>	array(self::BELONGS_TO, 'Jabatan', 'id_jabatan'),
			'getDivisi'			=>	array(self::BELONGS_TO, 'Divisi', 'divisi'), //nama model , field di 
			'getTender'			=>	array(self::BELONGS_TO, 'Tender', 'tender_id'), //nama model , field di 
			'getLokasiKerja' 	=> 	array(self::BELONGS_TO, 'LokasiKerja', 'lokasi_kerja'),
			// 'tenderJenis'	=>	array(self::HAS_ONE, 'tender', 'tender_id'), //nama modl , field di self gak nampil data kalo has_one
		
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pd' => 'NIP',
			'nm_pd' => 'Nama',
			'id_jabatan' => 'Jabatan',
			'stat_kawin' => 'Status Kawin',
			'divisi' => 'Divisi',
			'lokasi_kerja' => 'Lokasi Kerja',
			'jk' => 'Jenis Kelamin',
			'nik' => 'NIK',
			'tmpt_lahir' => 'Tempat Lahir',
			'tgl_lahir' => 'Tanggal Lahir',
			'id_agama' => 'Agama',
			'id_kk' => 'KK',
			'jln' => 'Jalan',
			'rt' => 'RT',
			'rw' => 'RW',
			'nm_dsn' => 'Nama Dusun',
			'ds_kel' => 'Desa Kelurahan',
			'id_wil' => 'Wiliyah',
			'kode_pos' => 'Kode Pos',
			'id_jns_tinggal' => 'Jenis Tinggal',
			'id_alat_transport' => 'Alat Transport',
			'telepon_rumah' => 'Telepon Rumah',
			'telepon_seluler' => 'Telepon Seluler',
			'email' => 'Email',
			'stat_pd' => 'Status',
			'nm_ayah' => 'Nama Ayah',
			'tgl_lahir_ayah' => 'Tanggal Lahir Ayah',
			'id_jenjang_pendidikan_ayah' => 'Jenjang Pendidikan Ayah',
			'id_pekerjaan_ayah' => 'Pekerjaan Ayah',
			'nm_ibu_kandung' => 'Nama Ibu Kandung',
			'tgl_lahir_ibu' => 'Tanggal Lahir Ibu',
			'id_jenjang_pendidikan_ibu' => 'Jenjang Pendidikan Ibu',
			'id_pekerjaan_ibu' => 'Pekerjaan Ibu',
			'kewarganegaraan' => 'Kewarganegaraan',
			// 'nm_wali'=>'Jumlah Anak',
			'regpd_id_jns_daftar'=>'Reg PD Jenis Daftar',
			'tender_id' => 'Tender',
			'id_ptkp' => 'Ptkp',
			'npwp' => 'NPWP',
			'tgl_masuk' => 'Tanggal Masuk',
			'akun_level' => 'Akun level',
			'jabatanku' => 'Jabatan',
			'divisiku' => 'Divisi',
			'id_bpjs' => 'BPJS',
			'kyw_datetime_insert' => 'Insert',
			'kyw_datetime_update' => 'Update',
			'kyw_datetime_delete' => 'Delete',
			'kyw_insert_by' => 'Insert By',
			'kyw_update_by' => 'Update By',
			'kyw_delete_by' => 'Delete By',
			'status_pd' => 'Status',
		);
	}

	public function beforeSave()
    {
    	$id_pd=Yii::app()->session->get('username');
        if($this->isNewRecord || $this->kyw_datetime_insert==null){
            $this->kyw_datetime_insert=new CDbExpression('NOW()');
            $this->kyw_insert_by = $id_pd;
        }else{
            $this->kyw_datetime_update = new CDbExpression('NOW()');
            $this->kyw_update_by = $id_pd;
        }
        return parent::beforeSave();
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		
		// $criteria=new CDbCriteria;
		// $criteria->select= "jml_ps FROM pagesize WHERE id_ps=1";
		// $criteria->condition = "id_ps=:id_ps";
		// $criteria->params = array (	
		// 	':id_ps'=>"id",
		// );
		// $criteria=new CDbCriteria;
		// $criteria->select='jml_ps as jml_ps FROM pagesize WHERE id_ps=1';  // only select the 'title' column
		// $criteria->condition='id_ps=:1';
		// $criteria->params=array(':id_ps'=>1);
		// $modelPagesize=Pagesize::model()->find('jml_ps=:jml_ps'); // $params is not needed


		// $modelPagesize=Pagesize::model()->find('jml_ps=:jml_ps AND id_ps=:id_ps', array(':id_ps=1'));
		// $modelPagesize=new Pagesize;
		// // $modelPagesize=Pagesize::model()->findAll();
		// $modelPagesize->attributes=$_POST['Pagesize'];
		// $baris=isset($_POST['Pagesize'] ['jml_ps']);
		// $modelPagesize=Pagesize::model()->findAll(array("select"=>"jml_ps WHERE id_ps=1"));
		// $criteria               = new CDbCriteria;
		// $criteria->select       = "jml_ps";
		// $criteria->condition    = "id_ps = 1";
		// $baris               	= Pagesize::model()->findAll($criteria);
		// $baris=25;
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		// $baris=Pagesize::model()->findAllByAttributes(array('id_ps'=>1),array('order'=>'id DESC'));
		$criteria=new CDbCriteria;
		$criteria->with=array('relasijabatan');
		$criteria->with=array('getTender');
		$criteria->with=array('getLokasiKerja');
		$criteria->with=array('getDivisi');
		$criteria->compare('tender_id',$this->tender_id);
		$criteria->compare('lokasi_kerja',$this->lokasi_kerja, true);
		$criteria->compare('divisi',$this->divisi, true);

		$criteria->compare('id_pd',$this->id_pd,true);
		$criteria->compare('nm_pd',$this->nm_pd,true);
		$criteria->compare('id_jabatan',$this->id_jabatan,true);
		$criteria->compare('stat_kawin',$this->stat_kawin,true);
		$criteria->compare('jk',$this->jk,true);
		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('tmpt_lahir',$this->tmpt_lahir,true);
		$criteria->compare('tgl_lahir',$this->tgl_lahir,true);
		$criteria->compare('id_agama',$this->id_agama);
		$criteria->compare('id_kk',$this->id_kk);
		$criteria->compare('jln',$this->jln,true);
		$criteria->compare('rt',$this->rt,true);
		$criteria->compare('rw',$this->rw,true);
		$criteria->compare('nm_dsn',$this->nm_dsn,true);
		$criteria->compare('ds_kel',$this->ds_kel,true);
		$criteria->compare('id_wil',$this->id_wil,true);
		$criteria->compare('kode_pos',$this->kode_pos,true);
		$criteria->compare('id_jns_tinggal',$this->id_jns_tinggal,true);
		$criteria->compare('id_alat_transport',$this->id_alat_transport,true);
		$criteria->compare('telepon_rumah',$this->telepon_rumah,true);
		$criteria->compare('telepon_seluler',$this->telepon_seluler,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('stat_pd',$this->stat_pd,true);
		$criteria->compare('nm_ayah',$this->nm_ayah,true);
		$criteria->compare('tgl_lahir_ayah',$this->tgl_lahir_ayah,true);
		$criteria->compare('id_jenjang_pendidikan_ayah',$this->id_jenjang_pendidikan_ayah,true);
		$criteria->compare('id_pekerjaan_ayah',$this->id_pekerjaan_ayah);
		$criteria->compare('nm_ibu_kandung',$this->nm_ibu_kandung,true);
		$criteria->compare('tgl_lahir_ibu',$this->tgl_lahir_ibu,true);
		$criteria->compare('id_jenjang_pendidikan_ibu',$this->id_jenjang_pendidikan_ibu,true);
		$criteria->compare('id_pekerjaan_ibu',$this->id_pekerjaan_ibu);
		$criteria->compare('kewarganegaraan',$this->kewarganegaraan,true);
		$criteria->compare('id_ptkp',$this->id_ptkp,true);
		$criteria->compare('npwp',$this->npwp,true);
		$criteria->compare('tgl_masuk',$this->tgl_masuk,true);
		$criteria->compare('akun_level',$this->akun_level,true);
		$criteria->compare('kyw_datetime_insert',$this->kyw_datetime_insert,true);
		$criteria->compare('kyw_datetime_update',$this->kyw_datetime_update,true);
		$criteria->compare('kyw_datetime_delete',$this->kyw_datetime_delete,true);
		$criteria->compare('kyw_insert_by',$this->kyw_insert_by,true);
		$criteria->compare('kyw_update_by',$this->kyw_update_by,true);
		$criteria->compare('kyw_delete_by',$this->kyw_delete_by,true);
		$criteria->compare('status_pd',$this->status_pd,true);
		// $criteria->compare('jabatanku',$this->jabatanku,true);
		// $criteria->order='status_pd DESC';
		$criteria->order='kyw_datetime_insert DESC';
		$criteria->compare('id_jabatan', $this->jabatanku, false, '='); //field di jabatan
		// $criteria->addCondition("status_pd = 1");
		// $criteria->with=array('relasidivisi');
		// $criteria->compare('relasidivisi.divisi', $this->divisiku, false, '='); //field 
		// $criteria->addCondition('jabatanku')
		// $criteria->with=array('divisi');
		// $criteria->compare('divisi.id_div', $this->divisiku, true);
		// $criteria->condition ='tender_id = 10'
		// $criteria->limit='5';
		// $criteria->group='jk';
		// $criteria->compare('id_lokasi_kerja',$this->kewarganegaraan,true);

		return new CActiveDataProvider($this, array(
			// 'modelPagesize'=>$modelPagesize,
			'criteria'=>$criteria,
			// 'criteriax'=>$criteriax,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
			'sort'=>array(
				'attributes'=>array(
					'jabatanku'=>array(
						'asc'=>'relasijabatan.nm_jabatan',
						'desc'=>'relasijabatan.nm_jabatan DESC',
						),
					'divisiku'=>array(
						'asc'=>'getDivisi.nm_div',
						'desc'=>'getDivisi.nm_div DESC',
						),
					'*',
					),
				),
		));

		// $this->render('', array(
		// 	'modelPagesize'=>$modelPagesize));
	}

	
	public static function listStatus() {
		$list = CHtml::listData(self::model()->findAll(array('order' => 'stat_pd ASC')), 'stat_pd', 'stat_pd');
		return $list;
	}
	
	public static function listJabatan() {
		$list = CHtml::listData(self::model()->findAll(array('order' => 'id_jabatan ASC')), 'id_jabatan', 'id_jabatan');
		return $list;
	}

	public static function listJabatanx() {
		$list = CHtml::listData(Jabatan::model()->findAll(array('order' => 'id_jabatan ASC')), 'id_jabatan', 'nm_jabatan');
		return $list;
	}

	public static function listBpjs() {
		$list = CHtml::listData(Bpjs::model()->findAll(array('order' => 'bpjs_id ASC')), 'bpjs_id', 'bpjs_nama');
		return $list;
	}
	
	public static function listDivisi() {
		$list = CHtml::listData(Divisi::model()->findAll(array('order' => 'id_div ASC')), 'id_div', 'nm_div');
		return $list;
	}
	
	public static function listLokker() {
		$list = CHtml::listData(LokasiKerja::model()->findAll(array('order' => 'id_lokasi_kerja ASC')), 'id_lokasi_kerja', 'nm_lokasi_kerja');
		return $list;
	}

	public static function listTender() {
		$list = CHtml::listData(Tender::model()->findAll(array('order' => 'id_tender DESC')), 'id_tender', 'client_name');
		return $list;
	}

	public function searchpay()
	{
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		// $baris=Pagesize::model()->findAllByAttributes(array('id_ps'=>1),array('order'=>'id DESC'));
		$criteria=new CDbCriteria;
		$criteria->with=array('relasijabatan');
		$criteria->with=array('getTender');
		$criteria->with=array('getLokasiKerja');
		$criteria->with=array('getDivisi');
		$criteria->compare('tender_id',$this->tender_id);
		$criteria->compare('lokasi_kerja',$this->lokasi_kerja, true);
		$criteria->compare('divisi',$this->divisi, true);
		$criteria->compare('id_jabatan', $this->id_jabatan, true); 

		$criteria->compare('id_pd',$this->id_pd,true);
		$criteria->compare('nm_pd',$this->nm_pd,true);
		$criteria->compare('stat_kawin',$this->stat_kawin,true);
		$criteria->compare('jk',$this->jk,true);
		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('tmpt_lahir',$this->tmpt_lahir,true);
		$criteria->compare('tgl_lahir',$this->tgl_lahir,true);
		$criteria->compare('id_agama',$this->id_agama);
		$criteria->compare('id_kk',$this->id_kk);
		$criteria->compare('jln',$this->jln,true);
		$criteria->compare('rt',$this->rt,true);
		$criteria->compare('rw',$this->rw,true);
		$criteria->compare('nm_dsn',$this->nm_dsn,true);
		$criteria->compare('ds_kel',$this->ds_kel,true);
		$criteria->compare('id_wil',$this->id_wil,true);
		$criteria->compare('kode_pos',$this->kode_pos,true);
		$criteria->compare('id_jns_tinggal',$this->id_jns_tinggal,true);
		$criteria->compare('id_alat_transport',$this->id_alat_transport,true);
		$criteria->compare('telepon_rumah',$this->telepon_rumah,true);
		$criteria->compare('telepon_seluler',$this->telepon_seluler,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('stat_pd',$this->stat_pd,true);
		$criteria->compare('nm_ayah',$this->nm_ayah,true);
		$criteria->compare('tgl_lahir_ayah',$this->tgl_lahir_ayah,true);
		$criteria->compare('id_jenjang_pendidikan_ayah',$this->id_jenjang_pendidikan_ayah,true);
		$criteria->compare('id_pekerjaan_ayah',$this->id_pekerjaan_ayah);
		$criteria->compare('nm_ibu_kandung',$this->nm_ibu_kandung,true);
		$criteria->compare('tgl_lahir_ibu',$this->tgl_lahir_ibu,true);
		$criteria->compare('id_jenjang_pendidikan_ibu',$this->id_jenjang_pendidikan_ibu,true);
		$criteria->compare('id_pekerjaan_ibu',$this->id_pekerjaan_ibu);
		$criteria->compare('kewarganegaraan',$this->kewarganegaraan,true);
		$criteria->compare('id_ptkp',$this->id_ptkp,true);
		$criteria->compare('npwp',$this->npwp,true);
		$criteria->compare('tgl_masuk',$this->tgl_masuk,true);
		$criteria->compare('akun_level',$this->akun_level,true);
		$criteria->compare('kyw_datetime_insert',$this->kyw_datetime_insert,true);
		$criteria->compare('kyw_datetime_update',$this->kyw_datetime_update,true);
		$criteria->compare('kyw_datetime_delete',$this->kyw_datetime_delete,true);
		$criteria->compare('kyw_insert_by',$this->kyw_insert_by,true);
		$criteria->compare('kyw_update_by',$this->kyw_update_by,true);
		$criteria->compare('kyw_delete_by',$this->kyw_delete_by,true);
		$criteria->compare('status_pd',$this->status_pd,true);
		$criteria->order='id_pd DESC';
		$criteria->addCondition('status_pd = 1');

		return new CActiveDataProvider($this, array(
			// 'modelPagesize'=>$modelPagesize,
			'criteria'=>$criteria,
			// 'criteriax'=>$criteriax,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
			'sort'=>array(
				'attributes'=>array(
					'jabatanku'=>array(
						'asc'=>'relasijabatan.nm_jabatan',
						'desc'=>'relasijabatan.nm_jabatan DESC',
						),
					'divisiku'=>array(
						'asc'=>'getDivisi.nm_div',
						'desc'=>'getDivisi.nm_div DESC',
						),
					'*',
					),
				),
		));

		// $this->render('', array(
		// 	'modelPagesize'=>$modelPagesize));
	}

	// public function getJabatan()
 //    {
 //        return $this->hasOne(Jabatan::className(), ['id_jabatan' => 'id_jabatan']);
 //    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Karyawan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
