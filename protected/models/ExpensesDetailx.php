<?php

/**
 * This is the model class for table "tbl_expenses_detail".
 *
 * The followings are the available columns in table 'tbl_expenses_detail':
 * @property integer $expd_id
 * @property integer $expd_exp_id
 * @property string $expd_id_pd
 * @property string $expd_desc
 * @property string $expd_debet
 * @property string $expd_credit
 * @property string $expd_datetime_insert
 * @property string $expd_datetime_update
 * @property integer $expd_status
 */
class ExpensesDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_expenses_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('expd_exp_id, expd_id_pd, expd_date', 'required'),
			array('expd_exp_id, expd_status', 'numerical', 'integerOnly'=>true),
			array('expd_id_pd', 'length', 'max'=>255),
			array('expd_debet, expd_credit', 'length', 'max'=>20),
			array('expd_desc, expd_date, expd_datetime_insert, expd_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('expd_id, expd_exp_id, expd_id_pd, expd_desc, expd_debet, expd_credit, expd_date, expd_datetime_insert, expd_datetime_update, expd_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'datakaryawan'=>array(self::BELONGS_TO, 'Karyawan', 'expd_id_pd'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'expd_id' => 'No. Exp Detail',
			'expd_exp_id' => 'Expd Exp',
			'expd_id_pd' => 'NIP',
			'expd_desc' => 'Description',
			'expd_debet' => 'Debet',
			'expd_credit' => 'Credit',
			'expd_date' => 'Expd Date',
			'expd_datetime_insert' => 'Expd Datetime Insert',
			'expd_datetime_update' => 'Expd Datetime Update',
			'expd_status' => 'Expd Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('expd_id',$this->expd_id);
		$criteria->compare('expd_exp_id',$this->expd_exp_id);
		$criteria->compare('expd_id_pd',$this->expd_id_pd,true);
		$criteria->compare('expd_desc',$this->expd_desc,true);
		$criteria->compare('expd_debet',$this->expd_debet,true);
		$criteria->compare('expd_credit',$this->expd_credit,true);
		$criteria->compare('expd_date',$this->expd_date,true);
		$criteria->compare('expd_datetime_insert',$this->expd_datetime_insert,true);
		$criteria->compare('expd_datetime_update',$this->expd_datetime_update,true);
		$criteria->compare('expd_status',$this->expd_status);
		$criteria->addCondition("expd_status = 1");
		$criteria->order='expd_id ASC';
		$criteria->with=array('datakaryawan');
		$criteria->compare('datakaryawan.id_pd', $this->expd_id_pd, false, '='); //field di jabatan

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ExpensesDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
