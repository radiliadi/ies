<?php

/**
 * This is the model class for table "tbl_deductions".
 *
 * The followings are the available columns in table 'tbl_deductions':
 * @property integer $ded_id
 * @property integer $ded_id_pd
 * @property integer $ded_loan
 * @property integer $ded_loann
 * @property integer $ded_loannn
 * @property integer $ded_status
 */
class Deductions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_deductions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ded_id_pd', 'required'),
			array('ded_loan, ded_loann, ded_loannn, ded_status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ded_id, ded_id_pd, ded_loan, ded_loann, ded_loannn, ded_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ded_id' => 'Ded',
			'ded_id_pd' => 'NIP',
			'ded_loan' => 'Pinjaman',
			'ded_loann' => 'Ded Loann',
			'ded_loannn' => 'Ded Loannn',
			'ded_status' => 'Ded Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('ded_id',$this->ded_id);
		$criteria->compare('ded_id_pd',$this->ded_id_pd);
		$criteria->compare('ded_loan',$this->ded_loan);
		$criteria->compare('ded_loann',$this->ded_loann);
		$criteria->compare('ded_loannn',$this->ded_loannn);
		$criteria->compare('ded_status',$this->ded_status);
		$criteria->addCondition("ded_status = 1");

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Deductions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
