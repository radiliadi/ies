<?php

/**
 * This is the model class for table "tbl_bank_log".
 *
 * The followings are the available columns in table 'tbl_bank_log':
 * @property string $bnkl
 * @property integer $bnkl_bnk_id
 * @property integer $bnkl_coa_id
 * @property integer $bnkl_coat_id
 * @property integer $bnkl_coar_id
 * @property string $bnkl_id_pd
 * @property string $bnkl_log
 * @property string $bnkl_saldo_current
 * @property string $bnkl_saldo_used
 * @property string $bnkl_datetime_insert
 * @property string $bnkl_datetime_update
 * @property string $bnkl_datetime_delete
 * @property string $bnkl_insert_by
 * @property string $bnkl_update_by
 * @property string $bnkl_delete_by
 * @property integer $bnkl_status
 */
class BankLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_bank_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bnkl_bnk_id, bnkl_coa_id, bnkl_coat_id, bnkl_coar_id, bnkl_status', 'numerical', 'integerOnly'=>true),
			array('bnkl_id_pd, bnkl_log, bnkl_insert_by, bnkl_update_by, bnkl_delete_by', 'length', 'max'=>255),
			array('bnkl_saldo_current, bnkl_saldo_used', 'length', 'max'=>20),
			array('bnkl_datetime_insert, bnkl_datetime_update, bnkl_datetime_delete', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('bnkl, bnkl_bnk_id, bnkl_coa_id, bnkl_coat_id, bnkl_coar_id, bnkl_id_pd, bnkl_log, bnkl_saldo_current, bnkl_saldo_used, bnkl_datetime_insert, bnkl_datetime_update, bnkl_datetime_delete, bnkl_insert_by, bnkl_update_by, bnkl_delete_by, bnkl_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bnkl' => 'Bnkl',
			'bnkl_bnk_id' => 'Bnkl Bnk',
			'bnkl_coa_id' => 'Bnkl Coa',
			'bnkl_coat_id' => 'Bnkl Coat',
			'bnkl_coar_id' => 'Bnkl Coar',
			'bnkl_id_pd' => 'Bnkl Id Pd',
			'bnkl_log' => 'Bnkl Log',
			'bnkl_saldo_current' => 'Bnkl Saldo Current',
			'bnkl_saldo_used' => 'Bnkl Saldo Used',
			'bnkl_datetime_insert' => 'Bnkl Datetime Insert',
			'bnkl_datetime_update' => 'Bnkl Datetime Update',
			'bnkl_datetime_delete' => 'Bnkl Datetime Delete',
			'bnkl_insert_by' => 'Bnkl Insert By',
			'bnkl_update_by' => 'Bnkl Update By',
			'bnkl_delete_by' => 'Bnkl Delete By',
			'bnkl_status' => 'Bnkl Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bnkl',$this->bnkl,true);
		$criteria->compare('bnkl_bnk_id',$this->bnkl_bnk_id);
		$criteria->compare('bnkl_coa_id',$this->bnkl_coa_id);
		$criteria->compare('bnkl_coat_id',$this->bnkl_coat_id);
		$criteria->compare('bnkl_coar_id',$this->bnkl_coar_id);
		$criteria->compare('bnkl_id_pd',$this->bnkl_id_pd,true);
		$criteria->compare('bnkl_log',$this->bnkl_log,true);
		$criteria->compare('bnkl_saldo_current',$this->bnkl_saldo_current,true);
		$criteria->compare('bnkl_saldo_used',$this->bnkl_saldo_used,true);
		$criteria->compare('bnkl_datetime_insert',$this->bnkl_datetime_insert,true);
		$criteria->compare('bnkl_datetime_update',$this->bnkl_datetime_update,true);
		$criteria->compare('bnkl_datetime_delete',$this->bnkl_datetime_delete,true);
		$criteria->compare('bnkl_insert_by',$this->bnkl_insert_by,true);
		$criteria->compare('bnkl_update_by',$this->bnkl_update_by,true);
		$criteria->compare('bnkl_delete_by',$this->bnkl_delete_by,true);
		$criteria->compare('bnkl_status',$this->bnkl_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BankLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
