<?php

/**
 * This is the model class for table "tbl_chart_of_account_type".
 *
 * The followings are the available columns in table 'tbl_chart_of_account_type':
 * @property integer $coat_id
 * @property string $coat_desc
 * @property string $coat_color
 * @property string $coat_span
 * @property string $coat_datetime_insert
 * @property integer $coat_status
 */
class ChartOfAccountType extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_chart_of_account_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('coat_status', 'numerical', 'integerOnly'=>true),
			array('coat_color, coat_span', 'length', 'max'=>255),
			array('coat_desc, coat_datetime_insert', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('coat_id, coat_coac_id, coat_desc, coat_color, coat_span, coat_datetime_insert, coat_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'coat_id' => 'Coat',
			'coat_coac_id' => 'Coat Coac',
			'coat_desc' => 'Coat Desc',
			'coat_color' => 'Coat Color',
			'coat_span' => 'Coat Span',
			'coat_datetime_insert' => 'Coat Datetime Insert',
			'coat_status' => 'Coat Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('coat_id',$this->coat_id);
		$criteria->compare('coat_coac_id',$this->coat_coac_id);
		$criteria->compare('coat_desc',$this->coat_desc,true);
		$criteria->compare('coat_color',$this->coat_color,true);
		$criteria->compare('coat_span',$this->coat_span,true);
		$criteria->compare('coat_datetime_insert',$this->coat_datetime_insert,true);
		$criteria->compare('coat_status',$this->coat_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChartOfAccountType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
