<?php

/**
 * This is the model class for table "tbl_laba".
 *
 * The followings are the available columns in table 'tbl_laba':
 * @property integer $lb_id
 * @property string $lb_desc
 * @property string $lb_date
 * @property integer $lb_is_checked
 * @property string $lb_checked_by
 * @property string $lb_datetime_checked
 * @property string $lb_datetime_insert
 * @property integer $lb_is_locked
 * @property integer $lb_status
 */
class Laba extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_laba';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('lb_status', 'required'),
			array('lb_is_checked, lb_is_locked, lb_status', 'numerical', 'integerOnly'=>true),
			array('lb_checked_by', 'length', 'max'=>255),
			array('lb_desc, lb_date, lb_datetime_checked, lb_datetime_insert, lb_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('lb_id, lb_desc, lb_date, lb_is_checked, lb_checked_by, lb_datetime_checked, lb_datetime_insert, lb_datetime_update, lb_datetime_delete, lb_insert_by, lb_update_by, lb_delete_by, lb_is_locked, lb_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lb_id' => 'Laba ID',
			'lb_desc' => 'Desc',
			'lb_date' => 'Date',
			'lb_datetime_checked' => 'Datetime Checked',
			'lb_datetime_insert' => 'Datetime Insert',
			'lb_datetime_update' => 'Datetime Update',
			'lb_datetime_delete' => 'Datetime Delete',
			'lb_checked_by' => 'Checked By',
			'lb_insert_by' => 'Inserted By',
			'lb_update_by' => 'Updated By',
			'lb_delete_by' => 'Deleted By',
			'lb_is_checked' => 'Is Checked',
			'lb_is_locked' => 'Is Locked',
			'lb_status' => 'Lb Status',
		);
	}

	public function beforeSave()
    {
    	$id_pd=Yii::app()->session->get('username');
        if($this->isNewRecord || $this->lb_datetime_insert==null){
            $this->lb_datetime_insert=new CDbExpression('NOW()');
            $this->lb_insert_by = $id_pd;
        }else{
            $this->lb_datetime_update = new CDbExpression('NOW()');
            $this->lb_update_by = $id_pd;
        }
        return parent::beforeSave();
    }

	public static function getCount(){
		$count = Laba::model()->findAll(array('select'=>'lb_id', 'condition'=>'lb_status=:lb_status', 'params'=>array(':lb_status'=>1)));
		return count($count);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lb_id',$this->lb_id);
		$criteria->compare('lb_desc',$this->lb_desc,true);
		$criteria->compare('lb_date',$this->lb_date,true);
		$criteria->compare('lb_is_checked',$this->lb_is_checked);
		$criteria->compare('lb_checked_by',$this->lb_checked_by,true);
		$criteria->compare('lb_datetime_checked',$this->lb_datetime_checked,true);
		$criteria->compare('lb_datetime_insert',$this->lb_datetime_insert,true);
		$criteria->compare('lb_is_locked',$this->lb_is_locked);
		$criteria->compare('lb_status',$this->lb_status);
		$criteria->order='lb_date DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Laba the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
