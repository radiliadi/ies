<?php

/**
 * This is the model class for table "tbl_payment_method".
 *
 * The followings are the available columns in table 'tbl_payment_method':
 * @property integer $pyt_id
 * @property string $pyt_name
 * @property string $pyt_datetime
 * @property integer $pyt_status
 */
class PaymentMethod extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_payment_method';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pyt_status', 'numerical', 'integerOnly'=>true),
			array('pyt_name, pyt_datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pyt_id, pyt_name, pyt_datetime, pyt_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pyt_id' => 'Pyt',
			'pyt_name' => 'Pyt Name',
			'pyt_datetime' => 'Pyt Datetime',
			'pyt_status' => 'Pyt Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pyt_id',$this->pyt_id);
		$criteria->compare('pyt_name',$this->pyt_name,true);
		$criteria->compare('pyt_datetime',$this->pyt_datetime,true);
		$criteria->compare('pyt_status',$this->pyt_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaymentMethod the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
