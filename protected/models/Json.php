<?php

class MyJSON extends CJSON {
    public static function encode($var, $options = null, $depth = null)
    {
        if (function_exists('json_encode') && version_compare(PHP_VERSION, '5.5.0') >= 0) {
            return json_encode($var, $options, $depth);
        } elseif (function_exists('json_encode') && version_compare(PHP_VERSION, '5.3.0') >= 0) {
            return json_encode($var, $options);
        } else {
            return parent::encode($var);
        }
    }
}