<?php

/**
 * This is the model class for table "tbl_arus_kas_detail".
 *
 * The followings are the available columns in table 'tbl_arus_kas_detail':
 * @property integer $akd_id
 * @property integer $akd_ak_id
 * @property integer $akd_is_generate
 * @property string $akd_date
 * @property string $akd_desc
 * @property string $akd_laba_jalan
 * @property string $akd_bbn_susut
 * @property string $akd_piutang_usaha
 * @property string $akd_piutang_kyw
 * @property string $akd_pdd
 * @property string $akd_aset_tetap
 * @property string $akd_setor_modal
 * @property string $akd_naik_turun
 * @property string $akd_generate_by
 * @property string $akd_datetime_insert
 * @property string $akd_datetime_update
 * @property integer $akd_status
 */
class ArusKasDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_arus_kas_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('akd_ak_id', 'required'),
			array('akd_ak_id, akd_is_generate, akd_status', 'numerical', 'integerOnly'=>true),
			array('akd_laba_jalan, akd_bbn_susut, akd_piutang_usaha, akd_piutang_kyw, akd_pdd, akd_aset_tetap, akd_setor_modal, akd_naik_turun, akd_hutang_pemegang_saham', 'length', 'max'=>20),
			array('akd_generate_by', 'length', 'max'=>255),
			array('akd_date, akd_desc, akd_datetime_insert, akd_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('akd_id, akd_ak_id, akd_is_generate, akd_date, akd_desc, akd_laba_jalan, akd_bbn_susut, akd_piutang_usaha, akd_piutang_kyw, akd_pdd, akd_aset_tetap, akd_setor_modal, akd_hutang_pemegang_saham, akd_naik_turun, akd_generate_by, akd_datetime_insert, akd_datetime_update, akd_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'akd_id' => 'Akd',
			'akd_ak_id' => 'Akd Ak',
			'akd_is_generate' => 'Akd Is Generate',
			'akd_date' => 'Akd Date',
			'akd_desc' => 'Akd Desc',
			'akd_laba_jalan' => 'Akd Laba Jalan',
			'akd_bbn_susut' => 'Akd Bbn Susut',
			'akd_piutang_usaha' => 'Akd Piutang Usaha',
			'akd_piutang_kyw' => 'Akd Piutang Kyw',
			'akd_pdd' => 'Akd Pdd',
			'akd_aset_tetap' => 'Akd Aset Tetap',
			'akd_setor_modal' => 'Akd Setor Modal',
			'akd_hutang_pemegang_saham' => 'Akd Hutang Pemegang Saham',
			'akd_naik_turun' => 'Akd Naik Turun',
			'akd_generate_by' => 'Akd Generate By',
			'akd_datetime_insert' => 'Akd Datetime Insert',
			'akd_datetime_update' => 'Akd Datetime Update',
			'akd_status' => 'Akd Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('akd_id',$this->akd_id);
		$criteria->compare('akd_ak_id',$this->akd_ak_id);
		$criteria->compare('akd_is_generate',$this->akd_is_generate);
		$criteria->compare('akd_date',$this->akd_date,true);
		$criteria->compare('akd_desc',$this->akd_desc,true);
		$criteria->compare('akd_laba_jalan',$this->akd_laba_jalan,true);
		$criteria->compare('akd_bbn_susut',$this->akd_bbn_susut,true);
		$criteria->compare('akd_piutang_usaha',$this->akd_piutang_usaha,true);
		$criteria->compare('akd_piutang_kyw',$this->akd_piutang_kyw,true);
		$criteria->compare('akd_pdd',$this->akd_pdd,true);
		$criteria->compare('akd_aset_tetap',$this->akd_aset_tetap,true);
		$criteria->compare('akd_setor_modal',$this->akd_setor_modal,true);
		$criteria->compare('akd_hutang_pemegang_saham',$this->akd_hutang_pemegang_saham,true);
		$criteria->compare('akd_naik_turun',$this->akd_naik_turun,true);
		$criteria->compare('akd_generate_by',$this->akd_generate_by,true);
		$criteria->compare('akd_datetime_insert',$this->akd_datetime_insert,true);
		$criteria->compare('akd_datetime_update',$this->akd_datetime_update,true);
		$criteria->compare('akd_status',$this->akd_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
					'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ArusKasDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
