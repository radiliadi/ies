<?php

/**
 * This is the model class for table "absensi".
 *
 * The followings are the available columns in table 'absensi':
 * @property integer $abs_id
 * @property string $abs_jkk_tetap
 * @property string $abs_jkk_kontrak
 * @property string $abs_timesheet
 * @property string $abs_lembur
 * @property double $latitude
 * @property double $longitude
 * @property double $abs_lat
 * @property double $abs_lon
 */
class Absensi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'absensi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('abs_jkk_tetap, abs_jkk_kontrak, abs_timesheet, abs_lembur, latitude, longitude, abs_lat, abs_lon', 'required'),
			array('latitude, longitude, abs_lat, abs_lon', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('abs_id, abs_jkk_tetap, abs_jkk_kontrak, abs_timesheet, abs_lembur, latitude, longitude, abs_lat, abs_lon', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'abs_id' => 'Abs',
			'abs_jkk_tetap' => 'Abs Jkk Tetap',
			'abs_jkk_kontrak' => 'Abs Jkk Kontrak',
			'abs_timesheet' => 'Abs Timesheet',
			'abs_lembur' => 'Abs Lembur',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'abs_lat' => 'Abs Lat',
			'abs_lon' => 'Abs Lon',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('abs_id',$this->abs_id);
		$criteria->compare('abs_jkk_tetap',$this->abs_jkk_tetap,true);
		$criteria->compare('abs_jkk_kontrak',$this->abs_jkk_kontrak,true);
		$criteria->compare('abs_timesheet',$this->abs_timesheet,true);
		$criteria->compare('abs_lembur',$this->abs_lembur,true);
		$criteria->compare('latitude',$this->latitude);
		$criteria->compare('longitude',$this->longitude);
		$criteria->compare('abs_lat',$this->abs_lat);
		$criteria->compare('abs_lon',$this->abs_lon);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Absensi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
