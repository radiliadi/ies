<?php

/**
 * This is the model class for table "gaji".
 *
 * The followings are the available columns in table 'gaji':
 * @property integer $id_gaji
 * @property integer $nip
 * @property string $tgl
 * @property string $bulan
 * @property integer $tahun
 * @property integer $gj_pokok
 * @property integer $tunjangan
 * @property integer $potongan
 * @property integer $tot_gaji
 */
class Gaji extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gaji';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	 
	 
	public $tahun;
	public $bulan;
	public $tanggal;
	public $tanggal1,$bulan1,$tahun1;	
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pd, gj_pokok, tunjangan, potongan, tot_gaji', 'required'),
			// array('id_gaji, gj_pokok, tunjangan, potongan, tot_gaji', 'numerical', 'integerOnly'=>true),
			array('tgl', 'length', 'max'=>15),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_gaji, id_pd, tgl, gj_pokok, tunjangan, potongan, tot_gaji, tot_taka, neto_sebulan, neto_setahun, ptkp_setahun, pkps, pph_setahun, pph_sebulan, bruto, overtime, transport, functional, breakfast, lunch, tot_deduc, deduc_basic, bpjs_kete, bpjs_kete_ktr, bpjs_kese, bpjs_kese_ktr, bpjs_pensi, bpjs_pensi_ktr, tunj_kese, tunj_kese_ktr, created_by, user_agent, date_created, gaji_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'gaji'=>array(self::HAS_MANY, 'gaji','karyawan'),
				'datakaryawan'=>array(self::BELONGS_TO, 'Karyawan', 'id_pd'),
				'datajabatan'=>array(self::BELONGS_TO, 'Jabatan', 'id_jabatan',
								'through'=>'datakaryawan'),
				'dataptkp'=>array(self::BELONGS_TO, 'Ptkp', 'id_ptkp',
								'through'=>'datakaryawan'),
				'datamc'=>array(MedicalClaim::BELONGS_TO, 'MedicalClaim', 'mc_id_pd',
								'through'=>'datamc'),
				'dataearn'=>array(Earnings::BELONGS_TO, 'Earnings', 'earn_id_pd',
								'through'=>'dataearn'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_gaji'=>'Id gaji',
			'id_pd' => 'id_pd',
			'tgl' => 'Tgl',
			'gj_pokok' => 'Gj Pokok',
			'tunjangan' => 'Tunjangan',
			'potongan' => 'Potongan',
			'tot_gaji' => 'Tot Gaji',
			'tot_taka' => 'Tot Gaji',
			'neto_sebulan' => 'Neto Sebulan',
			'neto_setahun' => 'Neto Sebulan',
			'ptkp_setahun' => 'Neto Sebulan',
			'pkps' => 'Neto Sebulan',
			'pph_setahun' => 'Neto Sebulan',
			'pph_sebulan' => 'Neto Sebulan',
			'bruto' => 'Tgl',
			'overtime' => 'Gj Pokok',
			'transport' => 'Tunjangan',
			'functional' => 'Potongan',
			'breakfast' => 'Tot Gaji',
			'lunch' => 'Neto Sebulan',
			'tot_deduc' => 'Neto Sebulan',
			'deduc_basic' => 'Neto Sebulan',
			'bpjs_pensi' => 'Neto Sebulan',
			'bpjs_kese' => 'Neto Sebulan',
			'bpjs_kete' => 'Neto Sebulan',
			'bpjs_pensi_ktr' => 'Neto Sebulan',
			'bpjs_kese_ktr' => 'Neto Sebulan',
			'bpjs_kete_ktr' => 'Neto Sebulan',
			'created_by' => 'Created By',
			'user_agent' => 'User Agent',
			'date_created' => 'Date Created',
			'gaji_status' => 'Date gaji_status',
			'tunj_kese' => 'Date gaji_status',
			'tunj_kese_ktr' => 'Date gaji_status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('id_gaji',$this->id_gaji);
		$criteria->compare('id_pd',$this->id_pd);
		$criteria->compare('tgl',$this->tgl,true);
		$criteria->compare('gj_pokok',$this->gj_pokok);
		$criteria->compare('tunjangan',$this->tunjangan);
		$criteria->compare('potongan',$this->potongan);
		$criteria->compare('tot_gaji',$this->tot_gaji);
		$criteria->compare('tot_taka',$this->tot_taka);
		$criteria->compare('neto_sebulan',$this->neto_sebulan);
		$criteria->compare('neto_setahun',$this->neto_setahun);
		$criteria->compare('ptkp_setahun',$this->ptkp_setahun);
		$criteria->compare('pkps',$this->pkps);
		$criteria->compare('pph_setahun',$this->pph_setahun);
		$criteria->compare('pph_sebulan',$this->pph_sebulan);
		$criteria->compare('bruto',$this->bruto,true);
		$criteria->compare('overtime',$this->overtime);
		$criteria->compare('transport',$this->transport);
		$criteria->compare('functional',$this->functional);
		$criteria->compare('breakfast',$this->breakfast);
		$criteria->compare('lunch',$this->lunch);
		$criteria->compare('tot_deduc',$this->tot_deduc);
		$criteria->compare('deduc_basic',$this->deduc_basic);
		$criteria->compare('bpjs_kete',$this->bpjs_kete);
		$criteria->compare('bpjs_kese',$this->bpjs_kese);
		$criteria->compare('bpjs_pensi',$this->bpjs_pensi);
		$criteria->compare('bpjs_kete_ktr',$this->bpjs_kete_ktr);
		$criteria->compare('bpjs_kese_ktr',$this->bpjs_kese_ktr);
		$criteria->compare('bpjs_pensi_ktr',$this->bpjs_pensi_ktr);
		$criteria->compare('tunj_kese',$this->tunj_kese);
		$criteria->compare('tunj_kese_ktr',$this->tunj_kese_ktr);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('user_agent',$this->user_agent);
		$criteria->compare('date_created',$this->date_created);
		$criteria->compare('gaji_status',$this->gaji_status);
		// $bulan = date('m');
		// $criteria->addCondition('tgl = :tgl');
		// $criteria->params=array(
		// 	':tgl' => '%'$bulan'%'
		// 	)

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Gaji the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
