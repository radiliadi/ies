<?php

/**
 * This is the model class for table "cv".
 *
 * The followings are the available columns in table 'cv':
 * @property integer $id_cv
 * @property string $nm_cv
 * @property string $link_cv
 */
class Cv extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nm_cv, link_cv, stat_cv', 'required'),
			array('nm_cv, link_cv', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_cv, nm_cv, link_cv, stat_cv', 'safe', 'on'=>'search'),
			array('link_cv', 'file','types'=>'pdf', 'allowEmpty'=>false,'on'=>array('create')),
			array('nm_cv','unique','message'=>'Nama File <b>{value}</b> sudah ada, mohon ubah nama file terlebih dahulu.'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_cv' => 'Id Cv',
			'nm_cv' => 'NIP',
			'link_cv' => 'Curriculum Vitae',
			'stat_cv' => 'Curriculum Vitae',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('id_cv',$this->id_cv);
		$criteria->compare('nm_cv',$this->nm_cv,true);
		$criteria->compare('link_cv',$this->link_cv,true);
		$criteria->compare('stat_cv',$this->stat_cv,true);
		$criteria->addCondition("stat_cv = 1");
		// $criteria->addInCondition("subject_id", array("1","2"));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	public function searchx()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('id_cv',$this->id_cv);
		$criteria->compare('nm_cv',$this->nm_cv,true);
		$criteria->compare('link_cv',$this->link_cv,true);
		$criteria->compare('stat_cv',$this->stat_cv,true);
		$criteria->addCondition("stat_cv = 2");
		// $criteria->addInCondition("subject_id", array("1","2"));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	public function searchy()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('id_cv',$this->id_cv);
		$criteria->compare('nm_cv',$this->nm_cv,true);
		$criteria->compare('link_cv',$this->link_cv,true);
		$criteria->compare('stat_cv',$this->stat_cv,true);
		$criteria->addCondition("stat_cv = 3");
		// $criteria->addInCondition("subject_id", array("1","2"));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	public static function getCvStatus() {
		$list = CHtml::listData(CvStatus::model()->findAll(array('order' => 'id_cv_status ASC')), 'id_cv_status', 'nm_cv_status');
		return $list;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cv the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
