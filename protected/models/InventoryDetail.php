<?php

/**
 * This is the model class for table "tbl_inventory_detail".
 *
 * The followings are the available columns in table 'tbl_inventory_detail':
 * @property integer $invd_id
 * @property integer $invd_inv_id
 * @property string $invd_date
 * @property string $invd_desc
 * @property string $invd_value
 * @property string $invd_qty
 * @property string $invd_tarif
 * @property string $invd_datetime_insert
 * @property string $invd_datetime_update
 * @property string $invd_datetime_delete
 * @property string $invd_insert_by
 * @property string $invd_update_by
 * @property string $invd_delete_by
 * @property integer $invd_status
 */
class InventoryDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_inventory_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('invd_inv_id, invd_status', 'numerical', 'integerOnly'=>true),
			array('invd_value, invd_qty', 'length', 'max'=>20),
			array('invd_insert_by, invd_update_by, invd_delete_by', 'length', 'max'=>255),
			array('invd_date, invd_desc, invd_tarif, invd_datetime_insert, invd_datetime_update, invd_datetime_delete', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('invd_id, invd_inv_id, invd_date, invd_desc, invd_value, invd_qty, invd_tarif, invd_datetime_insert, invd_datetime_update, invd_datetime_delete, invd_insert_by, invd_update_by, invd_delete_by, invd_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'invd_id' => 'Invd',
			'invd_inv_id' => 'Invd Inv',
			'invd_date' => 'Invd Date',
			'invd_desc' => 'Invd Desc',
			'invd_value' => 'Invd Value',
			'invd_qty' => 'Invd Qty',
			'invd_tarif' => 'Invd Tarif',
			'invd_datetime_insert' => 'Invd Datetime Insert',
			'invd_datetime_update' => 'Invd Datetime Update',
			'invd_datetime_delete' => 'Invd Datetime Delete',
			'invd_insert_by' => 'Invd Insert By',
			'invd_update_by' => 'Invd Update By',
			'invd_delete_by' => 'Invd Delete By',
			'invd_status' => 'Invd Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('invd_id',$this->invd_id);
		$criteria->compare('invd_inv_id',$this->invd_inv_id);
		$criteria->compare('invd_date',$this->invd_date,true);
		$criteria->compare('invd_desc',$this->invd_desc,true);
		$criteria->compare('invd_value',$this->invd_value,true);
		$criteria->compare('invd_qty',$this->invd_qty,true);
		$criteria->compare('invd_tarif',$this->invd_tarif,true);
		$criteria->compare('invd_datetime_insert',$this->invd_datetime_insert,true);
		$criteria->compare('invd_datetime_update',$this->invd_datetime_update,true);
		$criteria->compare('invd_datetime_delete',$this->invd_datetime_delete,true);
		$criteria->compare('invd_insert_by',$this->invd_insert_by,true);
		$criteria->compare('invd_update_by',$this->invd_update_by,true);
		$criteria->compare('invd_delete_by',$this->invd_delete_by,true);
		$criteria->compare('invd_status',$this->invd_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InventoryDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
