<?php

/**
 * This is the model class for table "status_mahasiswa".
 *
 * The followings are the available columns in table 'status_mahasiswa':
 * @property string $id_stat_mhs
 * @property string $nm_stat_mhs
 * @property string $ket_stat_mhs
 */
class StatusMahasiswa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'status_mahasiswa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_stat_mhs, nm_stat_mhs', 'required'),
			array('id_stat_mhs', 'length', 'max'=>1),
			array('nm_stat_mhs, ket_stat_mhs', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_stat_mhs, nm_stat_mhs, ket_stat_mhs', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_stat_mhs' => 'Id Stat Mhs',
			'nm_stat_mhs' => 'Nama Status Mahasiswa',
			'ket_stat_mhs' => 'Keterangan Status Mahasiswa',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('id_stat_mhs',$this->id_stat_mhs,true);
		$criteria->compare('nm_stat_mhs',$this->nm_stat_mhs,true);
		$criteria->compare('ket_stat_mhs',$this->ket_stat_mhs,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StatusMahasiswa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
