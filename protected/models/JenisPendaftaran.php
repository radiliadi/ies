<?php

/**
 * This is the model class for table "jenis_pendaftaran".
 *
 * The followings are the available columns in table 'jenis_pendaftaran':
 * @property string $id_jns_daftar
 * @property string $nm_jns_daftar
 * @property string $u_daftar_sekolah
 * @property string $u_daftar_rombel
 */
class JenisPendaftaran extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jenis_pendaftaran';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nm_jns_daftar', 'required'),
			array('id_jns_daftar, nm_jns_daftar', 'length', 'max'=>20),
			// array('u_daftar_sekolah, u_daftar_rombel', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_jns_daftar, nm_jns_daftar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_jns_daftar' => 'Id Jns Daftar',
			'nm_jns_daftar' => 'Nm Jns Daftar',
			// 'u_daftar_sekolah' => 'U Daftar Sekolah',
			// 'u_daftar_rombel' => 'U Daftar Rombel',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('id_jns_daftar',$this->id_jns_daftar,true);
		$criteria->compare('nm_jns_daftar',$this->nm_jns_daftar,true);
		// $criteria->compare('u_daftar_sekolah',$this->u_daftar_sekolah,true);
		// $criteria->compare('u_daftar_rombel',$this->u_daftar_rombel,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JenisPendaftaran the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
