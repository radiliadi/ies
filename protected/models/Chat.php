<?php

/**
 * This is the model class for table "chat".
 *
 * The followings are the available columns in table 'chat':
 * @property integer $chat_id
 * @property string $chat_from
 * @property string $chat_to
 * @property string $chat_text
 * @property string $chat_status
 * @property string $chat_date
 */
class Chat extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'chat';
	}

	public $userChat;

	/**
	 * @return array validation rules for model attributes.
	 */
	public $year="";
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('chat_from, chat_to, chat_text, chat_date', 'required'),
			array('chat_from, chat_to', 'length', 'max'=>25),
			array('chat_status', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('chat_id, chat_from, chat_to, chat_text, chat_status, chat_date, userChat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user'=>array(self::HAS_ONE, 'User', array('user_id'=>'chat_from')),
			// 'relasiuser'=>array(self::HAS_ONE, 'user', array('user_id'=>'chat_from')),
			// 'relasiuser'	=>	array(self::HAS_ONE, 'user', 'user_id'),
			// 'relasijabatan'	=>	array(self::BELONGS_TO, 'jabatan', 'id_jabatan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'chat_id' => 'Chat',
			'chat_from' => 'Chat From',
			'chat_to' => 'Chat To',
			'chat_text' => 'Chat Text',
			'chat_status' => 'Chat Status',
			'chat_date' => 'Chat Date',
			'userChat' => 'userChat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('chat_id',$this->chat_id);
		$criteria->compare('chat_from',$this->chat_from,true);
		$criteria->compare('chat_to',$this->chat_to,true);
		$criteria->compare('chat_text',$this->chat_text,true);
		$criteria->compare('chat_status',$this->chat_status,true);
		$criteria->compare('chat_date',$this->chat_date,true);
		$criteria->order='chat_id DESC';
		// $criteria->with=array('relasiuser');
		// $criteria->compare('relasijabatan.user_id', $this->userChat, false, '='); //field di user

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Chat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
