<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $username
 * @property string $password
 * @property string $level
 */
class UserFoto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	 //digunakan untuk memproses data setelah di validasi
protected function afterValidate() {
     parent::afterValidate();
            
     //melakukan enkripsi pada passwod yang di input
     $this->password = $this->encrypt($this->password);
}
        
//membuat sebuah fungsi untuk mengenkripsi data
public function encrypt($value){
     return md5($value);
}

	 
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('username, password, level, user_stat', 'required'),
			// array('username, password, level', 'length', 'max'=>25),
			// // The following rule is used by search().
			// // @todo Please remove those attributes that should not be searched.
			// // array('username','unique','message'=>'[{attribute} : {value}] Data telah tersedia!'),
			// // array('user_foto', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>false,'on'=>array('create')),
			// array('user_foto', 'file', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,png,bmp','maxSize'=>150*1024*1024,'tooLarge'=>'Ukuran foto terlalu besar', 'wrongType'=>'Jenis file hanya JPG, JPEG, BMP atau PNG', 'except'=>'insert'),
			// array('username, password, level, user_login, user_first_login, user_last_login, user_datetime, user_stat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				// 'usrst'=>array(UserStatus::HAS_MANY, 'usrst_id', 'user_stat',
				// 				'through'=>'usrst'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			// 'username' => 'Username',
			// 'password' => 'Password',
			// 'level' => 'Level',
			// 'user_login' => '',
			// 'user_first_login' => 'First Login',
			// 'user_last_login' => 'Last Login',
			// 'user_datetime' => 'User Created',
			// 'user_stat' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
