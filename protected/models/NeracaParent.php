<?php

/**
 * This is the model class for table "tbl_neraca_parent".
 *
 * The followings are the available columns in table 'tbl_neraca_parent':
 * @property string $nrcp_id
 * @property string $nrcp_desc
 * @property string $nrcp_datetime_insert
 * @property string $nrcp_datetime_update
 * @property string $nrcp_insert_by
 * @property string $nrcp_update_by
 * @property integer $nrcp_sort
 * @property integer $nrcp_status
 */
class NeracaParent extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_neraca_parent';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nrcp_sort, nrcp_status', 'numerical', 'integerOnly'=>true),
			array('nrcp_insert_by, nrcp_update_by', 'length', 'max'=>255),
			array('nrcp_desc, nrcp_datetime_insert, nrcp_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('nrcp_id, nrcp_desc, nrcp_datetime_insert, nrcp_datetime_update, nrcp_insert_by, nrcp_update_by, nrcp_sort, nrcp_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nrcp_id' => 'Nrcp',
			'nrcp_desc' => 'Nrcp Desc',
			'nrcp_datetime_insert' => 'Nrcp Datetime Insert',
			'nrcp_datetime_update' => 'Nrcp Datetime Update',
			'nrcp_insert_by' => 'Nrcp Insert By',
			'nrcp_update_by' => 'Nrcp Update By',
			'nrcp_sort' => 'Nrcp Sort',
			'nrcp_status' => 'Nrcp Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nrcp_id',$this->nrcp_id,true);
		$criteria->compare('nrcp_desc',$this->nrcp_desc,true);
		$criteria->compare('nrcp_datetime_insert',$this->nrcp_datetime_insert,true);
		$criteria->compare('nrcp_datetime_update',$this->nrcp_datetime_update,true);
		$criteria->compare('nrcp_insert_by',$this->nrcp_insert_by,true);
		$criteria->compare('nrcp_update_by',$this->nrcp_update_by,true);
		$criteria->compare('nrcp_sort',$this->nrcp_sort);
		$criteria->compare('nrcp_status',$this->nrcp_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NeracaParent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
