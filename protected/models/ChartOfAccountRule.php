<?php

/**
 * This is the model class for table "tbl_chart_of_account_rule".
 *
 * The followings are the available columns in table 'tbl_chart_of_account_rule':
 * @property integer $coar_id
 * @property string $coar_desc
 * @property string $coar_datetime
 * @property string $coar_datetime_insert
 * @property string $coar_datetime_update
 * @property string $coar_created_by
 * @property integer $coar_status
 */
class ChartOfAccountRule extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_chart_of_account_rule';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('coar_status', 'numerical', 'integerOnly'=>true),
			array('coar_created_by', 'length', 'max'=>255),
			array('coar_desc, coar_datetime, coar_datetime_insert, coar_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('coar_id, coar_desc, coar_datetime, coar_datetime_insert, coar_datetime_update, coar_created_by, coar_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'coar_id' => 'Coar',
			'coar_desc' => 'Coar Desc',
			'coar_datetime' => 'Coar Datetime',
			'coar_datetime_insert' => 'Coar Datetime Insert',
			'coar_datetime_update' => 'Coar Datetime Update',
			'coar_created_by' => 'Coar Created By',
			'coar_status' => '',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('coar_id',$this->coar_id);
		$criteria->compare('coar_desc',$this->coar_desc,true);
		$criteria->compare('coar_datetime',$this->coar_datetime,true);
		$criteria->compare('coar_datetime_insert',$this->coar_datetime_insert,true);
		$criteria->compare('coar_datetime_update',$this->coar_datetime_update,true);
		$criteria->compare('coar_created_by',$this->coar_created_by,true);
		$criteria->compare('coar_status',$this->coar_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChartOfAccountRule the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
