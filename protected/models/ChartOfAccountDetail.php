<?php

/**
 * This is the model class for table "tbl_chart_of_account_detail".
 *
 * The followings are the available columns in table 'tbl_chart_of_account_detail':
 * @property integer $coad_id
 * @property integer $coad_coa_id
 * @property integer $coad_coar_id
 * @property integer $coad_coat_id
 * @property string $coad_id_pd
 * @property string $coad_desc
 * @property string $coad_debet
 * @property string $coad_credit
 * @property string $coad_datetime
 * @property string $coad_datetime_insert
 * @property string $coad_datetime_update
 * @property integer $coad_is_submit
 * @property integer $coad_status
 */
class ChartOfAccountDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_chart_of_account_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('coad_coa_id, coad_coar_id, coad_coat_id, coad_id_pd', 'required'),
			array('coad_coa_id, coad_coar_id, coad_coat_id, coad_is_submit, coad_status', 'numerical', 'integerOnly'=>true),
			array('coad_id_pd', 'length', 'max'=>255),
			array('coad_debet, coad_credit, coad_color', 'length', 'max'=>20),
			array('coad_desc, coad_datetime, coad_datetime_insert, coad_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('coad_id, coad_coa_id, coad_coar_id, coad_coat_id, coad_bnk_id, coad_bnk_id_pd, coad_id_pd, coad_desc, coad_debet, coad_credit, coad_saldo, coad_current_saldo, coad_bb_saldo, coad_bb_is_saldo, coad_bb_current_saldo, coad_jurnal_saldo, coad_jurnal_is_saldo, coad_jurnal_current_saldo, coad_color, coad_datetime, coad_datetime_insert, coad_datetime_update, coad_is_saldo, coad_is_submit, coad_is_jurnal, coad_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'getCoa' 	=> 	array(self::BELONGS_TO, 'ChartOfAccount', 'coad_coa_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'coad_id' => 'ID',
			'coad_coa_id' => 'COA',
			'coad_coar_id' => 'Rule',
			'coad_coat_id' => 'Type',
			'coad_bnk_id' => 'Bank',
			'coad_bnk_id_pd' => 'By',
			'coad_id_pd' => 'Id Pd',
			'coad_desc' => 'Desc',
			'coad_debet' => 'Debet',
			'coad_credit' => 'Credit',
			'coad_saldo' => 'Saldo',
			'coad_is_saldo' => 'Is Saldo',
			'coad_current_saldo' => 'Current',
			'coad_bb_saldo' => 'BB Saldo',
			'coad_bb_is_saldo' => 'BB Is Saldo',
			'coad_bb_current_saldo' => 'BB Current',
			'coad_jurnal_saldo' => 'Jurnal Saldo',
			'coad_jurnal_is_saldo' => 'Jurnal Is Saldo',
			'coad_jurnal_current_saldo' => 'Jurnal Current',
			'coad_color' => 'Color',
			'coad_datetime' => 'Datetime',
			'coad_datetime_insert' => 'Datetime Insert',
			'coad_datetime_update' => 'Datetime Update',
			'coad_is_jurnal' => 'Is Jurnal',
			'coad_is_submit' => 'Is Submit',
			'coad_status' => 'Status',
		);
	}

	public static function listColor() {
		$list = CHtml::listData(Color::model()->findAll(array('order' => 'clr_id ASC')), 'clr_value', 'clr_desc');
		return $list;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->with=array('getCoa');
		$criteria->compare('coad_id',$this->coad_id);
		$criteria->compare('coad_coa_id',$this->coad_coa_id);
		$criteria->compare('coad_coar_id',$this->coad_coar_id);
		$criteria->compare('coad_coat_id',$this->coad_coat_id);
		$criteria->compare('coad_bnk_id',$this->coad_bnk_id);
		$criteria->compare('coad_bnk_id_pd',$this->coad_bnk_id_pd,true);
		$criteria->compare('coad_id_pd',$this->coad_id_pd,true);
		$criteria->compare('coad_desc',$this->coad_desc,true);
		$criteria->compare('coad_debet',$this->coad_debet,true);
		$criteria->compare('coad_credit',$this->coad_credit,true);
		$criteria->compare('coad_saldo',$this->coad_saldo,true);
		$criteria->compare('coad_is_saldo',$this->coad_is_saldo);
		$criteria->compare('coad_current_saldo',$this->coad_current_saldo,true);
		$criteria->compare('coad_bb_saldo',$this->coad_bb_saldo,true);
		$criteria->compare('coad_bb_is_saldo',$this->coad_bb_is_saldo);
		$criteria->compare('coad_bb_current_saldo',$this->coad_bb_current_saldo,true);
		$criteria->compare('coad_jurnal_saldo',$this->coad_jurnal_saldo,true);
		$criteria->compare('coad_jurnal_is_saldo',$this->coad_jurnal_is_saldo);
		$criteria->compare('coad_jurnal_current_saldo',$this->coad_jurnal_current_saldo,true);
		$criteria->compare('coad_color',$this->coad_color,true);
		$criteria->compare('coad_datetime',$this->coad_datetime,true);
		$criteria->compare('coad_datetime_insert',$this->coad_datetime_insert,true);
		$criteria->compare('coad_datetime_update',$this->coad_datetime_update,true);
		$criteria->compare('coad_is_submit',$this->coad_is_submit);
		$criteria->compare('coad_is_jurnal',$this->coad_is_jurnal);
		$criteria->compare('coad_status',$this->coad_status);
		$criteria->order='coad_datetime DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChartOfAccountDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
