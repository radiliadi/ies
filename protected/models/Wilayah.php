<?php

/**
 * This is the model class for table "wilayah".
 *
 * The followings are the available columns in table 'wilayah':
 * @property string $id_wil
 * @property string $nm_wil
 * @property string $asal_wil
 * @property string $kode_bps
 * @property string $kode_dagri
 * @property string $kode_keu
 * @property string $id_induk_wilayah
 * @property integer $id_level_wil
 * @property string $id_negara
 */
class Wilayah extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'wilayah';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_wil, nm_wil, asal_wil, kode_bps, kode_dagri, kode_keu, id_induk_wilayah, id_level_wil, id_negara', 'required'),
			array('id_level_wil', 'numerical', 'integerOnly'=>true),
			array('id_wil, asal_wil, id_induk_wilayah', 'length', 'max'=>8),
			array('nm_wil', 'length', 'max'=>50),
			array('kode_bps, kode_dagri', 'length', 'max'=>7),
			array('kode_keu', 'length', 'max'=>10),
			array('id_negara', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_wil, nm_wil, asal_wil, kode_bps, kode_dagri, kode_keu, id_induk_wilayah, id_level_wil, id_negara', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_wil' => 'Id Wil',
			'nm_wil' => 'Nm Wil',
			'asal_wil' => 'Asal Wil',
			'kode_bps' => 'Kode Bps',
			'kode_dagri' => 'Kode Dagri',
			'kode_keu' => 'Kode Keu',
			'id_induk_wilayah' => 'Id Induk Wilayah',
			'id_level_wil' => 'Id Level Wil',
			'id_negara' => 'Id Negara',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('id_wil',$this->id_wil,true);
		$criteria->compare('nm_wil',$this->nm_wil,true);
		$criteria->compare('asal_wil',$this->asal_wil,true);
		$criteria->compare('kode_bps',$this->kode_bps,true);
		$criteria->compare('kode_dagri',$this->kode_dagri,true);
		$criteria->compare('kode_keu',$this->kode_keu,true);
		$criteria->compare('id_induk_wilayah',$this->id_induk_wilayah,true);
		$criteria->compare('id_level_wil',$this->id_level_wil);
		$criteria->compare('id_negara',$this->id_negara,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Wilayah the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
