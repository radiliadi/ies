<?php

/**
 * This is the model class for table "tbl_bank".
 *
 * The followings are the available columns in table 'tbl_bank':
 * @property integer $bnk_id
 * @property string $bnk_name
 * @property string $bnk_desc
 * @property string $bnk_saldo_first
 * @property string $bnk_saldo_current
 * @property string $bnk_saldo_used
 * @property string $bnk_datetime
 * @property integer $bnk_status
 */
class Bank extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_bank';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bnk_status', 'numerical', 'integerOnly'=>true),
			array('bnk_saldo_first, bnk_saldo_current, bnk_saldo_used', 'length', 'max'=>20),
			array('bnk_name, bnk_desc, bnk_datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('bnk_id, bnk_coa_id, bnk_name, bnk_desc, bnk_saldo_first, bnk_saldo_current, bnk_saldo_used, bnk_saldo_update, bnk_datetime, bnk_datetime_update, bnk_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bnk_id' => 'Bnk',
			'bnk_coa_id' => 'Bnk COA',
			'bnk_name' => 'Name',
			'bnk_desc' => 'No. Rekening',
			'bnk_saldo_first' => 'Last Top Up',
			'bnk_saldo_current' => 'Current Saldo',
			'bnk_saldo_used' => 'Total Used',
			'bnk_saldo_update' => 'Bnk Saldo Update',
			'bnk_datetime' => 'Bnk Datetime',
			'bnk_datetime_update' => 'Bnk Datetime Update',
			'bnk_status' => 'Bnk Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bnk_id',$this->bnk_id);
		$criteria->compare('bnk_coa_id',$this->bnk_coa_id);
		$criteria->compare('bnk_name',$this->bnk_name,true);
		$criteria->compare('bnk_desc',$this->bnk_desc,true);
		$criteria->compare('bnk_saldo_first',$this->bnk_saldo_first,true);
		$criteria->compare('bnk_saldo_current',$this->bnk_saldo_current,true);
		$criteria->compare('bnk_saldo_used',$this->bnk_saldo_used,true);
		$criteria->compare('bnk_saldo_update',$this->bnk_saldo_update,true);
		$criteria->compare('bnk_datetime',$this->bnk_datetime,true);
		$criteria->compare('bnk_datetime_update',$this->bnk_datetime_update,true);
		$criteria->compare('bnk_status',$this->bnk_status);
		$criteria->addCondition('bnk_status = 1');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bank the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
