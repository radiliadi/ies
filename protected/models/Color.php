<?php

/**
 * This is the model class for table "tbl_color".
 *
 * The followings are the available columns in table 'tbl_color':
 * @property integer $clr_id
 * @property string $clr_desc
 * @property string $clr_value
 * @property string $clr_datetime
 * @property integer $clr_status
 */
class Color extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_color';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('clr_status', 'numerical', 'integerOnly'=>true),
			array('clr_value', 'length', 'max'=>255),
			array('clr_desc, clr_datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('clr_id, clr_desc, clr_value, clr_datetime, clr_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'clr_id' => 'Clr',
			'clr_desc' => 'Clr Desc',
			'clr_value' => 'Clr Value',
			'clr_datetime' => 'Clr Datetime',
			'clr_status' => 'Clr Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('clr_id',$this->clr_id);
		$criteria->compare('clr_desc',$this->clr_desc,true);
		$criteria->compare('clr_value',$this->clr_value,true);
		$criteria->compare('clr_datetime',$this->clr_datetime,true);
		$criteria->compare('clr_status',$this->clr_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Color the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
