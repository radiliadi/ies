<?php

/**
 * This is the model class for table "jenjang_pendidikan".
 *
 * The followings are the available columns in table 'jenjang_pendidikan':
 * @property integer $id_jenj_didik
 * @property string $nm_jenj_didik
 * @property string $u_jenj_lemb
 * @property string $u_jenj_org
 */
class JenjangPendidikan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jenjang_pendidikan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nm_jenj_didik', 'required'),
			array('nm_jenj_didik', 'length', 'max'=>25),
			array('u_jenj_lemb, u_jenj_org', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_jenj_didik, nm_jenj_didik, u_jenj_lemb, u_jenj_org', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_jenj_didik' => 'Id Jenj Didik',
			'nm_jenj_didik' => 'Nm Jenj Didik',
			'u_jenj_lemb' => 'U Jenj Lemb',
			'u_jenj_org' => 'U Jenj Org',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('id_jenj_didik',$this->id_jenj_didik);
		$criteria->compare('nm_jenj_didik',$this->nm_jenj_didik,true);
		$criteria->compare('u_jenj_lemb',$this->u_jenj_lemb,true);
		$criteria->compare('u_jenj_org',$this->u_jenj_org,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JenjangPendidikan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
