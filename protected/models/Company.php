<?php

/**
 * This is the model class for table "tbl_company".
 *
 * The followings are the available columns in table 'tbl_company':
 * @property string $cpy_id
 * @property string $cpy_fullname
 * @property string $cpy_dirut
 * @property string $cpy_finance
 * @property string $cpy_hrd
 * @property string $cpy_datetime_insert
 * @property string $cpy_datetime_update
 * @property string $cpy_datetime_delete
 * @property string $cpy_insert_by
 * @property string $cpy_update_by
 * @property string $cpy_delete_by
 * @property integer $cpy_status
 */
class Company extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_company';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cpy_status', 'numerical', 'integerOnly'=>true),
			array('cpy_fullname, cpy_shortname, cpy_province, cpy_city, cpy_dirut, cpy_finance, cpy_hrd, cpy_notes, cpy_insert_by, cpy_update_by, cpy_delete_by', 'length', 'max'=>255),
			array('cpy_shortname', 'length', 'max'=>5),
			array('cpy_datetime_insert, cpy_datetime_update, cpy_datetime_delete', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cpy_id, cpy_fullname, cpy_shortname, cpy_province, cpy_city, cpy_dirut, cpy_finance, cpy_hrd, cpy_notes, cpy_datetime_insert, cpy_datetime_update, cpy_datetime_delete, cpy_insert_by, cpy_update_by, cpy_delete_by, cpy_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cpy_id' => 'Cpy',
			'cpy_fullname' => 'Fullname',
			'cpy_shortname' => 'Shortname',
			'cpy_province' => 'Province',
			'cpy_city' => 'City',
			'cpy_dirut' => 'Dirut',
			'cpy_finance' => 'Finance',
			'cpy_hrd' => 'Human Resources',
			'cpy_notes' => 'Notes',
			'cpy_datetime_insert' => 'Datetime Insert',
			'cpy_datetime_update' => 'Datetime Update',
			'cpy_datetime_delete' => 'Datetime Delete',
			'cpy_insert_by' => 'Insert By',
			'cpy_update_by' => 'Update By',
			'cpy_delete_by' => 'Delete By',
			'cpy_status' => 'Status',
		);
	}

	public function beforeSave()
    {
    	$id_pd=Yii::app()->session->get('username');
        if($this->isNewRecord || $this->cpy_datetime_insert==null){
            $this->cpy_datetime_insert=new CDbExpression('NOW()');
            $this->cpy_insert_by = $id_pd;
        }else{
            $this->cpy_datetime_update = new CDbExpression('NOW()');
            $this->cpy_update_by = $id_pd;
        }
        return parent::beforeSave();
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cpy_id',$this->cpy_id,true);
		$criteria->compare('cpy_fullname',$this->cpy_fullname,true);
		$criteria->compare('cpy_shortname',$this->cpy_shortname,true);
		$criteria->compare('cpy_province',$this->cpy_province,true);
		$criteria->compare('cpy_city',$this->cpy_city,true);
		$criteria->compare('cpy_dirut',$this->cpy_dirut,true);
		$criteria->compare('cpy_finance',$this->cpy_finance,true);
		$criteria->compare('cpy_hrd',$this->cpy_hrd,true);
		$criteria->compare('cpy_notes',$this->cpy_notes,true);
		$criteria->compare('cpy_datetime_insert',$this->cpy_datetime_insert,true);
		$criteria->compare('cpy_datetime_update',$this->cpy_datetime_update,true);
		$criteria->compare('cpy_datetime_delete',$this->cpy_datetime_delete,true);
		$criteria->compare('cpy_insert_by',$this->cpy_insert_by,true);
		$criteria->compare('cpy_update_by',$this->cpy_update_by,true);
		$criteria->compare('cpy_delete_by',$this->cpy_delete_by,true);
		$criteria->compare('cpy_status',$this->cpy_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Company the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
