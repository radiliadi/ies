<?php

/**
 * This is the model class for table "tbl_medical_claim".
 *
 * The followings are the available columns in table 'tbl_medical_claim':
 * @property integer $mc_id
 * @property integer $mc_id_pd
 * @property integer $mc_bpjs_kete
 * @property integer $mc_bpjs_kese
 * @property integer $mc_bpjs_pensi
 * @property integer $mc_status
 */
class MedicalClaim extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_medical_claim';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mc_id_pd', 'required'),
			array('mc_bpjs_kete, mc_bpjs_kese, mc_bpjs_pensi, mc_tunj_kese', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('mc_id, mc_id_pd, mc_bpjs_kete, mc_bpjs_kese, mc_bpjs_pensi, mc_tunj_kese, mc_tunj_current, mc_tunj_used, mc_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mc_id' => 'Mc',
			'mc_id_pd' => 'NIP',
			'mc_bpjs_kete' => 'BPJS Ketenagakerjaan',
			'mc_bpjs_kese' => 'BPJS Kesehatan',
			'mc_bpjs_pensi' => 'BPJS Pensiun',
			'mc_tunj_kese' => 'Tunj. Kesehatan',
			'mc_tunj_current' => 'Saldo',
			'mc_tunj_used' => 'Used',
			'mc_status' => 'Mc Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('mc_id',$this->mc_id);
		$criteria->compare('mc_id_pd',$this->mc_id_pd,true);
		$criteria->compare('mc_bpjs_kete',$this->mc_bpjs_kete);
		$criteria->compare('mc_bpjs_kese',$this->mc_bpjs_kese);
		$criteria->compare('mc_bpjs_pensi',$this->mc_bpjs_pensi);
		$criteria->compare('mc_tunj_kese',$this->mc_tunj_kese);
		$criteria->compare('mc_tunj_current',$this->mc_tunj_current);
		$criteria->compare('mc_tunj_used',$this->mc_tunj_used);
		$criteria->compare('mc_status',$this->mc_status);
		$criteria->addCondition("mc_status = 1");

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MedicalClaim the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
