<?php

/**
 * This is the model class for table "agama".
 *
 * The followings are the available columns in table 'agama':
 * @property integer $id_agama
 * @property string $nm_agama
 */
class Agama extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agama';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('agm_desc', 'required'),
			// array('agm_id', 'numerical', 'integerOnly'=>true),
			array('agm_desc', 'length', 'max'=>25),
			array('agm_datetime_insert, agm_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('agm_desc, agm_desc', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'agm_id' => 'Id',
			'agm_desc' => 'Agama',
		);
	}

	public function beforeSave()
    {
    	$id_pd=Yii::app()->session->get('username');
        if($this->isNewRecord || $this->agm_datetime_insert==null){
            $this->agm_datetime_insert=new CDbExpression('NOW()');
            $this->agm_insert_by = $id_pd;
        }else{
            $this->agm_datetime_update = new CDbExpression('NOW()');
            $this->agm_update_by = $id_pd;
        }
        return parent::beforeSave();
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('agm_id',$this->agm_id);
		$criteria->compare('agm_desc',$this->agm_desc,true);
		$criteria->compare('agm_datetime_insert',$this->agm_datetime_insert,true);
		$criteria->compare('agm_datetime_update',$this->agm_datetime_update,true);
		$criteria->compare('agm_datetime_delete',$this->agm_datetime_delete,true);
		$criteria->compare('agm_insert_by',$this->agm_insert_by,true);
		$criteria->compare('agm_update_by',$this->agm_update_by,true);
		$criteria->compare('agm_delete_by',$this->agm_delete_by,true);
		$criteria->compare('agm_status',$this->agm_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Agama the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
