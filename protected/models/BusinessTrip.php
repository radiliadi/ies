<?php

/**
 * This is the model class for table "tbl_business_trip".
 *
 * The followings are the available columns in table 'tbl_business_trip':
 * @property integer $bt_id
 * @property integer $bt_id_pd
 * @property integer $bt_trip
 * @property integer $bt_tripp
 * @property integer $bt_status
 */
class BusinessTrip extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_business_trip';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bt_id_pd', 'required'),
			array('bt_trip, bt_tripp, bt_status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('bt_id, bt_id_pd, bt_trip, bt_tripp, bt_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bt_id' => 'Bt',
			'bt_id_pd' => 'NIP',
			'bt_trip' => 'Trip #1',
			'bt_tripp' => 'Trip #2',
			'bt_status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('bt_id',$this->bt_id);
		$criteria->compare('bt_id_pd',$this->bt_id_pd);
		$criteria->compare('bt_trip',$this->bt_trip);
		$criteria->compare('bt_tripp',$this->bt_tripp);
		$criteria->compare('bt_status',$this->bt_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BusinessTrip the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
