<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $username
 * @property string $password
 * @property string $level
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	 //digunakan untuk memproses data setelah di validasi
	protected function afterValidate() {
	     parent::afterValidate();
	            
	     //melakukan enkripsi pada passwod yang di input
	     $this->password = $this->encrypt($this->password);
	}
        
	//membuat sebuah fungsi untuk mengenkripsi data
	public function encrypt($value){
	     return md5($value);
	}

	 
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, level, user_status', 'required'),
			array('username, password, level', 'length', 'max'=>25),
			array('user_datetime_insert, user_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			// array('username','unique','message'=>'[{attribute} : {value}] Data telah tersedia!'),
			// array('user_foto', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>false,'on'=>array('create')),
			array('user_foto', 'file', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,png,bmp','maxSize'=>22*480*480,'tooLarge'=>'Ukuran foto terlalu besar', 'wrongType'=>'Jenis file hanya JPG, JPEG, BMP atau PNG', 'except'=>'insert'),
			array('username, password, level, user_login, user_first_login, user_last_login, user_datetime_insert, user_datetime_update, user_datetime_delete, user_insert_by, user_update_by, user_delete_by, user_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'usrst'=>array(UserStatus::HAS_MANY, 'usrst_id', 'user_status','through'=>'usrst'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'username' => 'Username',
			'password' => 'Password',
			'level' => 'Level',
			'user_login' => '',
			'user_first_login' => 'First Login',
			'user_last_login' => 'Last Login',
			'user_datetime_insert' => 'Insert',
			'user_datetime_update' => 'Update',
			'user_datetime_delete' => 'Delete',
			'user_insert_by' => 'Insert By',
			'user_update_by' => 'Update By',
			'user_delete_by' => 'Delete By',
			'user_status' => 'Status',
			'user_foto' => '',
		);
	}

	public function beforeSave()
    {
    	$id_pd=Yii::app()->session->get('username');
        if($this->isNewRecord || $this->user_datetime_insert==null){
            $this->user_datetime_insert=new CDbExpression('NOW()');
            $this->user_insert_by = $id_pd;
        }else{
            $this->user_datetime_update = new CDbExpression('NOW()');
            $this->user_update_by = $id_pd;
        }
        return parent::beforeSave();
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchinactive()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		
		$criteria=new CDbCriteria;

		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('level',$this->level,true);
		$criteria->compare('user_login',$this->user_login,true);
		$criteria->compare('user_first_login',$this->user_first_login,true);
		$criteria->compare('user_last_login',$this->user_last_login,true);
		$criteria->compare('user_datetime_insert',$this->user_datetime_insert,true);
		$criteria->compare('user_datetime_update',$this->user_datetime_update,true);
		$criteria->compare('user_datetime_delete',$this->user_datetime_delete,true);
		$criteria->compare('user_insert_by',$this->user_insert_by,true);
		$criteria->compare('user_update_by',$this->user_update_by,true);
		$criteria->compare('user_delete_by',$this->user_delete_by,true);
		$criteria->compare('user_status',$this->user_status,true);
		$criteria->addCondition("user_status = 0");
		$criteria->order='user_id DESC';
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				)
		));
	}

	public function searchactive()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('level',$this->level,true);
		$criteria->compare('user_login',$this->user_login,true);
		$criteria->compare('user_first_login',$this->user_first_login,true);
		$criteria->compare('user_last_login',$this->user_last_login,true);
		$criteria->compare('user_datetime_insert',$this->user_datetime_insert,true);
		$criteria->compare('user_datetime_update',$this->user_datetime_update,true);
		$criteria->compare('user_datetime_delete',$this->user_datetime_delete,true);
		$criteria->compare('user_insert_by',$this->user_insert_by,true);
		$criteria->compare('user_update_by',$this->user_update_by,true);
		$criteria->compare('user_delete_by',$this->user_delete_by,true);
		$criteria->compare('user_status',$this->user_status,true);
		$criteria->addCondition("user_status = 1");
		$criteria->order='user_id DESC';
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				)
		));
	}

	public function searchdeleted()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('level',$this->level,true);
		$criteria->compare('user_login',$this->user_login,true);
		$criteria->compare('user_first_login',$this->user_first_login,true);
		$criteria->compare('user_last_login',$this->user_last_login,true);
		$criteria->compare('user_datetime_insert',$this->user_datetime_insert,true);
		$criteria->compare('user_datetime_update',$this->user_datetime_update,true);
		$criteria->compare('user_datetime_delete',$this->user_datetime_delete,true);
		$criteria->compare('user_insert_by',$this->user_insert_by,true);
		$criteria->compare('user_update_by',$this->user_update_by,true);
		$criteria->compare('user_delete_by',$this->user_delete_by,true);
		$criteria->compare('user_status',$this->user_status,true);
		$criteria->addCondition("user_status = 2");
		$criteria->order='user_id DESC';
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				)
		));
	}

	public static function listUser() {
		$list = CHtml::listData(self::model()->findAll(array('order' => 'level ASC')), 'level', 'level');
		return $list;
	}

	public static function listUserstat() {
		$list = CHtml::listData(UserStatus::model()->findAll(), 'usrst_nm', 'usrst_nm');
		return $list;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
