<?php

/**
 * This is the model class for table "tbl_chart_of_account".
 *
 * The followings are the available columns in table 'tbl_chart_of_account':
 * @property integer $coa_id
 * @property integer $coa_coar_id
 * @property string $coa_desc
 * @property string $coa_datetime
 * @property string $coa_datetime_insert
 * @property string $coa_datetime_update
 * @property integer $coa_status
 */
class ChartOfAccount extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_chart_of_account';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('coa_id, coa_coar_id, coa_coat_id, coa_desc', 'required'),
			array('coa_id, coa_coar_id, coa_status, coa_code', 'numerical', 'integerOnly'=>true),
			array('coa_datetime_insert, coa_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('coa_id, coa_coar_id, coa_desc, coa_datetime_insert, coa_datetime_update, coa_datetime_delete, coa_insert_by, coa_update_by, coa_delete_by, coa_code, coa_status, coa_coat_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'getType' => array(self::BELONGS_TO, 'ChartOfAccountType', 'coa_coat_id'), //nama model dan nama field di model tsb
			'getRule' => array(self::BELONGS_TO, 'ChartOfAccountRule', 'coa_coar_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'coa_id' => 'Coa ID',
			'coa_coar_id' => 'Rule',
			'coa_coat_id' => 'Type',
			'coa_desc' => 'Description',
			'coa_datetime_insert' => 'Insert',
			'coa_datetime_update' => 'Update',
			'coa_datetime_delete' => 'Delete',
			'coa_insert_by' => 'Insert By',
			'coa_update_by' => 'Update By',
			'coa_delete_by' => 'Delete By',
			'coa_code' => 'Code',
			'coa_status' => 'Status',
		);
	}

	public function beforeSave()
    {
    	$id_pd=Yii::app()->session->get('username');
        if($this->isNewRecord || $this->coa_datetime_insert==null){
            $this->coa_datetime_insert=new CDbExpression('NOW()');
            $this->coa_insert_by = $id_pd;
        }else{
            $this->coa_datetime_update = new CDbExpression('NOW()');
            $this->coa_update_by = $id_pd;
        }
        return parent::beforeSave();
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('coa_id',$this->coa_id);
		$criteria->compare('coa_coar_id',$this->coa_coar_id);
		$criteria->compare('coa_coat_id',$this->coa_coat_id);
		$criteria->compare('coa_desc',$this->coa_desc,true);
		$criteria->compare('coa_datetime_insert',$this->coa_datetime_insert,true);
		$criteria->compare('coa_datetime_update',$this->coa_datetime_update,true);
		$criteria->compare('coa_datetime_delete',$this->coa_datetime_delete,true);
		$criteria->compare('coa_insert_by',$this->coa_insert_by,true);
		$criteria->compare('coa_update_by',$this->coa_update_by,true);
		$criteria->compare('coa_delete_by',$this->coa_delete_by,true);
		$criteria->compare('coa_code',$this->coa_code);
		$criteria->compare('coa_status',$this->coa_status);
		$criteria->with=array('getType');
		$criteria->with=array('getRule');
		// $criteria->addCondition("coa_status = 1");
		$criteria->order='coa_id ASC';
		// $criteria->order='coa_datetime_insert DESC';
		// $criteria->order='coa_status ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	public static function listType() {
		$list = CHtml::listData(ChartOfAccountType::model()->findAll(array('order' => 'coat_id ASC','condition' => 'coat_status < 100')), 'coat_id', 'coat_desc');
		return $list;
	}

	public static function listRule() {
		$list = CHtml::listData(ChartOfAccountRule::model()->findAll(array('order' => 'coar_id ASC','condition' => 'coar_status = 1')), 'coar_id', 'coar_desc');
		return $list;
	}

	public static function listCoa() {
		$list = CHtml::listData(self::model()->findAll(array('order' => 'coa_id ASC','condition' => 'coa_status = 1')), 'coa_id', 'coa_id');
		return $list;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChartOfAccount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
