<?php

/**
 * This is the model class for table "ptkp".
 *
 * The followings are the available columns in table 'ptkp':
 * @property integer $id_ptkp
 * @property integer $wp_pribadi
 * @property integer $wp_menikah
 * @property integer $wp_tanggungan
 * @property integer $persen_pph
 * @property integer $persen_jabatan
 */
class Ptkp extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ptkp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('wp_pribadi, wp_menikah, wp_tanggungan, ptkp_ket', 'required'),
			array('wp_pribadi, wp_menikah, wp_tanggungan', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_ptkp, wp_pribadi, wp_menikah, wp_tanggungan, ptkp_ket, ptkp_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ptkp' => 'Id Ptkp',
			'wp_pribadi' => 'Wp Pribadi',
			'wp_menikah' => 'Wp Menikah',
			'wp_tanggungan' => 'Wp Tanggungan',
			'ptkp_ket' => 'Keterangan',
			'ptkp_status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('id_ptkp',$this->id_ptkp);
		$criteria->compare('wp_pribadi',$this->wp_pribadi);
		$criteria->compare('wp_menikah',$this->wp_menikah);
		$criteria->compare('wp_tanggungan',$this->wp_tanggungan);
		$criteria->compare('ptkp_ket',$this->ptkp_ket);
		$criteria->compare('ptkp_status',$this->ptkp_status);
		$criteria->addCondition("ptkp_status = 1");

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ptkp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	// public function getItemtotal()
 //    {
 //        return $this->hasOne(ProductItem::className(), ['prt_prd_id' => 'prd_id'])->count();
 //    }
}
