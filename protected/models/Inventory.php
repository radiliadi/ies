<?php

/**
 * This is the model class for table "tbl_inventory".
 *
 * The followings are the available columns in table 'tbl_inventory':
 * @property integer $inv_id
 * @property string $inv_date
 * @property string $inv_desc
 * @property string $inv_datetime_insert
 * @property string $inv_datetime_update
 * @property string $inv_datetime_delete
 * @property string $inv_insert_by
 * @property string $inv_update_by
 * @property string $inv_delete_by
 * @property integer $inv_status
 */
class Inventory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_inventory';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('inv_status', 'numerical', 'integerOnly'=>true),
			array('inv_insert_by, inv_update_by, inv_delete_by', 'length', 'max'=>255),
			array('inv_date, inv_desc, inv_datetime_insert, inv_datetime_update, inv_datetime_delete', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('inv_id, inv_date, inv_desc, inv_datetime_checked, inv_datetime_insert, inv_datetime_update, inv_datetime_delete, inv_checked_by, inv_insert_by, inv_update_by, inv_delete_by, inv_is_checked, inv_is_locker, inv_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'inv_id' => 'Inventory ID',
			'inv_date' => 'Date',
			'inv_desc' => 'Desc',
			'inv_datetime_checked' => 'Checked',
			'inv_datetime_insert' => 'Insert',
			'inv_datetime_update' => 'Update',
			'inv_datetime_delete' => 'Delete',
			'inv_checked_by' => 'Checked By',
			'inv_insert_by' => 'Insert By',
			'inv_update_by' => 'Update By',
			'inv_delete_by' => 'Inv Delete By',
			'inv_is_checked' => 'Is Checked',
			'inv_is_locker' => 'Is Locked',
			'inv_status' => 'Status',
		);
	}

	public function beforeSave()
    {
    	$id_pd=Yii::app()->session->get('username');
        if($this->isNewRecord || $this->inv_datetime_insert==null){
            $this->inv_datetime_insert=new CDbExpression('NOW()');
            $this->inv_insert_by = $id_pd;
        }else{
            $this->inv_datetime_update = new CDbExpression('NOW()');
            $this->inv_update_by = $id_pd;
        }
        return parent::beforeSave();
    }

    public static function getCount(){
		$count = Inventory::model()->findAll(array('select'=>'inv_id', 'condition'=>'inv_status=:inv_status', 'params'=>array(':inv_status'=>1)));
		return count($count);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('inv_id',$this->inv_id);
		$criteria->compare('inv_date',$this->inv_date,true);
		$criteria->compare('inv_desc',$this->inv_desc,true);
		$criteria->compare('inv_datetime_insert',$this->inv_datetime_insert,true);
		$criteria->compare('inv_datetime_update',$this->inv_datetime_update,true);
		$criteria->compare('inv_datetime_delete',$this->inv_datetime_delete,true);
		$criteria->compare('inv_insert_by',$this->inv_insert_by,true);
		$criteria->compare('inv_update_by',$this->inv_update_by,true);
		$criteria->compare('inv_delete_by',$this->inv_delete_by,true);
		$criteria->compare('inv_status',$this->inv_status);
		$criteria->order='inv_datetime_insert DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Inventory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
