<?php

/**
 * This is the model class for table "negara".
 *
 * The followings are the available columns in table 'negara':
 * @property string $id_negara
 * @property string $nm_negara
 * @property string $a_ln
 */
class Negara extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'negara';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_negara, nm_negara, a_ln', 'required'),
			array('id_negara', 'length', 'max'=>2),
			array('nm_negara', 'length', 'max'=>45),
			array('a_ln', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_negara, nm_negara, a_ln', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_negara' => 'ID Negara',
			'nm_negara' => 'Nama Negara',
			'a_ln' => 'Apakah luar negeri?',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('id_negara',$this->id_negara,true);
		$criteria->compare('nm_negara',$this->nm_negara,true);
		$criteria->compare('a_ln',$this->a_ln,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Negara the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
