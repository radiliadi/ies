<?php

/**
 * This is the model class for table "pengumuman".
 *
 * The followings are the available columns in table 'pengumuman':
 * @property integer $id_pesan
 * @property string $id_sms
 * @property string $pengirim
 * @property string $penerima
 * @property string $judul
 * @property string $isi
 * @property string $file
 * @property string $datetime
 * @property string $sudahbaca
 */
class Pengumuman extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pengumuman';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public $years;
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pengirim, penerima, judul, isi, datetime, sudahbaca', 'required'),
			array('id_sms', 'length', 'max'=>25),
			array('pengirim, penerima', 'length', 'max'=>30),
			array('judul, file', 'length', 'max'=>100),
			array('sudahbaca', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pesan, id_sms, pengirim, penerima, judul, isi, file, datetime, sudahbaca', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pesan' => 'Id Pesan',
			'id_sms' => 'Id Sms',
			'pengirim' => 'Pengirim',
			'penerima' => 'Penerima',
			'judul' => 'Judul',
			'isi' => 'Isi',
			'file' => 'File',
			'datetime' => 'Datetime',
			'sudahbaca' => 'Sudahbaca',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('id_pesan',$this->id_pesan);
		$criteria->compare('id_sms',$this->id_sms,true);
		$criteria->compare('pengirim',$this->pengirim,true);
		$criteria->compare('penerima',$this->penerima,true);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('isi',$this->isi,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('sudahbaca',$this->sudahbaca,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	public function mhs()
	{
		$criteria=new CDbCriteria;
		$sms=Yii::app()->session->get('sms');
		
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('isi',$this->isi);
		$criteria->compare('datetime',$this->datetime);
		
		$criteria->order = "datetime DESC";
		$criteria->condition = "penerima='mahasiswa'";
		// $criteria->params = array (	
		// ':id_sms' => $sms,
		// );
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function dosen()
	{
		$criteria=new CDbCriteria;
		$sms=Yii::app()->session->get('sms');
		
		
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('isi',$this->isi);
		$criteria->compare('datetime',$this->datetime);
		
		$criteria->order = "datetime DESC";
		$criteria->condition = "penerima='dosen'";
		// $criteria->params = array (	
		
		// ':penerima' => $hari,
		// );
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function admin()
	{
		$criteria=new CDbCriteria;
		$sms=Yii::app()->session->get('sms');
		
		
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('isi',$this->isi);
		$criteria->compare('datetime',$this->datetime);
		
		$criteria->order = "datetime DESC";
		$criteria->condition = "penerima='admin'";
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pengumuman the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
