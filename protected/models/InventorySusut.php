<?php

/**
 * This is the model class for table "tbl_inventory_susut".
 *
 * The followings are the available columns in table 'tbl_inventory_susut':
 * @property integer $invs_id
 * @property integer $invs_invd_id
 * @property string $invs_date
 * @property string $invs_penyusutan
 * @property string $invs_nilai_buku
 * @property string $invs_datetime_insert
 * @property string $invs_datetime_update
 * @property string $invs_datetime_delete
 * @property string $invs_insert_by
 * @property string $invs_update_by
 * @property string $invs_delete_by
 * @property integer $invs_status
 */
class InventorySusut extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_inventory_susut';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('invs_inv_id, invs_invd_id, invs_status', 'numerical', 'integerOnly'=>true),
			array('invs_penyusutan, invs_nilai_buku', 'length', 'max'=>20),
			array('invs_insert_by, invs_update_by, invs_delete_by', 'length', 'max'=>255),
			array('invs_date, invs_datetime_insert, invs_datetime_update, invs_datetime_delete', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('invs_id, invs_inv_id, invs_invd_id, invs_date, invs_penyusutan, invs_nilai_buku, invs_datetime_insert, invs_datetime_update, invs_datetime_delete, invs_insert_by, invs_update_by, invs_delete_by, invs_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'invs_id' => 'Invs',
			'invs_inv_id' => 'Invs Invd',
			'invs_invd_id' => 'Invs Invd',
			'invs_date' => 'Invs Date',
			'invs_penyusutan' => 'Invs Penyusutan',
			'invs_nilai_buku' => 'Invs Nilai Buku',
			'invs_datetime_insert' => 'Invs Datetime Insert',
			'invs_datetime_update' => 'Invs Datetime Update',
			'invs_datetime_delete' => 'Invs Datetime Delete',
			'invs_insert_by' => 'Invs Insert By',
			'invs_update_by' => 'Invs Update By',
			'invs_delete_by' => 'Invs Delete By',
			'invs_status' => 'Invs Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('invs_id',$this->invs_id);
		$criteria->compare('invs_inv_id',$this->invs_inv_id);
		$criteria->compare('invs_invd_id',$this->invs_invd_id);
		$criteria->compare('invs_date',$this->invs_date,true);
		$criteria->compare('invs_penyusutan',$this->invs_penyusutan,true);
		$criteria->compare('invs_nilai_buku',$this->invs_nilai_buku,true);
		$criteria->compare('invs_datetime_insert',$this->invs_datetime_insert,true);
		$criteria->compare('invs_datetime_update',$this->invs_datetime_update,true);
		$criteria->compare('invs_datetime_delete',$this->invs_datetime_delete,true);
		$criteria->compare('invs_insert_by',$this->invs_insert_by,true);
		$criteria->compare('invs_update_by',$this->invs_update_by,true);
		$criteria->compare('invs_delete_by',$this->invs_delete_by,true);
		$criteria->compare('invs_status',$this->invs_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InventorySusut the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
