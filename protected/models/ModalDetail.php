<?php

/**
 * This is the model class for table "tbl_modal_detail".
 *
 * The followings are the available columns in table 'tbl_modal_detail':
 * @property integer $mdld_id
 * @property integer $mdld_mdl_id
 * @property string $mdld_saham
 * @property string $mdld_rugi
 * @property string $mdld_ekuitas
 * @property integer $mdld_is_generate
 * @property string $mdld_date
 * @property string $mdld_desc
 * @property string $mdld_modal
 * @property string $mdld_generate_by
 * @property string $mdld_datetime_insert
 * @property string $mdld_datetime_update
 * @property integer $mdld_status
 */
class ModalDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_modal_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mdld_mdl_id', 'required'),
			array('mdld_mdl_id, mdld_is_generate, mdld_status', 'numerical', 'integerOnly'=>true),
			array('mdld_saham, mdld_rugi, mdld_ekuitas, mdld_modal', 'length', 'max'=>20),
			array('mdld_generate_by', 'length', 'max'=>255),
			array('mdld_date, mdld_desc, mdld_datetime_insert, mdld_datetime_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('mdld_id, mdld_mdl_id, mdld_saham, mdld_rugi, mdld_ekuitas, mdld_is_generate, mdld_date, mdld_desc, mdld_modal, mdld_generate_by, mdld_datetime_insert, mdld_datetime_update, mdld_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mdld_id' => 'Mdld',
			'mdld_mdl_id' => 'Mdld Mdl',
			'mdld_saham' => 'Mdld Saham',
			'mdld_rugi' => 'Mdld Rugi',
			'mdld_ekuitas' => 'Mdld Ekuitas',
			'mdld_is_generate' => 'Mdld Is Generate',
			'mdld_date' => 'Mdld Date',
			'mdld_desc' => 'Mdld Desc',
			'mdld_modal' => 'Mdld Modal',
			'mdld_generate_by' => 'Mdld Generate By',
			'mdld_datetime_insert' => 'Mdld Datetime Insert',
			'mdld_datetime_update' => 'Mdld Datetime Update',
			'mdld_status' => 'Mdld Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('mdld_id',$this->mdld_id);
		$criteria->compare('mdld_mdl_id',$this->mdld_mdl_id);
		$criteria->compare('mdld_saham',$this->mdld_saham,true);
		$criteria->compare('mdld_rugi',$this->mdld_rugi,true);
		$criteria->compare('mdld_ekuitas',$this->mdld_ekuitas,true);
		$criteria->compare('mdld_is_generate',$this->mdld_is_generate);
		$criteria->compare('mdld_date',$this->mdld_date,true);
		$criteria->compare('mdld_desc',$this->mdld_desc,true);
		$criteria->compare('mdld_modal',$this->mdld_modal,true);
		$criteria->compare('mdld_generate_by',$this->mdld_generate_by,true);
		$criteria->compare('mdld_datetime_insert',$this->mdld_datetime_insert,true);
		$criteria->compare('mdld_datetime_update',$this->mdld_datetime_update,true);
		$criteria->compare('mdld_status',$this->mdld_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ModalDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
