<?php

/**
 * This is the model class for table "tender".
 *
 * The followings are the available columns in table 'tender':
 * @property integer $id_tender
 * @property string $info_source
 * @property string $client_code
 * @property string $client_name
 * @property string $tender_no
 * @property string $tender_status
 * @property string $tender_type
 * @property string $detail_task
 * @property string $ref_address
 * @property string $deadline
 * @property string $bid_bond
 */
class Tender extends CActiveRecord
{
	public $from_date;
	public $to_date;
	public $karyawanku;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tender';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('info_source, client_code, client_name, tender_no, tender_status, tender_type, detail_task, deadline, ref_address, bid_bond', 'required'),
			array('info_source, client_code, client_name, tender_no, tender_status, tender_type, detail_task, ref_address, bid_bond', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tender, from_date, to_date, info_source, client_code, client_name, tender_no, tender_status, tender_type, detail_task, ref_address, deadline, bid_bond, tndr_status, karyawanku', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tenderMember'	=>	array(self::BELONGS_TO, 'karyawan', 'id_tender'),
			// 'tenderMember'	=>	array(self::HAS_MANY, 'karyawan', 'id_tender'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tender' => 'Id Tender',
			'info_source' => 'Info Source',
			'client_code' => 'Code',
			'client_name' => 'Client',
			'tender_no' => 'No',
			'tender_status' => 'Status',
			'tender_type' => 'Type',
			'detail_task' => 'Detail Task',
			'ref_address' => 'Ref Address',
			'deadline' => '',
			'bid_bond' => 'Bid Bond',
			'tndr_status' => 'Status',
			'karyawanku' => 'Bid karyawanku',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));
		$criteria=new CDbCriteria;

		$criteria->compare('id_tender',$this->id_tender);
		$criteria->compare('info_source',$this->info_source,true);
		$criteria->compare('client_code',$this->client_code,true);
		$criteria->compare('client_name',$this->client_name,true);
		$criteria->compare('tender_no',$this->tender_no,true);
		$criteria->compare('tender_status',$this->tender_status,true);
		$criteria->compare('tender_type',$this->tender_type);
		$criteria->compare('detail_task',$this->detail_task,true);
		$criteria->compare('ref_address',$this->ref_address,true);
		$criteria->compare('deadline',$this->deadline,true);
		$criteria->compare('bid_bond',$this->bid_bond,true);
		$criteria->compare('tndr_status',$this->tndr_status,true);
		$criteria->addCondition("tndr_status = 1");
		if(!empty($this->from_date) && empty($this->to_date))
        {
            $criteria->condition = "deadline >= '$this->from_date'";  // date is database date column field
        }elseif(!empty($this->to_date) && empty($this->from_date))
        {
            $criteria->condition = "deadline <= '$this->to_date'";
        }elseif(!empty($this->to_date) && !empty($this->from_date))
        {
            $criteria->condition = "deadline  >= '$this->from_date' and deadline <= '$this->to_date'";
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	public function searchmember()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$baris=Pagesize::model()->find('id_ps=:id_ps', array('id_ps'=>1));

		$criteria=new CDbCriteria;
		
		$criteria->compare('id_tender',$this->id_tender);
		$criteria->compare('info_source',$this->info_source,true);
		$criteria->compare('client_code',$this->client_code,true);
		$criteria->compare('client_name',$this->client_name,true);
		$criteria->compare('tender_no',$this->tender_no,true);
		$criteria->compare('tender_status',$this->tender_status,true);
		$criteria->compare('tender_type',$this->tender_type,true);
		$criteria->compare('detail_task',$this->detail_task,true);
		$criteria->compare('ref_address',$this->ref_address,true);
		$criteria->compare('deadline',$this->deadline,true);
		$criteria->compare('bid_bond',$this->bid_bond,true);
		$criteria->addCondition("id_tender",$this->id_tender,'=');
		// $criteria->join='JOIN karyawan as kywn ON kywn.tender_id=t.id_tender';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$baris->jml_ps,
				),
		));
	}

	public static function listInfoSource() {
		$list = CHtml::listData(self::model()->findAll(array('order' => 'id_tender DESC')), 'info_source', 'info_source');
		return $list;
	}

	public static function listClientCode() {
		$list = CHtml::listData(self::model()->findAll(array('order' => 'id_tender DESC')), 'client_code', 'client_code');
		return $list;
	}

	public static function listClientName() {
		$list = CHtml::listData(self::model()->findAll(array('order' => 'id_tender DESC')), 'client_code', 'client_code');
		return $list;
	}

	public static function listTenderNo() {
		$list = CHtml::listData(self::model()->findAll(array('order' => 'id_tender DESC')), 'tender_no', 'tender_no');
		return $list;
	}

	public static function listDetailTask() {
		$list = CHtml::listData(self::model()->findAll(array('order' => 'id_tender DESC')), 'detail_task', 'detail_task');
		return $list;
	}

	public static function listRefAddress() {
		$list = CHtml::listData(self::model()->findAll(array('order' => 'id_tender DESC')), 'ref_address', 'ref_address');
		return $list;
	}

	public static function listTenderType() {
		$list = CHtml::listData(TendType::model()->findAll(array('order' => 'id_tend_type ASC')), 'nm_tend_type', 'nm_tend_type');
		return $list;
	}

	public static function listTenderStatus() {
		$list = CHtml::listData(TendStat::model()->findAll(array('order' => 'id_tend_stat ASC')), 'nm_tend_stat', 'nm_tend_stat');
		return $list;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tender the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
