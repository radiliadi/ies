<?php

class ProfilController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public $layout='//layouts/column2';
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}


	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */

	
	public function actionUpdate($id)
	{
		$msg = 'Invalid parameter';
		$model=$this->loadModel($id);
		$prevImage=$model->user_foto;
		$id_pd=Yii::app()->session->get('username');
		// $model->password;
		// echo '<pre>';
		// print_r($model->password);
		// echo '</pre>';
		// $model->unsetAttributes();
		
 
        if(isset($_POST['User']))
        {
            $rnd = rand(0,9999);
            $model->attributes=$_POST['User'];
            if(CUploadedFile::getInstance($model,'user_foto'))
			{ 
				$uploadedFile = CUploadedFile::getInstance($model,'user_foto');
				$fileName="{$rnd}-{$id_pd}-{$uploadedFile->name}";
				// $model->user_foto=$id_pd.'-'.$_POST['User']['user_foto'];
			}else{
			// $this->redirect(array('abc'));
				$model->user_foto=$prevImage;
			}

			// echo '<pre>';
			// print_r($prevImage);
			// echo '</pre>';
			// die;

			if(!empty($uploadedFile))  // check if uploaded file is set or not
            {
                $model->user_foto=$fileName;
                $model->save();
            	$uploadedFile->saveAs(Yii::app()->basePath.'/../images/'.$fileName);
            	$msg = 'sucees';
            }else{
            	$model->user_foto=$prevImage;
            	$model->save();
            	$msg = 'sucees';
            }

            // $model->save();
            // $uploadedFile->saveAs(Yii::app()->basePath.'/../images/'.$fileName);

            return $this->redirect(Yii::app()->request->urlReferrer);
 
        }
 
        $this->render('update',array(
            'model'=>$model,
		));

		$return     = ['msg'=>$msg];
        return json_encode($return);
	}
	
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}