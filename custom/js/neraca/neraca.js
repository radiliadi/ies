$(document).ready(function() {	

	$('.checkSingleCoa').change(function(){
		var _this  = $(this);
		var url    = _this.data('url');
		var id     = _this.data('id');;
		var fields = $('select,input,checkbox,textarea');
		
		$.ajax({
			type: 'POST',
			url: url,
			data: {id:id},
			success: function(json){
				console.log(json);
				fields.removeAttr('disabled');	
				
			}
		});	
		
	});

});